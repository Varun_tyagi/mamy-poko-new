//
//  NotificationViewController.swift
//  Content
//
//  Created by Hitesh Dhawan on 31/12/18.
//  Copyright © 2018 Varun Tyagi. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI

class NotificationViewController: UIViewController, UNNotificationContentExtension {

    @IBOutlet weak var imgView: UIImageView!
   // @IBOutlet var label: UILabel?
    
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any required interface initialization here.
       
    }
    
    func didReceive(_ notification: UNNotification) {
       
     //   self.label?.text = "" //notification.request.content.body
        
        if let attachment = notification.request.content.attachments.first {
            if attachment.url.startAccessingSecurityScopedResource() {
                guard  let image   = UIImage(contentsOfFile: attachment.url.path) else{
                   self.imageHeight.constant=1
                    return
                }
                let resizedImage = resizeImage(image, newWidth: self.view.frame.size.width)
                self.imgView.image = resizedImage
                self.imageHeight.constant = resizedImage.size.height
                self.view.layoutIfNeeded()
            }
        }
   
    }
     func resizeImage(_ image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.height
        let newHeight = image.size.width * scale
        UIGraphicsBeginImageContext(CGSize(width: newHeight,height: newWidth))
        image.draw(in: CGRect(x: 0, y: 0, width: newHeight, height: newWidth))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}
