//
//  Analytics.swift
//  MamyPoko
//
//  Created by Varun Tyagi on 11/06/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAnalytics
import AppsFlyerLib
import Mixpanel

class AnalyticsManager {

    class func LogFireBaseAnalytics( analyticsType:String , itemId:String,itemName:String,ContentType:String){

        Analytics.logEvent(analyticsType, parameters: [
            AnalyticsParameterItemID: itemId,
            AnalyticsParameterItemName: itemName,
            AnalyticsParameterContentType: ContentType
            ])
       
    }
    class func LogApsFlyerEvent(eventType:String, contentId:String){
        AppsFlyerTracker.shared().trackEvent(eventType,
                                             withValues: [
                                                AFEventParamContent: eventType,
                                                AFEventParamContentId: contentId
            ])
    }
    
    class func logMixPannel(_ event:String){
        Mixpanel.initialize(token: "bbabd4dc11ec9c7fefd7569a73409213")
        Mixpanel.mainInstance().track(event: event)
    }
    
    
}
