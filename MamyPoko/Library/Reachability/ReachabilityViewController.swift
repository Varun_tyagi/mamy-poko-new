//
//  ReachabilityViewController.swift
//  Smart-E
//
//  Created by Hitesh Dhawan on 15/01/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit

class ReachabilityViewController: UIViewController {
    
    let reachability = Reachability();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged , object: reachability)
        do{
            try reachability?.startNotifier()
        }catch{
//            print("Could not start notifier:\(error)")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.reachabilityChanged, object: nil)
    }
    
    
    @objc func internetChanged(note:Notification)  {
        let reachability  = note.object as! Reachability
        
        if reachability.connection != .none {
            
            if reachability.connection == .wifi{
                
            }else if reachability.connection == .cellular{
               
            }
        }else{
            DispatchQueue.main.async {
                PKSAlertController.alertForNetwok()
            }
        }
    }
    
    
}
