//
//  ExtensionAndSubClasses.swift
//  Smart-E
//
//  Created by Hitesh Dhawan on 09/01/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import Foundation
import UIKit


class BorderButton: UIButton {
    
//    @IBInspectable var cornerRadius: CGFloat = 0 {
//        didSet {
//            layer.cornerRadius = cornerRadius
//            layer.masksToBounds = cornerRadius > 0
//        }
//    }
//    @IBInspectable var borderWidth: CGFloat = 0 {
//        didSet {
//            layer.borderWidth = borderWidth
//        }
//    }
//    @IBInspectable var borderColor: UIColor? = UIColor.clear {
//        didSet {
//            layer.borderColor = borderColor?.cgColor
//        }
    
        
   // }
    
    
}

class RoundView: UIView {
    
//    @IBInspectable var cornerRadius: CGFloat = 0 {
//        didSet {
//            layer.cornerRadius = cornerRadius
//            layer.masksToBounds = cornerRadius > 0
//        }
//    }
//    @IBInspectable var borderWidth: CGFloat = 0 {
//        didSet {
//            layer.borderWidth = borderWidth
//        }
//    }
//    @IBInspectable var borderColor: UIColor? = UIColor.clear {
//        didSet {
//            layer.borderColor = borderColor?.cgColor
//        }
//
//
//    }
    
    
}

class RoundTextView: UITextView {
    
//    @IBInspectable var cornerRadius: CGFloat = 0 {
//        didSet {
//            layer.cornerRadius = cornerRadius
//            layer.masksToBounds = cornerRadius > 0
//        }
//    }
//    @IBInspectable var borderWidth: CGFloat = 0 {
//        didSet {
//            layer.borderWidth = borderWidth
//        }
//    }
//    @IBInspectable var borderColor: UIColor? = UIColor.clear {
//        didSet {
//            layer.borderColor = borderColor?.cgColor
//        }
//
//
//    }
	
	
	
}
extension UITextField{
	func setEyeButton() {
		let eyeView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40))
		let eyebtn = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40))
		eyebtn.setImage(#imageLiteral(resourceName: "eyeClose"), for: .normal)
		eyebtn.setImage(#imageLiteral(resourceName: "eyeOpen"), for: .selected)
		eyebtn.accessibilityElements = [self]
		eyebtn.addTarget(self, action: #selector(toggleSecureField(_:)), for: .touchUpInside)
		eyeView.addSubview(eyebtn)
		
		self.rightView = eyeView
		self.isSecureTextEntry=true
		self.rightViewMode = .always
		
	}
	
	@objc func toggleSecureField( _ sender: UIButton){
		guard   let textField = sender.accessibilityElements?.first as? UITextField else{
			return
		}
		if sender.isSelected {
			sender.isSelected = false
			textField.isSecureTextEntry = true
		}else{
			sender.isSelected = true
			textField.isSecureTextEntry = false
		}
	}
	func addBottomBorder(){
		let bottomLine = CALayer()
		bottomLine.frame = CGRect.init(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1)
		bottomLine.backgroundColor = UIColor.lightGray.cgColor
		self.borderStyle = UITextBorderStyle.none
		self.layer.addSublayer(bottomLine)
		
	}
	
}

class RoundTextField: UITextField {
	
	func setEyeButton(_ textField: UITextField) {
		let eyeView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40))
		let eyebtn = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40))
		eyebtn.setImage(#imageLiteral(resourceName: "eyeClose"), for: .normal)
		eyebtn.setImage(#imageLiteral(resourceName: "eyeOpen"), for: .selected)
		eyebtn.accessibilityElements = [textField]
		eyebtn.addTarget(self, action: #selector(toggleSecureField(_:)), for: .touchUpInside)
		eyeView.addSubview(eyebtn)
		
		textField.rightView = eyeView
		textField.isSecureTextEntry=true
		textField.rightViewMode = .always
		
	}
	
	@objc override func toggleSecureField( _ sender: UIButton){
		guard   let textField = sender.accessibilityElements?.first as? UITextField else{
			return
		}
		if sender.isSelected {
			sender.isSelected = false
			textField.isSecureTextEntry = true
		}else{
			sender.isSelected = true
			textField.isSecureTextEntry = false
		}
		
		func addBottomBorder(){
			let bottomLine = CALayer()
			bottomLine.frame = CGRect.init(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1)
			bottomLine.backgroundColor = UIColor.white.cgColor
			self.borderStyle = UITextBorderStyle.none
			self.layer.addSublayer(bottomLine)
			
		}
	}
//    @IBInspectable var cornerRadius: CGFloat = 0 {
//        didSet {
//            layer.cornerRadius = cornerRadius
//            layer.masksToBounds = cornerRadius > 0
//        }
//    }
//    @IBInspectable var borderWidth: CGFloat = 0 {
//        didSet {
//            layer.borderWidth = borderWidth
//        }
//    }
//    @IBInspectable var borderColor: UIColor? = UIColor.clear {
//        didSet {
//            layer.borderColor = borderColor?.cgColor
//        }
//
//
//    }
	
}

class RoundImageView: UIImageView {
    
//    @IBInspectable var cornerRadius: CGFloat = 0 {
//        didSet {
//            layer.cornerRadius = cornerRadius
//            layer.masksToBounds = cornerRadius > 0
//        }
//    }
//    @IBInspectable var borderWidth: CGFloat = 0 {
//        didSet {
//            layer.borderWidth = borderWidth
//        }
//    }
//    @IBInspectable var borderColor: UIColor? = UIColor.clear {
//        didSet {
//            layer.borderColor = borderColor?.cgColor
//        }
//
//
//    }
    
    
}

extension UIView {
    
    func dropShadow(scale: Bool = true) {
        
        // set the corner radius
        layer.cornerRadius = 6.0
        layer.masksToBounds = false
        // set the shadow properties
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        layer.shadowOpacity = 0.2
        layer.shadowRadius = 4.0
    }
    func dropShadow( cornerRadius: CGFloat) {
        
        // set the corner radius
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = false
        // set the shadow properties
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        layer.shadowOpacity = 0.2
        layer.shadowRadius = 4.0
    }
    enum ViewSide {
        case Left, Right, Top, Bottom
    }
    
    func addBorder(toSide side: ViewSide, withColor color: CGColor, andThickness thickness: CGFloat) {
        
        let border = CALayer()
        border.backgroundColor = color
        
        switch side {
        case .Left: border.frame = CGRect(x: frame.minX, y: frame.minY, width: thickness, height: frame.height); break
        case .Right: border.frame = CGRect(x: frame.maxX, y: frame.minY, width: thickness, height: frame.height); break
        case .Top: border.frame = CGRect(x: frame.minX, y: frame.minY, width: frame.width, height: thickness); break
        case .Bottom: border.frame = CGRect(x: frame.minX, y: frame.maxY, width: frame.width, height: thickness); break
        }
        layer.addSublayer(border)
    }
}


extension UIScrollView {
    
    func scrollToBottom() {
        let bottomOffset = CGPoint(x: 0, y: contentSize.height - bounds.size.height + contentInset.bottom)
        setContentOffset(bottomOffset, animated: true)
    }
}


extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat = 10){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

class RoundButton: UIButton {

//    @IBInspectable var cornerRadius: CGFloat = 0 {
//        didSet {
//            layer.cornerRadius = cornerRadius
//            layer.masksToBounds = cornerRadius > 0
//        }
//    }
//    @IBInspectable var borderWidth: CGFloat = 0 {
//        didSet {
//            layer.borderWidth = borderWidth
//        }
//    }
//    @IBInspectable var borderColor: UIColor? = UIColor.clear {
//        didSet {
//            layer.borderColor = borderColor?.cgColor
//        }
//
//
//    }

}
public enum ImageFormat {
    case png
    case jpeg(CGFloat)
}

extension UIImage {

    public func base64(format: ImageFormat) -> String? {
        var imageData: Data?
        switch format {
        case .png: imageData = UIImagePNGRepresentation(self)
        case .jpeg(let compression): imageData = UIImageJPEGRepresentation(self, compression)
        }
        return imageData?.base64EncodedString()
    }
}
/// Extend UITextView and implemented UITextViewDelegate to listen for changes
extension UITextView: UITextViewDelegate {

    /// Resize the placeholder when the UITextView bounds change
    override open var bounds: CGRect {
        didSet {
            self.resizePlaceholder()
        }
    }

    /// The UITextView placeholder text
    public var placeholder: String? {
        get {
            var placeholderText: String?

            if let placeholderLabel = self.viewWithTag(100) as? UILabel {
                placeholderText = placeholderLabel.text
            }

            return placeholderText
        }
        set {
            if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
                placeholderLabel.text = newValue
                placeholderLabel.sizeToFit()
            } else {
                self.addPlaceholder(newValue!)
            }
        }
    }

    /// When the UITextView did change, show or hide the label based on if the UITextView is empty or not
    ///
    /// - Parameter textView: The UITextView that got updated
    public func textViewDidChange(_ textView: UITextView) {
        if let placeholderLabel = self.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = self.text.count > 0
        }
    }

    /// Resize the placeholder UILabel to make sure it's in the same position as the UITextView text
    private func resizePlaceholder() {
        if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
            let labelX = self.textContainer.lineFragmentPadding
            let labelY = self.textContainerInset.top - 2
            let labelWidth = self.frame.width - (labelX * 2)
            let labelHeight = placeholderLabel.frame.height

            placeholderLabel.frame = CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
        }
    }

    /// Adds a placeholder UILabel to this UITextView
    private func addPlaceholder(_ placeholderText: String) {
        let placeholderLabel = UILabel()

        placeholderLabel.text = placeholderText
        placeholderLabel.sizeToFit()

        placeholderLabel.font = self.font
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.tag = 100

        placeholderLabel.isHidden = self.text.count > 0

        self.addSubview(placeholderLabel)
        self.resizePlaceholder()
        self.delegate = self
    }

}
extension UITextView{

    @IBInspectable var doneAccessory: Bool{
        get{
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone{
                addDoneButtonOnKeyboard()
            }
        }
    }

    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default

        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))

        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()

        self.inputAccessoryView = doneToolbar
    }

    @objc func doneButtonAction()
    {
        self.resignFirstResponder()
    }
}
extension UITextField {
    func setCalanderImageOntextField(){
        let eyeView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 35, height: 40))
        let imageV = UIImageView(frame:CGRect.init(x: 0, y: 12.5, width: 15, height: 15))
        imageV.image = #imageLiteral(resourceName: "calendarRegister")
        eyeView.addSubview(imageV)
        self.rightViewMode = .always
        self.rightView = eyeView
        
    }
}
import UIKit

@IBDesignable class PlaceholderUITextView: UIView, UITextViewDelegate {
    
    var view: UIView?
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var labelPlaceholder: UILabel!
    
    @IBInspectable var placeholderTexttv: String = "Enter data here ..." {
        didSet {
            labelPlaceholder.text = placeholderTexttv
        }
    }
    
    func commonXibSetup() {
        guard let view = loadViewFromNib() else
        {
            return
        }
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        textView.delegate = self
    }
    
    func loadViewFromNib() -> UIView? {
        
        let bundle =    Bundle(for: type(of: self))
        let nib = UINib(nibName: "PlaceholderUITextView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        return view
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonXibSetup()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        commonXibSetup()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if !textView.hasText {
            labelPlaceholder?.isHidden = false
        }
        else {
            labelPlaceholder?.isHidden = true
        }
    }
}
extension UIViewController {
    func addChildVC(_ child: UIViewController) {
        addChildViewController(child)
        view.addSubview(child.view)
        child.didMove(toParentViewController: self)
    }
    func removeChildVC() {
        guard parent != nil else {
            return
        }
        willMove(toParentViewController: nil)
        removeFromParentViewController()
        view.removeFromSuperview()
    }
}

extension String {
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        
        return String(data: data, encoding: .utf8)
    }
    
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
   
    func readBabyGrowthJson( _ value : String) -> [[Double]] {
            var desiredPlotArray:[[Double]] = []
            if let path = Bundle.main.path(forResource: self, ofType: "json") {
                do {
                    let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                    let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                    if let jsonResult = jsonResult as? [String:Any], let PlotValue = jsonResult[value] as? [[String:Any]] {
                        for (_,plotArr) in PlotValue.enumerated(){
                            if let doubleArr = plotArr.values.first as? String{
                                desiredPlotArray.append(doubleArr.components(separatedBy: ",").map{ NSString(string: $0).doubleValue })
                            }
                        }
                        return desiredPlotArray
                    }
                } catch {
                    return desiredPlotArray
                }
            }
            return desiredPlotArray
        }
}

extension UIScrollView {
    func updateContentView() {
        contentSize.height = subviews.sorted(by: { $0.frame.maxY < $1.frame.maxY }).last?.frame.maxY ?? contentSize.height
    }
}

extension String
{
    func toDateTime( dateFormate:String) -> Date
    {
        //Create Date Formatter
        let dateFormatter = DateFormatter()
        //Specify Format of String to Parse
        dateFormatter.dateFormat = "yyyy-MM-dd"
        //Parse into NSDate
        let dateFromString = dateFormatter.date(from: self) ?? Date()
        //Return Parsed Date
        return dateFromString
    }
    
}
extension Date {
    func toDateString( dateFormate:String) -> String{
        //Create Date Formatter
        let dateFormatter = DateFormatter()
        //Specify Format of String to Parse
        dateFormatter.dateFormat = "dd-MM-YYYY"
        //Parse into NSDate
        let dateFromString = dateFormatter.string(from: self)
        //Return Parsed Date
        return dateFromString
    }
}
extension UIView {
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.y")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = 0.4
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
	
}

extension String {
    
    func saveImageToDisk(fileIdentifier: String, data: UIImage, options: [NSObject : AnyObject]?) -> UNNotificationAttachment? {
        
        do {
            
            let attachment = try UNNotificationAttachment(identifier: fileIdentifier, url: self.saveImageToDocumentDirectory(data)!, options: options)
            return attachment
        } catch let error {
//            print("error \(error)")
        }
        
        return nil
    }
    func saveImageToDocumentDirectory(_ chosenImage: UIImage) -> URL? {
        
        let fileManager = FileManager.default
        do {
            let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
            let fileURL = documentDirectory.appendingPathComponent("CustomSamplePush.jpg")
            let image = chosenImage
            if let imageData = UIImageJPEGRepresentation(image, 0.5) {
                try imageData.write(to: fileURL)
                
                return fileURL
            }
        } catch {
//            print(error)
        }
        return nil
    }
}
import UserNotifications
import UserNotificationsUI
extension UNNotificationAttachment {
    
    static func create(identifier: String, image: UIImage, options: [NSObject : AnyObject]?) -> UNNotificationAttachment? {
        let fileManager = FileManager.default
        let tmpSubFolderName = ProcessInfo.processInfo.globallyUniqueString
        let tmpSubFolderURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(tmpSubFolderName, isDirectory: true)
        do {
            try fileManager.createDirectory(at: tmpSubFolderURL, withIntermediateDirectories: true, attributes: nil)
            let imageFileIdentifier = identifier+".png"
            let fileURL = tmpSubFolderURL.appendingPathComponent(imageFileIdentifier)
            guard let imageData = UIImagePNGRepresentation(image) else {
                return nil
            }
            try imageData.write(to: fileURL)
            let imageAttachment = try UNNotificationAttachment.init(identifier: imageFileIdentifier, url: fileURL, options: options)
            return imageAttachment
        } catch {
//            print("error " + error.localizedDescription)
        }
        return nil
    }
}
extension UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            let color = UIColor(cgColor: layer.borderColor!)
            return color
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowOffset = CGSize(width: 0, height: 2)
            layer.shadowOpacity = 0.4
            layer.shadowRadius = shadowRadius
        }
    }
    
}

