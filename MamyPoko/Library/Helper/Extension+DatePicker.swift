//
//  Extension+DatePicker.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 03/08/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import Foundation
import UIKit

extension UIDatePicker {
    
    func setBirthdayBashBabyDobValidation() {
        let currentDate: Date = Date()
        var calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        calendar.timeZone = TimeZone.current
        var components: DateComponents = DateComponents()
        components.calendar = calendar
        components.year = -1
        let maxDate: Date = calendar.date(byAdding: components, to: currentDate)!
        components.year = -6
        let minDate: Date = calendar.date(byAdding: components, to: currentDate)!
        self.minimumDate = minDate
        self.maximumDate = maxDate
    }
    func setBabyDobValidation() {
        let currentDate: Date = Date()
        var calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        calendar.timeZone = TimeZone.current
        var components: DateComponents = DateComponents()
        components.calendar = calendar
        components.day = -1
        let maxDate: Date = calendar.date(byAdding: components, to: currentDate)!
        components.month = -9
        let minDate: Date = calendar.date(byAdding: components, to: currentDate)!
        self.minimumDate = minDate
        self.maximumDate = maxDate
    }
    func setEighteenYearDobValidation() {
        let currentDate: Date = Date()
        var calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        calendar.timeZone = TimeZone.current
        var components = DateComponents()
        components.calendar = calendar
        components.year = -18
        let minDate: Date = calendar.date(byAdding: components, to: currentDate)!

        components.year = 0
        components.day = 0
        let maxDate: Date = calendar.date(byAdding: components, to: currentDate)!

        self.minimumDate = minDate
        self.maximumDate = maxDate
    }
    
    func setLastPeriodValidation() {
        let currentDate: Date = Date()
        var calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        calendar.timeZone = TimeZone.current
        var components: DateComponents = DateComponents()
        components.calendar = calendar
        components.day = -15
        let maxDate: Date = calendar.date(byAdding: components, to: currentDate)!
        components.month = -9
        let minDate: Date = calendar.date(byAdding: components, to: currentDate)!
        self.minimumDate = minDate
        self.maximumDate = maxDate
    }
    
    func setExpectedDateValidation() {
        let currentDate: Date = Date()
        var calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        calendar.timeZone = TimeZone.current
        var components: DateComponents = DateComponents()
        components.calendar = calendar
        components.day = +1
        let minDate: Date = calendar.date(byAdding: components, to: currentDate)!
        self.minimumDate = minDate
        
        components.month = +9
        let maxDate: Date = calendar.date(byAdding: components, to: currentDate)!
        
        self.maximumDate = maxDate
    }
    
}
