//
//  Extension+NavigationColor.swift
//  MamyPoko
//
//  Created by Arnab Maity on 03/10/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import Foundation
import Foundation

extension UIApplication {

    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }

}
