//
//  Extension+Spinner.swift
//  MamyPoko
//
//  Created by Arnab Maity on 29/07/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//
//import UIKit
//extension UIAlertController{
//	func addSpinner() {
//		let activity: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
//		view.addSubview(activity)
//		
//		activity.translatesAutoresizingMaskIntoConstraints = false
//		activity.addConstraint(NSLayoutConstraint(item: activity, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: activity.bounds.size.width))
//		activity.addConstraint(NSLayoutConstraint(item: activity, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: activity.bounds.size.height))
//		view.addConstraint(NSLayoutConstraint(item: activity, attribute: .centerXWithinMargins, relatedBy: .equal, toItem: view, attribute: .centerXWithinMargins, multiplier: 1.0, constant: 0.0))
//		view.addConstraint(NSLayoutConstraint(item: activity, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottomMargin, multiplier: 1.0, constant: -20.0))
//		
//		activity.startAnimating()
//	}
//}

