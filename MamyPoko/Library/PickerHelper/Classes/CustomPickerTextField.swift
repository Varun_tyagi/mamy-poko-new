

open class CustomPickerTextField: UITextField {

    public var doneHandler: CustomPicker.DoneHandler = { _ in }
    public var cancelHandler: CustomPicker.CancelHandler?
    public var selectionChangedHandler: CustomPicker.SelectionChangedHandler?
    public var textFieldWillBeginEditingHandler: ((_ selections: [Int:String]) -> Void)?

    public var inputViewMcPicker: CustomPicker? {
        didSet {
            self.delegate = inputViewMcPicker
        }
    }
}
