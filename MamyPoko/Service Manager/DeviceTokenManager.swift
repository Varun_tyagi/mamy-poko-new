//
//  DeviceTokenManager.swift
//  MamyPoko
//
//  Created by Surbhi Varma on 04/06/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import Foundation


class DeviceTokenManager{
	
	class func submitDeviceToken(_ activity:String){
		if Helper.isConnectedToNetwork(){
		// device_type, device_id, device_token, user_id, activity
				let urlstr = Constants.BASEURL + MethodName.registerDeviceToken

				let paramStr = "device_type=ios&device_id=\(AppDelegate.getUniqueDeviceIdentifierAsString())&device_token=\(AppDelegate.getDeviceToken())&user_id=\(UserDefaults.standard.value(forKey: Constants.USERID) ?? "0")&activity=\(activity)"
				
				Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
					

				}
			
		}
		else{
			PKSAlertController.alertForNetwok()
		}
	}
	
}
