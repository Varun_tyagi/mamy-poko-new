//
//  Server.swift
//  TestSwiftApp
//
//  Created by Surbhi on 21/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit
//import AFNetworking
import Alamofire


class Server: NSObject {
    
    static func getRequestWithURL(urlString: String, completionHandler:@escaping (_ response: Dictionary <String, AnyObject>?) -> Void) {
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        let urlRequest = NSMutableURLRequest.init(url: URL.init(string: urlString)!)
        urlRequest.httpMethod = "GET"
        
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
		
//        let authString = "Neuron:Neuro@4321"
//        let authData = authString.data(using: String.Encoding.utf8)
//        let authValue = "Basic \(authData!.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters))"
        let authValue = "Basic bWFteXBva29wYW50c0BnbWFpbC5jb206TmV1cm9AMTIz"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        
        let task = defaultSession.dataTask(with: urlRequest as URLRequest) { ( data, response, error) -> Void in
            
            if error != nil {
//                print("Error occurred: "+(error?.localizedDescription)!)
                completionHandler(nil)
                return;
            }
            do {
                let responseObjc = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String: AnyObject]
                completionHandler(responseObjc)
            }
            catch {
//                print("Error occurred parsing data: \(error)")
                completionHandler(nil)
            }
        }
        task.resume()
    }
	
		static func getRequestWithURLNew(urlString: String, completionHandler:@escaping (_ response: Data?) -> Void) {
			
			let defaultConfigObject = URLSessionConfiguration.default
			let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
			let urlRequest = NSMutableURLRequest.init(url: URL.init(string: urlString)!)
			urlRequest.httpMethod = "GET"
			
			urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
			urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
			
	//        let authString = "Neuron:Neuro@4321"
	//        let authData = authString.data(using: String.Encoding.utf8)
	//        let authValue = "Basic \(authData!.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters))"
			let authValue = "Basic bWFueXBva29AZ21haWwuY29tOk5ldXJvQDEyMw=="
			urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
			
			let task = defaultSession.dataTask(with: urlRequest as URLRequest) { ( data, response, error) -> Void in
				
				if error != nil {
	//                print("Error occurred: "+(error?.localizedDescription)!)
					completionHandler(nil)
					return;
				}
				do {
					let responseObjc = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String: AnyObject]
					completionHandler(data)
				}
				catch {
	//                print("Error occurred parsing data: \(error)")
					completionHandler(nil)
				}
			}
			task.resume()
		}

  
    static  func PostDataInDictionary(_ urlString: String, dict_data: Dictionary <String, Any>, completionHandler:@escaping (_ response: Dictionary <String, Any>?) -> Void) {
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        
        let urlRequest = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
        urlRequest.httpMethod = "POST"
        
        urlRequest.timeoutInterval = 90.0
        let authValue = "Basic bWFteXBva29wYW50c0BnbWFpbC5jb206TmV1cm9AMTIz"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict_data, options: .prettyPrinted)
            //
            //all fine with jsonData here
            urlRequest.httpBody = jsonData
             print(jsonData)

        } catch {
            //handle error
//            print(error)
        }
        
        let task = defaultSession.dataTask(with: urlRequest as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                completionHandler(nil)
                return
            }
            
            guard let data = data else {
                completionHandler(nil)
                return
            }
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? Dictionary<String, Any> {
                     print(json)
                  completionHandler(json )
                    // handle json...
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
                completionHandler(nil)
            }
        })
        
        task.resume()
        
    }
    
    static func postRequestWithURL(_ urlString: String, paramString: String, completionHandler:@escaping (_ response: Dictionary <String, AnyObject>) -> Void) {
        
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        
        let urlRequest = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = paramString.data(using: String.Encoding.utf8)
        
        // Handling Basic HTTPS Authorization
        let authValue = "Basic bWFteXBva29wYW50c0BnbWFpbC5jb206TmV1cm9AMTIz"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        
        let task = defaultSession.dataTask(with: urlRequest as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    completionHandler(json as Dictionary<String, AnyObject>)
                    // handle json...
                    
                }
                
            } catch let error {
                Constants.appDel.stopLoader()
                PKSAlertController.alert("Error", message: error.localizedDescription)
               // print(error.localizedDescription)
            }
        })
        
//        let task = defaultSession.dataTask(with: urlRequest as URLRequest) { ( data, response, error) -> Void in
//            
//            if error != nil {
//                print("Error occurred: "+(error?.localizedDescription)!)
//                //                Constants.appDelegate.stopIndicator()
//                return;
//            }
//            do {
//                let responseObjc = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String: AnyObject]
//                completionHandler(responseObjc)
//            }
//            catch {
//                print("Error occurred parsing data: \(error)")
//                completionHandler([:])
//            }
//        }
        task.resume()
    }
	
	static func postRequestWithEndcodedURL(_ urlString: String, postData: NSMutableData, completionHandler:@escaping (_ response: Dictionary <String, AnyObject>) -> Void) {

		let defaultConfigObject = URLSessionConfiguration.default
		let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
		
		let urlRequest = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
		urlRequest.httpMethod = "POST"
		urlRequest.httpBody = postData as Data
		//paramString.data(using: String.Encoding.utf8)
		
		// Handling Basic HTTPS Authorization
		let authValue = "Basic bWFteXBva29wYW50c0BnbWFpbC5jb206TmV1cm9AMTIz"
		urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
		
		let task = defaultSession.dataTask(with: urlRequest as URLRequest, completionHandler: { data, response, error in
			
			guard error == nil else {
				return
			}
			
			guard let data = data else {
				return
			}
			
			do {
				
				//create json object from data
				if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
					//print(json)
					completionHandler(json as Dictionary<String, AnyObject>)
					// handle json...
					
				}
				
			} catch let error {
				Constants.appDel.stopLoader()
				PKSAlertController.alert("Error", message: error.localizedDescription)
				//print(error.localizedDescription)
			}
		})
		
		task.resume()
	}
    
    
    static func toGetImageFromURL(url:String,completionHandler:@escaping (_ response: UIImage?) -> Void){
        let session = URLSession(configuration: .default)
        let imageURL = session.dataTask(with: URL.init(string: url)!) { (data, response, error) in
            if let e = error {
               // print("Error Occurred: \(e)")
                completionHandler(nil)
            } else {
                if (response as? HTTPURLResponse) != nil {
                    if let imageData = data {
                        let image = UIImage(data: imageData)
                        DispatchQueue.main.async {
                            completionHandler(image)
                        }
                    } else {
                        completionHandler(nil)
                        //print("Image file is currupted")
                    }
                } else {
                    completionHandler(nil)
                   // print("No response from server")
                }
            }
        }
        imageURL.resume()
        
    }

    
    static func UploadPictureRequestWithURL(_ urlString: String, paramString: String, completionHandler:@escaping (_ response: Dictionary <String, AnyObject>) -> Void) {
        
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        
        let urlRequest = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
        urlRequest.httpMethod = "POST"
        let boundary = "Boundary-\(NSUUID().uuidString)"

        let postData = paramString.data(using: String.Encoding.utf8)!
//        postData.append(ImageString.data(using: String.Encoding.utf8)!)
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        urlRequest.httpBody = postData//paramString.data(using: String.Encoding.utf8)
        
        // Handling Basic HTTPS Authorization
        let authValue = "Basic bWFteXBva29wYW50c0BnbWFpbC5jb206TmV1cm9AMTIz"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        
        let task = defaultSession.dataTask(with: urlRequest as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                   // print(json)
                    completionHandler(json as Dictionary<String, AnyObject>)
                    // handle json...

                }
                //create json object from data
//                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [Any] {
//                    print(json)
//                    //                    completionHandler(json as Dictionary<String, AnyObject>)
//                    // handle json...
//                    completionHandler(["svcyu":"wbqw"] as Dictionary<String, AnyObject>)
//                }
            } catch let error {
                Constants.appDel.stopLoader()
                PKSAlertController.alert("Error", message: error.localizedDescription)
               // print(error.localizedDescription)
            }
        })
        task.resume()
    }
    
    func generateBoundaryString() -> String
    {
        return "Boundary-\(NSUUID().uuidString)"
    }
  
    
    static func RegisterToApp(fname: String, lname : String,pass : String, channel: String, email : String, mobile: String, refferal : String , completionHandler:@escaping (_ response: Dictionary <String, AnyObject>) -> Void)
    {
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
      //  var paramString = String()
        let paramString1 = "firstName=\(fname)&lastName=\(lname)&password=\(pass)&channel=\(channel)&emailAddress=\(email)&mobilePrimary=\(mobile)&referalCode=\(refferal)"
      //  print(paramString1)
        let loginMethod = "Account/Register"
        let urlString = Constants.BASEURL+loginMethod
        
        
        let urlRequest = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
        // Handling Basic HTTPS Authorization
       /* / let authString = "Neuron:Neuro@4321"
        
       // let authData = authString.data(using: String.Encoding.utf8)
        
        //let authData = authString.data(using: .utf8)
        //let authValue = "Basic \(String(describing: authData?.base64EncodedData(options: [])))"
        
      //  print("auth value is",authValue)
        //let authValue = "Basic \(authData!.base64EncodedStringWithOptions(NSData.Base64EncodingOptions.Encoding64CharacterLineLength))"
        //urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        */
      //
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = paramString1.data(using: String.Encoding.utf8)
        
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        urlRequest.setValue( "application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
      //  print(urlRequest)
        
        
        let task = defaultSession.dataTask(with: urlRequest as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    //print(json)
                    completionHandler(json as Dictionary<String, AnyObject>)
                    // handle json...
                    
                }
                
            } catch let error {
              //  print(error.localizedDescription)
            }
        })
     
        task.resume()
    }
    
    class func postMultiPartdataQuiz(param: [String:Any], urlStr: String , completion: @escaping (_ response: Dictionary <String, Any>) -> Void ) {
        
        
        Alamofire.upload(multipartFormData:{ multipartFormData in
            for (key, value) in param {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
//            if let data = UIImagePNGRepresentation(image){
//                multipartFormData.append(data, withName: "image", fileName: "\(imageName).png", mimeType: "image/png")
//            }
            
        }, usingThreshold:UInt64.init(),  to:urlStr,  method:.post,
           headers:["Authorization": "Basic bWFueXBva29AZ21haWwuY29tOk5ldXJvQDEyMw=="],
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    do {
                        if let json = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as? [String: Any] {
                            // json.printJson()
                            completion(json as Dictionary<String, Any>)
                        }
                    }
                    catch _ {
                        //print(error.localizedDescription)
                        completion([:])
                    }
                    
                }
            case .failure( _):
                //print(encodingError)
                completion([:])
            }
        })
        
        
    }
    class func postMultiPartdata(param: [String:Any], urlStr: String , image:UIImage , imageName: String, completion: @escaping (_ response: Dictionary <String, Any>) -> Void ) {
        
        
        Alamofire.upload(multipartFormData:{ multipartFormData in
            for (key, value) in param {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = UIImagePNGRepresentation(image){
                multipartFormData.append(data, withName: "image", fileName: "\(imageName).png", mimeType: "image/png")
            }
            
        }, usingThreshold:UInt64.init(),  to:urlStr,  method:.post,
           headers:["Authorization": Constants.AuthorisationStr],
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    do {
                        if let json = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as? [String: Any] {
                            // json.printJson()
                            completion(json as Dictionary<String, Any>)
                        }
                    }
                    catch let error {
                        //print(error.localizedDescription)
                        completion([:])
                    }
                    
                }
            case .failure(let encodingError):
                //print(encodingError)
                completion([:])
            }
        })
        
        
    }
    
    static func postImageUpload(_ url: String ,_ image : UIImage , _ param : [String:Any] , completionHandler:@escaping (_ response : [String:Any] ) -> Void){


        let headers = [
            "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
            "accept": "application/x-www-form-urlencoded",
            "authorization": "Basic bWFteXBva29wYW50c0BnbWFpbC5jb206TmV1cm9AMTIz",
            ]

        let parameters = param

        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding(), headers: headers).responseJSON(completionHandler: { (response) in
           // print(response.result.value ?? "")

            if !(response.error==nil) {
               // print(response.error ?? "error")
                completionHandler(["message":"Error Uploading image","status":"0"])

            } else {

                guard let data = response.data, response.error == nil else { return }

                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:Any]
                    completionHandler(json)

                } catch let error as NSError {
                   // print(error)
                    completionHandler(["message":"Error Uploading image","status":"0"])
                }

            }
        })
    }

  }



