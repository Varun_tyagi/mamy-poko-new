//
//  MyBabyDetailTableViewCell.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 02/08/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit



class MyBabyDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var headTxt: RoundTextField!
    
    @IBOutlet weak var saveBtn: BorderButton!
    @IBOutlet weak var headInBtn: UIButton!
    @IBOutlet weak var headCMBtn: UIButton!
    @IBOutlet weak var heightInBtn: UIButton!
    @IBOutlet weak var heightCMBtn: UIButton!
    @IBOutlet weak var heightTxt: RoundTextField!
    @IBOutlet weak var weightKgBtn: UIButton!
    @IBOutlet weak var weightLblBtn: UIButton!
    @IBOutlet weak var weightTxt: RoundTextField!
    @IBOutlet weak var dobTxt: RoundTextField!
    @IBOutlet weak var genderTxt: RoundTextField!
    @IBOutlet weak var babyNameTxt: RoundTextField!
    
    var datePicker : UIDatePicker!
    var toolBar : UIToolbar!
    
    override func awakeFromNib() {
        super.awakeFromNib()

		weightTxt.setLeftPaddingPoints()
		heightTxt.setLeftPaddingPoints()
		headTxt.setLeftPaddingPoints()
		dobTxt.setLeftPaddingPoints()
		genderTxt.setLeftPaddingPoints()
		babyNameTxt.setLeftPaddingPoints()
		
        dobTxt.setCalanderImageOntextField()
       
    }
    func bindData(_ userData:UserDataModel?)  {
         setupDatePicker()
        babyNameTxt.text = userData?.babyDetails?.babyName ?? ""
        
        if userData != nil  && userData?.babyDetails != nil {
        genderTxt.text =   (userData?.babyDetails?.babyGender?.lowercased().contains("f"))! ?  "Female" : "Male"
        }
        
        dobTxt.text = userData?.babyDetails?.babyDob ?? ""
        weightTxt.text = userData?.babyDetails?.babyWeight ?? ""
        heightTxt.text = userData?.babyDetails?.babyHeight ?? ""
        headTxt.text = userData?.babyDetails?.headCircumfrence ?? ""
        
    }
    func BindRegistgerData(_ userData:UserDataModel?, babyDetails: BabyDetails?, vc:RegisterViewControllerStep2){
        self.setupDatePickerWithEighteenYearValdiation()
        
       vc.babyGenderTxt =   self.genderTxt
        vc.babyGenderTxt.delegate=vc

        vc.babyNameTxt = self.babyNameTxt
        
        vc.babyDOBTxt = self.dobTxt
         self.dobTxt.delegate=vc
        
        vc.babyDOBTxt = self.dobTxt
        vc.babyDOBTxt.delegate=vc
        
        vc.weightTxt=self.weightTxt
        vc.kgButton=self.weightKgBtn
        vc.lbsButton=self.weightLblBtn
        
        vc.circumferenceTxt=self.headTxt
        vc.cm_cButton=self.headCMBtn
        vc.inch_cButton=self.headInBtn
        
        vc.heightTxt=self.heightTxt
        vc.cmButton=self.heightCMBtn
        vc.inchButton=self.heightInBtn
        
        vc.weightTxt.keyboardType = .decimalPad
        vc.heightTxt.keyboardType = .decimalPad
        vc.circumferenceTxt.keyboardType = .decimalPad
        
        vc.kgButton?.isSelected = (babyDetails?.babyWeightUnit?.lowercased().contains("kg"))! ? true : false
        vc.lbsButton?.isSelected = (babyDetails?.babyWeightUnit?.lowercased().contains("kg"))! ? false : true
        
        vc.cm_cButton?.isSelected = (babyDetails?.headCircumfrenceUnit?.lowercased().contains("cm"))! ? true : false
        vc.inch_cButton?.isSelected = (babyDetails?.headCircumfrenceUnit?.lowercased().contains("cm"))! ? false :  true
        
        vc.cmButton?.isSelected = (babyDetails?.babyHeightUnit?.lowercased().contains("cm"))! ? true : false
        vc.inchButton?.isSelected = (babyDetails?.babyHeightUnit?.lowercased().contains("cm"))! ? false : true
        
        self.saveBtn.addTarget(vc, action: #selector(vc.SaveBabyDetailsPressed(_:)), for: .touchUpInside)
        
        self.weightKgBtn.addTarget(vc, action: #selector(vc.KGButtonPressed(_:)), for: .touchUpInside)
        self.weightLblBtn.addTarget(vc, action: #selector(vc.LBSButtonPressed(_:)), for: .touchUpInside)
        
        self.headInBtn.addTarget(vc, action: #selector(vc.Inch_cButtonPressed(_:)), for: .touchUpInside)
        self.headCMBtn.addTarget(vc, action: #selector(vc.CM_cButtonPressed(_:)), for: .touchUpInside)
        
        self.heightInBtn.addTarget(vc, action: #selector(vc.InchButtonPressed(_:)), for: .touchUpInside)
        self.heightCMBtn.addTarget(vc, action: #selector(vc.CMButtonPressed(_:)), for: .touchUpInside)
    }
    
    // MARK: -   Bind I am Mother Controler Data
    func BindIAmMotherData(_ userData:UserDataModel?, babyDetails: BabyDetails?, vc:IAmMotherViewController){
        self.setupDatePickerWithEighteenYearValdiation()
        
        vc.babyGenderTxt =   self.genderTxt
        vc.babyGenderTxt.delegate=vc
        
        vc.babyNameTxt = self.babyNameTxt
        
        vc.babyDOBTxt = self.dobTxt
        self.dobTxt.delegate=vc
        
        vc.babyDOBTxt = self.dobTxt
        vc.babyDOBTxt.delegate=vc
        
        vc.weightTxt=self.weightTxt
        vc.kgButton=self.weightKgBtn
        vc.lbsButton=self.weightLblBtn
        
        vc.circumferenceTxt=self.headTxt
        vc.cm_cButton=self.headCMBtn
        vc.inch_cButton=self.headInBtn
        
        vc.heightTxt=self.heightTxt
        vc.cmButton=self.heightCMBtn
        vc.inchButton=self.heightInBtn
        
        vc.weightTxt.keyboardType = .decimalPad
        vc.heightTxt.keyboardType = .decimalPad
        vc.circumferenceTxt.keyboardType = .decimalPad
        
        vc.kgButton?.isSelected = (babyDetails?.babyWeightUnit?.lowercased().contains("kg"))! ? true : false
        vc.lbsButton?.isSelected = (babyDetails?.babyWeightUnit?.lowercased().contains("kg"))! ? false : true
        
        vc.cm_cButton?.isSelected = (babyDetails?.headCircumfrenceUnit?.lowercased().contains("cm"))! ? true : false
        vc.inch_cButton?.isSelected = (babyDetails?.headCircumfrenceUnit?.lowercased().contains("cm"))! ? false :  true
        
        vc.cmButton?.isSelected = (babyDetails?.babyHeightUnit?.lowercased().contains("cm"))! ? true : false
        vc.inchButton?.isSelected = (babyDetails?.babyHeightUnit?.lowercased().contains("cm"))! ? false : true
        
        self.saveBtn.addTarget(vc, action: #selector(vc.SaveBabyDetailsPressed(_:)), for: .touchUpInside)
        
        self.weightKgBtn.addTarget(vc, action: #selector(vc.KGButtonPressed(_:)), for: .touchUpInside)
        self.weightLblBtn.addTarget(vc, action: #selector(vc.LBSButtonPressed(_:)), for: .touchUpInside)
        
        self.headInBtn.addTarget(vc, action: #selector(vc.Inch_cButtonPressed(_:)), for: .touchUpInside)
        self.headCMBtn.addTarget(vc, action: #selector(vc.CM_cButtonPressed(_:)), for: .touchUpInside)
        
        self.heightInBtn.addTarget(vc, action: #selector(vc.InchButtonPressed(_:)), for: .touchUpInside)
        self.heightCMBtn.addTarget(vc, action: #selector(vc.CMButtonPressed(_:)), for: .touchUpInside)
    }
    
    //MARK:- Set Date Picker
    func setupDatePickerWithEighteenYearValdiation()  {
        // SET MiNIMUM & MAximum DAte
        
        datePicker = UIDatePicker()
        datePicker.tag = 100
        datePicker.datePickerMode = UIDatePickerMode.date
        datePicker.addTarget(self, action: #selector(self.setDateEighteenYear(_sender:)), for: .valueChanged)
        datePicker.timeZone = TimeZone.current
        toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(dismissPicker))
        
        datePicker.setEighteenYearDobValidation()
        
        self.dobTxt?.inputView = datePicker
        //  self.dobTxt?.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Set Date Picker
    func setupDatePicker()  {
        // SET MiNIMUM & MAximum DAte
      
        datePicker = UIDatePicker()
        datePicker.tag = 100
        datePicker.datePickerMode = UIDatePickerMode.date
        datePicker.addTarget(self, action: #selector(self.setDate(_sender:)), for: .valueChanged)
        datePicker.timeZone = TimeZone.current
        toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(dismissPicker))
        
        datePicker.setBabyDobValidation()

        self.dobTxt?.inputView = datePicker
      //  self.dobTxt?.inputAccessoryView = toolBar
      
    }
    //MARK:- Date Picker
    
    @objc func setDateEighteenYear(_sender : UIDatePicker){
        if _sender.tag == 100 {
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let selectedDate: String = dateFormatter.string(from: _sender.date)
            
            // SET MiNIMUM & MAximum DAte
            datePicker.setEighteenYearDobValidation()
            
            if _sender.date > datePicker.minimumDate! && _sender.date < datePicker.maximumDate! {
                self.dobTxt?.text = "\(selectedDate)"
            }
            else{
                self.dobTxt?.text = ""
                datePicker.date = datePicker.minimumDate!
            }
        }
    }
    @objc func setDate(_sender : UIDatePicker){
        if _sender.tag == 100 {
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let selectedDate: String = dateFormatter.string(from: _sender.date)
            
            // SET MiNIMUM & MAximum DAte
            let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
            let currentDate: NSDate = NSDate()
            let components1: NSDateComponents = NSDateComponents()
            let components2: NSDateComponents = NSDateComponents()
            
            components1.year = -2
            let minDate : Date = gregorian.date(byAdding: components1 as DateComponents, to: currentDate as Date, options: NSCalendar.Options.init(rawValue: 0))!
            datePicker.minimumDate = minDate
            
            components2.day = 0
            let maxDate : Date = gregorian.date(byAdding: components2 as DateComponents, to: currentDate as Date, options: NSCalendar.Options.init(rawValue: 0))!
            datePicker.maximumDate = maxDate
            
            if _sender.date > minDate && _sender.date < maxDate {
                self.dobTxt?.text = "\(selectedDate)"
            }
            else{
                self.dobTxt?.text = ""
                datePicker.date = minDate
            }
        }
    }
    
    @objc func dismissPicker() {
        self.contentView.endEditing(true)
    }
    

}

