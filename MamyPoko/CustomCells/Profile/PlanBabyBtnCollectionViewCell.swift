//
//  PlanBabyBtnCollectionViewCell.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 06/08/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit

class PlanBabyBtnCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var changePragStatusBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        changePragStatusBtn.layer.shadowColor = UIColor.gray.cgColor
        changePragStatusBtn.layer.shadowOpacity = 2.0
        changePragStatusBtn.layer.shadowOffset = CGSize.zero
        changePragStatusBtn.layer.shadowRadius = 1.0
        changePragStatusBtn.layer.cornerRadius = 5
        changePragStatusBtn.layer.masksToBounds = false
        
        
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOpacity = 2.0
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 1.0
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = false
    }
    
}
