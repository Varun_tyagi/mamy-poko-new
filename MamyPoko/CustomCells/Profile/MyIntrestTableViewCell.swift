//
//  MyIntrestTableViewCell.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 02/08/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit
import HTagView
class MyIntrestTableViewCell: UITableViewCell {

    @IBOutlet weak var saveIntrestBtn: BorderButton!
    @IBOutlet weak var intrestTagView: HTagView!
    override func awakeFromNib() {
        super.awakeFromNib()
       
       
        intrestTagView.marg = 10
        intrestTagView.btwTags = 10
        intrestTagView.btwLines = 10
        intrestTagView.tagFont = UIFont.init(name: FONTS.Quicksand_Regular, size: 15)!
        intrestTagView.tagMainBackColor = UIColor.white
        intrestTagView.tagMainTextColor = Colors.MamyDarkBlue
        
        intrestTagView.tagSecondBackColor = UIColor.white
        intrestTagView.tagSecondTextColor = UIColor.black
        
        intrestTagView.tagBorderColor = UIColor.lightGray.cgColor
        intrestTagView.tagBorderWidth = 1
        
        intrestTagView.tagMaximumWidth = .HTagAutoMaximumWidth
        intrestTagView.tagCornerRadiusToHeightRatio = 0.5
        intrestTagView.reloadData()
    }

    func bindData(_ intrestArray:IntrestModel)  {
        intrestTagView.reloadData()
        for (index, element) in intrestArray.enumerated() {
            if element.selected != nil && (element.selected?.lowercased().contains("true"))!{
                intrestTagView.selectTagAtIndex(index)
            }
        }
    }

}


class MyIntrestCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var saveIntrestBtn: BorderButton!
    @IBOutlet weak var intrestTagView: HTagView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
       
        intrestTagView.marg = 10
        intrestTagView.btwTags = 10
        intrestTagView.btwLines = 10
        intrestTagView.tagFont = UIFont.init(name: FONTS.Quicksand_Regular, size: 15) ?? UIFont.systemFont(ofSize: 15)
        intrestTagView.tagMainBackColor = UIColor.white
        intrestTagView.tagMainTextColor = Colors.MamyDarkBlue
        
        intrestTagView.tagSecondBackColor = UIColor.white
        intrestTagView.tagSecondTextColor = UIColor.black
        
        intrestTagView.tagBorderColor = UIColor.lightGray.cgColor
        intrestTagView.tagBorderWidth = 1
        
        intrestTagView.tagMaximumWidth = .HTagAutoMaximumWidth
        intrestTagView.tagCornerRadiusToHeightRatio = 0.5
        intrestTagView.reloadData()
    }
    
    func bindData(_ intrestArray:IntrestModel)  {
        
        self.contentView.dropShadow()

        //        self.layer.shadowColor = UIColor.gray.cgColor
//        self.layer.shadowOpacity = 2.0
//        self.layer.shadowOffset = CGSize.zero
//        self.layer.shadowRadius = 1.0
//        self.layer.cornerRadius = 5
//        self.layer.masksToBounds = false
//
//        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        
        intrestTagView.reloadData()
        for (index, element) in intrestArray.enumerated() {
            if  element.selected != nil &&  (element.selected?.lowercased().contains("true"))!{
                intrestTagView.selectTagAtIndex(index)
            }
        }
    }
    
    
}
