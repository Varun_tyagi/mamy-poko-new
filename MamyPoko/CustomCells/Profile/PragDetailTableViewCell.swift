//
//  PragDetailTableViewCell.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 02/08/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift


class PragDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var firstDeliveryBtn: UIButton!
    
    @IBOutlet weak var savePragDetailBtn: BorderButton!
    
    @IBOutlet weak  var expectedDeliveryDateTxt: RoundTextField!
	@IBOutlet weak  var  lastPeriodDateTxt: RoundTextField!
    
    var datePicker = UIDatePicker()
    var datePicker2 = UIDatePicker()
    var toolBar : UIToolbar!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        expectedDeliveryDateTxt.setLeftPaddingPoints()
        lastPeriodDateTxt.setLeftPaddingPoints()
        expectedDeliveryDateTxt.setCalanderImageOntextField()
      	lastPeriodDateTxt.setCalanderImageOntextField()
       
        setupDatePicker()
    }

    func bindData(_ userData: UserDataModel?)  {
        //firstDeliveryBtn.isSelected = userData?.pregnancy?.firstDelivery
        expectedDeliveryDateTxt.text = userData?.pregnancy?.deliveryDate ?? ""
        lastPeriodDateTxt.text = userData?.pregnancy?.lastDateOfPeriod ?? ""
        if (userData?.pregnancy?.firstDelivery?.lowercased().contains("yes"))!{
            firstDeliveryBtn.isSelected = true
        }
    }
    //MARK:- Set Date Picker
    func setupDatePicker()  {
        
        datePicker.datePickerMode = UIDatePickerMode.date
        datePicker.addTarget(self, action: #selector(self.setDate(_sender:)), for: .valueChanged)
        datePicker.timeZone = TimeZone.current
        toolBar = UIToolbar()
        toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(dismissPicker))
    
        
        datePicker2.datePickerMode = UIDatePickerMode.date
        datePicker2.addTarget(self, action: #selector(self.setDate(_sender:)), for: .valueChanged)
        datePicker2.timeZone = TimeZone.current
        
        datePicker.setLastPeriodValidation()
        datePicker2.setExpectedDateValidation()

        self.lastPeriodDateTxt?.inputView = datePicker
        self.expectedDeliveryDateTxt?.inputView = datePicker2
        
    }
    //MARK:- Date Picker
    @objc func setDate(_sender : UIDatePicker){
        
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let selectedDate: String = dateFormatter.string(from: _sender.date)
        
        if _sender == datePicker {
           
            datePicker.setLastPeriodValidation()

            if _sender.date > datePicker.minimumDate! && _sender.date < datePicker.maximumDate! {
                self.lastPeriodDateTxt?.text = "\(selectedDate)"
            }
            else{
                self.lastPeriodDateTxt?.text = ""
                datePicker.date = datePicker.maximumDate!
            }
        }
        else if _sender==datePicker2{
           
            datePicker2.setExpectedDateValidation()
            
            if _sender.date > datePicker2.minimumDate! && _sender.date < datePicker2.maximumDate! {
                self.expectedDeliveryDateTxt?.text = "\(selectedDate)"
            }
            else{
                self.expectedDeliveryDateTxt?.text = ""
                datePicker2.date = datePicker2.minimumDate!
            }
        }
    }
    
    @objc func dismissPicker() {
        lastPeriodDateTxt.endEditing(true)
        expectedDeliveryDateTxt.endEditing(true)
        lastPeriodDateTxt.resignFirstResponder()
        expectedDeliveryDateTxt.resignFirstResponder()
        self.endEditing(true)
        self.contentView.endEditing(true)
    }


}
