//
//  ChangePasswordTableViewCell.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 02/08/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit

class ChangePasswordTableViewCell: UITableViewCell {

    
    @IBOutlet weak var changePassTopBtn: UIButton!
    
    @IBOutlet weak var changePasswordBtn: RoundButton!
    @IBOutlet weak var confirMPAsswordTxt: RoundTextField!
    @IBOutlet weak var newPasswordTxt: RoundTextField!
    @IBOutlet weak var oldPasswordTxt: RoundTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Helper.setImageOntextField(self.oldPasswordTxt!, imagename: "", mode: true, frame: CGRect(x:0, y:0, width:15,height: (self.oldPasswordTxt!.frame.height)))
        Helper.setImageOntextField(self.newPasswordTxt!, imagename: "", mode: true, frame: CGRect(x:0, y:0, width:15,height: (self.newPasswordTxt!.frame.height)))
        Helper.setImageOntextField(self.confirMPAsswordTxt!, imagename: "", mode: true, frame: CGRect(x:0, y:0, width:15,height: (self.confirMPAsswordTxt!.frame.height)))
        
        setEyeButton(oldPasswordTxt)
        setEyeButton(newPasswordTxt)
        setEyeButton(confirMPAsswordTxt)
    }

    func setEyeButton(_ textField: UITextField) {
        let eyeView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40))
        let eyebtn = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40))
        eyebtn.setImage(#imageLiteral(resourceName: "eyeClose"), for: .normal)
        eyebtn.setImage(#imageLiteral(resourceName: "eyeOpen"), for: .selected)
        eyebtn.accessibilityElements = [textField]
        eyebtn.addTarget(self, action: #selector(toggleSecureField(_:)), for: .touchUpInside)
        eyeView.addSubview(eyebtn)
        
        textField.rightView = eyeView
        textField.isSecureTextEntry=true
        textField.rightViewMode = .always
    }
    
    @objc func toggleSecureField( _ sender: UIButton){
        guard   let textField = sender.accessibilityElements?.first as? UITextField else{
            return
        }
        
        if sender.isSelected {
            sender.isSelected = false
            textField.isSecureTextEntry = true
        }
        else{
            sender.isSelected = true
            textField.isSecureTextEntry = false
        }
    }

}
