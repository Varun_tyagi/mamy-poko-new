//
//  MyProfileTableViewCell.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 02/08/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit

class MyProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var borderView: RoundView!
    @IBOutlet weak var myProfileBtn: UIButton!
    @IBOutlet weak var firstNameText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var genderText: UITextField!
    @IBOutlet weak var pincodeText: UITextField!
    @IBOutlet weak var dobText: UITextField!
    @IBOutlet weak var cityText: UITextField!
    @IBOutlet weak var mobileText: UITextField!
    @IBOutlet weak var myProfileImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//       borderView.dropShadow()
		
    }

    func bindData(_ userData: UserDataModel? , _ isProfileOpen : Bool = false)  {
        firstNameText.text  = userData?.userName ?? ""
        emailText.text=userData?.userEmail ?? ""
        mobileText.text=userData?.userPhone ?? ""
        cityText.text=userData?.userCity ?? ""
        genderText.text =  userData != nil ?  (userData?.gender?.lowercased().contains("f"))! ?  "Female" : "Male" : ""
        pincodeText.text=userData?.pincode ?? ""
    }

}
