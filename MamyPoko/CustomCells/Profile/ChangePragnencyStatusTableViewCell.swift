//
//  ChangePragnencyStatusTableViewCell.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 20/11/18.
//  Copyright © 2018 Varun Tyagi. All rights reserved.
//

import UIKit

class ChangePragnencyStatusTableViewCell: UITableViewCell {

    @IBOutlet weak var childArrivedBtn: RoundButton!
    
    @IBOutlet weak var childNotArrivedBtn: RoundButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
