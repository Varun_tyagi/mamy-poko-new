//
//  ManagePragStatusTableViewCell.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 02/08/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit

class ManagePragStatusTableViewCell: UITableViewCell {

    @IBOutlet weak var planningBabyBtn: UIButton!
    @IBOutlet weak var wifePragBtn: UIButton!
	
	@IBOutlet var lastPeriodDOBText : RoundTextField!
	@IBOutlet var expectedDeliveryText : RoundTextField!
	@IBOutlet var saveDetailBtn : UIButton!
	@IBOutlet var isFirstPregnancyBtn : UIButton!
	
	@IBOutlet var bottomViewHeight : NSLayoutConstraint!

	var datePicker = UIDatePicker()
	var datePicker2 = UIDatePicker()
	var toolBar : UIToolbar!

	
    override func awakeFromNib() {
        super.awakeFromNib()
		expectedDeliveryText.setLeftPaddingPoints()
		lastPeriodDOBText.setLeftPaddingPoints()
		expectedDeliveryText.setCalanderImageOntextField()
		lastPeriodDOBText.setCalanderImageOntextField()
		setupDatePicker()
    }
    func bindData(_ userData: UserDataModel?)  {

        if userData != nil {
            if (userData?.gender?.lowercased().contains("f"))!{
                wifePragBtn.setTitle("I am Pregnant", for: .normal)
            }else{
                wifePragBtn.setTitle("My wife is Pregnant", for: .normal)
            }
        }
        
    }

	func setupDatePicker()  {
		
		datePicker.datePickerMode = UIDatePickerMode.date
		datePicker.addTarget(self, action: #selector(self.setDate(_sender:)), for: .valueChanged)
		datePicker.timeZone = TimeZone.current
		toolBar = UIToolbar()
		toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(dismissPicker))
		
		
		datePicker2.datePickerMode = UIDatePickerMode.date
		datePicker2.addTarget(self, action: #selector(self.setDate(_sender:)), for: .valueChanged)
		datePicker2.timeZone = TimeZone.current
		
		datePicker.setLastPeriodValidation()
		datePicker2.setExpectedDateValidation()
		
		self.lastPeriodDOBText?.inputView = datePicker
		self.expectedDeliveryText?.inputView = datePicker2
	}
	
	//MARK:- Date Picker
	@objc func setDate(_sender : UIDatePicker){
		
		let dateFormatter: DateFormatter = DateFormatter()
		dateFormatter.dateFormat = "dd-MM-yyyy"
		let selectedDate: String = dateFormatter.string(from: _sender.date)
		
		if _sender == datePicker {
			
			datePicker.setLastPeriodValidation()
			
			if _sender.date > datePicker.minimumDate! && _sender.date < datePicker.maximumDate! {
				self.lastPeriodDOBText?.text = "\(selectedDate)"
			}
			else{
				self.lastPeriodDOBText?.text = ""
				datePicker.date = datePicker.maximumDate!
			}
		}
		else if _sender==datePicker2{
			
			datePicker2.setExpectedDateValidation()
			
			if _sender.date > datePicker2.minimumDate! && _sender.date < datePicker2.maximumDate! {
				self.expectedDeliveryText?.text = "\(selectedDate)"
			}
			else{
				self.expectedDeliveryText?.text = ""
				datePicker2.date = datePicker2.minimumDate!
			}
		}
	}
	
	@objc func dismissPicker() {
		lastPeriodDOBText.endEditing(true)
		expectedDeliveryText.endEditing(true)
		lastPeriodDOBText.resignFirstResponder()
		lastPeriodDOBText.resignFirstResponder()
		self.endEditing(true)
		self.contentView.endEditing(true)
	}
}
