//
//  BabyGrowthDataTableViewCell.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 22/10/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit

class BabyGrowthDataTableViewCell: UITableViewCell {

    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var weightLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    
    @IBOutlet weak var headLbl: UILabel!
    @IBOutlet weak var heightLbl: UILabel!
    
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var lblAction: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    func bindData( _ indexPath: IndexPath ){
       
        if indexPath.row==0{
            self.lblAction.isHidden=false
            self.editBtn.isHidden=true
            self.deleteBtn.isHidden=true
            self.dateLbl.textAlignment = .center
            self.lblAction.textAlignment = .center
            self.dateLbl.text = "Date"
            self.weightLbl.text = "Weight"
            self.headLbl.text = "Head"
            self.heightLbl.text = "Height"
            self.backgroundColor = #colorLiteral(red: 0.1843137255, green: 0.7098039216, blue: 0.9176470588, alpha: 1)  
        }else{
            self.backgroundColor = UIColor.white
            self.lblAction.isHidden=true
            self.editBtn.isHidden=false
            self.deleteBtn.isHidden=false
            self.dateLbl.textAlignment = .left
           
                let measurDAgta = BabyGrowthModel.sharedBabyGrowthModel?.measurementData![indexPath.row-1]
                self.dateLbl.text = measurDAgta?.measureDate ?? ""
                self.weightLbl.text = "\(measurDAgta?.weight ?? "") \(measurDAgta?.weightUnit ?? "Kg")"
                self.headLbl.text = "\(measurDAgta?.headCircum ?? "") \(measurDAgta?.headCircumUnit ?? "Cm")"
                self.heightLbl.text = "\(measurDAgta?.height ?? "") \(measurDAgta?.heightUnit ?? "Cm")"
        }
        
        
    }

   
}
