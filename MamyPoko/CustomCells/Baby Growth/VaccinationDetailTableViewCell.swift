//
//  VaccinationDetailTableViewCell.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 26/10/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit

class VaccinationDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var vaccProtectionBtn: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var dozeLbl: UILabel!
    @IBOutlet weak var vaccNameLbl: UILabel!
    @IBOutlet weak var vaccDateLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
   
    func bindData(indexPath: IndexPath, zero_ThreeArr : [String: [ScheduleArray]?],four_SixArr : [String: [ScheduleArray]?],seven_NineArr : [String: [ScheduleArray]?],Ten_TwelveArr : [String: [ScheduleArray]?], selectedSection:String) ->ScheduleArray?{
       
        var vaccCellData:ScheduleArray? = nil

        if  selectedSection ==  "0 to 3 months" {
            let vacData = Array(zero_ThreeArr.values)[indexPath.section]
            vaccCellData =  vacData?[indexPath.row]
        }
        else if selectedSection == "4 to 6 months" {
            let vacKeyArr =  Array(four_SixArr.keys.sorted())[indexPath.section]
            let vacData = four_SixArr[vacKeyArr]
            vaccCellData =  vacData??[indexPath.row]
        }
        else if selectedSection == "7 to 9 months" {
            let vacData = Array(seven_NineArr.values)[indexPath.section]
            vaccCellData =  vacData?[indexPath.row]
        }
        else if selectedSection == "10 to 12 months" {
            switch indexPath.section {
            case 0:
                vaccCellData = Ten_TwelveArr["12-to-15-months"]??[indexPath.row]
            case 1:
                vaccCellData = Ten_TwelveArr["15-to-18-months"]??[indexPath.row]
            case 2:
                vaccCellData = Ten_TwelveArr["4-to-6-years"]??[indexPath.row]
            case 3:
                vaccCellData = Ten_TwelveArr["11-to-12-years"]??[indexPath.row]
            case 4:
                vaccCellData = Ten_TwelveArr["16-to-18-years"]??[indexPath.row]
            default:
                break
            }
        }
        
        self.vaccNameLbl.text = vaccCellData?.vaccines ?? "NA"
        self.dozeLbl.text = vaccCellData?.dose ?? "-"
        if vaccCellData?.request==1{
            self.editBtn.isHidden=true
            self.vaccDateLbl.text = vaccCellData?.vaccDateTxt ?? "-"
            self.doneBtn.isSelected=true
        }else{
            self.editBtn.isHidden=false
            self.vaccDateLbl.text = "\(vaccCellData?.dateFromTxt ?? "-") to \(vaccCellData?.dateToTxt ?? "-")"
            if vaccCellData?.dateToTxt?.count==0{
              self.vaccDateLbl.text = "\(vaccCellData?.dateFromTxt ?? "-")"
            }
            if (vaccCellData?.vaccChooseDateTxt?.count)! >  0 {
                self.vaccDateLbl.text = vaccCellData?.vaccChooseDateTxt ?? "-"
            }
            self.doneBtn.isSelected=false
        }
       return vaccCellData
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
