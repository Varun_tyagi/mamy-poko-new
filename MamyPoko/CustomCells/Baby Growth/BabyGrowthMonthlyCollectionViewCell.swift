//
//  BabyGrowthMonthlyCollectionViewCell.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 24/10/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit
import SDWebImage

class BabyGrowthMonthlyTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewMoreBtn: UIButton!
    @IBOutlet weak var babyImgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var bgView: RoundView!

    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.dropShadow()
    }
    
    func bindData(_ babyMonthlyGrowthDict:[String:Any]){
       //self.titleLbl.text = "\(babyMonthlyGrowthDict["baby_name"] ?? "") is \(babyMonthlyGrowthDict["head"] ?? "")"
        
        self.titleLbl.text = "\(babyMonthlyGrowthDict["head"] ?? "")"

        babyImgView.sd_setImage(with: URL.init(string: "\(babyMonthlyGrowthDict["image"] ?? "")"))
   }
    
}

class BabyGrowthTrackButtonTableViewCell: UITableViewCell {
    
    @IBOutlet weak var babyGrowthBtn: UIButton!
    @IBOutlet weak var vaccinationBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        babyGrowthBtn.titleLabel?.textAlignment = .center
        vaccinationBtn.titleLabel?.textAlignment = .center
        babyGrowthBtn.titleLabel?.minimumScaleFactor=0.5
        vaccinationBtn.titleLabel?.minimumScaleFactor=0.5
        babyGrowthBtn.titleLabel?.numberOfLines=2
        vaccinationBtn.titleLabel?.numberOfLines=2
        babyGrowthBtn.dropShadow(cornerRadius: 5)
        vaccinationBtn.dropShadow(cornerRadius: 5)
    }
}
