//
//  MainVaccineCell.swift
//  MamyPoko
//
//  Created by Surbhi on 18/10/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import Foundation
import UIKit


class MainVaccineCell: UITableViewCell {
    
    @IBOutlet weak var detailVaccineTable: UITableView!

    @IBOutlet weak var detailVaccineTableHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func bindData(_ scheduleArray: [[String: [ScheduleArray]?]?], _ indexPath:IndexPath){
        let vaccenData = scheduleArray[indexPath.section]
        self.detailVaccineTableHeight.constant = 0
        self.detailVaccineTableHeight.constant = CGFloat((vaccenData?.keys.count)!*30)
        for vcc in vaccenData!{
            self.detailVaccineTableHeight.constant =  self.detailVaccineTableHeight.constant + CGFloat((vcc.value?.count ?? 1)!*70)
        }
        self.contentView.layer.borderWidth=0.5
        self.contentView.layer.borderColor = UIColor.lightGray.cgColor
    }
    
}
