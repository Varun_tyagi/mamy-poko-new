//
//  BabyImageCollectionViewCell.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 29/10/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit

class BabyImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageNameLbl: UILabel!
    @IBOutlet weak var deleteImgBtn: UIButton!
    @IBOutlet weak var babyImageView: UIImageView!
}
