//
//  babyBumptableCell.swift
//  MamyPoko
//
//  Created by Surbhi on 03/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import Foundation
import UIKit

class babyBumptableCell: UITableViewCell {
    
    @IBOutlet weak var viewPinkBg: UIView!
    @IBOutlet weak var uploadPhotoBtn: UIButton!
    @IBOutlet weak var takePictureBtn: UIButton!
    @IBOutlet weak var pictureCollection: UICollectionView!
    @IBOutlet weak var pictureCollectionHeight: NSLayoutConstraint!

    @IBOutlet weak var cellView: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       viewPinkBg.dropShadow()
        }
    
    func bindData(_ uploadedImgArray:[Any]){
        self.layer.cornerRadius = 8
        self.layer.masksToBounds = true
        self.clipsToBounds = true
        
        if uploadedImgArray.count > 0{
            self.pictureCollectionHeight.constant = 100
            self.cellView.constant = 215
        }
        else{
            self.pictureCollectionHeight.constant = 0
            self.cellView.constant = 115
            
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
