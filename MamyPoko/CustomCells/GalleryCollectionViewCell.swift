//
//  GalleryCollectionViewCell.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 09/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var captionLbl: UILabel!
    
}




