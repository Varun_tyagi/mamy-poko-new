//
//  PlanningBabyCell.swift
//  MamyPoko
//
//  Created by Surbhi on 18/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import Foundation

class PlanningBabyCell : UITableViewCell {
    
    @IBOutlet var saveDetailBtn : UIButton!
    @IBOutlet var isFirstdeliveryBtn : UIButton!
    @IBOutlet var diabitiesBtn : UIButton!
    @IBOutlet var thyroidBtn : UIButton!
    @IBOutlet var otherBtn : UIButton!

    @IBOutlet var otherTextview : UITextView!
    @IBOutlet weak var textviewHeight: NSLayoutConstraint!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
            
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}

