//
//  PregnancyDetailCell.swift
//  MamyPoko
//
//  Created by Surbhi on 27/06/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit

class PregnancyDetailCell: UITableViewCell {

    @IBOutlet var lastPeriodDOBText : RoundTextField!
    @IBOutlet var expectedDeliveryText : RoundTextField!
    @IBOutlet var saveDetailBtn : UIButton!
    @IBOutlet var isFirstPregnancyBtn : UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
		lastPeriodDOBText.setLeftPaddingPoints()
		expectedDeliveryText.setLeftPaddingPoints()

		lastPeriodDOBText.setCalanderImageOntextField()
		expectedDeliveryText.setCalanderImageOntextField()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
