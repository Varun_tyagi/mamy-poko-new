//
//  QuizButtonTableViewCell.swift
//  MamyPoko
//
//  Created by Arnab Maity on 23/10/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit

class QuizButtonTableViewCell: UITableViewCell {

	@IBOutlet weak var optionBtn: UIButton!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
