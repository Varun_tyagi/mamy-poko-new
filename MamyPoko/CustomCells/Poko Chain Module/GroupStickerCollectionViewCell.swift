//
//  GroupStickerCollectionViewCell.swift
//  
//
//  Created by Arnab Maity on 26/07/19.
//

import UIKit

class GroupStickerCollectionViewCell: UICollectionViewCell {

	var sticker: Sticker? {
		willSet {
			if newValue !== sticker {
				stickerImagesView.image = nil
			}
		}
		didSet {
			if oldValue !== sticker {
				if let currentSticker = sticker {
					StickerPackManager.queue.async {
						let image = currentSticker.imageData?.image(withSize: CGSize(width: 512, height: 512))
						DispatchQueue.main.async {
							if let sticker = self.sticker, currentSticker === sticker {
								self.stickerImagesView.image = image
							}
						}
					}
				}
			}
		}
	}
	
	@IBOutlet weak var stickerImagesView: UIImageView!
	
	
}
