//
//  AnwserTableCell.swift
//  MamyPoko
//
//  Created by Arnab Maity on 30/01/20.
//  Copyright © 2020 Varun Tyagi. All rights reserved.
//

import UIKit

class AnwserTableCell: UITableViewCell {

	@IBOutlet weak var QuestionLbl: UILabel!
	
	@IBOutlet weak var AnwserLbl: UILabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
