//
//  PreviousWinnerCollectionReusableView.swift
//  MamyPoko
//
//  Created by varun tyagi on 17/06/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit
import AAPlayer
import YouTubePlayer

class PreviousWinnerCollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var weekTxt: RoundTextField!
    
    @IBOutlet weak var searchTxt: RoundTextField!
    @IBOutlet weak var previousWinnerCollection: UICollectionView!
    @IBOutlet weak var winnerVideoPlayerView: YouTubePlayerView!
    
    @IBOutlet weak var previousVideoTxtHeight: NSLayoutConstraint!
    @IBOutlet weak var playerHeight: NSLayoutConstraint!
    
    @IBOutlet weak var previousWinnerCollectionHeight: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        searchTxt.setLeftPaddingPoints()
        weekTxt.setLeftPaddingPoints()
        
    }
    
}
