//
//  StickerCollectionViewCell.swift
//  MamyPoko
//
//  Created by Arnab Maity on 26/07/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit
import SDWebImage
protocol UpdateStickerPackDelegate {
	func  updateStickerFromCell(_ sticker:Sticker?)
}

class StickerCollectionViewCell: UICollectionViewCell {
	
	private var stickerPacks: [StickerPack] = []
	var stickerCallbackDelegate:UpdateStickerPackDelegate?
	var sticker: Sticker? {
//		willSet {
//			if newValue !== sticker {
//				stickerImageView.image = nil
//			}
//		}
		didSet {
			if (sticker?.isDownloaded)!{
				if let stickerImg = UIImage.init(data: (sticker?.imageData?.data)!){
			self.stickerImageView.image = stickerImg
				}
			}else {
				self.stickerImageView.getImageFromUrl(URL(string: sticker?.imageUrl ?? "")!) { (stickerDesiredImg) in
					var  newImg = stickerDesiredImg
					if newImg.size.width > 512 || newImg.size.height > 512 || UIImagePNGRepresentation(stickerDesiredImg)!.count > 100000{
						newImg = newImg.sd_resizedImage(with: CGSize.init(width: 512, height: 512), scaleMode: SDImageScaleMode.aspectFit)!
						//	print(UIImagePNGRepresentation(newImg)!.count)
					}
					
					let newSticker = self.sticker!
					
					newSticker.imageData = ImageData.init(data: UIImagePNGRepresentation(newImg)!, type: ImageDataExtension.png)
					
					newSticker.isDownloaded = true
					
					self.stickerCallbackDelegate?.updateStickerFromCell(newSticker)
				}
//			self.stickerImageView.sd_setImage(with: URL(string: sticker?.imageUrl ?? ""), placeholderImage: #imageLiteral(resourceName: "Wish Poko Chan"), options: SDWebImageOptions.handleCookies) { (stickerImg, err, cashe, uri) in
//
//				if let stickerDesiredImg = stickerImg{
//					var  newImg = stickerDesiredImg
//					if newImg.size.width > 512 || newImg.size.height > 512 || UIImagePNGRepresentation(stickerDesiredImg)!.count > 100000{
//						newImg = newImg.sd_resizedImage(with: CGSize.init(width: 512, height: 512), scaleMode: SDImageScaleMode.aspectFit)!
//					//	print(UIImagePNGRepresentation(newImg)!.count)
//					}
//
//					let newSticker = self.sticker!
//
//					newSticker.imageData = ImageData.init(data: UIImagePNGRepresentation(newImg)!, type: ImageDataExtension.png)
//
//                    newSticker.isDownloaded = true
//
//					self.stickerCallbackDelegate?.updateStickerFromCell(newSticker)
//		}
//	}
		}
		}
 }
	@IBOutlet weak var stickerImageView: UIImageView!
	

}
extension UIImageView{
	func getImageFromUrl(_ uri:URL, _ completion: @escaping (_ desiredImage: UIImage)->Void){
		DispatchQueue.global().async {
			do{
		let iamgeData = try  Data.init(contentsOf: uri)
			let desiredImg = UIImage.init(data: iamgeData)
				DispatchQueue.main.async {
					self.image = desiredImg
					completion(desiredImg!)
				}
			}catch{
				print(error.localizedDescription)
			}
		}
	}
}
