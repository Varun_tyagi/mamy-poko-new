//
//  PreviousWinnerCollectionViewCell.swift
//  MamyPoko
//
//  Created by varun tyagi on 15/06/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit
import SDWebImage


class PreviousWinnerCollectionViewCell: UICollectionViewCell {
   
    @IBOutlet weak var previousWinnerImgView: UIImageView!
    
    @IBOutlet weak var winnerLbl: UILabel!
    var winner:WinnerVideo?  {
        didSet{
            self.winnerLbl.text = "Week \(winner?.week ?? "1") Winners"
            var thumbUrl = "http://img.youtube.com/vi/$$$/0.jpg"
            if let videoID = (winner?.videoURL ?? "").components(separatedBy: "=").last {
             thumbUrl =    thumbUrl.replacingOccurrences(of: "$$$", with: videoID)
            }
            previousWinnerImgView.sd_setImage(with: URL.init(string: thumbUrl), placeholderImage: #imageLiteral(resourceName: "Poko Chan"), options: SDWebImageOptions.continueInBackground) { (img, err, cashe, uri) in
                
            }

        }
    }
    
    
}


class ParticipantCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var participantImgView: UIImageView!
    
    @IBOutlet weak var babyNameLbl: UILabel!
    var participant:PokoData?  {
        didSet{
           self.babyNameLbl.text = participant?.childName  ?? ""
            participantImgView.sd_setImage(with: URL.init(string: participant?.image ?? ""), placeholderImage: #imageLiteral(resourceName: "Poko Chan"), options: SDWebImageOptions.continueInBackground) { (img, err, cashe, uri) in
                
            }
        }
    }
    
}
