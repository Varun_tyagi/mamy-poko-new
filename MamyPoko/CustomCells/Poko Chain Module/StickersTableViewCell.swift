//
//  StickersTableViewCell.swift
//  MamyPoko
//
//  Created by Arnab Maity on 25/07/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit


protocol stickerTblView {
	func onClickCells(index:Int)
}

class StickersTableViewCell: UITableViewCell {

	@IBOutlet weak var sticker_img1: UIImageView!
	
	@IBOutlet weak var sticker_img2: UIImageView!
	
	@IBOutlet weak var sticker_img3: UIImageView!
	
	@IBOutlet weak var sticker_img4: UIImageView!
	
	@IBOutlet weak var nxtButton: UIButton!
	
	@IBOutlet weak var stickerLabel: UILabel!
	
	@IBOutlet weak var stikcerKbLabel: UILabel!
	
	@IBOutlet weak var stickerCollection: UICollectionView!
	
	var stickerPack: StickerPack? {
		didSet {
			stickerCollection.reloadData()
		}
	}
	
	
	var myIndexs = 0
	var mainStickerCollection : UICollectionView? = nil
	
	var cellDelegate:stickerTblView?
	var index:IndexPath?

	
	
	
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	
	@IBAction func selectedBtn(_ sender: Any) {

		cellDelegate?.onClickCells(index: (index?.row)!)
	}
	


}


