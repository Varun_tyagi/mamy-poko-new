//
//  TipsByMomHeaderTableViewCell.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 06/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit

protocol SelectedTipCategoryProtocol {
    func selectedCategory(_ tipCat: TipsCategoryModelElement?)
}

class TipsByMomHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var selectCatTxt: RoundTextField!

    @IBOutlet weak var filterBtn: UIButton!
    var tipsCatArray: TipsCategoryModel = []

    var tipCategoryPicker : UIPickerView!

    var tipCatDelegate: SelectedTipCategoryProtocol?

    override func awakeFromNib() {
        super.awakeFromNib()
        Helper.setImageOntextField(self.selectCatTxt!, imagename: "", mode: true, frame: CGRect(x:0, y:0, width:15,height: (self.selectCatTxt!.frame.height)))

    }
    func bindData(_ tipsCatagoryArray : TipsCategoryModel ,  _ isStory:Bool=false)  {
        self.tipsCatArray = tipsCatagoryArray
        if !isStory {
        setupCategoryPicker()
        }
		
		
    }
    func setupCategoryPicker(){
        tipCategoryPicker = UIPickerView()
        tipCategoryPicker.tag = 1
        tipCategoryPicker.delegate = self
        tipCategoryPicker.dataSource = self
        tipCategoryPicker.accessibilityElements = [tipsCatArray]
        self.selectCatTxt?.inputView = tipCategoryPicker
    }

}

extension TipsByMomHeaderTableViewCell : UIPickerViewDelegate,UIPickerViewDataSource{

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

        let tipArray = pickerView.accessibilityElements?.first as? TipsCategoryModel
        return    tipArray?[row].catName ?? ""
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let tipArray = pickerView.accessibilityElements?.first as? TipsCategoryModel

        return tipArray?.count ?? 0
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        let tipArray = pickerView.accessibilityElements?.first as? TipsCategoryModel
        let data = tipArray?[row]
        self.selectCatTxt?.text = data?.catName  ?? ""
        tipCatDelegate?.selectedCategory(data)
            endEditing(true)
    }

}
