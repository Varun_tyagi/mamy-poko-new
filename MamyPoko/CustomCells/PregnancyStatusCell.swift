//
//  PregnancyStatusCell.swift
//  MamyPoko
//
//  Created by Surbhi on 18/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import Foundation
class PregnancyStatusCell: UITableViewCell {
    
    @IBOutlet var statusLabel : UILabel!
    @IBOutlet var addButton : UIButton!
    @IBOutlet var categoryPregnantView : UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
