//
//  ProdileDataTableViewCell.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 02/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit

class ProfileDataTableViewCell: UITableViewCell {

    @IBOutlet weak var lblProfileData: UILabel!
    @IBOutlet weak var lblStaticText: UILabel!


    func bindData(data: [String:Any])  {
        lblStaticText.text = data.keys.first
        lblProfileData.text = data.values.first as? String
    }
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
