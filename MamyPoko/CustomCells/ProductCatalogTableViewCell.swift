//
//  ProductCatalogTableViewCell.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 05/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit

class ProductCatalogTableViewCell: UITableViewCell {

    @IBOutlet weak var catalogDesLbl: UILabel!
    @IBOutlet weak var catalogNameLbl: UILabel!
    @IBOutlet weak var catalogImg: UIImageView!

    @IBOutlet weak var heightProfCatlog: NSLayoutConstraint!
    @IBOutlet weak var viewAllBtn: UIButton!
    @IBOutlet weak var productCataogLbl: UILabel!
    
    func bindData(_ data:  ProductCatalogModel , _ indexPath: IndexPath)  {
        catalogNameLbl.text = data.name
        catalogDesLbl.text = data.tagLine
        catalogImg.image = UIImage(named: data.image)

    viewAllBtn.setTitle("View All \(data.btnName) ", for: .normal)
        
       productCataogLbl.text = indexPath.row == 0 ? "Product Catalogue" : ""
       heightProfCatlog.constant = indexPath.row == 0 ? 60 : 0
    }

}
