//
//  QuizQuestionCollectionViewCell.swift
//  MamyPoko
//
//  Created by Arnab Maity on 23/10/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit

class QuizQuestionCollectionViewCell: UICollectionViewCell {
    
	@IBOutlet weak var quizLabel: UILabel!
	
	@IBOutlet weak var quizButtonTableView: UITableView!
	
	@IBOutlet weak var optionA: UIButton!
	
	@IBOutlet weak var optionB: UIButton!
	
	@IBOutlet weak var optionC: UIButton!
	
	@IBOutlet weak var optionD: UIButton!
    
    
    func bindData(_ quizQues:QuestionModel){
        self.quizLabel.text = quizQues.question ?? ""
        self.optionA.setTitle("A.\(quizQues.optionA ?? "")", for: .normal)
        self.optionB.setTitle("B.\(quizQues.optionB ?? "")", for: .normal)
        self.optionC.setTitle("C.\(quizQues.optionC ?? "")", for: .normal)
        self.optionD.setTitle("D.\(quizQues.optionD ?? "")", for: .normal)
        
       
        optionA.backgroundColor =  quizQues.selectedAns == quizQues.optionA ? .white : .clear
        optionB.backgroundColor =  quizQues.selectedAns == quizQues.optionB ? .white : .clear
        optionC.backgroundColor =  quizQues.selectedAns == quizQues.optionC ? .white : .clear
        optionD.backgroundColor =  quizQues.selectedAns == quizQues.optionD ? .white : .clear

        optionA.accessibilityHint = quizQues.optionA
        optionB.accessibilityHint = quizQues.optionB
        optionC.accessibilityHint = quizQues.optionC
        optionD.accessibilityHint = quizQues.optionD
        
        
        if quizQues.optionB == nil  || quizQues.optionB!.isEmpty || quizQues.optionB?.count == 0 {
                          self.optionB.isHidden=true
                      }else{
                          self.optionB.isHidden=false
                      }
        
        if quizQues.optionC == nil  || quizQues.optionC!.isEmpty || quizQues.optionC?.count == 0 {
                   self.optionC.isHidden=true
               }else{
                   self.optionC.isHidden=false
               }
        
        if quizQues.optionD == nil  || quizQues.optionD!.isEmpty || quizQues.optionD?.count == 0 {
            self.optionD.isHidden=true
        }else{
            self.optionD.isHidden=false
        }
        
                self.optionA.accessibilityElements = [quizQues]
               self.optionB.accessibilityElements = [quizQues]
               self.optionC.accessibilityElements = [quizQues]
               self.optionD.accessibilityElements = [quizQues]
    }
}
