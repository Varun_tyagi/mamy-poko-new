//
//  topicsInterestCollectionViewCell.swift
//  MamyPoko
//
//  Created by Arnab Maity on 20/06/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit
import  HTagView
class TopicsInterestCollectionViewCell: UICollectionViewCell {
	
	@IBOutlet weak var saveIntrestBtn: BorderButton!
	@IBOutlet weak var intrestTagView: HTagView!
	override func awakeFromNib() {
		super.awakeFromNib()
		
		
		intrestTagView.marg = 10
		intrestTagView.btwTags = 10
		intrestTagView.btwLines = 10
		intrestTagView.tagFont = UIFont.init(name: FONTS.Quicksand_Regular, size: 15)!
		intrestTagView.tagMainBackColor = UIColor.white
		intrestTagView.tagMainTextColor = Colors.MamyDarkBlue
		
		intrestTagView.tagSecondBackColor = UIColor.white
		intrestTagView.tagSecondTextColor = UIColor.black
		
		intrestTagView.tagBorderColor = UIColor.lightGray.cgColor
		intrestTagView.tagBorderWidth = 1
		
		intrestTagView.tagMaximumWidth = .HTagAutoMaximumWidth
		intrestTagView.tagCornerRadiusToHeightRatio = 0.5
		intrestTagView.reloadData()
	}
	
	func bindData(_ intrestArray:IntrestModel)  {
		intrestTagView.reloadData()
		for (index, element) in intrestArray.enumerated() {
			if (element.selected?.lowercased().contains("true"))!{
				intrestTagView.selectTagAtIndex(index)
			}
		}
	}

	
}
