//
//  TrendingTopicsHomeCollectionCell.swift
//  MamyPoko
//
//  Created by Surbhi on 03/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import Foundation
import UIKit

class TrendingTopicsHomeCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    func bindData(_ trendingTopicsArray:[Any] , _ indexPath:IndexPath){
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOpacity = 2.0
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 1.0
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = false
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        
        if trendingTopicsArray.count > 0{
            let diction = trendingTopicsArray[indexPath.item] as! Dictionary <String, Any>
            var poster =  String ()
            
            if diction["image"] as?  Bool == false || diction["image"] as?  Int == 0{
                poster = ""
            }
            else{
                poster = (diction["image"] as?  String)!
            }
            
            if poster != "" && (poster.count) > 0 {
                let posterurl = URL(string: poster)
                self.posterImage.kf.setImage(with: posterurl)
                //.image = img
            }
            else{
                // placeholder Image
            }
            
            let userImg = diction["author_image"] as?  String
            if userImg != nil || (userImg?.count)! > 0 {
                let userImgurl = URL(string: userImg!)
                self.userImage.kf.setImage(with: userImgurl)
               
            }
            else{
                // placeholder Image
            }
            
            let userName = diction["author_name"] as?  String
            let dateStr = diction["topic_date"] as?  String
            let title = diction["title"] as?  String
            
            self.titleLabel.text = title!
            self.dateLabel.text = dateStr!
            self.userName.text = userName!
            
        }
        
    }
}

