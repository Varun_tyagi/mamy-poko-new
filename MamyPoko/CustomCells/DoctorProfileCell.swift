//
//  DoctorProfileCell.swift
//  MamyPoko
//
//  Created by Surbhi on 24/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import Foundation
import UIKit

class DoctorProfileCell : UITableViewCell {
    
    @IBOutlet weak var contentVerifiedImage: UIImageView!
    @IBOutlet weak var contentLabel: UILabel!
    
    @IBOutlet weak var contentLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var contentVerifiedImageHeight: NSLayoutConstraint!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var experienceLabel: UILabel!
    @IBOutlet weak var degreeLabel: UILabel!
    @IBOutlet weak var verifiedButton: UIButton!
    @IBOutlet weak var viewProfileButton: UIButton!
    @IBOutlet weak var doctorImage: UIImageView!
 
    @IBOutlet weak var coverView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
     func getBundleIdentifier()->String{
        return self.restorationIdentifier!
    }
    func bindCellData(_ babyMonthlyTip: BabyMonthlyTipModel)  {
        self.coverView.dropShadow(scale: true)
        let userImg =   babyMonthlyTip.disclaimer?.profileImage
        if userImg != nil || (userImg?.count)! > 0 || userImg != ""{
            let userImgurl = URL(string: userImg!)
            self.doctorImage.kf.setImage(with: userImgurl)
            
        }
        else{
            // placeholder Image
            self.doctorImage.image = #imageLiteral(resourceName: "userPlaceholderProfile")
        }
        
        let userName =  babyMonthlyTip.disclaimer?.doctorName
        self.nameLabel.text = userName
        
        let userExperience = babyMonthlyTip.disclaimer?.doctorExp
        self.experienceLabel.text = userExperience
        
        let userDegree = babyMonthlyTip.disclaimer?.doctorDegree
        let userProfile = babyMonthlyTip.disclaimer?.doctorProfile
        
        self.degreeLabel.text = userProfile! + userDegree!
        
        let contentSTR = babyMonthlyTip.disclaimer?.discHeading
        self.contentLabel.text = contentSTR
        
    }
    
    func bindData(_ disclaimerdict: [String:Any]){
        self.coverView.dropShadow(scale: true)
        let userImg = disclaimerdict["profile_image"] as?  String
        if userImg != nil || (userImg?.count)! > 0 || userImg != ""{
            let userImgurl = URL(string: userImg!)
            self.doctorImage.kf.setImage(with: userImgurl)
            
        }
        else{
            // placeholder Image
            self.doctorImage.image = #imageLiteral(resourceName: "userPlaceholderProfile")
        }
        
        let userName = disclaimerdict["doctor_name"] as?  String
        self.nameLabel.text = userName
        
        let userExperience = disclaimerdict["doctor_exp"] as?  String
        self.experienceLabel.text = userExperience
        
        let userDegree = disclaimerdict["doctor_degree"] as?  String
        let userProfile = disclaimerdict["doctor_profile"] as?  String
        
        self.degreeLabel.text = userProfile! + userDegree!
        
        let contentSTR = disclaimerdict["disc_heading"] as?  String
        self.contentLabel.text = contentSTR
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
