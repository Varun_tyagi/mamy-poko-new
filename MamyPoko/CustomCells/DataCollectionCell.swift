//
//  DataCollectionCell.swift
//  Demo
//
//  Created by Arnab  maity on 18/01/20.
//  Copyright © 2020 Arnab  maity. All rights reserved.
//

import UIKit

class DataCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var btn: UIButton!
	
	@IBOutlet weak var productImg: UIImageView!
	
	@IBOutlet weak var productDescLbl: UILabel!
	
	@IBOutlet weak var productSizeLbl: UILabel!
}
