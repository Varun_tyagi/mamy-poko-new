//
//  TipsByMomTableViewCell.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 06/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit
import FacebookShare
import FBSDKShareKit
protocol TipsByMomTableViewCellProtocol {

  func btnLikeClicked(tag : Int)
   
}

class TipsByMomTableViewCell: UITableViewCell {

    @IBOutlet weak var readMoreBtn: UIButton!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var tipDescLbl: UILabel!
    @IBOutlet weak var headerLbl: UILabel!
	
	@IBOutlet weak var facebookSharing: UIButton!
	
	@IBOutlet weak var waSharing: UIButton!
	
	@IBOutlet weak var mailSharing: UIButton!
	
    @IBOutlet weak var tipsImgView: UIImageView!
    
  @IBOutlet weak var btnLike: UIButton!
  @IBOutlet weak var lblCount: UILabel!
	
  var TipsByMomTableViewCellProtocol_Delegate : TipsByMomTableViewCellProtocol?
  
  @IBAction func btnLikeTapped(_ sender: UIButton) {
    
    TipsByMomTableViewCellProtocol_Delegate?.btnLikeClicked(tag: sender.tag)
	 btnLike.setImage(UIImage(named: "heart_red"), for: .normal)
	
  }
  
  override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    func bindData(_ tip : TipModelElement){
     self.userImgView.image = #imageLiteral(resourceName: "userPlaceholderProfile")
        if let  imgUrl =  tip.image {
            if !imgUrl.isEmpty{
                
        Server.toGetImageFromURL(url:imgUrl ) { (imgUser) in
            self.userImgView.image = imgUser==nil ? #imageLiteral(resourceName: "userPlaceholderProfile") : imgUser
                }
        }
        }
        headerLbl.text = tip.headtype
        userName.text = " \(tip.name ?? "") , \(tip.city ?? "") "


       tipDescLbl.text = tip.story
      
      if tip.like_count == "0"{
        btnLike.setImage(UIImage(named: "heart_grey"), for: .normal)
      }else{
        btnLike.setImage(UIImage(named: "heart_red"), for: .normal)
      }
      lblCount.text = tip.like_count

        if tip.isReadMore == true {
            readMoreBtn.isSelected = false
            if   (tip.story?.count)! > 250 {
                tipDescLbl.text = String(tip.story?.prefix(250) ?? "")
                appendReadMoreBtn("... Read More")

            }else{
                appendReadMoreBtn("")
            }
        }else{
            tipDescLbl.text = tip.story
           readMoreBtn.isSelected = true
            appendReadMoreBtn("Read Less")
        }
        self.layer.cornerRadius = 20
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        self.dropShadow()
        self.layer.masksToBounds  = true


    }

    func appendReadMoreBtn(_ readText : String) {
        let readmoreFont = UIFont(name: "Helvetica-Oblique", size: 14.0)

        let answerAttributed = NSMutableAttributedString(string: tipDescLbl.text ?? "", attributes: [NSAttributedStringKey.font: tipDescLbl.font])
        
        let readMoreAttributed = NSMutableAttributedString(string: readText, attributes: [NSAttributedStringKey.font: readmoreFont!, NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.8941176471, green: 0.5764705882, blue: 0.7019607843, alpha: 1)])
        answerAttributed.append(readMoreAttributed)
        tipDescLbl.attributedText = answerAttributed
    }
}
