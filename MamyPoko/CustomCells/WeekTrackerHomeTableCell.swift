//
//  WeekTrackerHomeTableCell.swift
//  MamyPoko
//
//  Created by Surbhi on 03/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import Foundation
import UIKit

class WeekTrackerHomeTableCell: UITableViewCell {
    
    @IBOutlet weak var viewBlueBG: UIView!
    @IBOutlet weak var yourPregnencyTxtLbl: UILabel!
    @IBOutlet weak var weekLabel: UILabel!
    @IBOutlet weak var daysLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var sizeImage: UIImageView!
    @IBOutlet weak var viewMoreBtn: UIButton!
    @IBOutlet weak var sizeImageHeight: NSLayoutConstraint!
    @IBOutlet weak var sizeLabelHeight: NSLayoutConstraint!

    @IBOutlet weak var changeParantalStatusBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewBlueBG.dropShadow()
    }
    func bindData(_ myWorldPostDict:[String:Any] , _ myWorldDictionary: [String:Any] ,  _ babyGrowthDict: [String:Any] ,_ userData: UserDataModel? ){
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 1.0
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 1.0
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = true
        self.clipsToBounds = true
        
        if myWorldDictionary.count > 0{
            var remainingDay = "0 days"
            
            if let remDay = myWorldPostDict["Remaining_days"] as? String{
                remainingDay = remDay
            }else{
                if let remDay = myWorldPostDict["Remaining_days"] as? Int {
                    remainingDay = String(remDay)
                }
            }
            let dictSub = babyGrowthDict["post"] as? Dictionary<String,Any>
            let titleSTr = dictSub?["title"] as? String
            if (titleSTr != nil){
                if (titleSTr?.count)! > 0 {
                    let titleSTRArr = (dictSub?["title"] as? String ?? "").components(separatedBy: "size of ")
                    self.sizeLabel.text = "\(titleSTRArr.first!)" + "size of " + "\(titleSTRArr.last!.uppercased())"
                }
                else{
                    self.sizeLabel.text = ""// + "size of " + ""
                }
            }else{
                self.sizeLabel.text = ""
            }
            
            let weekStr = babyGrowthDict["passedTime"] as? String ?? ""
            
            self.yourPregnencyTxtLbl.text = "Your wife's pregnancy is in"
            let gender = userData?.gender ?? "f"
            if gender.lowercased().contains("f") {
                self.yourPregnencyTxtLbl.text  = "Your pregnancy is in"
            }
            if remainingDay == "0"{
                remainingDay = "0 Days"
            }
            self.daysLabel.text = remainingDay
            self.weekLabel.text = weekStr
            
            let sizeimage = dictSub?["image"] as? String
            
            if sizeimage != nil{
                if  ((sizeimage?.count)! > 0) {
                    let sizeimageurl = URL(string: sizeimage!)
                    self.sizeImage.kf.setImage(with: sizeimageurl)
                    self.sizeImage.isHidden = false
                    self.sizeImage.contentMode = UIViewContentMode.scaleAspectFit
                }else{
                    self.sizeImage.isHidden = true
                }
            }
            else{
                self.sizeImage.isHidden = true
            }
        }
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
