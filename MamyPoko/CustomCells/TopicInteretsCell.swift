//
//  TopicInteretsCell.swift
//  MamyPoko
//
//  Created by Surbhi Varma on 11/06/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit

protocol tableViewNew {
	func onClickCell(index:Int)
}
class TopicInteretsCell: UITableViewCell {

	@IBOutlet weak var topicInterestLbl: UILabel!
	
	@IBOutlet weak var selectButton: UIButton!
	
	
	@IBOutlet weak var topicInterestLoremLabel: UILabel!
	
	
	var cellDelegate:tableViewNew?
	var index:IndexPath?
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

	@IBAction func selectedBtn(_ sender: UIButton) {
		if (sender.isSelected == true)
		{
			
			sender.setBackgroundImage(UIImage(named: "arrow"), for: UIControlState.normal)
			
			sender.isSelected = false;
		}
		else
		{
		
			sender.setBackgroundImage(UIImage(named: "highlightedArrow"), for: UIControlState.normal)
			
			sender.isSelected = true;
		}
		cellDelegate?.onClickCell(index: (index?.row)!)
	}
}
