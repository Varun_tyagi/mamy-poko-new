//
//  3DImageCollectionCell.swift
//  MamyPoko
//
//  Created by Arnab Maity on 09/10/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit

class _DImageCollectionCell: UICollectionViewCell {
	
	@IBOutlet weak var imgFoetus: UIImageView!
	
	
	
	@IBOutlet weak var btnNext: UIButton!
	  //////////////////////////////Previous
	  @IBOutlet weak var btnPrevious: UIButton!
	  //////////////////////////////Fruit
	  @IBOutlet weak var btnFruit: UIButton!
	  //////////////////////////////3D
	  @IBOutlet weak var btn3D: UIButton!
	  
	  @IBOutlet weak var lblFoetusWeekday: UILabel!
	  //3d outlets

	  
	  //friut iboutlets
	  
	  @IBOutlet weak var lblFruitWeekDayTopHeading: UILabel!
	  
	  
	  @IBOutlet weak var lblFruitBabySizeSubtitle: UILabel!
	  /*****************************************************************************/
	  @IBOutlet weak var lblFruitName: UILabel!
	  @IBOutlet weak var imgFruit: UIImageView!
	  //friut iboutlets
	@IBAction func btn3DClicked(_ sender: UIButton) {
           setUp3D()
	}
	@IBAction func btnFruitClicked(_ sender: UIButton) {
	   setUpFruitSize()
	}
}
extension _DImageCollectionCell{
    fileprivate func setUp3D(){
      btn3D.backgroundColor = UIColor.systemTeal
      btnFruit.backgroundColor = UIColor.lightGray
      
      imgFruit.isHidden = true
      lblFruitName.isHidden = true
      lblFruitBabySizeSubtitle.isHidden = true
      lblFruitWeekDayTopHeading.isHidden = true
      
      lblFoetusWeekday.isHidden  = false
      imgFoetus.image = UIImage(named: "sampleImage")

    }
    
    fileprivate func setUpFruitSize(){
      
      btnFruit.backgroundColor = UIColor.systemTeal
      btn3D.backgroundColor = UIColor.lightGray
      
      imgFruit.isHidden = false
      lblFruitName.isHidden = false
      lblFruitBabySizeSubtitle.isHidden = false
      lblFruitWeekDayTopHeading.isHidden = false
      
      lblFoetusWeekday.isHidden  = true
      imgFoetus.image = UIImage(named: "bgfruit")
    }
    //////////////////////////////3D
}
