//
//  pregnancyTrackerDetailCell.swift
//  MamyPoko
//
//  Created by Surbhi on 04/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import Foundation
import UIKit

class pregnancyTrackerDetailCell : UITableViewCell{
    
	@IBOutlet weak var foetusImgCollection: UICollectionView!
	
    @IBOutlet weak var weekLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var weekNumberLabel: UILabel!
    @IBOutlet weak var sizeImage: UIImageView!
    @IBOutlet weak var prevButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var sizeImageHeight: NSLayoutConstraint!
    @IBOutlet weak var titleHeight: NSLayoutConstraint!
    @IBOutlet weak var imgFoetus: UIImageView!
    @IBOutlet weak var lblDetail: UILabel!
    //////////////////////////////Next
    @IBOutlet weak var btnNext: UIButton!
    //////////////////////////////Previous
    @IBOutlet weak var btnPrevious: UIButton!
    //////////////////////////////Fruit
    @IBOutlet weak var btnFruit: UIButton!
    //////////////////////////////3D
    @IBOutlet weak var btn3D: UIButton!
    
    @IBOutlet weak var lblFoetusWeekday: UILabel!
    //3d outlets

    
    //friut iboutlets
    
    @IBOutlet weak var lblFruitWeekDayTopHeading: UILabel!
    
    
    @IBOutlet weak var lblFruitBabySizeSubtitle: UILabel!
    /*****************************************************************************/
    @IBOutlet weak var lblFruitName: UILabel!
    @IBOutlet weak var imgFruit: UIImageView!
    //friut iboutlets
    
    func bindData(babyMonthlyGrowthDict: [String:Any])  {
        if babyMonthlyGrowthDict.count > 0{
            let titleSTr = babyMonthlyGrowthDict["title"] as? String
            let imageStr = babyMonthlyGrowthDict["image"] as? String
            
            if (titleSTr?.count)! == 0{
                self.titleHeight.constant = 0
            }
            else{
                self.titleHeight.constant = 40
                self.sizeLabel.text = titleSTr!
            }
            
            
            if  imageStr != nil && (imageStr?.count)! > 0 {
                let imageStrurl = URL(string: imageStr!)
                self.sizeImage.kf.setImage(with: imageStrurl)
                self.sizeImageHeight.constant = 120
            }
            else{
                // placeholder Image
                self.sizeImageHeight.constant = 0
            }
        }
    }
    
    func bindCellData(_ babyMonthlyTip: BabyMonthlyTipModel)  {
        if babyMonthlyTip.data != nil{
            let titleSTr = babyMonthlyTip.data?.first?.title ?? ""
            let imageStr =  babyMonthlyTip.data?.first?.image ?? ""
            
            if titleSTr.count == 0{
                self.titleHeight.constant = 0
            }
            else{
                self.titleHeight.constant = 40
                self.sizeLabel.text = titleSTr
            }
            
            
            if   imageStr.count > 0 {
                let imageStrurl = URL(string: imageStr)
                self.sizeImage.kf.setImage(with: imageStrurl)
                self.sizeImageHeight.constant = 120
            }
            else{
                // placeholder Image
                self.sizeImageHeight.constant = 0
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //foetusImgCollection.reloadData()
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

	//MARK:- Actions
	@IBAction func btn3DClicked(_ sender: UIButton) {
           setUp3D()
	}
	@IBAction func btnFruitClicked(_ sender: UIButton) {
	   setUpFruitSize()
	}
	
}
extension pregnancyTrackerDetailCell{
    fileprivate func setUp3D(){
      btn3D.backgroundColor = UIColor.systemTeal
      btnFruit.backgroundColor = UIColor.lightGray


    }
    
    fileprivate func setUpFruitSize(){
      
      btnFruit.backgroundColor = UIColor.systemTeal
      btn3D.backgroundColor = UIColor.lightGray

    }
    //////////////////////////////3D
}



