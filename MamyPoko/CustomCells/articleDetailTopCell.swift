//
//  articleDetailTopCell.swift
//  MamyPoko
//
//  Created by Surbhi on 05/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import Foundation
import UIKit

class articleDetailTopCell : UITableViewCell {
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
