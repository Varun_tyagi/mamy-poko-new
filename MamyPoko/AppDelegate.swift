 //
//  App2Delegate.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 24/04/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FBSDKCoreKit
import GoogleSignIn
import FirebaseCore
import FirebaseAnalytics
import Fabric
import Crashlytics
import UserNotifications
import UserNotificationsUI
import FirebaseMessaging
import AppsFlyerLib
import OneSignal
import Mixpanel
 import SwiftRater
 import Siren

@UIApplicationMain

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             class AppDelegate: UIResponder, UIApplicationDelegate {

var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    let notificationDelegate = SampleNotificationDelegate()
	let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]

var navCtrl: UINavigationController? = nil
    //MARK:- Variables for Loader
    var activityViewLoader  = UIView()
    var backImage = UIImageView()
    var backgroundView = UIView()
    var imageForRotation = UIImageView ()
    var loadingLabel = UILabel ()
    var isLoadingOn : Bool = false
    
    var activityViewLoader_splash  = UIView()
    var backImage_splash = UIImageView()
    var backgroundView_splash = UIView()
    var imageForRotation_splash = UIImageView ()
    static var FirebaseToken : String? {
        return  Messaging.messaging().fcmToken
    }
    
    //MARK:- Application Methods
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		
		//Mix Panel integration:
//		Mixpanel.initialize(token: "bbabd4dc11ec9c7fefd7569a73409213")
//		Mixpanel.mainInstance().track(event: " IOS Tracked Event!")
		Mixpanel.initialize(token: "bbabd4dc11ec9c7fefd7569a73409213")
//		Mixpanel.mainInstance().track(event: "poko_chan_bday_form_from_menu")
//		
		//One Singal Notification
		
		OneSignal.initWithLaunchOptions(launchOptions,
										appId: "48cf17cb-bcf7-42b4-9166-d182c504feff",
										handleNotificationAction: nil,
										settings: onesignalInitSettings)
		// testing one signal: 4a9c6b0e-cc05-44d5-be0c-0ae5e4ddfc79
		
		OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
		// Recommend moving the below line to prompt for push after informing the user about
		//   how your app will use them.
		OneSignal.promptForPushNotifications(userResponse: { accepted in
//			print("User accepted notifications: \(accepted)")
		})
		
        // Override point for customization after application launch.
		
        self.window = UIWindow(frame: UIScreen.main.bounds)

		let root = Constants.mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        navCtrl = UINavigationController.init(rootViewController: root)
        self.window?.rootViewController = navCtrl

        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        
		
       // UIApplication.shared.statusBarStyle = .lightContent
//        if let statusbar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
//            statusbar.backgroundColor = Colors.MAMYBlue
//        }
		if #available(iOS 13.0, *) {
				let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
				statusBar.backgroundColor = Colors.MAMYBlue
				 UIApplication.shared.keyWindow?.addSubview(statusBar)
			} else {
				UIApplication.shared.statusBarView?.backgroundColor = Colors.MAMYBlue
			}
		
		
        // Indicator//Loader
      
      
        self.createIndicator()
        self.createIndicator_splash()
		ConfigureApsFlyer()

        //FB
		ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)

        // google
        GIDSignIn.sharedInstance().clientID = "854875623574-sqbp9ep31k4o20nlfm9iusfrk6gavvhu.apps.googleusercontent.com"
        
        //Firebase Analytics
        FirebaseApp.configure()
        
        //Crashlytics
        Fabric.with([Crashlytics.self])
        
        //push notification
		if #available(iOS 10.0, *) {
			// For iOS 10 display notification (sent via APNS)
			
			Messaging.messaging().delegate = self
			
			let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
			UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (flag, err) in
				if err == nil {
					application.registerForRemoteNotifications()
					UNUserNotificationCenter.current().delegate = self
				}
			}
			let generalCategory = UNNotificationCategory(identifier: "attachmentCategory", actions: [], intentIdentifiers: [], options: .customDismissAction)
			//INVITATION Category
			UNUserNotificationCenter.current().setNotificationCategories([generalCategory])
		}
		else {
			let settings: UIUserNotificationSettings =
				UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
			application.registerUserNotificationSettings(settings)
		}
		UIApplication.shared.registerForRemoteNotifications()
		UNUserNotificationCenter.current().delegate = self
		
		
		////// End
		
       
        
        Messaging.messaging().delegate = self
        Messaging.messaging().isAutoInitEnabled = true
        Messaging.messaging().shouldEstablishDirectChannel = true
        connectToFcm()
		
		
        application.beginBackgroundTask(withName: "showNotification", expirationHandler: nil)
        
//		AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.AppLaunch,
//											 withValues: [
//												AFEventParamContent: AppsFlyerConstant.AppLaunch,
//												AFEventParamContentId: "1"
//			])
        self.configureRAting()
        self.forceLocalizationCustomizationPresentationExample()
        return true
    }
    /// Forcing the language of the update alert to a specific localization (e.g., Russian is force in this function.
    func forceLocalizationCustomizationPresentationExample() {
        let siren = Siren.shared
        let rules = Rules(promptFrequency: .immediately, forAlertType: .force)
        siren.rulesManager = RulesManager(globalRules: rules)
        
        siren.presentationManager = PresentationManager(forceLanguageLocalization: .english)
        siren.wail { results in
            switch results {
            case .success(let updateResults):
                print("AlertAction ", updateResults.alertAction)
                print("Localization ", updateResults.localization)
                print("Model ", updateResults.model)
                print("UpdateType ", updateResults.updateType)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
	
    func ConfigureApsFlyer(){
        //Configure apps flyer
        AppsFlyerTracker.shared().appsFlyerDevKey = "MWAyyoLU4TTbu2C4vyihEC"
        AppsFlyerTracker.shared().appleAppID = "1437246964"
        AppsFlyerTracker.shared()?.delegate = self
		AppsFlyerTracker.shared().isDebug = true
        #if DEBUG
        AppsFlyerTracker.shared().isDebug = true
        #endif
    }
    func configureRAting(){
        SwiftRater.daysUntilPrompt = 7
        SwiftRater.usesUntilPrompt = 5
        SwiftRater.daysBeforeReminding = 5
        SwiftRater.appLaunched()
    }
    private func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        AppsFlyerTracker.shared().continue(userActivity, restorationHandler: nil)
        return true
    }

	
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.AppEnterBackground,
                                             withValues: [
                                                AFEventParamContent: AppsFlyerConstant.AppEnterBackground,
                                                AFEventParamContentId: "2"
            ])
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.AppEnterForground,
                                             withValues: [
                                                AFEventParamContent: AppsFlyerConstant.AppEnterForground,
                                                AFEventParamContentId: "3"
            ])
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
		AppEvents.activateApp()

        // Track Installs, updates & sessions(app opens) (You must include this API to enable tracking)
		AppsFlyerTracker.shared().trackAppLaunch()
		// your other code here.... }
		
//		print("\n\n@@@@@@@@@@@@@@\n\n App Device Token Fire Base \(String(describing: Messaging.messaging().fcmToken)) \n\n\n\n@@@@@@@@@@\n\n\n")
		
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        GIDSignIn.sharedInstance().signOut()
        AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.AppTerminate,
                                             withValues: [
                                                AFEventParamContent: AppsFlyerConstant.AppTerminate,
                                                AFEventParamContentId: "4"
            ])
    }
    
    
    //MARK:- ExtendeD Appplication Methods
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
		if let scheme = url.scheme,
			scheme.localizedCaseInsensitiveCompare("mamypoko") == .orderedSame,
            let _ = url.host {
            if url.absoluteString.contains("/blog/baby-growth-detail/") || url.absoluteString.contains("/blog/instant-childhood-immunization-schedule/") || url.absoluteString.contains("/blog/my-world/"){
                
                if !Constants.appDel.returnTopViewController().isKind(of: HomeVC.self){
                    let root = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SideMenuNavigation")
                    Constants.appDelegate.window?.rootViewController = root
                    UIApplication.shared.keyWindow?.rootViewController = root
                }
                
            }
			else if url.absoluteString.contains("/products/"){
				let blogID = url.absoluteString.replacingOccurrences(of: "mamypoko://", with: "")
				
				let  destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ProductCatelogViewController") as! ProductCatelogViewController
				DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
					NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "ProductDetailToOpen"), object: blogID)
				})
				
				if (self.window != nil){
					let root = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SideMenuNavigation")
					Constants.appDelegate.window?.rootViewController = root
					UIApplication.shared.keyWindow?.rootViewController = root
					root.sideMenuController()?.setContentViewController(destViewController)

				}
				else{
					if !Constants.appDel.returnTopViewController().isKind(of: HomeVC.classForCoder()){
						let root = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SideMenuNavigation")
						root.hideSideMenuView()
						Constants.appDelegate.window?.rootViewController = root
						UIApplication.shared.keyWindow?.rootViewController = root
						root.sideMenuController()?.setContentViewController(destViewController)
					}
					else{
						let home = Constants.appDel.returnTopViewController() as! HomeVC
						home.hideSideMenuView()
						home.navigationController?.pushViewController(destViewController, animated: true)
					}

				}
				
			}
		else if url.absoluteString.contains("/blog/"){
				let blogID = url.lastPathComponent
				DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
					NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "BlogDetailPage"), object: blogID)
				})
				
				if (self.window != nil){
					let root = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SideMenuNavigation")
					Constants.appDelegate.window?.rootViewController = root
					UIApplication.shared.keyWindow?.rootViewController = root

				}
				else{
				
					if !Constants.appDel.returnTopViewController().isKind(of: HomeVC.self){
						let root = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SideMenuNavigation")
						Constants.appDelegate.window?.rootViewController = root
						UIApplication.shared.keyWindow?.rootViewController = root
					}
				}
			}
		}
		
		
		return ApplicationDelegate.shared.application(app, open: url, options: options) ||  GIDSignIn.sharedInstance().handle(url,sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,annotation: options[UIApplicationOpenURLOptionsKey.annotation])
    }
    
    func application(_ application: UIApplication,
                     open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance().handle(url,
                                                 sourceApplication: sourceApplication,
                                                 annotation: annotation)
    }
    //MARK:- UNIQUE Identifier
    static func getUniqueDeviceIdentifierAsString() -> String {
        
        let appName = Bundle.main.infoDictionary![kCFBundleNameKey as String] as! String
        
    if let appUUID = SSKeychain.password(forService: appName, account: "com.pokochain.MamyPoko") {
            return appUUID
        }
        else {
            
            let appUUID = UIDevice.current.identifierForVendor?.uuidString
        SSKeychain.setPassword(appUUID, forService: appName, account: "com.pokochain.MamyPoko")
            return appUUID!
        }
    }
    
    
    //MARK:- DEVICE Token
    static func getDeviceToken() -> String {
        var str = UserDefaults.standard.string(forKey: "deviceToken")

        if str == nil || (str?.isEmpty)! {
            str = ""
        }
        
        return str!
    }
    
    //MARK:- RETURN TOP VIEW
    func returnTopViewController()-> (UIViewController){
        let naviController = self.window?.rootViewController as! SideMenuNavigation;
        return  naviController.topViewController!
    }

    //MARK:- Loader Methods
    func createIndicator()
    {
        activityViewLoader = UIView.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT))
        activityViewLoader.backgroundColor = UIColor.clear
        
        backImage = UIImageView.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT))
        backImage.backgroundColor = UIColor.black
        backImage.alpha = 0.5
        activityViewLoader.addSubview(backImage)
        
        backgroundView = UIView.init(frame: CGRect(x: (ScreenSize.SCREEN_WIDTH - 130)/2,y: (ScreenSize.SCREEN_HEIGHT - 130)/2, width: 130 ,height: 130))
        backgroundView.backgroundColor = UIColor.init(white: 1.0, alpha: 0.90)
        backgroundView.layer.cornerRadius = 18.0
        backgroundView.layer.shadowColor = Colors.Lighttextcolor.cgColor
        backgroundView.clipsToBounds = true
        
        imageForRotation = UIImageView.init(frame: CGRect(x: (backgroundView.frame.width - 50)/2,y: 20, width: 50,height: 50))
        imageForRotation.image = #imageLiteral(resourceName: "loader2")
        imageForRotation.backgroundColor = UIColor.clear
        
        
        loadingLabel = UILabel.init(frame: CGRect(x: 10, y: 80, width: 110, height: 40))
        loadingLabel.text = " LOADING... "
        loadingLabel.textColor = UIColor.black
        loadingLabel.font = UIFont.boldSystemFont(ofSize: 16.0)
        loadingLabel.textAlignment = NSTextAlignment.center
        
        backgroundView.addSubview(imageForRotation)
        backgroundView.addSubview(loadingLabel)
        activityViewLoader.addSubview(backgroundView)
        self.window?.addSubview(activityViewLoader)
        self.window?.sendSubview(toBack: activityViewLoader)
    }
    
    func startLoader()
    {
        if isLoadingOn == false {
            isLoadingOn = true
            activityViewLoader.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
            backImage.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
            backgroundView.frame = CGRect(x: (ScreenSize.SCREEN_WIDTH - 130)/2,y: (ScreenSize.SCREEN_HEIGHT - 130)/2, width: 130 ,height: 130)
            DispatchQueue.main.async {
                self.window!.addSubview(self.activityViewLoader)
                self.rotate360Degrees()
            }
        }
    }
    
    func stopLoader()
    {
        isLoadingOn = false
        DispatchQueue.main.async {
            self.imageForRotation.layer.removeAllAnimations()
            self.window?.willRemoveSubview(self.activityViewLoader)
            self.activityViewLoader.removeFromSuperview()
        }
    }
    
    func rotate360Degrees(_ duration: CFTimeInterval = 2.25, completionDelegate: AnyObject? = nil)
    {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = CGFloat(Double.pi * -2.0)
        rotateAnimation.toValue = 0.0
        rotateAnimation.duration = duration
        rotateAnimation.repeatCount = 100000
        
        if let delegate: AnyObject = completionDelegate {
            rotateAnimation.delegate = delegate as? CAAnimationDelegate
        }
        imageForRotation.layer.add(rotateAnimation, forKey: nil)
    }
    
    
    //MARK:- Splash Loader
    func createIndicator_splash()
    {
        activityViewLoader_splash = UIView.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT))
        activityViewLoader_splash.backgroundColor = UIColor.clear

        backgroundView_splash = UIView.init(frame: CGRect(x: (ScreenSize.SCREEN_WIDTH - 70)/2,y: ScreenSize.SCREEN_HEIGHT - 80, width: 70 ,height: 70))
        backgroundView_splash.backgroundColor = UIColor.clear

        imageForRotation_splash = UIImageView.init(frame: CGRect(x: 10,y: 10, width: 50,height: 50))
        imageForRotation_splash.image = #imageLiteral(resourceName: "loader1")
        imageForRotation_splash.backgroundColor = UIColor.clear
        
        backgroundView_splash.addSubview(imageForRotation_splash)
        activityViewLoader_splash.addSubview(backgroundView_splash)
        self.window?.addSubview(activityViewLoader_splash)
        self.window?.sendSubview(toBack: activityViewLoader_splash)
    }
    
    func startLoaderSplash()
    {
        activityViewLoader_splash.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
        backgroundView_splash.frame = CGRect(x: (ScreenSize.SCREEN_WIDTH - 70)/2,y: ScreenSize.SCREEN_HEIGHT - 80, width: 70 ,height: 70)
        DispatchQueue.main.async {
            self.window!.addSubview(self.activityViewLoader_splash)
            self.rotate360DegreesSplash()
        }
    }
    
    func stopLoaderSplash()
    {
        DispatchQueue.main.async {
            self.imageForRotation_splash.layer.removeAllAnimations()
            self.window?.willRemoveSubview(self.activityViewLoader_splash)
            self.activityViewLoader_splash.removeFromSuperview()
        }
    }
    
    func rotate360DegreesSplash(_ duration: CFTimeInterval = 2.75, completionDelegate: AnyObject? = nil)
    {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = CGFloat(Double.pi * -2.0)//0.0
        rotateAnimation.toValue = 0.0//CGFloat(Double.pi * -2.0)
        rotateAnimation.duration = duration
        rotateAnimation.repeatCount = 100000
        
        if let delegate: AnyObject = completionDelegate {
            rotateAnimation.delegate = delegate as? CAAnimationDelegate
        }
        imageForRotation_splash.layer.add(rotateAnimation, forKey: nil)
    }
}
 extension AppDelegate : UNUserNotificationCenterDelegate,MessagingDelegate{
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print("***** Device Token *****\n\n")
        print(deviceTokenString)
		print("\n\n***** Dvice Token *****")
        Messaging.messaging().apnsToken = deviceToken
        UserDefaults.standard.set(deviceTokenString, forKey: "deviceToken")
        
         Messaging.messaging().setAPNSToken(deviceToken, type: MessagingAPNSTokenType.sandbox)
		
        AppsFlyerTracker.shared().registerUninstall(deviceToken)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        if let messageID = userInfo[gcmMessageIDKey] {
//            print("Message ID: \(messageID)")
        }
    }
    func sendLocalNotification(_ title: String,_ subTitle:String, _ imageData:UIImage?,inSeconds: TimeInterval = 0 , completion: @escaping (Bool) -> ()) {
        
        // Create Notification content
        let notificationContent = UNMutableNotificationContent()
        
        notificationContent.title = title
        notificationContent.subtitle = subTitle
        //notificationContent.body = ""
        if imageData != nil {
            if let attachment = "".saveImageToDisk(fileIdentifier: "CustomSamplePush", data: imageData! , options: nil) {
            notificationContent.attachments = [attachment]
        }
        }
        // Note that 60 seconds is the smallest repeating interval.
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        
        // Create a notification request with the above components
        let request = UNNotificationRequest(identifier: "CustomSamplePush", content: notificationContent, trigger: trigger)
        
        // Add this notification to the UserNotificationCenter
        UNUserNotificationCenter.current().add(request, withCompletionHandler: { error in
            if error != nil {
//                print("\(String(describing: error))")
                completion(false)
            } else {
                completion(true)
            }
        })
    }
    func application(_ application: UIApplication,  didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping  (UIBackgroundFetchResult) -> Void) {
       
        if let messageID = userInfo[gcmMessageIDKey] {
//            print("Message ID: \(messageID)")
        }
        completionHandler(UIBackgroundFetchResult.newData)
    }
   
    
    @objc func tokenRefreshNotification(_ notification: Notification) {
        if let refreshedToken = Messaging.messaging().fcmToken {
//            print("InstanceID token: \(refreshedToken)")
            let defaults = UserDefaults.standard;
            defaults.set(refreshedToken, forKey: "deviceToken");
        }
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    func connectToFcm() {
//		Messaging.messaging().connect{ (err) in
//            if (err != nil){
//                print(err as Any)
//            }else{
//                print(err as Any)
//            }
//        }
        // Won't connect since there is no token
        guard let fcmToken = Messaging.messaging().fcmToken else {
            return
        }
        // Disconnect previous FCM connection if it exists.
        Messaging.messaging().shouldEstablishDirectChannel = true
		if let refreshedToken = Messaging.messaging().fcmToken {
			//print("InstanceID token: \(refreshedToken)")
			let defaults = UserDefaults.standard;
			defaults.set(refreshedToken, forKey: "deviceToken");
		}
		
    }
    
 
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
         completionHandler([.alert, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
//        print(userInfo)
		
        switch response.notification.request.content.categoryIdentifier
        {
        case "attachmentCategory":
            break
            
        case "INVITATION":
            switch response.actionIdentifier
            {
            case "remindLater":
                print("remindLater")
                
            case "accept":
                print("accept")
                
            case "decline":
                print("decline")
                
            case "comment":
                print("comment")
                
            default:
                break
            }
            
        default:
            break
        }
		
		let userID = UserDefaults.standard.string(forKey: Constants.USERID) ?? ""
		if userID.isEmpty ||  userID.count == 0 {
		}
		else
		{
			if  let underInfoDict = userInfo["aps"] as? [String:Any] , let alertInfo = underInfoDict["alert"] as? [String : Any], let url = alertInfo["url"] as? String, let pageSTatus = alertInfo["pageStatus"] as? String{
				if !(url.isEmpty) && url.count > 0 {
					if UIApplication.shared.canOpenURL(URL.init(string: url) ?? URL.init(string: "https://www.mamypoko.co.in/products")!){
						if #available(iOS 10.0, *) {
							UIApplication.shared.open(URL.init(string: url) ?? URL.init(string: "https://www.mamypoko.co.in/products")! , options: [:], completionHandler: nil)
						} else {
							UIApplication.shared.openURL(URL.init(string: url)!)
						}
					}
				} // Open in In-App Browser
				else {
					switch pageSTatus{
					case "pregnancy_week_change":// Home Page
						let root = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SideMenuNavigation")
						Constants.appDelegate.window?.rootViewController = root
						UIApplication.shared.keyWindow?.rootViewController = root
						break
						
					case "pregnancy_after_36_weeks_users":// Home Page with pop Up
						let root = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SideMenuNavigation")
						Constants.appDelegate.window?.rootViewController = root
						UIApplication.shared.keyWindow?.rootViewController = root
						NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "pregnancy_after_36_weeks_users"), object: nil)
						
						break
						
					case "baby_month_change": // Baby Growth Tracker
						
						DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
							NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "baby_month_change"), object: nil)
						})
						
						if !Constants.appDel.returnTopViewController().isKind(of: HomeVC.self){
							let root = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "HomeVC")
							Constants.appDelegate.window?.rootViewController = root
							UIApplication.shared.keyWindow?.rootViewController = root
						}
						
						
						break
						
					case "baby_growth_details": // Baby Growth Tracker
						DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
							NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "baby_growth_details"), object: nil)
						})
						
						if !Constants.appDel.returnTopViewController().isKind(of: HomeVC.self){
							let root = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SideMenuNavigation")
							Constants.appDelegate.window?.rootViewController = root
							UIApplication.shared.keyWindow?.rootViewController = root
						}
						break
						
					case "vaccination_reminder": // Vaccination Page
						DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
							NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "vaccination_reminder"), object: nil)
						})
						
						if !Constants.appDel.returnTopViewController().isKind(of: HomeVC.self){
							let root = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SideMenuNavigation")
							Constants.appDelegate.window?.rootViewController = root
							UIApplication.shared.keyWindow?.rootViewController = root
						}
						break
						
					case "vaccination_done_reminder": // Vaccination Page
						DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
							NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "vaccination_reminder"), object: nil)
						})
						
						if !Constants.appDel.returnTopViewController().isKind(of: HomeVC.self){
							let root = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SideMenuNavigation")
							Constants.appDelegate.window?.rootViewController = root
							UIApplication.shared.keyWindow?.rootViewController = root
						}
						break

					case "prompt_vaccination_tracker": // Vaccination Page
						DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
							NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "vaccination_reminder"), object: nil)
						})
						
						if !Constants.appDel.returnTopViewController().isKind(of: HomeVC.self){
							let root = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SideMenuNavigation")
							Constants.appDelegate.window?.rootViewController = root
							UIApplication.shared.keyWindow?.rootViewController = root
						}
						break

					case "preg_nb_product_notification": // Open in IN-APP Browser
						
						let pageValStr = alertInfo["pageVal"] as? String
						
						if pageValStr != nil && (pageValStr?.count)! > 0
						{
							DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
								NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "preg_nb_product_notification"), object: URL.init(string: pageValStr!))
							})
						}
						else
						{
							DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
								NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "preg_nb_product_notification"), object: nil)
							})
						}
						
						if !Constants.appDel.returnTopViewController().isKind(of: HomeVC.self){
							let root = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SideMenuNavigation")
							Constants.appDelegate.window?.rootViewController = root
							UIApplication.shared.keyWindow?.rootViewController = root
						}
						break
						
					case "baby_size_switch_notification":
						let pageValStr = alertInfo["pageVal"] as? String
						
						if pageValStr != nil && (pageValStr?.count)! > 0
						{
							DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
								NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "baby_size_switch_notification"), object: URL.init(string: pageValStr!))
							})
						}
						else
						{
							DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
								NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "baby_size_switch_notification"), object: nil)
							})
						}
						
						if !Constants.appDel.returnTopViewController().isKind(of: HomeVC.self){
							let root = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SideMenuNavigation")
							Constants.appDelegate.window?.rootViewController = root
							UIApplication.shared.keyWindow?.rootViewController = root
						}
						break
						
					case "offer_registration":
						//Create an option for this is Profile section
						
						
						DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
							NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "offer_registration"), object: nil)
						})
						
						if !Constants.appDel.returnTopViewController().isKind(of: HomeVC.self){
							let root = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SideMenuNavigation")
							Constants.appDelegate.window?.rootViewController = root
							UIApplication.shared.keyWindow?.rootViewController = root
						}
						
						break
						
						
						
					default:
						break
					}
				}
				
			}
		}
		
        completionHandler()
    }
 
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
       // print("Firebase registration token: \(fcmToken)")
        let _:[String: String] = ["token": fcmToken]
        UserDefaults.standard.set(fcmToken, forKey: "fcmToken")
        
        Messaging.messaging().shouldEstablishDirectChannel = true
        connectToFcm()
       
    }
 
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
       // print("Received data message: \(remoteMessage.appData)")
    }
	
	
	//MARK:- notification Methods
	func postNotificationMethod(){
		NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "baby_growth_details"), object: nil)
	}
 }
 
extension AppDelegate : GIDSignInDelegate{
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            //print("\(error.localizedDescription)")
            NotificationCenter.default.post(
                name: Notification.Name(rawValue: "ToggleAuthUINotification"), object: nil, userInfo: nil)
        } else {
            //print(user.profile.name)
        }
    }

    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        NotificationCenter.default.post(
            name: Notification.Name(rawValue: "ToggleAuthUINotification"),
            object: nil,
            userInfo: ["statusText": "User has disconnected."])
    }

}
 extension AppDelegate: AppsFlyerTrackerDelegate{
    
    func onConversionDataReceived(_ installData: [AnyHashable: Any]) {
        //Handle Conversion Data (Deferred Deep Link)
    }
    
    func onConversionDataRequestFailure(_ error: Error?) {
        //    print("\(error)")
    }
    
    func onAppOpenAttribution(_ attributionData: [AnyHashable: Any]) {
        //Handle Deep Link Data
        
    }
    
    func onAppOpenAttributionFailure(_ error: Error?) {
    }
    // Reports app open from a Universal Link for iOS 9 or later
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        AppsFlyerTracker.shared().continue(userActivity, restorationHandler: restorationHandler)
        return true
    }
    
   
    

 }


