//
//  PostTipViewController.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 06/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit

class PostTipViewController: UIViewController {

    @IBOutlet weak var selectCatTxt: RoundTextField!
    @IBOutlet weak var catDescTxtView: UITextView!

    var tipCategoryPicker : UIPickerView!
    var tipCatArray:TipsCategoryModel = []
     var tipsIdentifier = "Tips"
    @IBOutlet weak var headerLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
		self.BlockSideMenuSwipe()
        setupInitialView()
    }

	override func viewWillAppear(_ animated: Bool) {
		
	}
	
	func  setupInitialView()  {
    self.headerLbl.text = "\(self.tipsIdentifier == "tips" ? "Post your tips" : "Post your story")"
        catDescTxtView.placeholder = "Write your comment.."
        catDescTxtView.layer.cornerRadius = 20
        catDescTxtView.dropShadow()
        catDescTxtView.doneAccessory = true
        selectCatTxt.dropShadow()

        Helper.setImageOntextField(self.selectCatTxt!, imagename: "", mode: true, frame: CGRect(x:0, y:0, width:15,height: (self.selectCatTxt!.frame.height)))

        if self.tipsIdentifier == "tips"{
        tipCategoryPicker = UIPickerView()
        tipCategoryPicker.tag = 1
        tipCategoryPicker.delegate = self
        tipCategoryPicker.dataSource = self
        tipCategoryPicker.accessibilityElements = [tipCatArray]
        self.selectCatTxt?.inputView = tipCategoryPicker
        }
    }
  
    @IBAction func submitAction(_ sender: Any) {
        if Helper.isConnectedToNetwork(){

            if selectCatTxt.text!.replacingOccurrences(of: " ", with: "").isEmpty {
                PKSAlertController.alert(Constants.appName, message: "Select category!")
            }
            else if catDescTxtView.text!.replacingOccurrences(of: " ", with: "").isEmpty{
                PKSAlertController.alert(Constants.appName, message: "Enter Comment!")
            }
            else if !tncBtn.isSelected{
PKSAlertController.alert(Constants.appName, message: "Accept T&C")
        }
            else if self.CheckDataValidation() {
                Constants.appDel.startLoader()
                let urlstr = Constants.BASEURL + MethodName.AddTip
                let paramStr = "tips_cat=\(selectCatTxt.text ?? "")&story=\(catDescTxtView.text ?? "")&user_id=\(UserDefaults.standard.value(forKey: Constants.USERID) ?? "0")&share=\(self.tipsIdentifier)"
                Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
                    let responseDict = response as Dictionary<String, Any>
                    Constants.appDel.stopLoader()
                    let message = responseDict["message"] as? String

                    if responseDict["status"] as? NSNumber == NSNumber.init(value: 1)  || responseDict["status"] as? String == "1"{
                        DispatchQueue.main.async {
                            PKSAlertController.alert(Constants.appName, message: message ?? "Unable to post tip. Please Try again!", acceptMessage: "Ok", acceptBlock: {
                                self.navigationController?.popViewController(animated: true)
                            })
                        }
                    }
                    else{
                        PKSAlertController.alert("Error", message: message ?? "Unable to post tip. Please Try again!")
                    }
                }
            }
        }
        else{
            PKSAlertController.alertForNetwok()
        }
    }

    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func searchAction(_ sender: Any) {
        let searchVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(searchVc, animated: true)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func CheckDataValidation()-> (Bool){
        let category = "\(self.selectCatTxt!.text!)"
        let categoryDec = "\(self.catDescTxtView!.text!)"

        if category.isEmpty == true {
            PKSAlertController.alert(Constants.appName, message: "Select category!")
            return false
        }
        else if categoryDec.isEmpty == true {
            PKSAlertController.alert(Constants.appName, message: "Enter Comment!")
            return false
        }

        return true
    }
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }

    @IBOutlet weak var tncBtn: UIButton!
    @IBAction func agreeTNCAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    @IBAction func terms(_ sender: Any) {
        let termsvc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
		termsvc.modalPresentationStyle = .fullScreen
        termsvc.isComingFromTip=true
            termsvc.type = "tip"
            DispatchQueue.main.async {
                self.present(termsvc, animated: true, completion: nil)
            }
    }
}

extension PostTipViewController : UIPickerViewDelegate,UIPickerViewDataSource{

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

        return  tipCatArray[row].catName ?? ""
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {

        return tipCatArray.count
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        self.selectCatTxt?.text = tipCatArray[row].catName  ?? ""
        self.view.endEditing(true)
    }

}



