//
//  NewRegistrationViewController.swift
//  MamyPoko
//
//  Created by Surbhi Varma on 10/06/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit

class NewRegistrationViewController: UIViewController {

	@IBOutlet weak var fullName: UITextField!
	
	@IBOutlet weak var mobileNumber: UITextField!

	@IBOutlet weak var emailTxt: UITextField!
	
	var isWaitingForOTP : Bool = false
	
	 var registerStatus : Int = UserDefaults.standard.integer(forKey: Constants.RegisterStatus)
	
	
    override func viewDidLoad() {
        super.viewDidLoad()
		mobileNumber.delegate = self
        setupInitialView()
		
    }
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
if #available(iOS 13.0, *) {
	let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
	statusBar.backgroundColor = UIColor.clear
	 UIApplication.shared.keyWindow?.addSubview(statusBar)
} else {
	 UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
 }
	}
	@IBAction func loginBtn(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func continueBtn(_ sender: UIButton) {
		if  self.CheckDataValidation(){
			self.registerForStepOne()
		}
		
	}
	
	
	
	func registerForStepOne(){
		if Helper.isConnectedToNetwork() {
			
			let urlstr = Constants.BASEURL + MethodName.Register
			
			if self.CheckDataValidation() {
				Constants.appDel.startLoader()
				self.view.endEditing(true)
				let paramStr = "user_name=\(self.fullName.text ?? "")&user_email=\(self.emailTxt.text ?? "")&user_phone=\(self.mobileNumber.text ?? "")&gender=F&device_type=iOS&device_id=\(AppDelegate.getUniqueDeviceIdentifierAsString())&device_token=\(AppDelegate.getDeviceToken())"
				
				
				
				Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
					let responseDict = response as Dictionary<String, Any>
					
					Constants.appDel.stopLoader()
					
					if responseDict["status"] as? Int ==  1   || responseDict["status"] as? String == "1" {
						
						DispatchQueue.main.async {
							//UserDefaults.standard.set(["name":self.txt_name?.text], forKey: Constants.PROFILE)
					Helper.saveDataInNsDefault(object: ["name":self.fullName?.text] as AnyObject, key: Constants.PROFILE)
							let otpVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
							otpVc.mobileNumber = self.mobileNumber.text ?? ""
							otpVc.fullName = self.fullName.text ?? ""
							otpVc.email = self.emailTxt.text  ?? ""
							otpVc.isRegister = true
							
							self.navigationController?.pushViewController(otpVc, animated: true)
						}
					}
					else{
						PKSAlertController.alert("Error", message: responseDict["message"] as! String)
					}
				}
			}
			
		}else{
			PKSAlertController.alertForNetwok()
		}
	}
	
}

//MARK:- Custom Methods
extension NewRegistrationViewController{
    
    fileprivate func setupInitialView(){
        self.fullName.setLeftPaddingPoints()
        self.mobileNumber.setLeftPaddingPoints()
		self.emailTxt.setLeftPaddingPoints()
    }
	
	fileprivate func isWaitingOtp(){
		if isWaitingForOTP == false{
			PKSAlertController.alertForNetwok()
		}
		else{
			let otpvc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "OTPVC" ) as! OTPVC
			self.navigationController?.pushViewController(otpvc, animated: true)

		}
	}
	func CheckDataValidation()-> (Bool){
		let mobile = "\(self.mobileNumber!.text!.trim())"
		let name = "\(self.fullName!.text!.trim())"
		let email = "\(self.emailTxt!.text!.trim())"
		
		if name.isEmpty == true {
			PKSAlertController.alert(Constants.appName, message: "Enter your full name!")
			return false
		}
	
		if mobile.isEmpty == true {
			PKSAlertController.alert(Constants.appName, message: "Enter your mobile number!")
			return false
		}
		else if !Helper.isValidPhoneNumber(phoneNumber: mobile) {
			PKSAlertController.alert(Constants.appName, message: "Enter your valid phone number!")
			return false
		}
		if email.isEmpty == true {
			PKSAlertController.alert(Constants.appName, message: "Enter email id !")
			return false
		}
		else if Helper.isValidEmail(email) == false {
			PKSAlertController.alert(Constants.appName, message: "Enter valid email id !")
			return false
		}
		
		return true
	}
	//MARK:- Webservice
	
	
	func resendOTPAPI(){
		if Helper.isConnectedToNetwork() {
			Constants.appDel.startLoader()
			let urlstr = Constants.BASEURL + MethodName.ResendOTP
			let paramStr = "user_mobile=\(self.mobileNumber!.text!)"
			
			Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
				let responseDict = response as Dictionary<String, Any>
				Constants.appDel.stopLoader()
				
				if responseDict["status"] as! Int ==  1  {
					DispatchQueue.main.async {
						UIView.animate(withDuration: 0.2, animations: {
							
						}) { (value) in
							self.isWaitingForOTP = true;
							
						}
					}
				}
				else{
					PKSAlertController.alert("Error", message: responseDict["message"] as! String)
				}
			}
		}else{
			PKSAlertController.alertForNetwok()
		}
	}
	
	fileprivate func loginViaOtp() {
		let urlstr = Constants.BASEURL + MethodName.ResendOTP
		let paramStr = "user_mobile=\(self.mobileNumber!.text ?? "")"
		
		Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
			let responseDict = response as Dictionary<String, Any>
			Constants.appDel.stopLoader()
			
			if responseDict["status"] as? Int ==  1  {
				DispatchQueue.main.async {
					let otpVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
					otpVc.mobileNumber = self.mobileNumber.text ?? ""
					self.navigationController?.pushViewController(otpVc, animated: true)
				}
			}
			else{
				PKSAlertController.alert("Error", message: responseDict["message"] as? String ?? KSomeErrorTryAgain)
			}
		}
		
	}
	
	
}
extension NewRegistrationViewController:UITextFieldDelegate{
	func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
		return true
	}
	
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		if textField == mobileNumber{
			let maxLength = 15
			let currentString: NSString = textField.text! as NSString
			let newString: NSString =
				currentString.replacingCharacters(in: range, with: string) as NSString
			return newString.length <= maxLength
		}
		
		return true
	}
}
