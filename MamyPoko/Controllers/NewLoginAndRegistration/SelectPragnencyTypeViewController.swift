//
//  TopicInterestViewController.swift
//  MamyPoko
//
//  Created by Surbhi Varma on 11/06/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit

class SelectPragnencyTypeViewController: UIViewController {

	var topicInterest = ["I’m Planning","I’m Pregnant","I’m a Parent"]
	var subHeadingTopicInterest = ["Everything you want to know, about planning","Track your Pregnancy, Week-by-Week","Track your baby's Growth and Vaccinations"]
	var myIndex = 0
	var registerStatus : Int = UserDefaults.standard.integer(forKey: Constants.RegisterStatus)
    //var tableFooterView: UIView?
	
	@IBOutlet weak var tblView: UITableView!
	
	override func viewDidLoad() {
        super.viewDidLoad()
		
     tblView.tableFooterView = UIView()
    }
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
if #available(iOS 13.0, *) {
	let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
	statusBar.backgroundColor = UIColor.clear
	 UIApplication.shared.keyWindow?.addSubview(statusBar)
} else {
	 UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
 }
	}
}


extension SelectPragnencyTypeViewController:UITableViewDelegate,UITableViewDataSource{
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return topicInterest.count
		
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tblView.dequeueReusableCell(withIdentifier: "TopicCell", for: indexPath) as! TopicInteretsCell
		cell.topicInterestLbl?.text = topicInterest[indexPath.row]
		cell.topicInterestLoremLabel?.text = subHeadingTopicInterest[indexPath.row]
		cell.cellDelegate = self
		cell.index = indexPath
		return cell
 	}
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 100
	}
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		myIndex = indexPath.row
		
	}
	
}
extension SelectPragnencyTypeViewController:tableViewNew{
	func onClickCell(index: Int) {
		switch index {
		case  0:
			let planningVC = Constants.mainStoryboard.instantiateViewController(withIdentifier: "IAmPlanningVC") as! IAmPlanningVC
			planningVC.planningTypeStr = topicInterest[index]
			planningVC.planningDescStr = subHeadingTopicInterest[index]
			self.navigationController?.pushViewController(planningVC, animated: true)
		case  1:
			let pregancyVC = Constants.mainStoryboard.instantiateViewController(withIdentifier: "PregnantStatusViewController") as! PregnantStatusViewController
			pregancyVC.pragTypeStr = topicInterest[index]
			pregancyVC.pragTypeDescStr = subHeadingTopicInterest[index]
			self.navigationController?.pushViewController(pregancyVC, animated: true)
		default:
			let motherVC = Constants.mainStoryboard.instantiateViewController(withIdentifier: "IAmMotherViewController") as! IAmMotherViewController
			motherVC.motherStrType = topicInterest[index]
			motherVC.motherDescStrType = subHeadingTopicInterest[index]
			self.navigationController?.pushViewController(motherVC, animated: true)
		}
		
	}
}

