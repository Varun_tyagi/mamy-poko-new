//
//  Register1VC1.swift
//  MamyPoko
//
//  Created by Surbhi Varma on 06/06/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit

class NewRegisterVC: UIViewController {

	@IBOutlet weak var fullName: RoundTextField!
	
	@IBOutlet weak var mobileNumber: RoundTextField!
	
	override func viewDidLoad() {
        super.viewDidLoad()

		self.fullName.setLeftPaddingPoints()
		self.mobileNumber.setLeftPaddingPoints()
        // Do any additional setup after loading the view.
    }
    
}
