//
//  IAmPlanningVC.swift
//  MamyPoko
//
//  Created by Arnab Maity on 20/06/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import AppsFlyerLib

class IAmPlanningVC: UIViewController {

	// Marks:Outlets
	
	@IBOutlet weak var planningLabel: UILabel!
	
	@IBOutlet weak var planningLabelSubHeading: UILabel!
	
	@IBOutlet weak var textV: UITextView!
	@IBOutlet weak var firstdeliveryBtn : UIButton!
	@IBOutlet weak var diabetesbtn : UIButton!
	@IBOutlet weak var thyroidbtn : UIButton!
	@IBOutlet weak var otehrbtn : UIButton!
	// properties
	var planningTypeStr = ""
	var planningDescStr = ""
	
	var isOtherBtnSelected : Bool = false
	
	override func viewDidLoad() {
        super.viewDidLoad()
		self.planningLabel.text = self.planningTypeStr
		self.planningLabelSubHeading.text = self.planningDescStr
        // Do any additional setup after loading the view.
    }
	//Marks: Action
	
	@IBAction func firstdeliveryBtnPressed(_ sender: UIButton) {
		 firstdeliveryBtn.isSelected = !firstdeliveryBtn.isSelected
	}
	
	@IBAction func diabetesBtnPressed(_ sender: UIButton) {
		self.diabetesbtn.isSelected =  !self.diabetesbtn.isSelected
	}
	
	@IBAction func thyroidBtnPressed(_ sender: UIButton) {
		self.thyroidbtn.isSelected =  !self.thyroidbtn.isSelected
	}
	
	@IBAction func othersBtnPressed(_ sender: UIButton) {
		self.isOtherBtnSelected = self.isOtherBtnSelected==true ? false : true
		otehrbtn.isSelected = self.isOtherBtnSelected==true ? false : true
	}
	
	
	@IBAction func continuePressed(_ sender: Any) {
		registerForStepThree_Planning()
	}
	
	func registerForStepThree_Planning(){
		if Helper.isConnectedToNetwork() {
			let urlstr = Constants.BASEURL + MethodName.RegisterStep2
			let isFirstDelivery = firstdeliveryBtn.isSelected == true ? "YES" : "NO"
			var medicalhistoryArr = [String]()
			var description : String = ""
			if diabetesbtn.isSelected {
				medicalhistoryArr.append("sugar")
			}
			if thyroidbtn.isSelected {
				medicalhistoryArr.append("thyroid")
			}
			if otehrbtn.isSelected {
				medicalhistoryArr.append("other")
				description = textV.text
			}
			
			Constants.appDel.startLoader()
			let userid = UserDefaults.standard.string(forKey: Constants.USERID)
			
			let paramStr = "user_id=\(userid!)&category=planning-baby&last_date_of_period=&delivery_date=&baby_name=&baby_gender=&baby_dob=&baby_weight=&baby_weight_unit=&baby_height=&baby_height_unit=&head_circumfrence=&head_circumfrence_unit=&medical_history=\(medicalhistoryArr)&description=\(description)&first_delivery=\(isFirstDelivery)"
			
			Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
				let responseDict = response as Dictionary<String, Any>
				Constants.appDel.stopLoader()
				
				AnalyticsManager.LogFireBaseAnalytics(analyticsType: AnalyticsEventViewItem, itemId: "id-Registration_Planning_Baby_Click", itemName: "Registration_Planning_Baby_Click", ContentType: "Registration_Planning_Baby")
				AnalyticsManager.LogApsFlyerEvent(eventType: AppsFlyerConstant.RegisterPlanningBabyClick, contentId: "20")

				if responseDict["status"] as? Int == 3  || responseDict["status"] as? String == "3"{
					UserDefaults.standard.set(2, forKey: Constants.RegisterStatus)
				
					
				}
				else if responseDict["status"] as? Int == 1 || responseDict["status"] as? String == "1"{
					let responseDictionary = responseDict["data"] as! Dictionary<String, Any>
					UserDefaults.standard.set(responseDictionary, forKey: Constants.PROFILE)
					UserDefaults.standard.set(3, forKey: Constants.RegisterStatus)
					// TAKE to HOME SCreen
					DispatchQueue.main.async {
						let root = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SideMenuNavigation")
						UserDefaults.standard.set("1", forKey: firstLogin)
						Constants.appDelegate.window?.rootViewController = root
						UIApplication.shared.keyWindow?.rootViewController = root
					}
				}
				//self.MakeDecisionForStepThreeAndHome(responseDict)
			}
		}
		else{
			PKSAlertController.alertForNetwok()
		}
	}
}
