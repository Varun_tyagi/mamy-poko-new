//
//  LoginVC.swift
//  MamyPoko
//
//  Created by Surbhi Varma on 07/06/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

let login = "login"


import UIKit
import IQKeyboardManagerSwift
import FirebaseAnalytics
import AppsFlyerLib
import FirebaseCore
import FirebaseMessaging


class LoginVC: UIViewController {
	
    //MARK:- Outlets
	@IBOutlet weak var loginUsingPasswordBtn: UIButton!
	@IBOutlet weak var textfieldHeight: NSLayoutConstraint!
	@IBOutlet weak var mobileNumber: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
	@IBOutlet weak var continueBtn: UIButton!
	
    @IBOutlet weak var forgotPassBtn: UIButton!
    //MARK:- Properties

	
    //MARK:- ViewLife Cycle
	override func viewDidLoad() {
		super.viewDidLoad()
		self.setupInitialView()

        
		
        DeviceTokenManager.submitDeviceToken("Info Screen")
		print(DeviceTokenManager.self)
	}
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
if #available(iOS 13.0, *) {
	let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
	statusBar.backgroundColor = UIColor.clear
	 UIApplication.shared.keyWindow?.addSubview(statusBar)
} else {
	 UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
 }
	}
	
	// Mark: Action
	@IBAction func loginUsingPasswordPressed(_ sender: UIButton) {
    self.textfieldHeight.constant = sender.isSelected ? 0 : 50
    self.loginUsingPasswordBtn.setTitle( sender.isSelected ? "or Login using Password" :" or Login via OTP", for: .normal)
		sender.isSelected = !sender.isSelected
		UIView.animate(withDuration: 0.3) {
			if sender.isSelected{
				self.mobileNumber.placeholder = "Email Id"
                self.mobileNumber.keyboardType = UIKeyboardType.emailAddress
                self.forgotPassBtn.isHidden=false
			}else{
				self.mobileNumber.keyboardType = UIKeyboardType.numberPad
				self.mobileNumber.placeholder = "Mobile Number"
                self.forgotPassBtn.isHidden=true
			}
			
			self.view.layoutIfNeeded()
		}
	}
	
	@IBAction func continuePressed(_ sender: Any) {
		if Helper.isConnectedToNetwork() {
            if self.CheckDataValidation(){
			 Constants.appDel.startLoader()
            self.view.endEditing(true)
                self.LoginViaPassword()
              }
        }
		else{
			PKSAlertController.alertForNetwok()
		}
    }
    
    
    @IBAction func registerBtn(_ sender: Any) {
		let registerView = Constants.mainStoryboard.instantiateViewController(withIdentifier: "NewRegistrationViewController") as! NewRegistrationViewController
		self.navigationController?.pushViewController(registerView, animated: true)
	}
	
	@IBAction func forgetPasswordbtn(_ sender: Any) {
		let forgotVC = Constants.mainStoryboard.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
		self.add(forgotVC)
	}
	
	
}

//MARK:- Custom Methods
extension LoginVC{
    fileprivate func setupInitialView(){
        self.mobileNumber?.setLeftPaddingPoints()
        self.passwordTxt?.setLeftPaddingPoints()
        self.passwordTxt.setEyeButton()
        IQKeyboardManager.shared.enable = true
        self.mobileNumber.keyboardType = UIKeyboardType.numberPad

        self.hideKeyboardWhenTappedAround()
        self.BlockSideMenuSwipe()
        self.hideKeyboardWhenTappedAround()
    }
    //MARK:- Go To Topic Of Interest Page
    func TopicOfInterestSelection() {
        // Hide current screen and display new Screen
        let topiInterest = Constants.mainStoryboard.instantiateViewController(withIdentifier: "TopicOfInterestVC") as! TopicOfInterestVC
        DispatchQueue.main.async {
            self.present(topiInterest, animated: true, completion: nil)
        }
        
    }
    
    //MARK:- Webservice
    fileprivate func sendOtp() {
        let urlstr = Constants.BASEURL + MethodName.ResendOTP
        let paramStr = "user_mobile=\(self.mobileNumber!.text ?? "")"
        
        Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
            let responseDict = response as Dictionary<String, Any>
			
            Constants.appDel.stopLoader()
            
            if responseDict["status"] as? Int ==  1  {
				print(responseDict)
				print(response)
                DispatchQueue.main.async {
					
        let otpVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
	   
        otpVc.mobileNumber = self.mobileNumber.text ?? ""
        self.navigationController?.pushViewController(otpVc, animated: true)
                }
            }
            else{
                PKSAlertController.alert("Error", message: responseDict["message"] as? String ?? KSomeErrorTryAgain)
            }
        }
        
    }
    fileprivate  func LoginViaPassword(){
        
        let paramStr = "email=\(self.mobileNumber.text ?? "")&password=\(self.passwordTxt!.text ?? "")&device_type=ios&device_id=\(AppDelegate.getUniqueDeviceIdentifierAsString())&device_token=\(AppDelegate.getDeviceToken())"
		print(paramStr)
        Server.postRequestWithURL(Constants.BASEURL + MethodName.Login, paramString: paramStr) { (response) in
            
          
          let responseDict = response as Dictionary<String, Any>
            Constants.appDel.stopLoader()
			print(response)
			print(responseDict)
            AnalyticsManager.LogFireBaseAnalytics(analyticsType: AnalyticsEventAppOpen, itemId: "id-Login_Success", itemName: "Login_Success", ContentType: "Login")
            AnalyticsManager.LogApsFlyerEvent(eventType: AppsFlyerConstant.LoginSuccess, contentId: "17")
            
            
            if responseDict["status"] as? Int == 1 || responseDict["status"] as? String == "1"  {
				let responseDC = responseDict["data"] as? [String:Any]  ?? [:]
                let userID = "\(responseDC["user_id"] ?? "")"
                UserDefaults.standard.set(0, forKey: Constants.UserDefaultsConstant.isSocialLogin)
                UserDefaults.standard.set(userID, forKey: Constants.USERID)
                UserDefaults.standard.set(3, forKey: Constants.RegisterStatus)
                UserDefaults.standard.synchronize()
                Helper.saveDataInNsDefault(object: responseDC as AnyObject, key: Constants.PROFILE)
                
                DispatchQueue.main.async {
                    // TAKE to HOME SCreen
					if !self.loginUsingPasswordBtn.isSelected{
						let otpVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
						
						otpVc.mobileNumber = self.mobileNumber.text ?? ""
						self.navigationController?.pushViewController(otpVc, animated: true)
					}else{
                    let root = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SideMenuNavigation")
                    UserDefaults.standard.set("1", forKey: firstLogin)
                    Constants.appDelegate.window?.rootViewController = root
                    UIApplication.shared.keyWindow?.rootViewController = root
					}
                }
            }
				
            else{
                PKSAlertController.alert("Error", message: responseDict["message"] as? String ?? KSomeErrorTryAgain)
            }
        }
    }
    
    // Mark:Validation
    func CheckDataValidation()-> (Bool){
        let mobile = "\(self.mobileNumber!.text!.trim())"
        let password = "\(self.passwordTxt!.text!.trim())"
        
        if mobile.isEmpty == true {
            PKSAlertController.alert(Constants.appName, message: "Enter your mobile!")
            return false
        }

        if loginUsingPasswordBtn.isSelected{
            if password.isEmpty == true {
                PKSAlertController.alert(Constants.appName, message: "Enter password!")
                return false
            }
            else if password.count < 6 {
                PKSAlertController.alert(Constants.appName, message: "Enter min 6 character password!")
                return false
            }
        }
        
        return true
    }
}
