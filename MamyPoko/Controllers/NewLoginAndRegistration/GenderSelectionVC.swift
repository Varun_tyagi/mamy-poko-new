//
//  GenderSelectionVC.swift
//  MamyPoko
//
//  Created by Arnab Maity on 24/06/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit

class GenderSelectionVC: UIViewController {
	
	var field:UITextField?
	
	var genderArray = ["Male","Female"]
	
	var picker = UIPickerView()
	
	var genderCallBack: ((String?) -> ())?

	override func viewDidLoad() {
		super.viewDidLoad()
		picker.delegate = self
		picker.dataSource = self
		field?.inputView = picker
		
		let alertController = UIAlertController(title: "", message: "Select your gender", preferredStyle: UIAlertController.Style.alert)
		let messageFont = [NSAttributedString.Key.font: UIFont(name: "Quicksand-Bold", size: 17.0)!]
		
		let messageAttrString = NSMutableAttributedString(string: "Select your gender", attributes: messageFont)
		
		alertController.setValue(messageAttrString, forKey: "attributedMessage")
		alertController.addTextField { (textField : UITextField) -> Void in
			textField.placeholder = "Gender"
			textField.layer.borderColor = UIColor.blue.cgColor
			self.field = textField
			self.field?.delegate = self
			self.field?.rightViewMode = .always
			self.field?.rightViewMode = UITextField.ViewMode.always
			self.addKeyboardToolBar()
			let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 12, height: 15))
			let image = UIImage(named: "Untitled-1")
			imageView.image = image
			self.field?.rightView = imageView
			self.field?.layer.borderWidth = 0
			self.field?.layer.masksToBounds = true
			self.field?.layer.borderColor = #colorLiteral(red: 0.1215686277, green: 0.01176470611, blue: 0.4235294163, alpha: 1)
			self.field?.borderStyle = UITextField.BorderStyle.none
			
		}
		
		let okAction = UIAlertAction(title: "Continue", style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
			self.removeChildVC()
			self.genderCallBack?(self.field?.text ?? "")
		}
		okAction.setValue(UIColor(red:0/255.0, green:115.0/255.0, blue:255.0/255.0, alpha:1.0), forKey: "titleTextColor")
		alertController.addAction(okAction)
		self.present(alertController, animated: true, completion: nil)
		
	}
	@objc func doneAction(_ sender:UIBarButtonItem){
		if (self.field?.text!.isEmpty)!{
			self.field?.text = genderArray.first
		}
		self.field?.resignFirstResponder()
	}
	func addKeyboardToolBar() {
		
		var nextButton: UIBarButtonItem?
		let keyboardToolBar = UIToolbar(frame: CGRect(x: CGFloat(0), y:
			CGFloat(0), width: CGFloat(picker.frame.size.width), height: CGFloat(25)))
		keyboardToolBar.sizeToFit()
		keyboardToolBar.barStyle = .default
		field?.inputAccessoryView = keyboardToolBar
		nextButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneAction(_:)))
		keyboardToolBar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), nextButton] as? [UIBarButtonItem]
		
	}
}
extension GenderSelectionVC:UITextFieldDelegate{
	func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
		if textField == self.field{
			field?.inputView = picker
			return true
		}
		return false
	}
}
extension GenderSelectionVC: UIPickerViewDataSource, UIPickerViewDelegate {
	
	func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		return self.genderArray.count
	}
	
	func numberOfComponents(in pickerView: UIPickerView) -> Int {
		return 1
	}
	
	func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
		return self.genderArray[row]
	}
	
	func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
		self.field?.text = self.genderArray[row]
	}
}
