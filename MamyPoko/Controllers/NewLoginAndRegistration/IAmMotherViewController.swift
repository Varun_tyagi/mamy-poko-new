//
//  IAmMotherViewController.swift
//  MamyPoko
//
//  Created by Varun Tyagi on 14/06/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit

class IAmMotherViewController: UIViewController {
	
	//Outlets
	
	@IBOutlet weak var tblView: UITableView!
	
	@IBOutlet weak var amMotherHeadingLbl: UILabel!
	
	@IBOutlet weak var amMotherSubHeadingLbl: UILabel!
	
	

    var babyNameTxt, babyGenderTxt, babyDOBTxt: RoundTextField!
    var weightTxt, heightTxt, circumferenceTxt: RoundTextField!
    var lbsButton,kgButton, cmButton, inchButton, cm_cButton,inch_cButton: UIButton!
    var babyDetails: BabyDetails? = nil
	
	var motherStrType = ""
	var motherDescStrType = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.navigationController?.isNavigationBarHidden = true
        self.SetupView()
		
    }
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
if #available(iOS 13.0, *) {
	let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
	statusBar.backgroundColor = UIColor.clear
	 UIApplication.shared.keyWindow?.addSubview(statusBar)
} else {
	 UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
 }
	}
    fileprivate func SetupView(){
        self.babyDetails = BabyDetails()
        self.babyDetails?.babyWeightUnit = "kg."
        self.babyDetails?.headCircumfrenceUnit="cm."
        self.babyDetails?.babyHeightUnit="cm."
		self.amMotherHeadingLbl.text = motherStrType
		self.amMotherSubHeadingLbl.text = motherDescStrType
    }
    
    //MARK:- BABY DETAILS Action
    @IBAction func KGButtonPressed(_ sender: UIButton) {
        kgButton.isSelected = true
        lbsButton.isSelected = false
        self.babyDetails?.babyWeightUnit = "kg."
    }
    @IBAction func LBSButtonPressed(_ sender: UIButton) {
        kgButton.isSelected = false
        lbsButton.isSelected = true
        self.babyDetails?.babyWeightUnit = "lb."
    }
    @IBAction func CMButtonPressed(_ sender: UIButton) {
        cmButton.isSelected = true
        inchButton.isSelected = false
        self.babyDetails?.babyHeightUnit = "cm."
    }
    @IBAction func InchButtonPressed(_ sender: UIButton) {
        cmButton.isSelected = false
        inchButton.isSelected = true
        self.babyDetails?.babyHeightUnit = "in."
    }
    @IBAction func CM_cButtonPressed(_ sender: UIButton) {
        cm_cButton.isSelected = true
        inch_cButton.isSelected = false
        self.babyDetails?.headCircumfrenceUnit = "cm."
    }
    @IBAction func Inch_cButtonPressed(_ sender: UIButton) {
        cm_cButton.isSelected = false
        inch_cButton.isSelected = true
        self.babyDetails?.headCircumfrenceUnit = "in."
    }
    @IBAction func SaveBabyDetailsPressed(_ sender: UIButton) {
        // hit service for baby details // check ProfileApiHelper.Swift for method defination
        self.view.endEditing(true)
        if  (self.babyNameTxt.text?.isEmpty)!{
            PKSAlertController.alert(Constants.appName, message: "Enter baby name.")
            return
        }else if  (self.babyGenderTxt.text?.isEmpty)!{
            PKSAlertController.alert(Constants.appName, message: "Select gender!")
            return
        }
        else if  (self.babyDOBTxt.text?.isEmpty)!{
            PKSAlertController.alert(Constants.appName, message: "Enter DOB!")
            return
        }else if  (self.weightTxt.text?.isEmpty)!{
            PKSAlertController.alert(Constants.appName, message: "Enter weight!")
            return
        }else if  (self.heightTxt.text?.isEmpty)!{
            PKSAlertController.alert(Constants.appName, message: "Enter height!")
            return
        }else if  (self.circumferenceTxt.text?.isEmpty)!{
            PKSAlertController.alert(Constants.appName, message: "Enter head circumference!")
            return
        }
        registerForBabyDetails()
        
    }
    func registerForBabyDetails(){
        if Helper.isConnectedToNetwork() {
            Constants.appDel.startLoader()
            self.view.endEditing(true)
            let paramStr = "user_id=\( UserDefaults.standard.string(forKey: Constants.USERID) ?? "0" )&category=mother&last_date_of_period= &delivery_date= &baby_name=\(babyNameTxt.text ?? "")&baby_gender=\((babyGenderTxt.text?.lowercased().contains("male"))! ? "M" : "F")&baby_dob=\(babyDOBTxt.text ?? "")&baby_weight=\(weightTxt.text ?? "")&baby_weight_unit=\(lbsButton.isSelected==true ? "lb." : "kg.")&baby_height=\(heightTxt.text ?? "")&baby_height_unit=\(cmButton.isSelected ? "cm." : "in.")&head_circumfrence=\(circumferenceTxt.text ?? "")&head_circumfrence_unit=\(cm_cButton.isSelected ? "cm." : "in.")&medical_history=&description=&first_delivery=NO"
            
            Server.postRequestWithURL(Constants.BASEURL + MethodName.RegisterStep2, paramString: paramStr) { (response) in
                let responseDict = response as Dictionary<String, Any>
               
				DispatchQueue.main.async {
					Constants.appDel.stopLoader()
					
					if responseDict["status"] as? Int == 3  || responseDict["status"] as? String == "3"{
						UserDefaults.standard.set(2, forKey: Constants.RegisterStatus)
//						self.delegate?.registerationStep3Complete()
						
					}
					else if responseDict["status"] as? Int == 1 || responseDict["status"] as? String == "1"{
						let responseDictionary = responseDict["data"] as! Dictionary<String, Any>
						UserDefaults.standard.set(responseDictionary, forKey: Constants.PROFILE)
						UserDefaults.standard.set(3, forKey: Constants.RegisterStatus)
						// TAKE to HOME SCreen
						let root = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SideMenuNavigation")
						UserDefaults.standard.set("1", forKey: firstLogin)
						Constants.appDelegate.window?.rootViewController = root
						UIApplication.shared.keyWindow?.rootViewController = root
					}
					else{
						PKSAlertController.alert("Error", message: responseDict["message"] as? String ?? KSomeErrorTryAgain)
					}
				}
//                self.MakeDecisionForStepThreeAndHome(responseDict)
            }
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
	
}
    
extension IAmMotherViewController : UITableViewDelegate,UITableViewDataSource{
        //MARK:- TableView Methods
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return  1
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableViewAutomaticDimension
        }
        
    
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           
let cell = tableView.dequeueReusableCell(withIdentifier: "MyBabyDetailTableViewCell", for: indexPath) as! MyBabyDetailTableViewCell
            
    cell.BindIAmMotherData(nil, babyDetails: self.babyDetails, vc: self)
            
            return cell
           
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
        }
        
    }
    extension IAmMotherViewController : UITextFieldDelegate {
        func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
            if textField == babyGenderTxt {
                babyDOBTxt.resignFirstResponder()
                babyNameTxt.resignFirstResponder()
                self.view.endEditing(true)
                CustomPicker.show(data: [["Male", "Female"]]) {  [weak self] (selections: [Int : String]) -> Void in
                    if let name = selections[0] {
                        self?.babyGenderTxt.text = name
                    }
                }
                return false
            }
            return true
        }
        func textFieldDidEndEditing(_ textField: UITextField) {
            self.view.endEditing(true)
        }
}
