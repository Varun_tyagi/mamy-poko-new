//
//  OTPVC.swift
//  MamyPoko
//
//  Created by Surbhi Varma on 06/06/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit

class OTPVC: UIViewController {
	
	
	// Marks:Outlet
	
	@IBOutlet weak var otpView: VPMOTPView!

	@IBOutlet weak var otpTxtField: UITextField!
	
//	var enteredOtp:String?

    var mobileNumber = ""
	var email = ""
	var fullName = ""
    var isRegister = false
	
	override func viewDidLoad() {
        super.viewDidLoad()
        otpTxtField.delegate = self
		self.otpTxtField.addBottomBorder()
		//setupInitialView()
    }
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
if #available(iOS 13.0, *) {
	let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
	statusBar.backgroundColor = UIColor.clear
	 UIApplication.shared.keyWindow?.addSubview(statusBar)
} else {
	 UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
 }
	}
	//Marks: Action
	
	@IBAction func resendBtn(_ sender: Any) {
		resendOtp()
	}
	
	@IBAction func submitBtn(_ sender: UIButton) {
		
		if(otpTxtField?.text == nil){
			///error
			return
		} else if((otpTxtField?.text?.count)! < 6) {
			///error
			return
		} else{
		self.verifyOtp()
		}

	}
}
extension OTPVC:UITextFieldDelegate{
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		guard let text = textField.text else { return true }
		let newLength = text.count + string.count - range.length
		return newLength <= 6
	}
}

//MARK:- Custom Methods
extension OTPVC{
    

	
	
	// Webservices
	fileprivate func resendOtp() {
		if Helper.isConnectedToNetwork() {
		 Constants.appDel.startLoader()
			 self.view.endEditing(true)
		let urlstr = Constants.BASEURL + MethodName.ResendOTP
		let paramStr = "user_mobile=\(self.mobileNumber)"
		
		Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
			let responseDict = response as Dictionary<String, Any>
			Constants.appDel.stopLoader()
			
			if responseDict["status"] as? Int !=  1  {
				DispatchQueue.main.async {
					PKSAlertController.alertForNetwok(title: "Success", message: "OTP send to your number")
				}
			}
			else{
				PKSAlertController.alert("Success", message: responseDict["message"] as? String ?? KSomeErrorTryAgain)
			}
		}
	  }
		else{
			PKSAlertController.alertForNetwok()
		}
	}
	fileprivate func verifyOtp() {
		if Helper.isConnectedToNetwork() {
			Constants.appDel.startLoader()
			 self.view.endEditing(true)
		let urlstr = Constants.BASEURL + MethodName.verifyOtp
		self.view.endEditing(true)
		let postData = NSMutableData(data: "user_mobile=\(mobileNumber)".data(using: String.Encoding.utf8)!)
		postData.append("&user_email=\(email)".data(using: String.Encoding.utf8)!)
		postData.append("&otp_val=\(otpTxtField.text!)".data(using: String.Encoding.utf8)!)
		postData.append("&user_name=\(fullName)".data(using: String.Encoding.utf8)!)

		Server.postRequestWithEndcodedURL(urlstr, postData: postData) { (response) in
			let responseDict = response as Dictionary<String, Any>
			Constants.appDel.stopLoader()
			response.printJson()
			if responseDict["status"] as? Int ==  1 || responseDict["status"] as? String == "1"  {
				let responseDC = responseDict["data"] as! Dictionary<String, Any>
				
			    let userID = "\(responseDC["user_id"] as! NSNumber)"
				UserDefaults.standard.set(userID, forKey: Constants.USERID)
				UserDefaults.standard.synchronize()
				Helper.saveDataInNsDefault(object: responseDC as AnyObject, key: Constants.PROFILE)
				DispatchQueue.main.async {
					if self.isRegister {
						UserDefaults.standard.set(2, forKey: Constants.RegisterStatus)
						let registvc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SelectPragnencyTypeViewController") as! SelectPragnencyTypeViewController
						self.navigationController?.pushViewController(registvc, animated: true)
					}else{
						UserDefaults.standard.set(3, forKey: Constants.RegisterStatus)
						let root = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SideMenuNavigation")
						UserDefaults.standard.set("1", forKey: login)
						Constants.appDelegate.window?.rootViewController = root
						UIApplication.shared.keyWindow?.rootViewController = root
					}
					}
			}
			else if responseDict["status"] as? Int ==  2 || responseDict["status"] as? String == "2" {
				let responseDC = responseDict["data"] as! Dictionary<String, Any>
//				let userName = "\(responseDC["user_name"] ?? "")"
				let userID = "\(responseDC["user_id"] as! NSNumber)"
				UserDefaults.standard.set(userID, forKey: Constants.USERID)
				UserDefaults.standard.set(2, forKey: Constants.RegisterStatus)
				UserDefaults.standard.synchronize()
				Helper.saveDataInNsDefault(object: responseDC as AnyObject, key: Constants.PROFILE)
				
				DispatchQueue.main.async {
					let registvc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SelectPragnencyTypeViewController") as! SelectPragnencyTypeViewController
					self.navigationController?.pushViewController(registvc, animated: true)
					}
				}
			else{
				PKSAlertController.alert("Error", message: responseDict["message"] as? String ?? KSomeErrorTryAgain)
			}
		}
 	}
		else{
	       PKSAlertController.alertForNetwok()
		}
		
	}
	
}


