//
//  ArticleDetailVC.swift
//  MamyPoko
//
//  Created by Surbhi on 05/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import Foundation
import UIKit
import FacebookShare
import FBSDKShareKit
import AMTooltip
import FirebaseAnalytics
import AppsFlyerLib
import MessageUI


class  ArticleDetailCellText : UITableViewCell {
    
    @IBOutlet weak var webviewHeight: NSLayoutConstraint!
}
class ArticleDetailVC: UIViewController , UITextViewDelegate, MFMailComposeViewControllerDelegate {
    
 
    @IBOutlet weak var setFontBtn: UIButton!
    
    @IBOutlet weak var detailTable: UITableView!
    @IBOutlet weak var fontView: UIView!

    var postID = String()
    var commentArray = [Any]()
    var postDatadict = [String : Any]()
    var disclaimerdict = [String : Any]()

    var commentTextField = UITextField()
    var commentTextView = UITextView()
    var countlabel = UILabel()
    var documentInteractionController: UIDocumentInteractionController = UIDocumentInteractionController()
    var postImage = UIImage()
    var bodyFont : Float = 14.0
    var headerFont : Float = 18.0
    var webViewData:UIWebView?
    var isFinishLoad=false
    var isPostDisclaimerNeeded = false
    var postDisclaimerStr = "The information shared in the article is indicative in nature. You should consult your medical practitioner before following the suggestions in this article. Each Individual is different & may react differently to the suggested measures.The author, publisher, company & its officials disclaim any liability in connection with the use of this information."
    //MARK:- View Methods
    override func viewDidLoad() {
		
		self.BlockSideMenuSwipe()
        self.hideKeyboardWhenTappedAround()
        self.GetPostsForDetailVC(postID: self.postID)

		Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
            AnalyticsParameterItemID: "id-\(AppsFlyerConstant.BlogDetail)",
            AnalyticsParameterItemName: "\(AppsFlyerConstant.BlogDetail)",
            AnalyticsParameterContentType: "Blog_Detail"
            ])
        AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.BlogDetail,
                                             withValues: [
                                                AFEventParamContent: AppsFlyerConstant.BlogDetail,
                                                AFEventParamContentId: "27"
            ])
        setFontBtn.layer.shadowColor = UIColor.gray.cgColor
        setFontBtn.layer.shadowOffset = CGSize(width: 0.5, height: 2.5)
        setFontBtn.layer.shadowOpacity = 0.6
        setFontBtn.layer.shadowRadius = 0.0
        setFontBtn.layer.masksToBounds = false
        setFontBtn.layer.cornerRadius = setFontBtn.frame.size.width/2
        
        if UserDefaults.standard.value(forKey: firstLogin) != nil && UserDefaults.standard.string(forKey: firstLogin) == "1"{
            AMTooltipView(
                options:AMTooltipViewOptions(
                    textColor: UIColor.white,
                    font: UIFont(name: FONTS.AdiraDisplaySSi, size: 20) ?? UIFont.systemFont(ofSize: 20), textBoxBackgroundColor: Colors.MAMYBlue,
                    addOverlayView: true,
                    lineColor: UIColor.gray,
                    dotColor: UIColor.lightGray,
                    dotBorderColor: UIColor.gray,
                    focusViewRadius: setFontBtn.frame.size.width/2+5,
                    focustViewVerticalPadding : 10,
                    focustViewHorizontalPadding : 10
                ),
                message: "Tap to increase the font size",
                focusView: setFontBtn,
                target: self)
            UserDefaults.standard.removeObject(forKey: firstLogin)
       }

    }
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		if #available(iOS 13.0, *) {
			let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
			statusBar.backgroundColor = Colors.MAMYBlue
			 UIApplication.shared.keyWindow?.addSubview(statusBar)
		} else {
			 UIApplication.shared.statusBarView?.backgroundColor = Colors.MAMYBlue
		 }
	}
	
    //MARK:- API
    
    func GetPostsForDetailVC(postID : String){
        if Helper.isConnectedToNetwork() {
            
            Constants.appDel.startLoader()
            
            let url_str = Constants.BASEURL + MethodName.ArticleDetails + "\(postID)"
            Server.getRequestWithURL(urlString: url_str, completionHandler: { (response) in

                Constants.appDel.stopLoader()
                if !(response==nil){
                    if (response?.count)! > 1 && response?["status"] as! NSNumber == NSNumber.init(value: 1)   || response?["status"] as? String == "1" {
                        let data = response?["data"] as! Dictionary <String, Any>
                        self.postDatadict = data["post_data"] as! Dictionary <String, Any>
                        self.commentArray = data["post_comments"] as! Array
                        
                        if data.keys.contains("disclaimer") == true{
                            self.disclaimerdict = data["disclaimer"] as! Dictionary <String, Any>
                        }
                        if let postDisclaimer = self.postDatadict["post_desclaimer"] as? String {
                          self.postDisclaimerStr = postDisclaimer
                            self.isPostDisclaimerNeeded = true
                        }
                        DispatchQueue.main.async {
                            self.detailTable.reloadData()
                            self.detailTable.scrollToRow(at: IndexPath.init(row: 0, section: 0 ), at: UITableViewScrollPosition.top, animated: false)
                        }
                    }else{
                        PKSAlertController.alert(Constants.appName, message: response?["message"] as! String)
                    }
                
                }else{
                    PKSAlertController.alert(Constants.appName, message: "Some error has occured! Please try again")
                }
               
            })
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
    
    
    func PostCommentForAticle(comment : String){
		
		let newcomment = comment.replacingOccurrences(of: "\n", with: " ")
        if Helper.isConnectedToNetwork() {
            
            let urlstr = Constants.BASEURL + MethodName.SaveComment
            let userID = UserDefaults.standard.string(forKey: Constants.USERID)
            
            if userID == nil || userID?.count == 0 {
                PKSAlertController.alert(Constants.appName, message: "Invalid User ID")
            }
            else{
                Constants.appDel.startLoader()
                let paramStr = "user_id=\(userID!)&post_id=\(self.postID)&comment_content=\(newcomment)&comment_author_IP=\(AppDelegate.getUniqueDeviceIdentifierAsString())"
                Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
                    let responseDict = response as Dictionary<String, Any>
                    Constants.appDel.stopLoader()
                    Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
                        AnalyticsParameterItemID: "id-Blog_Comment",
                        AnalyticsParameterItemName: "Blog_Comment",
                        AnalyticsParameterContentType: "Event"
                        ])
                    AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.BlogComment,
                                                         withValues: [
                                                            AFEventParamContent: AppsFlyerConstant.BlogComment,
                                                            AFEventParamContentId: "28"
                        ])
                    if responseDict["status"] as? NSNumber == NSNumber.init(value: 1)  || responseDict["status"] as? String == "1" {
                        DispatchQueue.main.async {
                            self.GetPostsForDetailVC(postID: self.postID)
                        }
                    }
                    else if responseDict["status"] as? NSNumber == NSNumber.init(value: 0) || responseDict["status"] as? String == "0"{
                        DispatchQueue.main.async {
                            PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                        }
                    }
                    else{
                        PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                    }
                }
            }
            
        }else{
            PKSAlertController.alertForNetwok()
        }
    }

    
    
    //MARK:- Action Methods
    @IBAction func BackButtonPressed(){
        self.dismiss(animated: true, completion: nil)
		self.navigationController?.popViewController(animated: true)
    }

    
    @objc func FBButtonPressed(){
		
        let link = self.postDatadict["sharing_link"] as? String
		let content = ShareLinkContent.init()
		content.contentURL = URL.init(string: link!)!
		content.hashtag = Hashtag.init("#SheCaresSheShares")
        
        
		let dialog = ShareDialog.init()
        dialog.fromViewController = self;
        dialog.shareContent = content
        
        let isInstalled : Bool = UIApplication.shared.canOpenURL(URL.init(string: "fb://")!)
        
        if isInstalled {
			dialog.mode = ShareDialog.Mode.shareSheet

        } else {
			dialog.mode = ShareDialog.Mode.feedBrowser
        }
        dialog.show()
		
    }
    
    
    @objc func WhatsAppButtonPressed(){

        let link = self.postDatadict["sharing_link"] as! String
        let whatsappUrlString = "whatsapp://send?text=\(link)"

        if let whatsappURL = NSURL(string: whatsappUrlString) {
            if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                UIApplication.shared.open(whatsappURL as URL, options: [:], completionHandler: nil)
            }
            else{
                PKSAlertController.alert("WhatsApp Error", message: "Download WhatsApp from Appstore to continue")
            }
        }
    }
    
    @objc func GmailButtonPressed(){

        let title = self.postDatadict["title"] as! String
        let hastags = "SheCaresSheShares"
        let link = self.postDatadict["sharing_link"] as! String
        let subject = "\(hastags)\n\(title)"

		if !MFMailComposeViewController.canSendMail() {
			PKSAlertController.alert("Error!", message: "Mail services are not available")
			return
		}
		
		let mailVC = MFMailComposeViewController()
		mailVC.mailComposeDelegate = self as? MFMailComposeViewControllerDelegate
		mailVC.setSubject(subject)
		guard  (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String) != nil else {
			return
		}
		
		mailVC.setMessageBody("\(title)\n\n#\(hastags)\n\n\(link)", isHTML: false)
		
		present(mailVC, animated: true, completion: nil)
    }

	func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
		controller.dismiss(animated: true, completion: nil)
	}
		
	
    
    @objc func TwitterButtonPressed(sender : UIButton){

        let title = self.postDatadict["title"] as? String
        let hastags = "\n #SheCaresSheShares \n\n"
        let link = self.postDatadict["sharing_link"] as? String
        let linkURL = URL.init(string: link!)

        
        let shareString = "https://twitter.com/intent/tweet?text=\(title ?? "")\(hastags)&url=\(String(describing: linkURL ?? URL(string: "")))"
        
        // encode a space to %20 for example
        let escapedShareString = shareString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        // cast to an url
        let url = URL(string: escapedShareString)
        
        // open in safari
        if  UIApplication.shared.canOpenURL(url!) {
            UIApplication.shared.open(url!, options: [:]) { (flag) in
                
            }
        }
    }
    @objc func linkedinButtonPressed(sender : UIButton){
        
        let title = self.postDatadict["title"] as? String
        let hastags = "\n #SheCaresSheShares \n\n"
        let link = self.postDatadict["sharing_link"] as? String
        let linkURL = URL.init(string: "http://www.linkedin.com")

		if  UIApplication.shared.canOpenURL(linkURL!) {
			UIApplication.shared.open(linkURL!, options: [:]) { (flag) in
				
			}
		}
		else{
			let linkURL2 = URL.init(string: link!)

			let alert = UIActivityViewController.init(activityItems: [title!,hastags, linkURL2!, postImage ], applicationActivities: nil)
			DispatchQueue.main.async {
				self.present(alert, animated: true, completion: nil)
			}
		}
    }
    
    @objc func SubmitCommentButtonPressed(sender : UIButton){
        if self.commentTextView.text == "" || self.commentTextView.text.count == 0 {
            //Do Nothing
        }
        else{
            self.PostCommentForAticle(comment: self.commentTextView.text!)
        }
    }
    @IBAction func searchAction(_ sender: Any) {
        let searchVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        searchVc.isPresent = true
        present(searchVc, animated: false, completion: nil)
    }
    
    @IBAction func FontButtonPressed(_ sender: Any) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.25, animations: {
                self.fontView.isHidden = false
                self.view.bringSubview(toFront: self.fontView)
            }) { (val) in
                self.fontView.isHidden = false
                self.view.bringSubview(toFront: self.fontView)
            }
        }
    }
    @IBAction func FontIncreaseButtonPressed(_ sender: Any) {

        if self.bodyFont <= 20 {
            self.bodyFont = self.bodyFont + 1.5
            self.headerFont = self.headerFont + 1.5
            isFinishLoad=false
            DispatchQueue.main.async {
                self.detailTable.reloadData()
                self.webViewData?.reload()
            }
        }
    }
    
    @IBAction func FontDecreaseButtonPressed(_ sender: Any) {
        if self.bodyFont >= 17 {
            self.bodyFont = self.bodyFont - 1.5
            self.headerFont = self.headerFont - 1.5
            isFinishLoad=false
            DispatchQueue.main.async {
                self.detailTable.reloadData()
                self.webViewData?.reload()
            }
        }
    }
    
    @IBAction func FontButtonClosed(_ sender: Any) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.25, animations: {
                self.view.sendSubview(toBack: self.fontView)
            }) { (val) in
                self.fontView.isHidden = true
                self.view.sendSubview(toBack: self.fontView)
            }
        }
    }

    @objc func OpenDoctorProfile(){
        DispatchQueue.main.async {
            let searchVc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "PanelistProfileVC") as! PanelistProfileVC
            searchVc.disclaimerdict = self.disclaimerdict
            self.present(searchVc, animated: false, completion: nil)
        }
    }

    //MARK:- TextView Delegate
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentCharacterCount = textView.text?.count ?? 0
        let newLength = currentCharacterCount + text.count - range.length
        
        if newLength <= 200{
            self.countlabel.text = "\(newLength)/200"
        }
        else{
            self.countlabel.text = "200/200"
        }
        return newLength <= 200
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Write your comment here ..." {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.count == 0 {
            textView.text = "Write your comment here ..."
            textView.textColor = UIColor.gray
        }
        else{
            textView.textColor = UIColor.black
        }
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
 
    var webViewDataHeight:CGFloat = 0.0
}
extension ArticleDetailVC : UIWebViewDelegate{
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if isFinishLoad==true{
            return
        }
        webView.frame.size.height = 1
        webView.frame.size = CGSize.init(width: detailTable.frame.size.width-20, height: webView.scrollView.contentSize.height + 10)
        webView.scrollView.contentSize.width = detailTable.frame.size.width-20
        webViewDataHeight = webView.scrollView.contentSize.height
        webViewData?.reload()
        detailTable.reloadData()
        isFinishLoad = true

    }
   
}

extension ArticleDetailVC : UITableViewDelegate,UITableViewDataSource{
    //MARK:- TableView
    func numberOfSections(in tableView: UITableView) -> Int {
        if isPostDisclaimerNeeded{
            if self.disclaimerdict.count > 0{
                return 7
            }
            return 6
        }else{
            if self.disclaimerdict.count > 0{
                return 6
            }
            return 5
        }
       
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isPostDisclaimerNeeded{
            if self.disclaimerdict.count > 0{
                if section == 6 {
                    return self.commentArray.count
                }
                else{
                    if self.postDatadict.count > 0{
                        return 1
                    }
                }
            }
            else{
                if section == 5 {
                    return self.commentArray.count
                }
                else{
                    if self.postDatadict.count > 0{
                        return 1
                    }
                }
            }
            return 0
        }else{
            if self.disclaimerdict.count > 0{
                if section == 5 {
                    return self.commentArray.count
                }
                else{
                    if self.postDatadict.count > 0{
                        return 1
                    }
                }
            }
            else{
                if section == 4 {
                    return self.commentArray.count
                }
                else{
                    if self.postDatadict.count > 0{
                        return 1
                    }
                }
            }
            return 0
        }
    }
    
    func renderDoctorProfile(_ tableView: UITableView, _ indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DoctorProfileCell", for: indexPath) as! DoctorProfileCell
        
        cell.coverView.dropShadow(scale: true)
        let userImg = self.disclaimerdict["profile_image"] as?  String
        if userImg != nil || (userImg?.count)! > 0 {
            let userImgurl = URL(string: userImg!)
            cell.doctorImage.kf.setImage(with: userImgurl)
        }
        else{
            // placeholder Image
        }
        
        let userName = self.disclaimerdict["doctor_name"] as?  String
        cell.nameLabel.text = userName
        
        let userExperience = self.disclaimerdict["doctor_exp"] as?  String
        cell.experienceLabel.text = userExperience
        
        let userDegree = self.disclaimerdict["doctor_degree"] as?  String
        let userProfile = self.disclaimerdict["doctor_profile"] as?  String
        
        cell.degreeLabel.text = userProfile! + userDegree!.htmlToString
        
        let contentSTR = self.disclaimerdict["disc_heading"] as?  String
        cell.contentLabel.text = contentSTR?.htmlToString
        
        cell.viewProfileButton.addTarget(self, action: #selector(OpenDoctorProfile), for: .touchUpInside)
        
        tableView.estimatedRowHeight = 300
        tableView.rowHeight = UITableViewAutomaticDimension
        
        return cell
    }
    
    func renderArticlePostComment(_ tableView: UITableView, _ indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArticlePostCommentCell", for: indexPath)
        self.commentTextView  = cell.contentView.viewWithTag(523) as! UITextView
        self.commentTextView.delegate = self
		self.commentTextView.isScrollEnabled = true
        self.countlabel = cell.contentView.viewWithTag(524) as! UILabel
        self.countlabel.text = "0/200"
        self.commentTextView.text = "Write your comment here ..."
        self.commentTextView.textColor = UIColor.gray
        
        let submitBtn = cell.contentView.viewWithTag(522) as! UIButton
        submitBtn.addTarget(self, action: #selector(SubmitCommentButtonPressed), for: .touchUpInside)
        
        tableView.estimatedRowHeight = 170
        tableView.rowHeight = 170
        
        return cell
    }
    
    func renderCommentCell(_ tableView: UITableView, _ indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath)
        let nameLabel = cell.contentView.viewWithTag(531) as! UILabel
        let commentLabel = cell.contentView.viewWithTag(532) as! UILabel
        
        let dict = commentArray[indexPath.row] as! Dictionary<String,Any>
        nameLabel.text = dict["comment_author"] as? String
        commentLabel.text = dict["comment"] as? String
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "articleDetailTopCell", for: indexPath) as! articleDetailTopCell
			
			if self.postDatadict != nil {
				let poster = self.postDatadict["image"] as?  String
				
				if poster != nil || (poster?.count)! > 0 {
					let posterurl = URL(string: poster!)
					cell.posterImage.kf.setImage(with: posterurl)
					//.image = img
					postImage = cell.posterImage.image ?? #imageLiteral(resourceName: "userPlaceholderProfile")
				}
				else{
					// placeholder Image
				}
				
				let userImg = self.postDatadict["author_image"] as?  String
				if userImg != nil || (userImg?.count)! > 0 {
					let userImgurl = URL(string: userImg!)
					cell.userImage.kf.setImage(with: userImgurl)
					//.image = img
				}
				else{
					// placeholder Image
				}
				
			
				cell.titleLabel.text = self.postDatadict["author_name"] as?  String
				cell.dateLabel.text = self.postDatadict["date"] as?  String
				cell.userName.text = self.postDatadict["title"] as?  String

			}
            
            tableView.estimatedRowHeight = 225
            tableView.rowHeight = UITableViewAutomaticDimension
            return cell
        }
        else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "shareButoonsCell", for: indexPath) as? ShareButoonsTableViewCell
            
            
            cell?.fbBtn.addTarget(self, action: #selector(FBButtonPressed), for: .touchUpInside)
            cell?.whatsappBtn.addTarget(self, action: #selector(WhatsAppButtonPressed), for: .touchUpInside)
            cell?.gmailBtn.addTarget(self, action: #selector(GmailButtonPressed), for: .touchUpInside)
            cell?.twitterBtn.addTarget(self, action: #selector(TwitterButtonPressed), for: .touchUpInside)
            cell?.linkedBtn.addTarget(self, action: #selector(linkedinButtonPressed), for: .touchUpInside)
            
            if self.commentArray.count > 0{
                cell?.commentLabel.text = "\(self.commentArray.count) Comments"
            }
            else if self.commentArray.count == 1{
                cell?.commentLabel.text = "1 Comment"
            }
            else{
                cell?.commentLabel.text = ""
            }
            
            tableView.estimatedRowHeight = 50
            tableView.rowHeight = 50
            
            return cell!
        }
        else if indexPath.section == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleDetailCellText", for: indexPath) as! ArticleDetailCellText
            let detailtextSectionLabel = cell.contentView.viewWithTag(511) as! UILabel
            
            var contentStr = self.postDatadict["content"] as! String
            let marginHeader = 15.0
            contentStr = contentStr.replacingOccurrences(of: "\n", with: "\n<br />")
            contentStr = contentStr.replacingOccurrences(of: "<ul>\r\n<br />", with: "<ul>\r\n")
            contentStr = contentStr.replacingOccurrences(of: "</li>\r\n<br />", with: "</li>\r\n")
            contentStr = contentStr.replacingOccurrences(of: "</h4>\r\n<br />", with: "</h4>\r\n")
            contentStr = contentStr.replacingOccurrences(of: "<h4>", with: "<h4><br />")
            
            let ratio = 1300 / (SCREEN_WIDTH - 20)
            let newHeight = 840 / ratio
            
            contentStr = contentStr.replacingOccurrences(of: "1300", with: "\(SCREEN_WIDTH - 20)")
            contentStr = contentStr.replacingOccurrences(of: "840", with: "\(newHeight)")
            contentStr = contentStr.replacingOccurrences(of: "width=\"560\" height=\"450\"", with: "width=\"\(SCREEN_WIDTH - 20)\" height=\"\(newHeight)\"")
            
            
            var descriptionAlign = "<style type='text/css'>html,body {margin: 0 0 \(marginHeader)px 0;padding: 0;width: 100%;height: 100%; font-family: \(FONTS.Quicksand_Regular); font-size: \(bodyFont)px;}html {display: table;}body {display: table-cell;vertical-align: middle;padding: 0px;text-align: justify;-webkit-text-size-adjust: none;} h2 {font-size: \(headerFont); color: #333; margin:  0 0 \(marginHeader)px 0;}h2 .head-yel {color: #f2b413;}h4 {font-size: \(headerFont)px;}p {margin: 0 0 \(marginHeader)px 0;} img { max-width:100%; height:auto; font-smoothing:antialiased; } </style>"
            
            descriptionAlign.append(contentStr)
            
            detailtextSectionLabel.attributedText = descriptionAlign.htmlToAttributedString
            let detailtextSectionTxtView = cell.contentView.viewWithTag(711) as! UITextView
            detailtextSectionTxtView.attributedText = descriptionAlign.htmlToAttributedString
            
            //webView.scalesPageToFit = true
            let detailtextSectionwebView = cell.contentView.viewWithTag(712) as! UIWebView
            descriptionAlign = descriptionAlign.replacingOccurrences(of: "\n", with: "<br>")
            descriptionAlign = descriptionAlign.replacingOccurrences(of: "\r<br><br />\r<br><br />", with: "\r<br><br />")
            
            detailtextSectionwebView.loadHTMLString(descriptionAlign, baseURL: nil)
            self.webViewData=detailtextSectionwebView
            self.webViewData?.backgroundColor=UIColor.white
            detailtextSectionwebView.backgroundColor=UIColor.white
            detailtextSectionwebView.isOpaque=false
            self.webViewData?.isOpaque=false
            self.webViewData?.delegate=self
            detailtextSectionwebView.delegate=self
            detailtextSectionwebView.scrollView.isScrollEnabled=false
            
            cell.webviewHeight.constant = self.webViewDataHeight
            tableView.estimatedRowHeight = 100
            tableView.rowHeight = UITableViewAutomaticDimension
            
            return cell
        }
        else  if isPostDisclaimerNeeded  {
            if indexPath.section == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleDetailDisclaimer", for: indexPath)
                let detailtextSectionLabel = cell.contentView.viewWithTag(511) as! UILabel
                detailtextSectionLabel.text = self.postDisclaimerStr
                return cell
            }
            if self.disclaimerdict.count > 0 {
                if indexPath.section == 4{
                    return renderDoctorProfile(tableView, indexPath)
                }
                else  if indexPath.section == 5{
                    return renderArticlePostComment(tableView, indexPath)
                }
                else {
                    return renderCommentCell(tableView, indexPath)
                }
            }else{
                if indexPath.section == 4{
                    return renderArticlePostComment(tableView, indexPath)
                }
                else{
                    return renderCommentCell(tableView, indexPath)
                    
                }
                
            }
        }else{
            
            if self.disclaimerdict.count > 0 {
                if indexPath.section == 3{
                    return renderDoctorProfile(tableView, indexPath)
                }
                else  if indexPath.section == 4{
                    return renderArticlePostComment(tableView, indexPath)
                }
                else {
                    return renderCommentCell(tableView, indexPath)
                }
            }else{
                if indexPath.section == 3{
                    return renderArticlePostComment(tableView, indexPath)
                }
                else{
                    return renderCommentCell(tableView, indexPath)
                    
                }
                
            }
        }
        
    }
    
    
}
class CustomButton: UIButton {
    
    var shadowLayer: CAShapeLayer!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if shadowLayer == nil {
            shadowLayer = CAShapeLayer()
            shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: 12).cgPath
            shadowLayer.fillColor = UIColor.white.cgColor
            
            shadowLayer.shadowColor = UIColor.darkGray.cgColor
            shadowLayer.shadowPath = shadowLayer.path
            shadowLayer.shadowOffset = CGSize(width: 2.0, height:5.0)
            shadowLayer.shadowOpacity = 1.0
            shadowLayer.shadowRadius = 2
            
            layer.insertSublayer(shadowLayer, at: 0)
        }
    }
    
}
