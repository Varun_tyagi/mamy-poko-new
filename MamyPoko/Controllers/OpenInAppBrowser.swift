//
//  OpenInAppBrowser.swift
//  MamyPoko
//
//  Created by Surbhi Varma on 04/03/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import Foundation
import WebKit


class OpenInAppBrowser: UIViewController, UIWebViewDelegate {

	@IBOutlet weak var headerLbl: UILabel!
	@IBOutlet weak var webView = UIWebView()
	@IBOutlet weak var heightHeaderLbl: NSLayoutConstraint!
	@IBOutlet weak var menuBtn: UIButton!

	var pageUrl = ""

	override func viewDidLoad() {
		super.viewDidLoad()
		self.hideKeyboardWhenTappedAround()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		
		setupView()
	}
	
	func setupView()  {
		
		heightHeaderLbl.constant =  0
		let url = URL(string: pageUrl)!
		self.webView?.loadRequest(URLRequest.init(url: url))
	}
	
	//MARK:- Action
	@IBAction func BackButtonPressed(){
		self.navigationController?.popViewController(animated: true)
	}
	
	
	@IBAction func searchAction(_ sender: Any) {
		let searchVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
		self.navigationController?.pushViewController(searchVc, animated: true)
	}
	
	
	func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
		return true
	}
	
}
