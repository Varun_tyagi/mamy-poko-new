//
//  EditProfileViewController.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 04/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import AppsFlyerLib

class EditProfileViewController: UIViewController {

    var  userData : UserDataModel?

    @IBOutlet weak var femaleBtn: UIButton!
    @IBOutlet weak var maleBtn: UIButton!

    @IBOutlet weak var userImg: RoundImageView!
    @IBOutlet weak var userNameTxt: RoundTextField!
    
    @IBOutlet weak var genderLbl: UILabel!
    @IBOutlet weak var cityTxt: RoundTextField!
    @IBOutlet weak var mobileTxt: RoundTextField!
    @IBOutlet weak var emailTxt: RoundTextField!
    @IBOutlet weak var pincodeTxt: RoundTextField!

    @IBOutlet weak var dobTxt: RoundTextField!

    var datePicker : UIDatePicker!
    var toolBar : UIToolbar!

    fileprivate var imagePicker = ImagePicker()

    //Mark: - view Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        Analytics.logEvent(AnalyticsEventViewItem, parameters: [
            AnalyticsParameterItemID: "id-Profile_Edit_Click",
            AnalyticsParameterItemName: "Profile_Edit_Click",
            AnalyticsParameterContentType: "Profile_Edit"
            ])
        AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.ProfileEditVisit,
                                             withValues: [
                                                AFEventParamContent: AppsFlyerConstant.ProfileEditVisit,
                                                AFEventParamContentId: "24"
            ])
        setupView()
        setupDatePicker()
        setupInitialData()
    }

    //MARK:- Action

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func uploadPhotoAction(_ sender: UIButton) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))

        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))

        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender
            alert.popoverPresentationController?.sourceRect = sender.bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }

        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func maleAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        femaleBtn.isSelected = sender.isSelected ? false : true
    }

    @IBAction func femaleAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        maleBtn.isSelected = sender.isSelected ? false : true
    }

    @IBAction func submitAction(_ sender: Any) {
        editProfileService()
    }
	
    @IBAction func searchAction(_ sender: UIButton) {
        let searchVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController

        self.navigationController?.pushViewController(searchVc, animated: true)
    }
    //MARK: - webservice
    func editProfileService(){
        if Helper.isConnectedToNetwork() {
            AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.ProfileEditSubmit,
                                                 withValues: [
                                                    AFEventParamContent: AppsFlyerConstant.ProfileEditSubmit,
                                                    AFEventParamContentId: "24"
                ])
            let urlstr = Constants.BASEURL + MethodName.UpdateProfile

            if self.CheckDataValidation(){

                Constants.appDel.startLoader()
                let gender = maleBtn.isSelected ? "M" : "F"

                let paramStr = "user_name=\(self.userNameTxt!.text!)&user_email=\(self.emailTxt!.text!)&user_phone=\(self.mobileTxt!.text!)&user_dob=\(self.dobTxt!.text!)&user_city=\(self.cityTxt!.text!)&pincode=\(self.pincodeTxt!.text!)&gender=\(gender)&device_type=iOS&device_id=\(AppDelegate.getUniqueDeviceIdentifierAsString())&device_token=\(AppDelegate.getDeviceToken())&user_id=\(UserDefaults.standard.value(forKey: Constants.USERID) ?? "0")"

                Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
                    let responseDict = response as Dictionary<String, Any>
                    Constants.appDel.stopLoader()

                if responseDict["status"] as? Int ==  1  || responseDict["status"] as? String == "1" {
                        DispatchQueue.main.async {
                            PKSAlertController.alert(Constants.appName, message: responseDict["message"] as! String, buttons: ["Ok"], tapBlock: { (alert, index) in
                                self.navigationController?.popViewController(animated: true)

                            })
                        }
                    }
                    else{
                        PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                    }
                }
            }

        }else{
            PKSAlertController.alertForNetwok()
        }
    }
    func uploadProfileImageService(_ userImage: UIImage){
        if Helper.isConnectedToNetwork() {

            let urlstr = Constants.BASEURL + MethodName.ProfileImgUpload

            if self.CheckDataValidation(){

                Constants.appDel.startLoader()

                let userId = "\(UserDefaults.standard.value(forKey: Constants.USERID) ?? "0")"

                let base64Str = "\(userImage.base64(format: ImageFormat.png) ?? "")"
                let parameters = [
                    "user_id": userId,
                    "image":base64Str
                ]
                Server.postImageUpload(urlstr, userImage ,parameters) { (responseDict) in

                    Constants.appDel.stopLoader()

                    if responseDict["status"] as? Int == 1  || responseDict["status"] as? String == "1" {
                        DispatchQueue.main.async {
                            PKSAlertController.alert(Constants.appName, message: responseDict["message"] as? String ?? "Profile Image Updated Successfully.", buttons: ["Ok"], tapBlock: { (alert, index) in
                                self.userImg.image = userImage

                            })
                        }
                    }
                    else{
                        PKSAlertController.alert("Error", message: (responseDict["message"] as? String) ?? "Unable to upload image")
                    }
                }
            }

        }else{
            PKSAlertController.alertForNetwok()
        }
    }


    func CheckDataValidation()-> (Bool){

        let userName = "\(self.userNameTxt!.text!)"
        let userPhone = "\(String(describing: self.mobileTxt!.text!))"
        let userDOB = "\(String(describing: self.dobTxt!.text!))"
        let userPin = "\(String(describing: self.pincodeTxt!.text!))"

        if userName.isEmpty == true {
            PKSAlertController.alert(Constants.appName, message: "Enter your name !")
            return false
        }

        if self.emailTxt!.text!.isEmpty == true {
            PKSAlertController.alert(Constants.appName, message: "Enter email id !")
            return false
        }
        else if Helper.isValidEmail(self.emailTxt!.text!) == false {
            PKSAlertController.alert(Constants.appName, message: "Enter valid email id !")
            return false
        }
        if userPhone.isEmpty == true {
            PKSAlertController.alert(Constants.appName, message: "Enter mobile no !")
            return false
        }
        else if !Helper.isValidPhoneNumber(phoneNumber: userPhone){
            PKSAlertController.alert(Constants.appName, message: "Enter valid mobile no !")
            return false
        }


        if userPin.isEmpty == true {
            PKSAlertController.alert(Constants.appName, message: "Enter pincode !")
            return false
        }
        else if !Helper.isValidPinCode(pinnumber: userPin){
            PKSAlertController.alert(Constants.appName, message: "Enter valid pincode !")
            return false
        }

        if userDOB.isEmpty == true {
            PKSAlertController.alert(Constants.appName, message: "Enter DOB !")
            return false
        }

        return true
    }

    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }

}

//MARK: - Custom Methods
extension EditProfileViewController{

    fileprivate func presentImagePicker(sourceType: UIImagePickerControllerSourceType) {
        imagePicker.controller.sourceType = sourceType
        DispatchQueue.main.async {
            self.present(self.imagePicker.controller, animated: true, completion: nil)
        }
    }
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            presentImagePicker(sourceType: .camera)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    func openGallary()
    {
        imagePicker.galleryAsscessRequest()
    }

    func setupView()  {

          Helper.setImageOntextField(self.userNameTxt!, imagename: "", mode: true, frame: CGRect(x:0, y:0, width:15,height: (self.userNameTxt!.frame.height)))
          Helper.setImageOntextField(self.emailTxt!, imagename: "", mode: true, frame: CGRect(x:0, y:0, width:15,height: (self.emailTxt!.frame.height)))
          Helper.setImageOntextField(self.mobileTxt!, imagename: "", mode: true, frame: CGRect(x:0, y:0, width:15,height: (self.mobileTxt!.frame.height)))
          Helper.setImageOntextField(self.pincodeTxt!, imagename: "", mode: true, frame: CGRect(x:0, y:0, width:15,height: (self.pincodeTxt!.frame.height)))
        Helper.setImageOntextField(self.dobTxt!, imagename: "", mode: true, frame: CGRect(x:0, y:0, width:15,height: (self.dobTxt!.frame.height)))
        Helper.setImageOntextField(self.cityTxt!, imagename: "", mode: true, frame: CGRect(x:0, y:0, width:15,height: (self.cityTxt!.frame.height)))

 imagePicker.delegate = self

    }
    func setupInitialData()  {
        userNameTxt.text = userData?.userName ?? ""
        emailTxt.text = userData?.userEmail  ?? ""
        mobileTxt.text = userData?.userPhone ?? ""
        cityTxt.text = userData?.userCity ?? ""
        pincodeTxt.text = userData?.pincode ?? ""
       maleBtn.isSelected =  (userData?.gender?.lowercased().contains("m"))! ? true : false
        femaleBtn.isSelected =  (userData?.gender?.lowercased().contains("f"))! ? true : false
        dobTxt.text = userData?.userDob ?? ""
        if !(userData?.image?.isEmpty)!  {
            Server.toGetImageFromURL(url:(userData?.image)!) { (userImage) in
                if userImage == nil {
                    self.userImg.image = #imageLiteral(resourceName: "userPlaceholderProfile")
                }else{
                    self.userImg.image = userImage
                }
            }
        }
    }
    func setupDatePicker()  {

        datePicker = UIDatePicker()
        datePicker.tag = 100
        datePicker.datePickerMode = UIDatePickerMode.date
        datePicker.addTarget(self, action: #selector(self.setDate(_sender:)), for: .valueChanged)
        datePicker.timeZone = TimeZone.current
        toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(dismissPicker))

        self.dobTxt.delegate = self
        self.dobTxt?.inputView = datePicker
        self.dobTxt?.inputAccessoryView = toolBar
       

    }

    //MARK:- Date Picker
    @objc func setDate(_sender : UIDatePicker){

        if _sender.tag == 100 {
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let selectedDate: String = dateFormatter.string(from: _sender.date)

            // SET MiNIMUM & MAximum DAte
            let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
            let currentDate: NSDate = NSDate()
            let components1: NSDateComponents = NSDateComponents()
            let components2: NSDateComponents = NSDateComponents()

            components1.year = -18
            let minDate : Date = gregorian.date(byAdding: components1 as DateComponents, to: currentDate as Date, options: NSCalendar.Options.init(rawValue: 0))!
            datePicker.minimumDate = minDate

            components2.year = -70
            let maxDate : Date = gregorian.date(byAdding: components2 as DateComponents, to: currentDate as Date, options: NSCalendar.Options.init(rawValue: 0))!
            datePicker.maximumDate = maxDate

            if _sender.date < minDate && _sender.date > maxDate {
                self.dobTxt?.text = "\(selectedDate)"
            }
            else{
                self.dobTxt?.text = ""
                datePicker.date = minDate
            }
        }

    }

    @objc func dismissPicker() {
        view.endEditing(true)
    }

    //MARK:- PINCODE AND CITY API
    func getCityForPinCode(pin : String){
        if Helper.isConnectedToNetwork() {
            Constants.appDel.startLoader()
            let url_str = Constants.BASEURL + MethodName.Pincode + "\(pin)"
            Server.getRequestWithURL(urlString: url_str, completionHandler: { (response) in
                Constants.appDel.stopLoader()
                if (response?.count)! > 1 && response?["status"] as! Int ==  1 {
                    let data = response?["data"] as! String
                    DispatchQueue.main.async {
                        self.cityTxt?.text = "\(data)"
                    }
                }else{
                    PKSAlertController.alert(Constants.appName, message: response?["message"] as! String)
                }
            })
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
}
extension EditProfileViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == mobileTxt {
            let currentCharacterCount = textField.text?.count ?? 0
            let set = NSCharacterSet(charactersIn: "0123456789").inverted
            if (range.length + range.location > currentCharacterCount){
                return false
            }


            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 10 && string.rangeOfCharacter(from: set) == nil
        }

        if textField == self.pincodeTxt {
            let currentCharacterCount = textField.text?.count ?? 0
            let set = NSCharacterSet(charactersIn: "0123456789").inverted
            if (range.length + range.location > currentCharacterCount){
                return false
            }

            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 6 && string.rangeOfCharacter(from: set) == nil
        }

        if textField == self.userNameTxt {
            let set = NSCharacterSet(charactersIn: "ABCDEFGHIJKLMONPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ").inverted
            return string.rangeOfCharacter(from: set) == nil
        }

        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == userNameTxt {
            pincodeTxt.becomeFirstResponder()
        }
        if textField == self.pincodeTxt{
            //HiT API
            if textField.text?.isEmpty == false{
                self.getCityForPinCode(pin : (self.pincodeTxt?.text)!)
            }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
}
extension EditProfileViewController: ImagePickerDelegate {

    func imagePickerDelegate(didSelect image: UIImage, imageName:String, delegatedForm: ImagePicker) {
        uploadProfileImageService(image)
        imagePicker.dismiss()
    }

    func imagePickerDelegate(didCancel delegatedForm: ImagePicker) {
        imagePicker.dismiss()
    }

    func imagePickerDelegate(canUseGallery accessIsAllowed: Bool, delegatedForm: ImagePicker) {
        if accessIsAllowed {
            presentImagePicker(sourceType: .photoLibrary)
        }
    }

    func imagePickerDelegate(canUseCamera accessIsAllowed: Bool, delegatedForm: ImagePicker) {
        if accessIsAllowed {
            // works only on real device (crash on simulator)
            presentImagePicker(sourceType: .camera)
        }
    }
}





