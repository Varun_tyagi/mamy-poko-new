//
//  ProfileTableDataBindingClass.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 04/08/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import Foundation

extension ProfileVC {
    
	//MARK: - Change PAssword
	
//    func BindChangePasswordCellData(_ cell: ChangePasswordTableViewCell) {
//        self.oldPasswordTxt = cell.oldPasswordTxt
//        self.confirmPasswordTxt = cell.confirMPAsswordTxt
//        self.newPasswordTxt = cell.newPasswordTxt
//        self.newPasswordTxt.delegate=self
//        self.oldPasswordTxt.delegate=self
//        self.confirmPasswordTxt.delegate=self
//        cell.changePasswordBtn.addTarget(self, action: #selector(changePAsswordAction(_:)), for: .touchUpInside)
//        cell.contentView.dropShadow()
//    }
	
    //MARK: - My intrest
    func BindMyInterstCellData(_ cell: MyIntrestTableViewCell) {
        self.intrestTagView =   cell.intrestTagView
		self.intrestTagView.delegate = self
		self.intrestTagView.dataSource = self
        cell.bindData(self.intrestArray)
        cell.saveIntrestBtn.addTarget(self, action: #selector(saveIntrestAction(_:)), for: .touchUpInside)
    }
    
    //MARK: - Pragnency status
    func BindPragnencyStatusCellData(_ cell: ManagePragStatusTableViewCell) {
        cell.bindData(userData)
        planningBtn = cell.planningBabyBtn
        pregnantBtn = cell.wifePragBtn
		planningViewHeightContraint = cell.bottomViewHeight
		
        if planningBtn.isSelected==true && pregnantBtn.isSelected==false{
            cell.planningBabyBtn.isSelected=true
            cell.wifePragBtn.isSelected=false
        }
		else   if planningBtn.isSelected==false && pregnantBtn.isSelected==true{
            cell.planningBabyBtn.isSelected=false
            cell.wifePragBtn.isSelected=true
        }

        cell.planningBabyBtn.addTarget(self, action: #selector(isPlanningPressed_PregnancyStatus(_:)), for: .touchUpInside)
        cell.wifePragBtn.addTarget(self, action: #selector(isPregnantPressed_PregnancyStatus(_:)), for: .touchUpInside)
		
		
		cell.bindData(userData)
		self.lastPeriodDateTxt = cell.lastPeriodDOBText
		self.expectedDeliveryDateTxt = cell.expectedDeliveryText
		self.firstDeliveryBtn = cell.isFirstPregnancyBtn
		cell.saveDetailBtn.addTarget(self, action: #selector(savePragnencyAction(_:)), for: .touchUpInside)
		cell.isFirstPregnancyBtn.addTarget(self, action: #selector(firstDeliveryAction(_:)), for: .touchUpInside)
		cell.lastPeriodDOBText.delegate=self
		cell.expectedDeliveryText.delegate=self
		self.lastPeriodDateTxt.delegate = self
		self.expectedDeliveryDateTxt.delegate = self

		
    }
	
    //MARK: - Pragnency Detail

     func BindPragnencyDetailCellData(_ cell: PragDetailTableViewCell) {
        cell.bindData(userData)
        self.lastPeriodDateTxt = cell.lastPeriodDateTxt
        self.expectedDeliveryDateTxt = cell.expectedDeliveryDateTxt
        self.firstDeliveryBtn = cell.firstDeliveryBtn
        cell.savePragDetailBtn.addTarget(self, action: #selector(savePragnencyAction(_:)), for: .touchUpInside)
        cell.firstDeliveryBtn.addTarget(self, action: #selector(firstDeliveryAction(_:)), for: .touchUpInside)
        cell.lastPeriodDateTxt.delegate=self
        cell.expectedDeliveryDateTxt.delegate=self
        self.lastPeriodDateTxt.delegate = self
        self.expectedDeliveryDateTxt.delegate = self
    }
	
    
    //MARK: - Baby Details
    func BindBabyDetailCellData(_ cell: MyBabyDetailTableViewCell) {
        cell.bindData(userData)
        babyGenderTxt =   cell.genderTxt
        babyNameTxt = cell.babyNameTxt
        babyGenderTxt.delegate=self
        
        self.babyDOBTxt = cell.dobTxt
        cell.dobTxt.delegate=self
        self.babyDOBTxt.delegate=self
        
        weightTxt=cell.weightTxt
        kgButton=cell.weightKgBtn
        lbsButton=cell.weightLblBtn
        
        circumferenceTxt=cell.headTxt
        cm_cButton=cell.headCMBtn
        inch_cButton=cell.headInBtn
        
        heightTxt=cell.heightTxt
        cmButton=cell.heightCMBtn
        inchButton=cell.heightInBtn
        
        
        kgButton?.isSelected = (self.babyDetails?.babyWeightUnit?.lowercased().contains("kg"))! ? true : false
        lbsButton?.isSelected = (self.babyDetails?.babyWeightUnit?.lowercased().contains("kg"))! ? false : true
        
        cm_cButton?.isSelected = (self.babyDetails?.headCircumfrenceUnit?.lowercased().contains("cm"))! ? true : false
        inch_cButton?.isSelected = (self.babyDetails?.headCircumfrenceUnit?.lowercased().contains("cm"))! ? false :  true
        
        cmButton?.isSelected = (self.babyDetails?.babyHeightUnit?.lowercased().contains("cm"))! ? true : false
        inchButton?.isSelected = (self.babyDetails?.babyHeightUnit?.lowercased().contains("cm"))! ? false : true
        
        cell.saveBtn.addTarget(self, action: #selector(SaveBabyDetailsPressed(_:)), for: .touchUpInside)
        
        cell.weightKgBtn.addTarget(self, action: #selector(KGButtonPressed(_:)), for: .touchUpInside)
        cell.weightLblBtn.addTarget(self, action: #selector(LBSButtonPressed(_:)), for: .touchUpInside)
        
        cell.headInBtn.addTarget(self, action: #selector(Inch_cButtonPressed(_:)), for: .touchUpInside)
        cell.headCMBtn.addTarget(self, action: #selector(CM_cButtonPressed(_:)), for: .touchUpInside)
        
        cell.heightInBtn.addTarget(self, action: #selector(InchButtonPressed(_:)), for: .touchUpInside)
        cell.heightCMBtn.addTarget(self, action: #selector(CMButtonPressed(_:)), for: .touchUpInside)
    }
    
}
