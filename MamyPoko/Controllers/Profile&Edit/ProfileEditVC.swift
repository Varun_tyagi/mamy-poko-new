//
//  ProfileEditVC.swift
//  MamyPoko
//
//  Created by Surbhi Varma on 15/05/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAnalytics
import AppsFlyerLib

class ProfileEditVC: UIViewController {
	
	var  userData : UserDataModel?
	
	@IBOutlet weak var back: UIButton!
	@IBOutlet weak var edit: UIButton!
	
	@IBOutlet weak var backImage: UIImageView!
	@IBOutlet weak var userImage: UIImageView!

	fileprivate var imagePicker = ImagePicker()

	//Mark: - view Life cycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		Analytics.logEvent(AnalyticsEventViewItem, parameters: [
			AnalyticsParameterItemID: "id-Profile_Image_Click",
			AnalyticsParameterItemName: "Profile_Image_Click",
			AnalyticsParameterContentType: "Profile_Image_Click"
			])
		AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.ProfileImageView,
											 withValues: [
												AFEventParamContent: AppsFlyerConstant.ProfileImageView,
												AFEventParamContentId: "40"
			])
		setupInitialData()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		
	}
	override func viewDidDisappear(_ animated: Bool) {
if #available(iOS 13.0, *) {
	let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
	statusBar.backgroundColor = UIColor.clear
	 UIApplication.shared.keyWindow?.addSubview(statusBar)
} else {
	 UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
 }
	}

	//MARK:- Action
	
	@IBAction func backAction(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func uploadPhotoAction(_ sender: UIButton) {
		let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
		alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
			self.openCamera()
		}))
		
		alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
			self.openGallary()
		}))
		
		alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
		switch UIDevice.current.userInterfaceIdiom {
		case .pad:
			alert.popoverPresentationController?.sourceView = sender
			alert.popoverPresentationController?.sourceRect = sender.bounds
			alert.popoverPresentationController?.permittedArrowDirections = .up
		default:
			break
		}
		
		self.present(alert, animated: true, completion: nil)
	}

	

	//MARK: - webservice
	
	func uploadProfileImageService(_ userImage: UIImage){
		if Helper.isConnectedToNetwork() {
			
			let urlstr = Constants.BASEURL + MethodName.ProfileImgUpload
			
				Constants.appDel.startLoader()
				
				let userId = "\(UserDefaults.standard.value(forKey: Constants.USERID) ?? "0")"
				
				let base64Str = "\(userImage.base64(format: ImageFormat.png) ?? "")"
				let parameters = [
					"user_id": userId,
					"image":base64Str
				]
				Server.postImageUpload(urlstr, userImage ,parameters) { (responseDict) in
					
					Constants.appDel.stopLoader()
					
					if responseDict["status"] as? Int == 1  || responseDict["status"] as? String == "1" {
						DispatchQueue.main.async {
							PKSAlertController.alert(Constants.appName, message: responseDict["message"] as! String, buttons: ["Ok"], tapBlock: { (alert, index) in
								self.userImage.image = userImage
								self.navigationController?.popViewController(animated: true)
							})
						}
						
					}
					else{
						PKSAlertController.alert("Error", message: (responseDict["message"] as? String) ?? "Unable to upload image")
					}
					
					
				}
			}
			
		else{
			PKSAlertController.alertForNetwok()
		}
	}
	
	override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }

}


//MARK: - Custom Methods
extension ProfileEditVC{
	
	fileprivate func presentImagePicker(sourceType: UIImagePickerControllerSourceType) {
		imagePicker.controller.sourceType = sourceType
		DispatchQueue.main.async {
			self.present(self.imagePicker.controller, animated: true, completion: nil)
		}
	}
	
	func openCamera()
	{
		if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
		{
			presentImagePicker(sourceType: .camera)
		}
		else
		{
			let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
			alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
			self.present(alert, animated: true, completion: nil)
		}
	}
	
	func openGallary()
	{
		imagePicker.galleryAsscessRequest()
	}
	
	func setupInitialData()  {
		imagePicker.delegate = self
		if !(userData?.image?.isEmpty)!  {
			Server.toGetImageFromURL(url:(userData?.image)!) { (userImage) in
				if userImage == nil {
					self.userImage.image = #imageLiteral(resourceName: "userPlaceholderProfile")
				}else{
					self.userImage.image = userImage
				}
			}
		}
		else{
			
		}
	}
	
	@objc func dismissPicker() {
		view.endEditing(true)
	}
	
}


extension ProfileEditVC: ImagePickerDelegate {
	
	func imagePickerDelegate(didSelect image: UIImage, imageName:String, delegatedForm: ImagePicker) {
		uploadProfileImageService(image)
		imagePicker.dismiss()
	}
	
	func imagePickerDelegate(didCancel delegatedForm: ImagePicker) {
		imagePicker.dismiss()
	}
	
	func imagePickerDelegate(canUseGallery accessIsAllowed: Bool, delegatedForm: ImagePicker) {
		if accessIsAllowed {
			presentImagePicker(sourceType: .photoLibrary)
		}
	}
	
	func imagePickerDelegate(canUseCamera accessIsAllowed: Bool, delegatedForm: ImagePicker) {
		if accessIsAllowed {
			// works only on real device (crash on simulator)
			presentImagePicker(sourceType: .camera)
		}
	}
}
