//
//  ProfileVC.swift
//  MamyPoko
//
//  Created by Surbhi Varma on 07/05/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import Foundation
import UIKit
import HTagView
import Kingfisher
import IQKeyboardManagerSwift
import FirebaseAnalytics
import AppsFlyerLib

class ProfileVC: UIViewController {
	@IBOutlet weak var mainProfileTbl: UITableView!
	
	//Profile Table cell
	var profileCell : MyProfileTableViewCell!
	
	//Pragnency status
	var planningBtn, pregnantBtn: UIButton!
	var planningViewHeightContraint : NSLayoutConstraint!
	var genderPicker : UIPickerView!
	
	// My interest
	var intrestTagView: HTagView!
	
	//Pragnency Details
	var firstDeliveryBtn: UIButton!
	var expectedDeliveryDateTxt, lastPeriodDateTxt: RoundTextField!
	
	// baby details
	var babyNameTxt, babyGenderTxt, babyDOBTxt: RoundTextField!
	var weightTxt, heightTxt, circumferenceTxt: RoundTextField!
	var lbsButton,kgButton, cmButton, inchButton, cm_cButton,inch_cButton: UIButton!
	
	//View Selection buttons
	@IBOutlet weak var profileButton : UIButton!
	@IBOutlet weak var interestButton : UIButton!
	@IBOutlet weak var detailButton: UIButton!
	@IBOutlet weak var detailLabel: UILabel!
	@IBOutlet weak var detailImageview: UIImageView!
	@IBOutlet weak var actionView: UIView!
	@IBOutlet weak var profileImageButton : RoundButton!
	var selectedButton : Int = 0
	
	//View ActionView variables
	@IBOutlet weak var profileLabel : UILabel!
	@IBOutlet weak var interestLabel : UILabel!
	@IBOutlet weak var profileImageview: UIImageView!
	@IBOutlet weak var interestImageview: UIImageView!
	
	//StackView
	@IBOutlet weak var actionStackview: UIStackView!
	@IBOutlet weak var profileView: UIView!
	@IBOutlet weak var interestView: UIView!
	@IBOutlet weak var detailView: UIView!

	//MARK:- Variable Declerations
	var userData : UserDataModel?
	var babyDetails: BabyDetails?
	var intrestArray:IntrestModel = []
	var parentalStatus  = ""
	var isHereChangeStatus : Bool!
	var offerregistrationViewShow : Bool!
	
	var datePicker:UIDatePicker = UIDatePicker()
	let toolBar = UIToolbar()
	var anotherAlert = UIAlertController()
	var flagBabyArrived = false
	
	
	//MARK:- View Methods
	override func viewDidLoad() {
		super.viewDidLoad()
		self.BlockSideMenuSwipe()
		
		let dict_profile = Helper.getDataFromNsDefault(key: Constants.PROFILE) as [String : Any]
		parentalStatus = dict_profile["parental_status"] as! String

        self.setProfileData()
		ManageHeader()
		
		if selectedButton == 0{
			self.profileButton.isSelected = true
			self.interestButton.isSelected = false
			self.detailButton.isSelected = false
		}
		else if selectedButton == 1{
			self.profileButton.isSelected = false
			self.interestButton.isSelected = true
			self.detailButton.isSelected = false
		}
		else if selectedButton == 2{
			self.profileButton.isSelected = false
			self.interestButton.isSelected = false
			self.detailButton.isSelected = true
		}
		
		actionView.dropShadow(cornerRadius: 10.0)
		self.ManageActionView()

		profileImageButton.cornerRadius = 20
		profileImageButton.borderColor = UIColor.clear
		
		IQKeyboardManager.shared.enableAutoToolbar=true
		IQKeyboardManager.shared.previousNextDisplayMode = IQPreviousNextDisplayMode.alwaysHide
	}
	
	override func viewDidDisappear(_ animated: Bool) {
		
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
    if #available(iOS 13.0, *) {
	let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
	statusBar.backgroundColor = UIColor.clear
	 UIApplication.shared.keyWindow?.addSubview(statusBar)
} else {
	 UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
 }
		getItemsFromAPI()
	}
	
//	override var preferredStatusBarStyle: UIStatusBarStyle {
//		return .lightContent
//	}
	
	//MARK:- IBAction
    @IBAction func searchAction(_ sender:UIButton){
        let searchVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(searchVc, animated: true)
    }
	@IBAction func backAction(_ sender: UIButton) {
//        let homeVc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
//        var navArr = self.navigationController?.viewControllers
//        if (navArr?.contains(homeVc))!{
//            self.navigationController?.popViewController(animated: true)
//        }
//        else{
//            navArr?.append(homeVc)
//            self.navigationController?.viewControllers = navArr!
//            self.navigationController?.popToViewController(homeVc, animated: true)
//        }
        self.navigationController?.popViewController(animated: true)
	}
	
	
	@IBAction func MyInterestSelected(_ sender: UIButton) {
		self.interestButton.isSelected = true
		self.detailButton.isSelected = false
		self.profileButton.isSelected = false
		self.ManageActionView()
	}
	
	@IBAction func PregnancyDetailsSelected(_ sender: UIButton) {
		self.detailButton.isSelected = true
		//if parentalStatus != "planning-baby"{
			self.interestButton.isSelected = false
	//	}
		self.profileButton.isSelected = false
		self.ManageActionView()
	}
	
	@IBAction func ProfileButtonAction(_ sender: UIButton) {
		self.profileButton.isSelected = true
	//	if parentalStatus != "planning-baby"{
			self.interestButton.isSelected = false
		//}
		self.detailButton.isSelected = false
		self.ManageActionView()
	}
	
	@IBAction func ProfileImageButtonPressed(_ sender: UIButton) {
		
		let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ProfileEditVC") as! ProfileEditVC
		destViewController.userData = userData
		self.navigationController?.pushViewController(destViewController, animated: true)
	}
	
	
	@IBAction func firstDeliveryAction(_ sender: UIButton) {
		sender.isSelected = !sender.isSelected
	}
	
	@IBAction func savePragnencyAction(_ sender: UIButton) {
		self.view.endEditing(true)
		if  intrestTagView?.selectedIndices.count == 0 {
			PKSAlertController.alert(Constants.appName, message: "Please select your interest first")
		}
		else{
			savePragnencyDetails()
		}
	}
	
	@IBAction func isPregnantPressed_PregnancyStatus(_ sender: UIButton) {
		self.pregnantBtn.isSelected = true
		self.planningBtn.isSelected = false
		manageParentStatusView()
	}
	
	@IBAction func isPlanningPressed_PregnancyStatus(_ sender: UIButton) {
		self.pregnantBtn.isSelected = false
		self.planningBtn.isSelected = true
		manageParentStatusView()
	}
	
	func manageParentStatusView(){
		if self.isHereChangeStatus == false {
			// remove baby details && add Pragnency details according to gender
			if (pregnantBtn != nil) {
				if pregnantBtn.isSelected{
					planningViewHeightContraint.constant = 320
				}
				else{
					planningViewHeightContraint.constant = 0
					planningBtn.isSelected=true
				}
			}
		}
		else if self.isHereChangeStatus == true{
			// remove baby details && add Pragnency details according to gender
			pregnantBtn?.isSelected = true
			if (pregnantBtn != nil) {
				if pregnantBtn.isSelected{
					planningViewHeightContraint.constant = 320
					planningBtn.isSelected = false
					
				}
				else{
					planningViewHeightContraint.constant = 0
					planningBtn.isSelected=true
				}
			}
			self.isHereChangeStatus = false
		}
		mainProfileTbl.reloadData()
	}
	

	
	//MARK: Profile Detail Functions
	//MARK:- PINCODE AND CITY API
	func getCityForPinCode(pin : String, textfield : UITextField){
		if Helper.isConnectedToNetwork() {
			Constants.appDel.startLoader()
			let url_str = Constants.BASEURL + MethodName.Pincode + "\(pin)"
			Server.getRequestWithURL(urlString: url_str, completionHandler: { (response) in
				Constants.appDel.stopLoader()
				if (response?.count)! > 1 && response?["status"] as! Int ==  1 {
					let data = response?["data"] as! String
					DispatchQueue.main.async {
						textfield.text =  "\(data)"
						self.UpdateProfileData()
					}
				}else{
					PKSAlertController.alert(Constants.appName, message: response?["message"] as! String)
				}
			})
		}else{
			PKSAlertController.alertForNetwok()
		}
	}
	
	//MARK:- Profile Update
	func UpdateProfileData(){
		let profiletableCell = self.mainProfileTbl.cellForRow(at: IndexPath.init(row: 0, section: 0)) as! MyProfileTableViewCell

		let name = profiletableCell.firstNameText.text ?? ""
		let email = profiletableCell.emailText.text ?? ""
		let mobile = profiletableCell.mobileText.text ?? ""
		let city = profiletableCell.cityText.text ?? ""
		let pin = profiletableCell.pincodeText.text ?? ""
		let gender = profiletableCell.genderText.text ?? ""
		let dob = profiletableCell.dobText.text ?? ""

		if Helper.isConnectedToNetwork() {
			AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.ProfileEditSubmit,
												 withValues: [
													AFEventParamContent: AppsFlyerConstant.ProfileEditSubmit,
													AFEventParamContentId: "24"
				])
			let urlstr = Constants.BASEURL + MethodName.UpdateProfile
			
			
			Constants.appDel.startLoader()
			let gender = gender == "Male" ? "M" : "F"
			
			let paramStr = "user_name=\(name)&user_email=\(email)&user_phone=\(mobile)&user_city=\(city)&pincode=\(pin)&gender=\(gender)&user_dob=\(dob)&device_type=iOS&device_id=\(AppDelegate.getUniqueDeviceIdentifierAsString())&device_token=\(AppDelegate.getDeviceToken())&user_id=\(UserDefaults.standard.value(forKey: Constants.USERID) ?? "0")"
			
			Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
				let responseDict = response as Dictionary<String, Any>
				Constants.appDel.stopLoader()
			
				
				if responseDict["status"] as? Int ==  1  || responseDict["status"] as? String == "1" {
					DispatchQueue.main.async {
						self.callApiGetProfile()
						PKSAlertController.alert(Constants.appName, message:  responseDict["message"] as? String ?? KSomeErrorTryAgain)
					}
				}
				else{
					PKSAlertController.alert(Constants.appName, message: responseDict["message"] as? String ?? KSomeErrorTryAgain)
				}
			}
		}
		else{
			PKSAlertController.alertForNetwok()
		}
	}
	
	//MARK:- Gender Picker
	func setupPicker(textfield : UITextField){
		genderPicker = UIPickerView()
		genderPicker.tag = 1
		genderPicker.delegate = self
		genderPicker.dataSource = self
		textfield.inputView = genderPicker
	}
	
	//MARK:- Date Picker
	func setupDatePicker(textfield : UITextField)  {
		
		datePicker = UIDatePicker()
		datePicker.tag = 100
		datePicker.datePickerMode = UIDatePickerMode.date
		datePicker.addTarget(self, action: #selector(self.setDate(_sender:)), for: .valueChanged)
		datePicker.timeZone = TimeZone.current
		let toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(dismissPicker))
		
		textfield.delegate = self
		textfield.inputView = datePicker
		textfield.inputAccessoryView = toolBar
	}
	
	@objc func setDate(_sender : UIDatePicker){
		
		if _sender.tag == 100 {
			let dateFormatter: DateFormatter = DateFormatter()
			dateFormatter.dateFormat = "dd-MM-yyyy"
			let selectedDate: String = dateFormatter.string(from: _sender.date)
			
			// SET MiNIMUM & MAximum DAte
			let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
			let currentDate: NSDate = NSDate()
			let components1: NSDateComponents = NSDateComponents()
			let components2: NSDateComponents = NSDateComponents()
			
			components1.year = -18
			let minDate : Date = gregorian.date(byAdding: components1 as DateComponents, to: currentDate as Date, options: NSCalendar.Options.init(rawValue: 0))!
			datePicker.minimumDate = minDate
			
			components2.year = -70
			let maxDate : Date = gregorian.date(byAdding: components2 as DateComponents, to: currentDate as Date, options: NSCalendar.Options.init(rawValue: 0))!
			datePicker.maximumDate = maxDate
			
//			datePicker.date = minDate
			
			let profiletableCell = self.mainProfileTbl.cellForRow(at: IndexPath.init(row: 0, section: 0)) as! MyProfileTableViewCell
			
			if _sender.date < minDate && _sender.date > maxDate {
				profiletableCell.dobText?.text = "\(selectedDate)"
			}
			else{
				profiletableCell.dobText?.text = ""//"\(minDate)"//userData?.userDob ?? ""
			}
		}
		
	}
	
	@objc func dismissPicker() {
		view.endEditing(true)
	}
	
	//MARK:- Interest Action
	@IBAction func saveIntrestAction(_ sender: UIButton) {
		
		if  intrestTagView?.selectedIndices.count == 0 {
			PKSAlertController.alert(Constants.appName, message: "Please select atleast one interest")
		}
		else{
			self.registerInterests() // chaeck ProfileApiHelper.Swift for method defination
		}
		self.view.endEditing(true)
	}
	
	
	//MARK
	//MARK:- BABY DETAILS FUNCTIONS
	@IBAction func KGButtonPressed(_ sender: UIButton) {
		kgButton.isSelected = true
		lbsButton.isSelected = false
		self.babyDetails?.babyWeightUnit = "kg."
	}
	@IBAction func LBSButtonPressed(_ sender: UIButton) {
		kgButton.isSelected = false
		lbsButton.isSelected = true
		self.babyDetails?.babyWeightUnit = "lb."
	}
	@IBAction func CMButtonPressed(_ sender: UIButton) {
		cmButton.isSelected = true
		inchButton.isSelected = false
		self.babyDetails?.babyHeightUnit = "cm."
	}
	@IBAction func InchButtonPressed(_ sender: UIButton) {
		cmButton.isSelected = false
		inchButton.isSelected = true
		self.babyDetails?.babyHeightUnit = "in."
	}
	@IBAction func CM_cButtonPressed(_ sender: UIButton) {
		cm_cButton.isSelected = true
		inch_cButton.isSelected = false
		self.babyDetails?.headCircumfrenceUnit = "cm."
	}
	@IBAction func Inch_cButtonPressed(_ sender: UIButton) {
		cm_cButton.isSelected = false
		inch_cButton.isSelected = true
		self.babyDetails?.headCircumfrenceUnit = "in."
	}
	@IBAction func SaveBabyDetailsPressed(_ sender: UIButton) {
		self.view.endEditing(true)
		self.userData?.babyDetails = self.babyDetails
		registerForBabyDetails()
	}
	
	//MARK
	//MARK:- Manage TableView Display
	func ManageHeader() {
//		if parentalStatus == "planning-baby" {
//			self.interestView.isHidden=true
//		}	else{
//			self.interestView.isHidden=false
//		}
		ManageActionView()
	}
	
	func ManageActionView(){
	
		if self.profileButton.isSelected {
			profileLabel.text = "My Profile"
			profileImageview.image = UIImage.init(named: "profileSelected")
			InterestViewSelectionSetting()
			
			if  parentalStatus == "mother"{
				self.detailImageview.image = UIImage.init(named: "babyUnselected")
				detailLabel.text = "Baby's Detail"
			}
			else if parentalStatus == "planning-baby"{
				self.detailImageview.image =   #imageLiteral(resourceName: "babyUnselected") // UIImage.init(named: "pregnancyUnselected")
				detailLabel.text = "Planning Baby"
			}
			else{
				self.detailImageview.image = UIImage.init(named: "pregnancyUnselected")
				detailLabel.text = "Pregnancy Details"
			}
		}
		else if self.interestButton.isSelected { // parentalStatus != "planning-baby" &&
			profileLabel.text = "My Profile"
			profileImageview.image = UIImage.init(named: "profileUnselected")
			interestLabel.text = "My Interests"
			interestImageview.image = UIImage.init(named: "interestSelected")
			
			if  parentalStatus == "mother"{
				self.detailImageview.image = UIImage.init(named: "babyUnselected")
				detailLabel.text = "Baby's Detail"
			}
			else if parentalStatus == "planning-baby"{
				self.detailImageview.image =  #imageLiteral(resourceName: "babyUnselected") //UIImage.init(named: "pregnancyUnselected")
				detailLabel.text = "Planning Baby"
			}
			else{
				self.detailImageview.image = UIImage.init(named: "pregnancyUnselected")
				detailLabel.text = "Pregnancy Details"
			}
		}
		else if self.detailButton.isSelected {
			
			profileLabel.text = "My Profile"
			profileImageview.image = UIImage.init(named: "profileUnselected")

			InterestViewSelectionSetting()
			if  parentalStatus == "mother"{
				self.detailImageview.image = UIImage.init(named: "babySelected")
				detailLabel.text = "Baby's Detail"
			}
			else if parentalStatus == "planning-baby"{
				self.detailImageview.image = #imageLiteral(resourceName: "babySelected")//UIImage.init(named: "pregnancySelected")
				detailLabel.text = "Planning Baby"
			}
			else{
				self.detailImageview.image = UIImage.init(named: "pregnancySelected")
				detailLabel.text = "Pregnancy Details"
			}
		}
	
		self.mainProfileTbl.reloadData()
	}
	
	
	//MARK:- DetailButtonSelectedSetting{
	func InterestViewSelectionSetting(){
		
		//if parentalStatus != "planning-baby" {
			
			interestLabel.text = "My Interests"
			interestImageview.image = UIImage.init(named: "interestUnselected")
		//}
	}
	
	
	func setProfileData()  {

		Helper.GetProfileImageForButton(butName: self.profileImageButton)
      //  self.offerregistrationViewShow = Bool.init(exactly: NSNumber.init(value: (userData?.offerSubscribed)!))
        self.offerregistrationViewShow = userData?.offerSubscribed == 1  ? true : false


		if  parentalStatus == "mother"{
			self.detailLabel.text = "Baby's Detail"
			self.detailImageview.image = UIImage.init(named: "babyUnselected")
		}
		else if parentalStatus == "planning-baby"{
			self.detailLabel.text = "Planning Baby"
			self.detailImageview.image =   #imageLiteral(resourceName: "babySelected")//UIImage.init(named: "pregnancyUnselected")
		}
		else{
			self.detailLabel.text = "Pregnancy Details"
			self.detailImageview.image = UIImage.init(named: "pregnancyUnselected")
		}
		self.ManageActionView()
	}
	
}

//MARK
//MARK: PROFILE TABLE DATASOURCE
// go check ProfileTableDataBindingClass.swift

extension ProfileVC : UITableViewDataSource{
	func numberOfSections(in tableView: UITableView) -> Int {
		if self.profileButton.isSelected {
			return 2
		}
		else if   self.interestButton.isSelected{ //  parentalStatus != "planning-baby" &&
			return 1
		}
		return 1
		
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
			return 1
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

		if self.profileButton.isSelected {
			if indexPath.section == 0 {
				// Profile Cell
				let cell  = tableView.dequeueReusableCell(withIdentifier: "MyProfileTableViewCell") as! MyProfileTableViewCell

				cell.firstNameText.delegate = self
				cell.emailText.delegate = self
				cell.mobileText.delegate = self
				cell.cityText.delegate = self
				cell.pincodeText.delegate = self
				cell.genderText.delegate = self
				cell.dobText.delegate = self

				cell.firstNameText.text = userData?.name ?? ""
				cell.emailText.text = userData?.userEmail  ?? ""
				cell.mobileText.text = userData?.userPhone  ?? ""
				cell.cityText.text = userData?.userCity  ?? ""
				cell.pincodeText.text = userData?.pincode  ?? ""
				cell.genderText.text = userData?.gender  ?? ""
				cell.dobText.text = userData?.userDob ?? ""
				
				self.setupPicker(textfield: cell.genderText)
				self.setupDatePicker(textfield: cell.dobText)
				
				cell.bindData(userData, false)
				return cell
			}
//			else if indexPath.section == 1{
//				// change password cell
//				let cell  = tableView.dequeueReusableCell(withIdentifier: "ChangePasswordCell1")
//				return cell!
//			}
			else {
				// Offer subscription cell
				let cell  = tableView.dequeueReusableCell(withIdentifier: "offerRegistrationCell")
				let butn = cell!.contentView.viewWithTag(2101) as? UIButton
				butn?.addTarget(self, action: #selector(RegisterForOffersAPI), for: UIControlEvents.touchUpInside)
				if offerregistrationViewShow == true{
					butn?.isSelected = true
					butn?.isUserInteractionEnabled = false
				}
				else {
					butn?.isSelected = false
					butn?.isUserInteractionEnabled = true
				}
				return cell!
			}
		}
		else if self.interestButton.isSelected{ //  parentalStatus != "planning-baby"  &&
			//Interest Cell
			let cell  = tableView.dequeueReusableCell(withIdentifier: "MyIntrestTableViewCell") as! MyIntrestTableViewCell
			BindMyInterstCellData(cell)
			cell.saveIntrestBtn.addTarget(self, action: #selector(saveIntrestAction(_:)), for: .touchUpInside)
			return cell

		}
		else {
			// Baby Detail Cell if status is Mother
			//Pregnancy detail cell is status is pregnant

			if  parentalStatus == "mother"{
				let cell  =  tableView.dequeueReusableCell(withIdentifier: "MyBabyDetailTableViewCell") as! MyBabyDetailTableViewCell
				BindBabyDetailCellData(cell)
				return cell
			}
			else if parentalStatus == "planning-baby"{
				let cell  = tableView.dequeueReusableCell(withIdentifier: "ManagePragStatusTableViewCell") as! ManagePragStatusTableViewCell
				BindPragnencyStatusCellData(cell)
				
				if self.isHereChangeStatus == false{
					if (pregnantBtn != nil) {
						if pregnantBtn.isSelected{
							cell.bottomViewHeight.constant = 320
						}
						else{
							planningBtn.isSelected=true
							cell.bottomViewHeight.constant = 0
						}
					}
				}
				else{
					pregnantBtn?.isSelected = true
					if (pregnantBtn != nil) {
						if pregnantBtn.isSelected{
							planningBtn.isSelected = false
							cell.bottomViewHeight.constant = 320
						}else{
							planningBtn.isSelected=true
							cell.bottomViewHeight.constant = 0
						}
					}
					self.isHereChangeStatus = false
				}
				
				return cell
			}
			else if parentalStatus == "pregnancy"{ // pregnant
				let cell  = tableView.dequeueReusableCell(withIdentifier: "PragDetailTableViewCell") as! PragDetailTableViewCell
				BindPragnencyDetailCellData(cell)
				return cell
			}
			else
			{
				let cell  = tableView.dequeueReusableCell(withIdentifier: "ManagePragStatusTableViewCell") as! ManagePragStatusTableViewCell
				return cell
			}
		}
	}
}


extension ProfileVC : UITableViewDelegate{
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

//		if self.profileButton.isSelected {
//			if indexPath.section == 1{
//				// change password cell
//				let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "PasswordVD") as! PasswordVD
//				self.navigationController?.pushViewController(destViewController, animated: true)
//			}
//		}
////		else if self.detailButton.isSelected{
////			//Interest Cell
////		}
//		else {
//			//Baby Detail Cell if status is Mother
//			//Pregnancy detail cell is status is pregnant
//		}
	}
}



//MARK: - Intrest View Data source
extension ProfileVC:HTagViewDataSource{
	func numberOfTags(_ tagView: HTagView) -> Int {
		return intrestArray.count
	}
	func tagView(_ tagView: HTagView, titleOfTagAtIndex index: Int) -> String {
		return intrestArray[index].name!
	}
	func tagView(_ tagView: HTagView, tagTypeAtIndex index: Int) -> HTagType {
		return .select
	}
	func tagView(_ tagView: HTagView, tagWidthAtIndex index: Int) -> CGFloat {
		return .HTagAutoWidth
	}
}
//MARK: - Intrest View  Delegate
extension ProfileVC:HTagViewDelegate{
	func tagView(_ tagView: HTagView, tagSelectionDidChange selectedIndices: [Int]) {
		
	}
	
	func tagView(_ tagView: HTagView, didCancelTagAtIndex index: Int) {
		tagView.reloadData()
	}
}

//MARK: - TEXTFIELD DELEGATE

extension ProfileVC : UITextFieldDelegate {
	
	func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
		if textField == babyGenderTxt {
			babyNameTxt.resignFirstResponder()
			self.view.endEditing(true)
			CustomPicker.show(data: [["Male", "Female"]]) {  [weak self] (selections: [Int : String]) -> Void in
				if let name = selections[0] {
					self?.babyGenderTxt.text = name
				}
			}
			return false
		}
		return true
	}
	
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		
		if self.profileButton.isSelected {
			let profiletableCell = self.mainProfileTbl.cellForRow(at: IndexPath.init(row: 0, section: 0)) as! MyProfileTableViewCell
			if textField == profiletableCell.mobileText {
				let currentCharacterCount = textField.text?.count ?? 0
				let set = NSCharacterSet(charactersIn: "0123456789").inverted
				if (range.length + range.location > currentCharacterCount){
					return false
				}
				
				
				let newLength = currentCharacterCount + string.count - range.length
				return newLength <= 10 && string.rangeOfCharacter(from: set) == nil
			}
			if textField == profiletableCell.pincodeText{
				let currentCharacterCount = textField.text?.count ?? 0
				let set = NSCharacterSet(charactersIn: "0123456789").inverted
				if (range.length + range.location > currentCharacterCount){
					return false
				}
				let newLength = currentCharacterCount + string.count - range.length
				return newLength <= 6 && string.rangeOfCharacter(from: set) == nil

			}
			if textField == profiletableCell.firstNameText{
				let set = NSCharacterSet(charactersIn: "ABCDEFGHIJKLMONPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ").inverted
				return string.rangeOfCharacter(from: set) == nil
			}
			
			if textField == profiletableCell.dobText{
				let currentCharacterCount = textField.text?.count ?? 0
				if currentCharacterCount == 0 || textField.text == "" {
					profiletableCell.dobText.text = userData?.userDob ?? ""
					return false
					}
			}
		}

		
		return true
	}
	
	func textFieldDidEndEditing(_ textField: UITextField) {
		if self.profileButton.isSelected {
			let profiletableCell = self.mainProfileTbl.cellForRow(at: IndexPath.init(row: 0, section: 0)) as! MyProfileTableViewCell
			if textField == profiletableCell.firstNameText || textField == profiletableCell.emailText || textField == profiletableCell.mobileText || textField == profiletableCell.genderText {
				//hit api
				self.UpdateProfileData()
			}
			if textField == profiletableCell.pincodeText{
				if textField.text?.isEmpty == false{
					self.getCityForPinCode(pin : (profiletableCell.pincodeText?.text)!, textfield: profiletableCell.cityText!)
				}
			}
		}

	}
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		textField.endEditing(true)
		return true
	}
}


//MARK
//MARK:- PickerView Gender
extension ProfileVC : UIPickerViewDelegate,UIPickerViewDataSource{
	
	func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
		
		if row == 0 {
			return "Male"
		}
		if row == 1{
			return "Female"
		}
		return "Select Gender"
	}
	
	func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		
		return 2
	}
	
	func numberOfComponents(in pickerView: UIPickerView) -> Int {
		return 1
	}
	
	func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
		
		let profiletableCell = self.mainProfileTbl.cellForRow(at: IndexPath.init(row: 0, section: 0)) as! MyProfileTableViewCell

		if row == 0 {
			profiletableCell.genderText.text = "Male"
		}
		if row == 1{
			profiletableCell.genderText.text = "Female"
		}
	}
}
