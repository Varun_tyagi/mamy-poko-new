//
//  ProfileAPIHelper.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 04/08/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import Foundation

//MARK: - Webservice

extension ProfileVC {
    //MARK: - Post babay details
    func registerForBabyDetails(){
        if Helper.isConnectedToNetwork() {
            
            let urlstr = Constants.BASEURL + MethodName.RegisterStep2
            let lastDate =  ""
            let expectedDate = ""
            let isFirstDelivery =  "NO"
            
            Constants.appDel.startLoader()
            let userid = UserDefaults.standard.string(forKey: Constants.USERID)// UserDefaults.standard.value(forKey: Constants.USERID) as! String
            
            var gender = ""
            if !((babyGenderTxt.text?.isEmpty)!){
                gender =  (babyGenderTxt.text?.lowercased().contains("male"))! ? "M" : "F"
            }
            var babyWeight = ""
            if !(weightTxt.text?.isEmpty)!{
                babyWeight = weightTxt.text ?? ""
            }
            var babyH = ""
            if !(heightTxt.text?.isEmpty)!{
                babyH = heightTxt.text ?? ""
            }
            var babyName = ""
            if !(babyNameTxt.text?.isEmpty)!{
                babyName = babyNameTxt.text ?? ""
            }
            var babyDob = ""
            if !(babyDOBTxt.text?.isEmpty)!{
                babyDob = babyDOBTxt.text ?? ""
            }
            var babyHead = ""
            if !(circumferenceTxt.text?.isEmpty)!{
                babyHead = circumferenceTxt.text ?? ""
            }
            
            let paramStr = "user_id=\(userid!)&category=mother&last_date_of_period=\(lastDate)&delivery_date=\(expectedDate)&baby_name=\(babyName)&baby_gender=\(gender)&baby_dob=\(babyDob)&baby_weight=\(babyWeight)&baby_weight_unit=\(lbsButton.isSelected==true ? "lb." : "kg.")&baby_height=\(babyH)&baby_height_unit=\(cmButton.isSelected ? "cm." : "in.")&head_circumfrence=\(babyHead)&head_circumfrence_unit=\(cm_cButton.isSelected ? "cm." : "in.")&medical_history=&description=&first_delivery=\(isFirstDelivery)"
            
            Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
                
                DispatchQueue.main.async {
					Constants.appDel.stopLoader()
//                    self.callApiGetProfile()
                }
            }
            
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
    //MARK;- Register Intrest
    func registerInterests() {
        if Helper.isConnectedToNetwork() {
            Constants.appDel.startLoader()
            let urlstr = Constants.BASEURL + MethodName.RegisterStep3
            let userid = UserDefaults.standard.string(forKey: Constants.USERID)
            var selectedcategories :  [String] = []
            for tag in intrestTagView.selectedIndices {
                selectedcategories.append("\(intrestArray[tag].alias ?? "")")
            }
            
            let paramStr = "user_id=\(userid!)&area_of_interest=\(selectedcategories)"
            
            Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
                let responseDict = response as Dictionary<String, Any>
                Constants.appDel.stopLoader()
                
                if responseDict["status"] as! NSNumber == NSNumber.init(value: 1) {
                    
                    DispatchQueue.main.async {
//                        self.backAction(UIButton())
						PKSAlertController.alert("Success", message: "Interest has been updated")
					}
                    
                }
                else{
                    PKSAlertController.alert("Error", message: responseDict["message"] as? String ?? KSomeErrorTryAgain)
                }
            }
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
    
    //MARK: - Save pragnency Details
    func savePragnencyDetails(){
        if Helper.isConnectedToNetwork() {
        NotificationCenter.default.post(name: NSNotification.Name.init(HomePageDataRefreshNotification), object: nil)
            let urlstr = Constants.BASEURL + MethodName.RegisterStep2
            let lastDate = "\(self.lastPeriodDateTxt!.text!)"
            let expectedDate = "\(self.expectedDeliveryDateTxt!.text!)"
            let isFirstDelivery = firstDeliveryBtn.isSelected == true ? "YES" : "NO"
            
            if lastDate.isEmpty == true && expectedDate.isEmpty == true{
                PKSAlertController.alert(Constants.appName, message: "Please Select Last Date of Period or Expected Delivery Date.")
            }
            else{
                Constants.appDel.startLoader()
                let userid = UserDefaults.standard.string(forKey: Constants.USERID)
                
                let paramStr = "user_id=\(userid!)&category=pregnancy&last_date_of_period=\(lastDate)&delivery_date=\(expectedDate)&baby_name=&baby_gender=&baby_dob=&baby_weight=&baby_weight_unit=&baby_height=&baby_height_unit=&head_circumfrence=&head_circumfrence_unit=&medical_history=&description=&first_delivery=\(isFirstDelivery)"
                
                Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
                    let responseDict = response
                    Constants.appDel.stopLoader()
                    
                    if responseDict["status"] as? NSNumber == NSNumber.init(value: 3) || responseDict["status"] as? String == "3" {
                        DispatchQueue.main.async {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                    else{
                        PKSAlertController.alert(Constants.appName, message: responseDict["message"] as? String ?? KSomeErrorTryAgain)
                    }
                }
            }
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
    
    //MARK: - Get profile Data
    func callApiGetProfile()
    {
        if Helper.isConnectedToNetwork() {
          //  Constants.appDel.startLoader()
            let url_str = Constants.BASEURL + MethodName.UserProfile + "\(UserDefaults.standard.value(forKey: Constants.USERID) ?? "0")"
            Server.getRequestWithURL(urlString: url_str, completionHandler: { (response) in
                Constants.appDel.stopLoader()
                if !(response==nil){
                    if (response?.count)! > 1 && response?["status"] as! NSNumber == NSNumber.init(value: 1) || response?["status"] as? String == "1"{
						
                        
                        let data = response?["data"] as Any
                        var userDataDict = data as! [String:Any]
                        userDataDict["name"] =  userDataDict["user_name"]
						
                        
                        Helper.saveDataInNsDefault(object: userDataDict as AnyObject, key: Constants.PROFILE)
                        NotificationCenter.default.post(name: NSNotification.Name.init(rawValue:  Constants.KUpdateProfileNotification), object: nil)
                        DispatchQueue.main.async {
                            if let theJSONData = try? JSONSerialization.data(
                                withJSONObject: data,
                                options: []) {
                                let theJSONText = String(data: theJSONData,
                                                         encoding: .ascii)
                                self.userData = try? UserDataModel.init(theJSONText!)
                                self.babyDetails = self.userData?.babyDetails
                                self.parentalStatus = self.userData?.parentalStatus ?? "Pregnancy"
                                self.setProfileData()
                            }
                            
                        }
                        
                    }else{
                        PKSAlertController.alert(Constants.appName, message: response?["message"] as? String ?? KSomeErrorTryAgain)
                    }
                }else{
                    PKSAlertController.alert(Constants.appName, message: response?["message"] as? String ?? KSomeErrorTryAgain)
                }
                
            })
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
    //MARK: - Get my intrest data
    func getItemsFromAPI() {
        if Helper.isConnectedToNetwork() {
           // Constants.appDel.startLoader()
            let url_str = Constants.BASEURL + MethodName.GET_Categories + "/\(UserDefaults.standard.value(forKey: Constants.USERID) ?? "0")"
            Server.getRequestWithURL(urlString: url_str, completionHandler: { (response) in
                Constants.appDel.stopLoader()
                if response?["status"] as? NSNumber == NSNumber.init(value: 1)  || response?["status"] as? String == "1" {
                    let   data = response!["data"] as! [[String:Any]]
                    DispatchQueue.main.async {
                        if let theJSONData = try? JSONSerialization.data(
                            withJSONObject: data,
                            options: []) {
                            let theJSONText = String(data: theJSONData,  encoding: .ascii)
                            self.intrestArray  = try! IntrestModel.init(theJSONText!)
                        }
                    }
                }else{
                    
                    PKSAlertController.alert(Constants.appName, message: response?["message"] as? String ?? "Request Timed Out!")
                }
                DispatchQueue.main.async {
                    self.callApiGetProfile()
                }
            })
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
    //MARK:- Change PAssword
    func changePasswordService(){
//        if Helper.isConnectedToNetwork() {
//
//            let urlstr = Constants.BASEURL + MethodName.ChangePass
//
//            if self.CheckDataValidation(){
//
//                Constants.appDel.startLoader()
//
//                let paramStr = "old_password=\(self.oldPasswordTxt!.text!)&new_password=\(self.newPasswordTxt!.text!)&confirm_new_password=\(self.confirmPasswordTxt!.text!)&user_id=\(UserDefaults.standard.value(forKey: Constants.USERID) ?? "0")"
//
//                Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
//                    let responseDict = response as Dictionary<String, Any>
//                    Constants.appDel.stopLoader()
//
//                    if responseDict["status"] as? NSNumber == NSNumber.init(value: 1)  || responseDict["status"] as? String == "1" {
//                        DispatchQueue.main.async {
//                            if let msg = responseDict["message"] as? String {
//                                PKSAlertController.alert(Constants.appName , message: msg)
//                            }else{
//                                PKSAlertController.alert(Constants.appName , message: "Your password has been changed successfully.")
//
//                            }
//                            self.logoutGoToInitial()
//                        }
//                    }
//                    else{
//                        PKSAlertController.alert("Error", message: responseDict["message"] as! String)
//                    }
//                }
//            }
//
//        }else{
//            PKSAlertController.alertForNetwok()
//        }
    }
	
    func CheckDataValidation()-> (Bool){
        
//        let oldPassword = "\(String(describing: self.oldPasswordTxt!.text!))"
//        let password = "\(String(describing: self.newPasswordTxt!.text!))"
//        let confirmPassword = "\(String(describing: self.confirmPasswordTxt!.text!))"
//
//        if oldPassword.isEmpty == true {
//            PKSAlertController.alert(Constants.appName, message: "Enter old password !")
//            return false
//        }
//        else if self.oldPasswordTxt!.text!.count < 6 {
//            PKSAlertController.alert(Constants.appName, message: "Enter min 6 character password !")
//            return false
//        }
//
//        if password.isEmpty == true {
//            PKSAlertController.alert(Constants.appName, message: "Enter new password !")
//            return false
//        }
//        else if self.newPasswordTxt!.text!.count < 6 {
//            PKSAlertController.alert(Constants.appName, message: "Enter min 6 character password !")
//            return false
//        }
//
//        if confirmPassword.isEmpty == true {
//            PKSAlertController.alert(Constants.appName, message: "Enter confirm password !")
//            return false
//        }
//        else if confirmPassword != password {
//            PKSAlertController.alert(Constants.appName, message: "Enter same password !")
//            return false
//        }
		
        return true
    }

	
	//Register for Offers
	
	@objc func RegisterForOffersAPI(){
		if Helper.isConnectedToNetwork() {
			
			let urlstr = Constants.BASEURL + MethodName.OfferRegister
			Constants.appDel.startLoader()
				
			let paramStr = "user_id=\(UserDefaults.standard.value(forKey: Constants.USERID) ?? "0")"
			
			Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
				let responseDict = response as Dictionary<String, Any>
				Constants.appDel.stopLoader()
				
				if responseDict["status"] as? NSNumber == NSNumber.init(value: 1)  || responseDict["status"] as? String == "1" {
					DispatchQueue.main.async {
						if let msg = responseDict["message"] as? String {
							self.offerregistrationViewShow = true
							PKSAlertController.alert(Constants.appName , message: msg)
						}else{
							PKSAlertController.alert(Constants.appName , message: responseDict["message"] as? String ?? KSomeErrorTryAgain)
							
						}
						self.callApiGetProfile()
					}
				}
				else{
					PKSAlertController.alert("Error", message: responseDict["message"] as? String ?? KSomeErrorTryAgain)
				}
			}
			
		}else{
			PKSAlertController.alertForNetwok()
		}
	}

}
