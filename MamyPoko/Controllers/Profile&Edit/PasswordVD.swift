//
//  PasswordVD.swift
//  MamyPoko
//
//  Created by Surbhi Varma on 15/05/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import Foundation
import UIKit
import IQKeyboardManagerSwift
import FirebaseAnalytics
import AppsFlyerLib

class PasswordVD: UIViewController {
	@IBOutlet weak var oldPassText: UITextField!
	@IBOutlet weak var newPassText: UITextField!
	@IBOutlet weak var confirmPassText: UITextField!

	
	//MARK:- View Methods
	override func viewDidLoad() {
		super.viewDidLoad()
		self.BlockSideMenuSwipe()
		
		oldPassText.setLeftPaddingPoints()
		newPassText.setLeftPaddingPoints()
		confirmPassText.setLeftPaddingPoints()
		
		IQKeyboardManager.shared.enableAutoToolbar=true
		IQKeyboardManager.shared.previousNextDisplayMode = IQPreviousNextDisplayMode.alwaysHide
	}

	override func viewDidDisappear(_ animated: Bool) {
if #available(iOS 13.0, *) {
	let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
	statusBar.backgroundColor = UIColor.clear
	 UIApplication.shared.keyWindow?.addSubview(statusBar)
} else {
	 UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
 }
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		
	}
	
	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
	
	//MARK:- IBAction
	@IBAction func backAction(_ sender: UIButton) {
		self.navigationController?.popViewController(animated: true)
       
	}
	
	@IBAction func ChangePassword(_ sender: UIButton) {
		self.changePasswordService()
	}
	
	
	func CheckDataValidation()-> (Bool){
		
		let oldPassword = "\(String(describing: self.oldPassText!.text!))"
		let password = "\(String(describing: self.newPassText!.text!))"
		let confirmPassword = "\(String(describing: self.confirmPassText!.text!))"

		if oldPassword.isEmpty == true {
			PKSAlertController.alert(Constants.appName, message: "Enter old password !")
			return false
		}
		else if self.oldPassText!.text!.count < 6 {
			PKSAlertController.alert(Constants.appName, message: "Enter min 6 character password !")
			return false
		}

		if password.isEmpty == true {
			PKSAlertController.alert(Constants.appName, message: "Enter new password !")
			return false
		}
		else if self.newPassText!.text!.count < 6 {
			PKSAlertController.alert(Constants.appName, message: "Enter min 6 character password !")
			return false
		}

		if confirmPassword.isEmpty == true {
			PKSAlertController.alert(Constants.appName, message: "Enter confirm password !")
			return false
		}
		else if confirmPassword != password {
			PKSAlertController.alert(Constants.appName, message: "Enter same password !")
			return false
		}

		return true
	}
	
	func changePasswordService(){
		if Helper.isConnectedToNetwork() {

			let urlstr = Constants.BASEURL + MethodName.ChangePass

			if self.CheckDataValidation(){

				Constants.appDel.startLoader()

				let paramStr = "old_password=\(self.oldPassText!.text!)&new_password=\(self.newPassText!.text!)&confirm_new_password=\(self.confirmPassText!.text!)&user_id=\(UserDefaults.standard.value(forKey: Constants.USERID) ?? "0")"

				Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
					let responseDict = response as Dictionary<String, Any>
					Constants.appDel.stopLoader()

					if responseDict["status"] as? NSNumber == NSNumber.init(value: 1)  || responseDict["status"] as? String == "1" {
						DispatchQueue.main.async {
							if let msg = responseDict["message"] as? String {
								PKSAlertController.alert(Constants.appName , message: msg)
							}else{
								PKSAlertController.alert(Constants.appName , message: "Your password has been changed successfully.")

							}
							self.logoutGoToInitial()
						}
					}
					else{
						PKSAlertController.alert("Error", message: responseDict["message"] as! String)
					}
				}
			}

		}else{
			PKSAlertController.alertForNetwok()
		}
	}
	
	func logoutGoToInitial()  {
		let root = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
		let   navCtrl = UINavigationController.init(rootViewController: root)
		navCtrl.navigationBar.isHidden = true
		Constants.appDelegate.window?.rootViewController = navCtrl
		Helper.resetDefaults()
        UserDefaults.standard.set(0, forKey: Constants.RegisterStatus)

	}
}

extension PasswordVD : UITextFieldDelegate {
	func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
		return true
	}
	
	func textFieldDidEndEditing(_ textField: UITextField) {
		self.view.endEditing(true)
	}
	
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		
		if textField == self.newPassText || textField == self.oldPassText || textField == self.confirmPassText{
//			let set = NSCharacterSet(charactersIn: "ABCDEFGHIJKLMONPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789").inverted
			
			let currentCharacterCount = textField.text?.count ?? 0
			if (range.length + range.location > currentCharacterCount){
				return false
			}
			let newLength = currentCharacterCount + string.count - range.length
			return newLength <= 10 //&& string.rangeOfCharacter(from: set) == nil
		}
		
		return true
	}
}
