
//MARK: - NOTE NOTE NOTE Seprate class extension
//Check ProfileApiHelper.swift for webservices
//Check Profile ProfileTableDataBindingClass for table cell data binding

import UIKit
import HTagView
import Kingfisher
import IQKeyboardManagerSwift
import FirebaseAnalytics
import AppsFlyerLib

public enum ProfileSections:String{
    case MYProfile = "My Profile"
    case ChangePassword = "Change Password"
    case ManagePragnency = "Manage Pragnency"
    case MyIntrests  = "My Interests"
    case PragnencyDetails = "My Pregnancy Details"
    case MyBabysDetails = "My baby's Details"
    case WifePragnencyDetails = "My wife's Pregnancy Details"
    case ChangePArantalStatus = "Change Parental Status"
	case offerRegistration = "Offer Registration Status"

}


class ProfileViewController: UIViewController {
    //MARK:- IBOUTLETS
    @IBOutlet weak var mainProfileTbl: UITableView!
    @IBOutlet weak var userEmailLbl: UILabel!
    @IBOutlet weak var editProfileBtn: UIButton!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userImgView: RoundImageView!
    
    // change password
    var confirmPasswordTxt, newPasswordTxt, oldPasswordTxt: RoundTextField!
  
    //Pragnency status
    var planningBtn, pregnantBtn: UIButton!
  
    // My interest
    var intrestTagView: HTagView!
   
    //Pragnency Details
    var firstDeliveryBtn: UIButton!
    var expectedDeliveryDateTxt, lastPeriodDateTxt: RoundTextField!
   
    // baby details
    var babyNameTxt, babyGenderTxt, babyDOBTxt: RoundTextField!
    var weightTxt, heightTxt, circumferenceTxt: RoundTextField!
    var lbsButton,kgButton, cmButton, inchButton, cm_cButton,inch_cButton: UIButton!

    //MARK:- Variable Declerations
    var profileSectionArray = [ProfileSections.MYProfile,ProfileSections.ChangePassword,ProfileSections.ManagePragnency,ProfileSections.MyIntrests]
    var hidden:[Bool] = [true, true,true,true,true,true,true,true, true]
    var userData : UserDataModel?
    var babyDetails: BabyDetails?
    var intrestArray:IntrestModel = []
    var parentalStatus  = ""
    var selectedSection = ""
    var isHereChangeStatus : Bool!
	var offerregistrationViewShow : Bool!

    
    
    var datePicker:UIDatePicker = UIDatePicker()
    let toolBar = UIToolbar()
    var anotherAlert = UIAlertController()
    var flagBabyArrived = false

    //MARK:- View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // change password setting

        Analytics.logEvent(AnalyticsEventViewItem, parameters: [
            AnalyticsParameterItemID: "id-Profile_Page_Visit",
            AnalyticsParameterItemName: "Profile_Page_Visit",
            AnalyticsParameterContentType: "Profile"
            ])
        AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.ProfileVisit,
                                             withValues: [
                                                AFEventParamContent: AppsFlyerConstant.ProfileVisit,
                                                AFEventParamContentId: "23"
            ])
        manageChangePasswordAppearence()
         IQKeyboardManager.shared.enableAutoToolbar=true
         IQKeyboardManager.shared.previousNextDisplayMode = IQPreviousNextDisplayMode.alwaysHide
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//       getItemsFromAPI() // func declaration in profileapihelper.swift
    }
    
    func manageChangePasswordAppearence()  {
        guard let isSocialLogin  = UserDefaults.standard.value(forKey: Constants.UserDefaultsConstant.isSocialLogin) as? Int else { return }
        // 1 means login from social login else nrml login
        if isSocialLogin == 1 && profileSectionArray.contains(.ChangePassword) {
            profileSectionArray.remove(at: 1)
        }
    }
    
    //MARK:- Set Profile data
    fileprivate func manageParentStatusView() {
      
        //Pregnancy Status
        if  parentalStatus == "planning-baby" && self.isHereChangeStatus == false {
            // remove baby details && add Pragnency details according to gender
            profileSectionArray = [ProfileSections.MYProfile,ProfileSections.ChangePassword,ProfileSections.ManagePragnency]
            if (pregnantBtn != nil) {
            if pregnantBtn.isSelected{
               profileSectionArray = [ProfileSections.MYProfile,ProfileSections.ChangePassword,ProfileSections.ManagePragnency,ProfileSections.MyIntrests, (userData?.gender?.lowercased().contains("f"))! ?  .PragnencyDetails : .WifePragnencyDetails ]
            }else{
                planningBtn.isSelected=true
                }
            }
        }
        else if  parentalStatus == "planning-baby" && self.isHereChangeStatus == true{
            // remove baby details && add Pragnency details according to gender
            profileSectionArray = [ProfileSections.MYProfile,ProfileSections.ChangePassword,ProfileSections.ManagePragnency]
            pregnantBtn?.isSelected = true
            if (pregnantBtn != nil) {
                if pregnantBtn.isSelected{
                    profileSectionArray = [ProfileSections.MYProfile,ProfileSections.ChangePassword,ProfileSections.ManagePragnency,ProfileSections.MyIntrests, (userData?.gender?.lowercased().contains("f"))! ?  .PragnencyDetails : .WifePragnencyDetails ]
                    planningBtn.isSelected = false
                    self.hidden = [true,true,true,false,false]
                    mainProfileTbl.reloadData()
                    self.hidden = [true,true,true,true,false]
                    mainProfileTbl.reloadData()
                }else{
                    planningBtn.isSelected=true
                }

            }
            self.isHereChangeStatus = false
            
        }
        else if  parentalStatus == "mother"{
			self.offerregistrationViewShow = Bool.init(exactly: NSNumber.init(value: (userData?.offerSubscribed)!))

        profileSectionArray = [ProfileSections.MYProfile,ProfileSections.ChangePassword,ProfileSections.MyIntrests,ProfileSections.MyBabysDetails,ProfileSections.offerRegistration]
            
        }
        else{
            let gender = userData?.gender ?? "f"
        	profileSectionArray = [ProfileSections.MYProfile,ProfileSections.ChangePassword,ProfileSections.MyIntrests, (gender.lowercased().contains("f")) ?  .PragnencyDetails : .WifePragnencyDetails,ProfileSections.ChangePArantalStatus,ProfileSections.offerRegistration]
			
            if  (self.userData?.pregnancy?.week ?? 0) >= 36  && !profileSectionArray.contains(ProfileSections.ChangePArantalStatus){
            	profileSectionArray.append(ProfileSections.ChangePArantalStatus)
            }

        }
        if !profileSectionArray.contains(ProfileSections.ManagePragnency){
            self.hidden = [true,true,true,true,true,true,true,true,true]
        }
        manageChangePasswordAppearence()
        mainProfileTbl.reloadData()
    }
    //MARK:- Set Up If Remaining Days are 0
    @objc  func childhasArived(){
        DispatchQueue.main.async {
            let popvc =  Constants.mainStoryboard2.instantiateViewController(withIdentifier: "BabyArrivedPopViewController") as! BabyArrivedPopViewController
            self.flagBabyArrived=true
            popvc.didSaveBabyData = { [weak self](item) in
                if self != nil {
                    // hit service for baby not arrived with flag arrived = true delicvery type = 2
                    DispatchQueue.main.async {
                        popvc.removeChildVC()
                        if !(item.isEmpty){
                            self?.babyHasNotArrivedPostAPI(item,true)
                        }
                    }
                }
            }
            self.addChildVC(popvc)
        }
    }
    @objc func childHAsNotArrived(){
        self.anotherAlert = UIAlertController.init(title: Constants.appName, message: "Waiting for your little one to be welcomed into this world? This truly is going to be an amazing conclusion to that 10 month journey of yours!", preferredStyle: UIAlertControllerStyle.alert)
        
        self.anotherAlert.addTextField(configurationHandler: { (textField) in
            textField.text = ""
            textField.placeholder = "Expected delivery date"
            self.doDatePicker()
            textField.inputView = self.datePicker
            textField.inputAccessoryView = self.toolBar
        })
        
        self.anotherAlert.addAction(UIAlertAction.init(title: "Save & Continue", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) in
            let textField = self.anotherAlert.textFields![0] // Force unwrapping because we know it exists.
            if textField.text != ""{
                // Hit the API
                DispatchQueue.main.async {
                    self.babyHasNotArrivedPostAPI()
                }
                
            }
        }))
        self.anotherAlert.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(self.anotherAlert, animated: true, completion: nil)
    }
    
    
    func doDatePicker(){
        // DatePicker
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: self.view.frame.size.height - 220, width:self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        datePicker.datePickerMode = .date
        
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        // SET MiNIMUM & MAximum DAte
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let currentDate: NSDate = NSDate()
        let components: NSDateComponents = NSDateComponents()
        
        datePicker.minimumDate = Date.init()
        
        components.day = +30
        let maxDate : Date = gregorian.date(byAdding: components as DateComponents, to: currentDate as Date, options: NSCalendar.Options.init(rawValue: 0))!
        datePicker.maximumDate = maxDate
        datePicker.date = Date.init()
        datePicker.addTarget(self, action: #selector(self.setDate(_sender:)), for: .valueChanged)
        
        // ToolBar
        
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        toolBar.isUserInteractionEnabled = true
        self.toolBar.isHidden = false
        
    }
    
    //MARK:- Date Picker
    @objc func setDate(_sender : UIDatePicker){
        let dateFormatter: DateFormatter = DateFormatter()
        // Set date format
        dateFormatter.dateFormat = "dd-MM-yyyy"
        // Apply date format
        let selectedDate: String = dateFormatter.string(from: _sender.date)
        
        let textField = self.anotherAlert.textFields![0]
        textField.text = "\(selectedDate)"
    }
    @objc func doneClick() {
        let textField = self.anotherAlert.textFields![0]
        textField.endEditing(true)
        view.endEditing(true)
    }
    
    @objc func cancelClick() {
        
        let textField = self.anotherAlert.textFields![0]
        textField.endEditing(true)
        view.endEditing(true)
    }
	
    func babyArrivedTrackPostAPI(_ babyDetails:String = ""){
        self.view.endEditing(true)
        if Helper.isConnectedToNetwork() {
            let urlstr = Constants.BASEURL + MethodName.BabyTrackDetails
            let userID = UserDefaults.standard.string(forKey: Constants.USERID)
            
            if userID == nil || userID?.count == 0 {
                PKSAlertController.alert(Constants.appName, message: "Invalid User ID")
            }
            else{
                Constants.appDel.startLoader()
                
                let paramStr = babyDetails
                Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
                    let responseDict = response as Dictionary<String, Any>
                    Constants.appDel.stopLoader()
                    if responseDict["status"] as? Int == 1  || responseDict["status"] as? String == "1" {
                        DispatchQueue.main.async {
                            // self.GetPostsForHomeVC()
//                            self.callApiGetProfile()
                        }
                    }
                    else if responseDict["status"] as! Int == 0 {
                        DispatchQueue.main.async {
                            PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                        }
                    }
                    else{
                        PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                        
                    }
                }
            }
            
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
    func babyHasNotArrivedPostAPI(_ babyDetails:String = "" , _ flagBabyArrived: Bool = false ){
        self.view.endEditing(true)
        if Helper.isConnectedToNetwork() {
            
            let urlstr = Constants.BASEURL + MethodName.BabyHasnotArrived
            let userID = UserDefaults.standard.string(forKey: Constants.USERID)
            
            if userID == nil || userID?.count == 0 {
                PKSAlertController.alert(Constants.appName, message: "Invalid User ID")
            }
            else{
                Constants.appDel.startLoader()
                let paramStr = "user_id=\(userID!)&delivery=\(self.flagBabyArrived==true ? 1 : 2)&delivery_date=\( self.flagBabyArrived==true ? "" :  self.anotherAlert.textFields![0].text ?? "")"
                Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
                    let responseDict = response as Dictionary<String, Any>
                    Constants.appDel.stopLoader()
                    if responseDict["status"] as? Int == 1  || responseDict["status"] as? String == "1" {
                        DispatchQueue.main.async {
                            if flagBabyArrived==true{
                             self.babyArrivedTrackPostAPI(babyDetails)
                            }else{
//                            self.callApiGetProfile()
                            }
                        }
                    }
                    else if responseDict["status"] as! Int == 0 {
                        DispatchQueue.main.async {
                            PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                        }
                    }
                }
            }
            
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
    func setProfileData()  {
        // user data
        userNameLbl.text = userData?.userName ?? ""
        userEmailLbl.text = userData?.userEmail  ?? ""

        if  userData != nil &&  !(userData?.image?.isEmpty)!  {
            Server.toGetImageFromURL(url:(userData?.image)!) { (userImage) in
                self.userImgView.image = userImage == nil ?  #imageLiteral(resourceName: "userPlaceholderProfile") : userImage
                }
            }
        
          mainProfileTbl.reloadData()
        if  parentalStatus == "planning-baby"{
			if planningBtn != nil{
				planningBtn.isSelected=true
				}
        }
		if  parentalStatus == "mother"{
			self.offerregistrationViewShow = Bool.init(exactly: NSNumber.init(value: (userData?.offerSubscribed)!))
		}
		
        manageParentStatusView()
    }

    //MARK: - IBAction
    @IBAction func backAction(_ sender: UIButton) {
        let homeVc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        var navArr = self.navigationController?.viewControllers
        if (navArr?.contains(homeVc))!{
            self.navigationController?.popViewController(animated: true)
        }
		else{
            navArr?.append(homeVc)
            self.navigationController?.viewControllers = navArr!
            self.navigationController?.popToViewController(homeVc, animated: true)
        }
    }
    
    //Edit Profile Action
    @IBAction func editProfileAction(_ sender: Any) {
        let editVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        editVc.userData = userData
        self.navigationController?.pushViewController(editVc, animated: true)
    }
    
    @IBAction func changePAsswordAction(_ sender: RoundButton) {
        DispatchQueue.main.async { self.view.endEditing(true) }
//        changePasswordService() // chaeck ProfileApiHelper.Swift for method defination
    }

    @IBAction func saveIntrestAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if  intrestTagView?.selectedIndices.count == 0 {
            PKSAlertController.alert(Constants.appName, message: "Please select atleast one interest")
        }
        else{
//            self.registerInterests() // chaeck ProfileApiHelper.Swift for method defination
        }
    }
    
    @IBAction func firstDeliveryAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func savePragnencyAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if  intrestTagView?.selectedIndices.count == 0 {
            PKSAlertController.alert(Constants.appName, message: "Please select your interest first")
        }
        else{
//             savePragnencyDetails() // chaeck ProfileApiHelper.Swift for method defination
        }
    }
    
    @IBAction func isPregnantPressed_PregnancyStatus(_ sender: UIButton) {
        self.pregnantBtn.isSelected = true
        self.planningBtn.isSelected = false
        manageParentStatusView()
    }
    
    @IBAction func isPlanningPressed_PregnancyStatus(_ sender: UIButton) {
        self.pregnantBtn.isSelected = false
        self.planningBtn.isSelected = true
     
        manageParentStatusView()
    }
    
    @IBAction func searchAction(_ sender: UIButton) {
        let searchVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(searchVc, animated: true)
    }
    
    @IBAction func logoutAction(_ sender: UIButton) {
        PKSAlertController.alert(Constants.appName, message: "Are you sure you want to Logout?", buttons: ["Ok","Cancel"], tapBlock: { (alert, index) in
            if index == 0 {
                self.logoutGoToInitial()
            }
        })
    }
   
    // BABY DETAILS
    @IBAction func KGButtonPressed(_ sender: UIButton) {
        kgButton.isSelected = true
        lbsButton.isSelected = false
        self.babyDetails?.babyWeightUnit = "kg."
    }
    @IBAction func LBSButtonPressed(_ sender: UIButton) {
        kgButton.isSelected = false
        lbsButton.isSelected = true
        self.babyDetails?.babyWeightUnit = "lb."
    }
    @IBAction func CMButtonPressed(_ sender: UIButton) {
        cmButton.isSelected = true
        inchButton.isSelected = false
        self.babyDetails?.babyHeightUnit = "cm."
    }
    @IBAction func InchButtonPressed(_ sender: UIButton) {
        cmButton.isSelected = false
        inchButton.isSelected = true
        self.babyDetails?.babyHeightUnit = "in."
    }
    @IBAction func CM_cButtonPressed(_ sender: UIButton) {
        cm_cButton.isSelected = true
        inch_cButton.isSelected = false
        self.babyDetails?.headCircumfrenceUnit = "cm."
    }
    @IBAction func Inch_cButtonPressed(_ sender: UIButton) {
        cm_cButton.isSelected = false
        inch_cButton.isSelected = true
        self.babyDetails?.headCircumfrenceUnit = "in."
    }
    @IBAction func SaveBabyDetailsPressed(_ sender: UIButton) {
       // hit service for baby details // check ProfileApiHelper.Swift for method defination
        self.view.endEditing(true)
        self.userData?.babyDetails = self.babyDetails
//        registerForBabyDetails()
    }
    //LOGOUT APP
    func logoutGoToInitial()  {
        let root = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        let   navCtrl = UINavigationController.init(rootViewController: root)
        navCtrl.navigationBar.isHidden = true
		navCtrl.navigationBar.barTintColor = UIColor.clear
        Constants.appDelegate.window?.rootViewController = navCtrl
        Helper.resetDefaults()
        UserDefaults.standard.set(0, forKey: Constants.RegisterStatus)

        
    }
	
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
	
	//MARK:- NOTIFICATION METHOD
	
	@objc func NotificationAction(_ sender: NSNotification){
		
		
	}
	
}
//MARK: - Table view Cell data binding methods
// go check ProfileTableDataBindingClass.swift
 //MARK: - Profile table Data source
extension ProfileViewController : UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return profileSectionArray.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if profileSectionArray[section] == ProfileSections.ManagePragnency{
            hidden[section]=false
            return 1
        } else {
            return hidden[section] ? 0 : 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        switch  profileSectionArray[indexPath.section] {
//        case  .MYProfile :
//            let cell  = tableView.dequeueReusableCell(withIdentifier: "MyProfileTableViewCell") as! MyProfileTableViewCell
//            BindProfileCellData(cell, indexPath)
//            return cell
//
//        case .ChangePassword :
//                let cell  = tableView.dequeueReusableCell(withIdentifier: "ChangePasswordTableViewCell") as! ChangePasswordTableViewCell
//                BindChangePasswordCellData(cell)
//                return cell
//
//        case .MyIntrests :
//                let cell  = tableView.dequeueReusableCell(withIdentifier: "MyIntrestTableViewCell") as! MyIntrestTableViewCell
//                BindMyInterstCellData(cell)
//                return cell
//
//        case .ManagePragnency :
//                let cell  = tableView.dequeueReusableCell(withIdentifier: "ManagePragStatusTableViewCell") as! ManagePragStatusTableViewCell
//                BindPragnencyStatusCellData(cell)
//                hidden[indexPath.section]=false
//                return cell
//
//        case .PragnencyDetails , .WifePragnencyDetails :
//            let cell  = tableView.dequeueReusableCell(withIdentifier: "PragDetailTableViewCell") as! PragDetailTableViewCell
//            BindPragnencyDetailCellData(cell)
//            return cell
//
//        case .MyBabysDetails :
//            let cell  =  tableView.dequeueReusableCell(withIdentifier: "MyBabyDetailTableViewCell") as! MyBabyDetailTableViewCell
//            BindBabyDetailCellData(cell)
//            return cell
//        case .ChangePArantalStatus:
//             let cell  =  tableView.dequeueReusableCell(withIdentifier: "ChangePragnencyStatusTableViewCell") as! ChangePragnencyStatusTableViewCell
//             cell.childArrivedBtn.addTarget(self, action: #selector(self.childhasArived), for: UIControlEvents.touchUpInside)
//             cell.childNotArrivedBtn.addTarget(self, action: #selector(self.childHAsNotArrived), for: UIControlEvents.touchUpInside)
//             //cell.dropShadow()xx
//            return cell
//
//		case .offerRegistration:
			let cell  = tableView.dequeueReusableCell(withIdentifier: "offerRegistrationCell")
//			let butn = cell!.contentView.viewWithTag(2101) as? UIButton
////			butn?.addTarget(self, action: #selector(RegisterForOffersAPI), for: UIControlEvents.touchUpInside)
//			if offerregistrationViewShow == true{
//				butn?.isSelected = true
//				butn?.isUserInteractionEnabled = false
//			}
//			else {
//				butn?.isSelected = false
//				butn?.isUserInteractionEnabled = true
//			}
			return cell!
//        }
    }
        }

//MARK: - Profile table Delegate

extension ProfileViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableViewAutomaticDimension
		
	}
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat { return 45 }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == mainProfileTbl{
            selectedSection = (selectedSection == profileSectionArray[indexPath.section].rawValue) ? "" :
                        profileSectionArray[indexPath.section].rawValue
        }
        tableView.reloadData()
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let  sectionView = UIView(frame: CGRect(x: 0, y: 0, width: Int(tableView.frame.size.width), height: 45))
        sectionView.tag = section
        
        let skyBlueView = RoundView(frame: CGRect(x: 20, y: 0, width: Int(tableView.frame.size.width-40), height: 45))
        skyBlueView.backgroundColor = hidden[section]==false ? UIColor.white : #colorLiteral(red: 0.168627451, green: 0.7019607843, blue: 0.9215686275, alpha: 1)
        skyBlueView.layer.cornerRadius = 10
        skyBlueView.layer.masksToBounds=true
        
        let sectionTitleLbl = UILabel(frame: CGRect(x: 25, y: 0, width: Int(tableView.frame.size.width-50), height: 45))
        sectionTitleLbl.textColor = hidden[section]==false ? UIColor.black : UIColor.white
        sectionTitleLbl.font = UIFont.init(name: FONTS.AdiraDisplaySSi, size: 16.0)
        sectionTitleLbl.text = profileSectionArray[section].rawValue
        
        let sectionImg = UIImageView.init(frame: CGRect.init(x: 25 + sectionTitleLbl.frame.size.width-30, y: 10, width: 20, height: 20))
        sectionImg.image =  hidden[section]==false ? #imageLiteral(resourceName: "minus") :  #imageLiteral(resourceName: "plus")
		
	
		
        sectionView.addSubview(skyBlueView)
        sectionView.addSubview(sectionTitleLbl)
        sectionView.addSubview(sectionImg)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction))
        sectionView.isUserInteractionEnabled = true
        sectionView.addGestureRecognizer(tap)
       
        if  hidden[section]==true {
			sectionView.dropShadow()
        }
		else{
			let borderview = UIView.init(frame: CGRect.init(x: skyBlueView.frame.origin.x-5, y: 0, width: skyBlueView.frame.width+10, height: 54))
			borderview.layer.addBorder(edge: UIRectEdge.top,  color: #colorLiteral(red: 0.5176470588, green: 0.5176470588, blue: 0.5176470588, alpha: 1), thickness: 1)
			borderview.layer.addBorder(edge: UIRectEdge.left,  color: #colorLiteral(red: 0.5176470588, green: 0.5176470588, blue: 0.5176470588, alpha: 1), thickness: 1)
			borderview.layer.addBorder(edge: UIRectEdge.right, color: #colorLiteral(red: 0.5176470588, green: 0.5176470588, blue: 0.5176470588, alpha: 1), thickness: 1)
			borderview.clipsToBounds=false
			borderview.backgroundColor=UIColor.clear
			borderview.dropShadow()
        }
		
        // show prag decision view always
        if profileSectionArray[section] == ProfileSections.ManagePragnency{
            hidden[section]=false
        }
		
		if self.offerregistrationViewShow == true && profileSectionArray[section] == ProfileSections.offerRegistration{
			sectionImg.image = #imageLiteral(resourceName: "checkUnselected")//hidden[section]==false ? #imageLiteral(resourceName: "minus") :
			sectionView.isUserInteractionEnabled = true
		}
        sectionView.backgroundColor = UIColor.clear
        sectionTitleLbl.backgroundColor =  UIColor.clear
        return sectionView
    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        let section = sender.view!.tag

        for i in 0..<hidden.count{
            hidden[i] = (i==section) ? !hidden[section] : true
			
        }
        // show prag decision view always
        if profileSectionArray[section] == ProfileSections.ManagePragnency{
            hidden[section]=false
        }
		
		if self.offerregistrationViewShow == true{
			if profileSectionArray[section] == ProfileSections.offerRegistration{
				hidden[section] = true
			}
		}
		
		
        mainProfileTbl.beginUpdates()
        let range = NSMakeRange(0, self.mainProfileTbl.numberOfSections)
        let sections = NSIndexSet(indexesIn: range)
        self.mainProfileTbl.reloadSections(sections as IndexSet, with: UITableViewRowAnimation.automatic)
        mainProfileTbl.endUpdates()
        if mainProfileTbl.contentOffset.y < 0{
            mainProfileTbl.setContentOffset(CGPoint.zero, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return profileSectionArray[section] == ProfileSections.ManagePragnency ? 0 : 54
    }
}
////MARK: - Intrest View Data source
//extension ProfileViewController:HTagViewDataSource{
//    func numberOfTags(_ tagView: HTagView) -> Int {   return intrestArray.count }
//    func tagView(_ tagView: HTagView, titleOfTagAtIndex index: Int) -> String { return intrestArray[index].name! }
//    func tagView(_ tagView: HTagView, tagTypeAtIndex index: Int) -> HTagType { return .select }
//    func tagView(_ tagView: HTagView, tagWidthAtIndex index: Int) -> CGFloat {  return .HTagAutoWidth }
//}
////MARK: - Intrest View  Delegate
//extension ProfileViewController:HTagViewDelegate{
//    func tagView(_ tagView: HTagView, tagSelectionDidChange selectedIndices: [Int]) {
//    }
//    func tagView(_ tagView: HTagView, didCancelTagAtIndex index: Int) {
//        tagView.reloadData()
//    }
//}

extension ProfileViewController : UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == babyGenderTxt {
            babyNameTxt.resignFirstResponder()
            self.view.endEditing(true)
            CustomPicker.show(data: [["Male", "Female"]]) {  [weak self] (selections: [Int : String]) -> Void in
                if let name = selections[0] {
                    self?.babyGenderTxt.text = name
                }
            }
            return false
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == self.newPasswordTxt || textField == self.oldPasswordTxt || textField == self.confirmPasswordTxt{
            let set = NSCharacterSet(charactersIn: "ABCDEFGHIJKLMONPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789").inverted
            
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 10 && string.rangeOfCharacter(from: set) == nil
        }
        
        
        return true
    }
}
