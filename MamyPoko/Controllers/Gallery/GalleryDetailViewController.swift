//
//  GalleryDetailViewController.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 09/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit
import FacebookShare
import FBSDKShareKit


class GalleryDetailViewController: UIViewController {

    @IBOutlet weak var createdOnLbl: UILabel!
    @IBOutlet weak var remarkLbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    var documentInteractionController = UIDocumentInteractionController()
    var dictGalleryItem: [String:Any] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
		self.BlockSideMenuSwipe()
        let sizeimageurl = URL(string: (dictGalleryItem["image"] as? String) ?? "")
        imgView.kf.setImage(with: sizeimageurl)
        remarkLbl.text = (dictGalleryItem["name"] as? String) ?? ""
        createdOnLbl.text = (dictGalleryItem["created"] as? String) ?? ""

    }

    @IBAction func facebookAction(_ sender: Any) {

		let content = SharePhotoContent.init()
		content.hashtag = Hashtag.init("#SheCaresSheShares")
        
		let sharePhoto = SharePhoto()
        sharePhoto.image = imgView.image ?? UIImage()

		let dialog = ShareDialog.init()
        dialog.fromViewController = self;
        dialog.shareContent = content

        let isInstalled : Bool = UIApplication.shared.canOpenURL(URL.init(string: "fb://")!)

        if isInstalled {
			dialog.mode = ShareDialog.Mode.shareSheet

        } else {
			dialog.mode = ShareDialog.Mode.feedBrowser
        }
        dialog.show()


    }
    @IBAction func shareAction(_ sender: Any) {
        let title = self.dictGalleryItem["name"] as? String
        let hastags = "\n #SheCaresSheShares \n\n"


        let alert = UIActivityViewController.init(activityItems: [title!,hastags, imgView.image ?? UIImage()], applicationActivities: nil)
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func whatsappAction(_ sender: Any) {

        let title = (self.dictGalleryItem["name"] as? String) ?? ""

//        let whatsappUrlString = "whatsapp://send?text=\(link)"
//
//        if let whatsappURL = NSURL(string: whatsappUrlString) {
//            if UIApplication.shared.canOpenURL(whatsappURL as URL) {
//                UIApplication.shared.open(whatsappURL as URL, options: [:], completionHandler: nil)
//            }
//            else{
//                PKSAlertController.alert("WhatsApp Error", message: "Download WhatsApp from Appstore to continue")
//            }
//        }
        let urlWhats = "whatsapp://app"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed) {
            if let whatsappURL = URL(string: urlString) {

                if UIApplication.shared.canOpenURL(whatsappURL as URL) {

                    if let image = imgView.image {
                        if let imageData = UIImageJPEGRepresentation(image, 1.0) {
                            let tempFile = URL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent("Documents/\(title).jpeg")
                            do {
                                try imageData.write(to: tempFile, options: .atomic)
                                self.documentInteractionController = UIDocumentInteractionController(url: tempFile)
                                self.documentInteractionController.uti = "net.whatsapp.image"
                                self.documentInteractionController.presentOpenInMenu(from: CGRect.zero, in: self.view, animated: true)

                            } catch {
                               // print(error)
                            }
                        }
                    }

                } else {
                    // Cannot open whatsapp
                     PKSAlertController.alert("WhatsApp Error", message: "Download WhatsApp from Appstore to continue")
                }
            }
        }
    }
    @IBAction func gmailAction(_ sender: Any) {

        let title = dictGalleryItem["name"] as? String
        let hastags = "SheCaresSheShares"
        let link = ""
        var subject = "\(hastags)\n\(title ?? "")"
        subject = subject.replacingOccurrences(of: " ", with: "%0D")
        subject = subject.replacingOccurrences(of: "\n", with: "%0A")

        let googleUrlString = "googlegmail:///co?subject=\(subject)&body=\(link)"
        if let googleUrl = NSURL(string: googleUrlString) {
            // show alert to choose app
            if UIApplication.shared.canOpenURL(googleUrl as URL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(googleUrl as URL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(googleUrl as URL)
                }
            }
            else{
                PKSAlertController.alert("Gmail Error", message: "Download Gmail App from Appstore to continue")
            }
        }
    }

    @IBAction func searchAction(_ sender: Any) {
        let searchVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(searchVc, animated: true)
    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }

}
