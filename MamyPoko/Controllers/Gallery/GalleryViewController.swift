//
//  GalleryViewController.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 09/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit
import Kingfisher

class GalleryViewController: UIViewController {

    @IBOutlet weak var galleryCollection: UICollectionView!

    var galleryArray:[[String:Any]] = []

    var selectedIndex = 0
    @IBOutlet weak var editView: UIView!
    
    @IBOutlet weak var captionTxt: UITextField!
    @IBOutlet weak var detailGalImgVIew: UIImageView!
    @IBOutlet weak var topEditView: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
			self.BlockSideMenuSwipe()
        topEditView.constant = view.frame.size.height+20

    }

    @IBAction func cancelAction(_ sender: Any) {
topEditView.constant = view.frame.size.height+20
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    @IBAction func uploadAction(_ sender: Any) {
        if self.captionTxt.text?.count == 0 {
            PKSAlertController.alert("Error", message: "Enter remark title !")
        }
        else{
            //let strBase64 = detailGalImgVIew.image?.base64(format: ImageFormat.png)
            self.updateRemark()
        }
    }
    @IBAction func menuAction(_ sender: Any) {
      //  toggleSideMenuView()
        dismiss(animated: true, completion: nil)
    }
    @IBAction func searchAction(_ sender: Any) {
        let searchVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController

        self.navigationController?.pushViewController(searchVc, animated: true)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }

}
extension GalleryViewController : UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return galleryArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCollectionViewCell", for: indexPath) as! GalleryCollectionViewCell
        let data = galleryArray[indexPath.row]

        let sizeimageurl = URL(string: (data["image"] as? String) ?? "")
        cell.imgView.kf.setImage(with: sizeimageurl)

        cell.captionLbl.text = (data["name"] as? String) ?? ""
        cell.editBtn.tag = indexPath.row
        cell.editBtn.addTarget(self, action: #selector(editAction(_:)), for: .touchUpInside)
        return cell
    }
    @objc func editAction(_ sender: UIButton){
        topEditView.constant = 0
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
        let data = galleryArray[sender.tag]
        let sizeimageurl = URL(string: (data["image"] as? String) ?? "")
        detailGalImgVIew.kf.setImage(with: sizeimageurl)
        captionTxt.text = (data["name"] as? String) ?? ""
        selectedIndex = sender.tag

    }

    func updateRemark(){
        if Helper.isConnectedToNetwork() {

            let urlstr = Constants.BASEURL + MethodName.UpdateBmpImage
            let userID = UserDefaults.standard.string(forKey: Constants.USERID)

            if userID == nil || userID?.count == 0 {
                PKSAlertController.alert(Constants.appName, message: "Invalid User ID")
            }
            else{
                Constants.appDel.startLoader()

                let paramStr = "user_id=\(userID!)&image_id=\(selectedIndex)&title=\(captionTxt.text ?? "")"

                Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in

                    let responseDict = response as Dictionary<String, Any>
                    Constants.appDel.stopLoader()
                    let message = responseDict["message"] as! String
                    if responseDict["status"] as! NSNumber == NSNumber.init(value: 1) {
                        DispatchQueue.main.async {
                            self.cancelAction(UIButton())
                            let alert = UIAlertController(title: "Success", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            let acceptButton = UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction) in
                               // self.BottomDataPostAPI()
                                var dict = self.galleryArray[self.selectedIndex]
                                dict["name"] = self.captionTxt.text
                                self.galleryArray[self.selectedIndex] = dict
                            self.galleryCollection.reloadData()
                               NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: Constants.KUpdateBumpGalleryNotification), object: nil)
                            })
                            alert.addAction(acceptButton)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    else if responseDict["status"] as! NSNumber == NSNumber.init(value: 0) {
                        DispatchQueue.main.async {
                            PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                        }
                    }
                    else{
                        PKSAlertController.alert("Error", message: responseDict["message"] as! String)

                    }
                }
            }

        }else{
            PKSAlertController.alertForNetwok()
        }
    }


}
extension GalleryViewController : UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let detailVc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "GalleryDetailViewController") as! GalleryDetailViewController
        detailVc.dictGalleryItem = galleryArray[indexPath.row]
        self.navigationController?.pushViewController(detailVc, animated: true)

    }
}
extension GalleryViewController : UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: (collectionView.frame.size.width/2)-10, height: (collectionView.frame.size.width/2)-10)
    }
}
extension GalleryViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}

