//
//  BabyArrivedPopViewController.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 01/08/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
protocol BabyArrivedDelegate {
    func babyArrivedData(_ paramStr : String)
}

public enum BabyTrack:String {
    case baby_name
    case baby_gender
    case baby_dob
    case  user_id
}
class BabyArrivedPopViewController: UIViewController {

    //MARK: - Properties
    
    @IBOutlet weak var bgImgBiew: UIImageView!
    @IBOutlet weak var dobTxt: RoundTextField!
    @IBOutlet weak var babyNameTxt: RoundTextField!
    @IBOutlet weak var genderTxt: RoundTextField!
    var didSaveBabyData: ((_ param: String) -> Void)?
    var datePicker : UIDatePicker!

    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupInitialView()
        showAnimate()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        IQKeyboardManager.shared.enableAutoToolbar = true
         IQKeyboardManager.shared.previousNextDisplayMode =  .alwaysHide
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        IQKeyboardManager.shared.enableAutoToolbar = false
    }

    //MARK:- Action
    
    @IBAction func closePressed(_ sender: Any) {
       removeAnimate()
    }
    
    @IBAction func savePressed(_ sender: Any) {
        if CheckDataValidation(){
            let userID = UserDefaults.standard.string(forKey: Constants.USERID)

            let paramStr = "user_id=\(userID!)&baby_name=\(babyNameTxt.text ?? "")&baby_gender=\( genderTxt.text ?? "")&baby_dob=\(dobTxt.text ?? "")"
            didSaveBabyData?(paramStr)
            
        }
    }
    
    //MARK:- Date Picker
    @objc func setDate(_sender : UIDatePicker){
        let dateFormatter: DateFormatter = DateFormatter()
        // Set date format
        dateFormatter.dateFormat = "dd-MM-yyyy"
        // Apply date format
        let selectedDate: String = dateFormatter.string(from: _sender.date)
        
        // SET MiNIMUM & MAximum DAte
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let currentDate: NSDate = NSDate()
        let components: NSDateComponents = NSDateComponents()
        
        components.year = 0
        let minDate : Date = gregorian.date(byAdding: components as DateComponents, to: currentDate as Date, options: NSCalendar.Options.init(rawValue: 0))!
        datePicker.minimumDate = minDate
        
        components.year = -2
        let maxDate : Date = gregorian.date(byAdding: components as DateComponents, to: currentDate as Date, options: NSCalendar.Options.init(rawValue: 0))!
        datePicker.maximumDate = maxDate
        
        
        if _sender.date < minDate{
            if _sender.date < maxDate {
                self.dobTxt?.text = dateFormatter.string(from: maxDate)
                datePicker.date = maxDate
            }else{
            self.dobTxt?.text = "\(selectedDate)"
            }
        }
        else{
            if _sender.date < maxDate {
                self.dobTxt?.text = ""
                datePicker.date = maxDate
            }else{
            self.dobTxt?.text = ""
            datePicker.date = minDate
            }
        }
        
    }
    
    @objc func dismissPicker() {
        
        view.endEditing(true)
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }

}
//MARK: - Custom Method
extension BabyArrivedPopViewController {
    func setupInitialView()  {
         dobTxt.setLeftPaddingPoints()
        genderTxt.setLeftPaddingPoints()
        babyNameTxt.setLeftPaddingPoints()
        bgImgBiew.layer.cornerRadius = 20
        bgImgBiew.layer.masksToBounds = true
        datePicker = UIDatePicker()
        datePicker.tag = 100
        datePicker.datePickerMode = UIDatePickerMode.date
        datePicker.addTarget(self, action: #selector(self.setDate(_sender:)), for: .valueChanged)
        datePicker.timeZone = TimeZone.current
        self.dobTxt?.inputView = datePicker
        let toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(dismissPicker))
        self.dobTxt?.inputAccessoryView = toolBar
        // SET MiNIMUM & MAximum DAte
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let currentDate: NSDate = NSDate()
        let components: NSDateComponents = NSDateComponents()
        
        components.year = 0
        let minDate : Date = gregorian.date(byAdding: components as DateComponents, to: currentDate as Date, options: NSCalendar.Options.init(rawValue: 0))!
        datePicker.minimumDate = minDate
        datePicker.date = minDate
        
        components.year = -2
        let maxDate : Date = gregorian.date(byAdding: components as DateComponents, to: currentDate as Date, options: NSCalendar.Options.init(rawValue: 0))!
        datePicker.maximumDate = maxDate
    }
    func CheckDataValidation()-> (Bool){
        let userDOB = "\(String(describing: self.dobTxt!.text!))"
        let gender = "\(String(describing: self.genderTxt!.text!))"
       
        if gender.isEmpty == true{
            PKSAlertController.alert(Constants.appName, message: "Select Gender !")
            return false
        }
        
        if userDOB.isEmpty == true {
            PKSAlertController.alert(Constants.appName, message: "Enter DOB !")
            return false
        }
        
        if self.genderTxt!.text!.isEmpty == true{
            PKSAlertController.alert(Constants.appName, message: "Select gender !")
            return false
        }
        if self.dobTxt!.text!.isEmpty == true{
            PKSAlertController.alert(Constants.appName, message: "Select date of birth !")
            return false
        }
        
        
        return true
    }
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0
        }, completion: {(finished : Bool) in
            if(finished)
            {
                self.willMove(toParentViewController: nil)
                self.view.removeFromSuperview()
                self.removeFromParentViewController()
            }
        })
    }
}
extension BabyArrivedPopViewController : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == genderTxt {
            babyNameTxt.resignFirstResponder()
           self.view.endEditing(true)
            CustomPicker.show(data: [["Male", "Female"]]) {  [weak self] (selections: [Int : String]) -> Void in
                if let name = selections[0] {
                    self?.genderTxt.text = name
                }
            }
            return false
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == genderTxt {
            babyNameTxt.resignFirstResponder()
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       
        if textField == babyNameTxt{
            genderTxt.becomeFirstResponder()
        }
        
        return true
    }
}
