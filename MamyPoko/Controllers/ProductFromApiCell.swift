//
//  ProductFromApiCell.swift
//  MamyPoko
//
//  Created by Arnab Maity on 14/01/20.
//  Copyright © 2020 Varun Tyagi. All rights reserved.
//

import UIKit

class ProductFromApiCell: UITableViewCell {

	@IBOutlet weak var ProductCatalogLbl: UILabel!
	
	@IBOutlet weak var catalogImg: UIImageView!
	
	@IBOutlet weak var catalogNameLbl:UILabel!
	
	@IBOutlet weak var catalogDesLbl: UILabel!
	
//	@IBOutlet weak var viewAllBtn: RoundButton!
	
	@IBOutlet weak var viewAllBtn: UIButton!
	
	
	@IBOutlet weak var heightProfCatlog: NSLayoutConstraint!
	
	override func awakeFromNib() {
        super.awakeFromNib()
	}


}
