//
//  TipsByMomsViewController.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 06/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import AppsFlyerLib
import FacebookShare
import FBSDKShareKit
import MessageUI
class TipsByMomsViewController: UIViewController,MFMailComposeViewControllerDelegate,UITextFieldDelegate {
 
  
    let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_"

   
     

       
       @IBOutlet weak var backBtn: UIButton!
       @IBOutlet weak var tipsTable: UITableView!
       var tipsArray:TipModel = []
       var tipsCatArray: TipsCategoryModel = []
       var selectedTipCat: TipsCategoryModelElement?
       var tipsCatTxt:RoundTextField?
       var tipsIdentifier = "Tips"
       var postDatadict = [String : Any]()
       var pageOffset = 1
       var isLoading: Bool = false
       var userImgDownload = [Int:UIImage]()
       
       
       @IBOutlet weak var profileImageButton : RoundButton!

       
       override func viewDidLoad() {
           super.viewDidLoad()
         tipsCatTxt?.delegate = self
           profileImageButton.cornerRadius = 20
           profileImageButton.borderColor = UIColor.clear

           Helper.GetProfileImageForButton(butName: self.profileImageButton)

           let tipelement = try! TipModelElement.init("{\"status\":\"\",\"city\":\"\",\"\":\"\",\"name\":\"\",\"image\":\"\",\"share\":\"\",\"headtype\":\"\"}")
          tipsArray.append(tipelement)
           tipsTable.reloadData()
           getTipsByCatFromAPI("")
           
           AnalyticsManager.LogFireBaseAnalytics(analyticsType: AnalyticsEventViewItem, itemId: "id-\(AppsFlyerConstant.TipsByMomsView)", itemName: "\(AppsFlyerConstant.TipsByMomsVC)", ContentType: "Visit")
           AnalyticsManager.LogApsFlyerEvent(eventType: AppsFlyerConstant.TipsByMomView, contentId: "31")

       }
       override func viewDidAppear(_ animated: Bool) {
           self.AllowSideMenuSwipe()
       }
       override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(true)
           getCategoryFromAPI()
       }

       //MARK:- ACTIONS
       
       @IBAction func ViewProfilePressed(){
           let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
           destViewController.isHereChangeStatus = false
           destViewController.selectedButton = 0
           if Constants.appDel.returnTopViewController().isKind(of: HomeVC.classForCoder()){
               let home = Constants.appDel.returnTopViewController() as! HomeVC
               home.hideSideMenuView()
               home.navigationController?.pushViewController(destViewController, animated: true)
           }
           else{
               sideMenuController()?.setContentViewController(destViewController)
           }
       }
       
       @IBAction func backAction(_ sender: UIButton) {
           //self.navigationController?.popViewController(animated: true)
           toggleSideMenuView()
       }

       @IBAction func addRipAction(_ sender: UIButton) {
           let addTipVc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "PostTipViewController") as! PostTipViewController
           addTipVc.tipCatArray = tipsCatArray
           addTipVc.tipsIdentifier = self.tipsIdentifier
           self.navigationController?.pushViewController(addTipVc, animated: true)
       }
       @IBAction func searchAction(_ sender: Any) {
           let searchVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
           self.navigationController?.pushViewController(searchVc, animated: true)
       }
      // override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }

       @IBAction func facebookSharing(_ sender: UIButton) {

   //        let link = self.postDatadict["sharing_link"] as? String
           //let link = self.tipsArray[sender.tag].image ?? "www.google.com"
       //    let id = tipsArray[sender.tag].id ?? "755"
           let link = "https://www.mamypoko.co.in/blog/she-cares-she-shares?token=755#share_755"
           let content = ShareLinkContent.init()
           content.contentURL = URL.init(string: link)!
           content.hashtag = Hashtag.init("#SheCaresSheShares")
           
           
           let dialog = ShareDialog.init()
           dialog.fromViewController = self;
           dialog.shareContent = content
           
           let isInstalled : Bool = UIApplication.shared.canOpenURL(URL.init(string: "fb://")!)
           
           if isInstalled {
               dialog.mode = ShareDialog.Mode.shareSheet

           } else {
               dialog.mode = ShareDialog.Mode.feedBrowser
           }
           dialog.show()
           
       }
       
       @IBAction func whatsAppSharing(_ sender: UIButton) {
   //        let link = self.postDatadict["sharing_link"] as! String
           let id = tipsArray[sender.tag].id ?? "755"
           let link = "https://www.mamypoko.co.in/blog/she-cares-she-shares?token=\(id)#share_\(id)"
       
                 let whatsappUrlString = "whatsapp://send?text=\(link)"

                 if let whatsappURL = NSURL(string: whatsappUrlString) {
                     if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                         UIApplication.shared.open(whatsappURL as URL, options: [:], completionHandler: nil)
                     }
                     else{
                         PKSAlertController.alert("WhatsApp Error", message: "Download WhatsApp from Appstore to continue")
                     }
                 }
       }
       
       @IBAction func gmailSharing(_ sender: UIButton) {
   //        let title = self.postDatadict["title"] as! String
           let id = tipsArray[sender.tag].id ?? "755"
           let link = "https://www.mamypoko.co.in/blog/she-cares-she-shares?token=\(id)#share_\(id)"
                  let hastags = "SheCaresSheShares"
   //               let link = self.postDatadict["sharing_link"] as! String
                  let subject = "\(hastags)\n\(title)"

                  if !MFMailComposeViewController.canSendMail() {
                      PKSAlertController.alert("Error!", message: "Mail services are not available")
                      return
                  }
                  
                  let mailVC = MFMailComposeViewController()
                  mailVC.mailComposeDelegate = self as? MFMailComposeViewControllerDelegate
                  mailVC.setSubject(subject)
                  guard  (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String) != nil else {
                      return
                  }
                  
                  mailVC.setMessageBody("\(title)\n\n#\(hastags)\n\n\(link)", isHTML: false)
                  
                  present(mailVC, animated: true, completion: nil)
              }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
              controller.dismiss(animated: true, completion: nil)
          }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
         
         let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
         let filtered = string.components(separatedBy: cs).joined(separator: "")

         return (string == filtered)
       }
       }
       
      
           
       
       

   extension TipsByMomsViewController:TipsByMomTableViewCellProtocol{
       func btnLikeClicked(tag: Int) {
          //updateLocalLike(tag: tag)
          
          if Helper.isConnectedToNetwork() {
            let url_str = Constants.BASEURL + MethodName.offers_likes // create base64 for tipcat then append
              let escapedString = url_str.replacingOccurrences(of: " ", with: "%20")
            var tip = tipsArray[tag]
           if tip.isLikeByMe == 1{
               return
           }
            Constants.appDel.startLoader()

            if tip.id != nil{
              let paramStr = "ip_address=\(AppDelegate.getUniqueDeviceIdentifierAsString())&id=\(tip.id!)"
              
              Server.postRequestWithURL(escapedString, paramString: paramStr) { (response) in
                  response.printJson()
                DispatchQueue.main.async {
                  Constants.appDel.stopLoader()
                 // self.getTipsByCatFromAPI("")
                   if response["status"] as? Int  == 1 || response["status"] as? String  == "1"{
                       var updatedTip = self.tipsArray[tag]
                       var newLikeCount =  Int(updatedTip.like_count ?? "0")! + 1
                       updatedTip.like_count = "\(newLikeCount)"
                       self.tipsArray[tag] = updatedTip
                       self.tipsTable.reloadData()
                   }
                   
               }
                
              }
            }else{
              print("tip id must not be nil")
            }
            
          }else{
            PKSAlertController.alertForNetwok()

          }

        }
   }

   //MARK: - Webservice
   extension TipsByMomsViewController {
       fileprivate  func getCategoryFromAPI() {
           if Helper.isConnectedToNetwork() {
               Constants.appDel.startLoader()
               let url_str = Constants.BASEURL + MethodName.GET_tipsCategory
               Server.getRequestWithURL(urlString: url_str, completionHandler: { (response) in
                   Constants.appDel.stopLoader()
                   if (response?.count)! > 1 && response?["status"] as! NSNumber == NSNumber.init(value: 1)   || response?["status"] as? String == "1" {
                       let   data = response? ["data"] as? [[String:Any]]
                       DispatchQueue.main.async {
                           if let theJSONData = try? JSONSerialization.data(
                               withJSONObject: data ?? [[:]],
                               options: []) {
                               let theJSONText = String(data: theJSONData, encoding: .ascii)
                               self.tipsCatArray = try! TipsCategoryModel(theJSONText!)
//							self.tipsCatArray.removeAll()
                               self.tipsTable.reloadData()
                           }
                       }
                   }else{
                       PKSAlertController.alert(Constants.appName, message: response?["message"] as! String)
                   }

               })
           }else{
               PKSAlertController.alertForNetwok()
           }
       }

       fileprivate  func getTipsByCatFromAPI(_ tipCat:String) {
           if Helper.isConnectedToNetwork() {
               Constants.appDel.startLoader()
               if tipCat.count == 0 {
                   Constants.appDel.stopLoader()
               }
               let utf8str = tipCat.data(using: String.Encoding.utf8)
               var tipCatEncoded : String = ""
               
               if let base64Encoded = utf8str?.base64EncodedString()
               {
                   
                   tipCatEncoded = base64Encoded
                   if NSData(base64Encoded: base64Encoded, options:   NSData.Base64DecodingOptions(rawValue: 0))
                       .map({ NSString(data: $0 as Data, encoding: String.Encoding.utf8.rawValue) }) != nil
                   {
                       
                   }
               }
               
               
               let url_str = Constants.BASEURL + MethodName.Tips_pagination // create base64 for tipcat then append
               let escapedString = url_str.replacingOccurrences(of: " ", with: "%20")
           
               let paramStr = "offset=\(pageOffset)&tips_cat=\(tipCatEncoded)&share=\(tipsIdentifier)&search=\(self.tipsCatTxt?.text ?? "")"
               Server.postRequestWithURL(escapedString, paramString: paramStr) { (response) in
                                   Constants.appDel.stopLoader()
                   if (response.count) > 1 && response["status"] as? NSNumber == NSNumber.init(value: 1)   || response["status"] as? String == "1" {
                                       let   data = response ["data"] as? [[String:Any]]

                       self.isLoading = data!.count < 10 ? true : false
                       
                                       DispatchQueue.main.async {
                                           if let theJSONData = try? JSONSerialization.data(
                                               withJSONObject: data ?? [[:]],
                                               options: []) {
                                               let theJSONText = String(data: theJSONData,
                                                                        encoding: .ascii)
                                               let tipArr = try! TipModel(theJSONText!)
                                               //self.tipsArray.removeAll()
                                               //self.tipsArray.append(tipArr.first!)
                                               for tipelement in tipArr {
                                                   var newElement  = tipelement
                                                   newElement.isReadMore = true
                                                   self.tipsArray.append(newElement)
                                                   
                                               }
                                               self.tipsTable.reloadData()
                                           }
                                       }
                                   }else{
                                       // PKSAlertController.alert(Constants.appName, message: response?["message"] as? String ?? "Some error has occured! Please try again")
                       self.isLoading =  true

                                   }
                   
                               }
           }else{
               PKSAlertController.alertForNetwok()
           }
       }
   }

   //MARK: - Selected Tip delegate
   extension TipsByMomsViewController: UIScrollViewDelegate{
       
       func scrollViewDidScroll(_ scrollView: UIScrollView) {
           let contentOffsetX = scrollView.contentOffset.x
           if contentOffsetX >= (scrollView.contentSize.width - scrollView.bounds.width) - 20 /* Needed offset */ {
               guard !self.isLoading else { return }
               self.isLoading = true
               // load more data
               // than set self.isLoading to false when new data is loaded
             //  if let tabeView =  scrollView as? UITableView{
             //  if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
               DispatchQueue.main.async {
                   if  self.tipsArray.count > 1 {
                       print(" ===========   This is last index load more datga here ====== \n")
                       self.pageOffset = self.pageOffset + 1
                       self.getTipsByCatFromAPI(self.selectedTipCat==nil ? "" : self.selectedTipCat?.catName ?? "")
                   }
               }
                   
              // }
             //  }
               
           }
       }
   }
   extension TipsByMomsViewController : SelectedTipCategoryProtocol{

       func selectedCategory(_ tipCat: TipsCategoryModelElement?) {
           self.isLoading = false
           self.pageOffset=1
           self.tipsArray.removeAll()
           self.selectedTipCat = tipCat
           getTipsByCatFromAPI(tipCat?.catName ?? "")
           view.endEditing(true)

       }
   }
   //MARK: - Table View  Data Source delegate
   extension TipsByMomsViewController : UITableViewDataSource{
       func numberOfSections(in tableView: UITableView) -> Int {
           return tipsArray.count
       }
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

           return 1
       }
       @objc func filterAction(_ sender: UIButton){
           self.view.endEditing(true)
           self.tipsCatTxt?.becomeFirstResponder()

       }
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           if indexPath.section == 0{
           let cell = tableView.dequeueReusableCell(withIdentifier: "TipsByMomHeaderTableViewCell") as! TipsByMomHeaderTableViewCell
             
                self.tipsCatTxt = cell.selectCatTxt
               if self.tipsIdentifier == "tips"{
                   cell.bindData(tipsCatArray)
               cell.tipCatDelegate = self
               cell.filterBtn.isHidden = false
               }else{

                   cell.bindData(tipsCatArray,true)
                 cell.filterBtn.isHidden = true
                 self.tipsCatTxt?.delegate = self
                  self.tipsCatTxt?.addTarget(self, action: #selector(self.searchTxtAction(_:)), for: UIControlEvents.editingChanged)
               }
               cell.headerLbl.text = "\(self.tipsIdentifier == "tips" ? "Tips" : "Stories") By Moms"
               cell.filterBtn.addTarget(self, action: #selector(filterAction(_:)), for: .touchUpInside)
               

           return cell
           }else{
               let cell = tableView.dequeueReusableCell(withIdentifier: "TipsByMomTableViewCell") as! TipsByMomTableViewCell
               if  tipsArray.count > 0 {
               let tip = tipsArray[indexPath.section]
               cell.bindData(tip)

                   
               cell.readMoreBtn.tag = indexPath.section
                 cell.btnLike.tag = indexPath.section
                   cell.facebookSharing.tag = indexPath.section
                   cell.waSharing.tag = indexPath.section
                   cell.mailSharing.tag = indexPath.section
               cell.readMoreBtn.accessibilityElements = [tip]
               cell.readMoreBtn.addTarget(self, action: #selector(readMoreTapped(_:)), for: .touchUpInside)
                
                
                if self.tipsIdentifier == "tips"{
                    cell.tipsImgView.image = UIImage(named:"tips_New")
                }else{
                    cell.tipsImgView.image = UIImage(named:"story")
                }
               // cell.tipsImgView.image = cell.tipsImgView.image?.imageWithColor(tintColor: Colors.MamyDarkBlue)
               }

             cell.TipsByMomTableViewCellProtocol_Delegate  = self
               return cell
           }
       }
       @objc func searchTxtAction(_ sender: UITextField){
         
           print(sender.text ?? "")
           self.isLoading = false
           self.pageOffset=1
           self.tipsArray.removeAll()
           self.getTipsByCatFromAPI("")
       }

       
       @objc func loadTable() {
           self.tipsTable.reloadData()
       }

       @objc func readMoreTapped(_ sender:UIButton){
           var tip = sender.accessibilityElements?.first as! TipModelElement
           tip.isReadMore = sender.isSelected
           tipsArray[sender.tag] = tip
           sender.isSelected = !sender.isSelected

           tipsTable.reloadData()
           tipsTable.scrollToRow(at: IndexPath.init(row: 0, section: sender.tag), at: .top, animated: false)
       }
   }

   //MARK: - Table View delegate
   extension TipsByMomsViewController : UITableViewDelegate{
       
       func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
           return section==0 ? 0 : 10
       }
       func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
           return UIView()
       }
   //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
   //        return 250
   //    }
      
   }
extension UIImage {
    func imageWithColor(tintColor: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
        tintColor.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}
