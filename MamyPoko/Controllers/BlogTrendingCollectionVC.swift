//
//  BlogTrendingCollectionVC.swift
//  MamyPoko
//
//  Created by Surbhi on 06/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import Foundation
import UIKit


protocol BlogDetailPageDelegate {
    // Classes that adopt this protocol MUST define
    // this method -- and hopefully do something in
    // that definition.
    func didselectArticle(forCategory: String, atindex : Int)
}

class BlogTrendingCollectionVC : UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var trendingCollection: UICollectionView!
    var collectionArray = [Any]()
    var typeClass = String()
    var delegate: BlogDetailPageDelegate?

    //MARK:- VIEW MEthods
    override func viewDidLoad() {
        super.viewDidLoad()
		self.BlockSideMenuSwipe()
        if self.collectionArray.count > 0{
            self.perform(#selector(reloadCollection), with: nil, afterDelay: 5.0)
        }
    }
    
    @objc func reloadCollection() {
        self.trendingCollection.reloadData()
    }
    
    //MARK:- Collection Flow Layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let ratio : CGFloat = 375/314
        let ht = (SCREEN_WIDTH - 20)/ratio
        return CGSize.init(width: (SCREEN_WIDTH - 20), height: ht)
    }
    
    
    //MARK:- CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrendingTopicsHomeCollectionCell", for: indexPath) as! TrendingTopicsHomeCollectionCell
        
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOpacity = 1.0
        cell.layer.shadowOffset = CGSize.zero
        cell.layer.shadowRadius = 1.0
        cell.layer.cornerRadius = 5
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(rect: cell.bounds).cgPath
        
        let diction = self.collectionArray[indexPath.item] as! Dictionary <String, Any>
        
        if diction["image"] as?  NSNumber == NSNumber.init(value: 0){
            
        }
        else{
            let poster = diction["image"] as?  String
            if poster != nil || (poster?.count)! > 0 {
                let posterurl = URL(string: poster!)
                cell.posterImage.kf.setImage(with: posterurl)
                //.image = img
            }
            else{
                // placeholder Image
            }
        }
        
        let userImg = diction["author_image"] as?  String
        if userImg != nil || (userImg?.count)! > 0 {
            let userImgurl = URL(string: userImg!)
            cell.userImage.kf.setImage(with: userImgurl)
            //.image = img
        }
        else{
            // placeholder Image
        }
        
        let userName = diction["author_name"] as?  String
        let dateStr = diction["topic_date"] as?  String
        let title = diction["title"] as?  String
        
        cell.titleLabel.text = title!
        cell.dateLabel.text = dateStr!
        cell.userName.text = userName!
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didselectArticle(forCategory: self.typeClass, atindex: indexPath.item)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }

}
