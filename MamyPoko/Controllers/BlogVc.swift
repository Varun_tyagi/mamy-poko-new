//
//  BlogVc.swift
//  MamyPoko
//
//  Created by Surbhi on 06/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import Foundation
import UIKit
import MXParallaxHeader

class BlogVc: UIViewController ,UIScrollViewDelegate, BlogDetailPageDelegate,MXScrollViewDelegate{
    
    var isBlog : Bool = false
    
    @IBOutlet weak var mainScroll: MXScrollView!
    @IBOutlet weak var mainScrollContentView: UIView!
    @IBOutlet weak var mainScrollContentHeight: NSLayoutConstraint!
    @IBOutlet weak var detailScrollContentHeight: NSLayoutConstraint!
    
    @IBOutlet weak var headerScroll: UIScrollView!
    @IBOutlet weak var detailScroll: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var detailScrollContentwidth: NSLayoutConstraint!
    
    @IBOutlet weak var headerLabel: UILabel!
    var lastContentOffset : CGPoint = CGPoint.init(x: 0, y: 0 )
    var headerArray = [String]()
    var dataArray = [Any]()
    //var indicatorLabel = UILabel()
    var ControllerArray = Array<Any>()
    var selectedType = String()
    var selectedIndex = Int()
    
    //MARK:- Start
    override func viewDidLoad() {
        super.viewDidLoad()
		self.BlockSideMenuSwipe()
        self.GetPostsForHomeVC(forType: self.selectedType)
        
        self.headerScroll.layer.shadowColor = UIColor.gray.cgColor
        self.headerScroll.layer.shadowOpacity = 1.0
        self.headerScroll.layer.shadowOffset = CGSize.zero
        self.headerScroll.layer.shadowRadius = 1.0
        self.headerScroll.layer.masksToBounds = false
        
        self.headerLabel.text = self.selectedType == "Blog" ? "Blog" : "Trending Topics"
        self.mainScroll.delegate = self
        self.mainScroll.parallaxHeader.view = self.headerLabel
       // self.mainScroll.isPagingEnabled=true
        self.mainScroll.parallaxHeader.height = 40
        self.mainScroll.parallaxHeader.minimumHeight = 0
        self.mainScroll.parallaxHeader.mode = MXParallaxHeaderMode.fill
    }
	
	override func viewWillAppear(_ animated: Bool) {
		
	}
    //MARK:- Action Methods
    @IBAction func BackButtonPressed(){
        
        if self.navigationController == nil{
            self.dismiss(animated: true, completion: nil)
        }
        else if (self.navigationController?.childViewControllers.count)! > 0 && (self.navigationController?.childViewControllers.last?.isKind(of: BlogVc.classForCoder()))!{
            let homeVc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            var navArr = self.navigationController?.viewControllers
            if (navArr?.contains(homeVc))!{
                self.navigationController?.popViewController(animated: true)
            }else{
                navArr?.append(homeVc)
                self.navigationController?.viewControllers = navArr!
                self.navigationController?.popToViewController(homeVc, animated: true)
            }
            // self.navigationController?.popViewController(animated: true)
        }
        else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func searchAction(_ sender: Any) {
        let searchVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        searchVc.isPresent = true
        self.present(searchVc, animated: false, completion: nil)
        
    }
    
    //MARK:- API
    func GetPostsForHomeVC(forType : String){
        if Helper.isConnectedToNetwork() {
            Constants.appDel.startLoader()
            
            let userID = UserDefaults.standard.string(forKey: Constants.USERID)
            
            if (userID == nil || userID?.count == 0) && forType == "Blog" {
                PKSAlertController.alert(Constants.appName, message: "Invalid User ID")
            }
            else{
                var url_str = Constants.BASEURL + MethodName.CatPost
                
                if forType == "MyInterest"{
                    url_str = url_str + "\(userID!)"
                }
                
                Server.getRequestWithURL(urlString: url_str, completionHandler: { (response) in
                    let responseDict = response
                    
                    if response != nil && (response?.count)! > 1 && (response?["status"] as? Int == 1 || response?["status"] as? String == "1") {
                        self.dataArray = responseDict!["data"] as! Array<Any>
                        
                        self.headerArray.removeAll()
                        for dc in self.dataArray{
                            let dict = dc as! Dictionary<String,Any>
                            let val = dict["category"] as! String
                            if !self.headerArray.contains(val){
                                self.headerArray.append(val)
                            }
                        }
                        
                        DispatchQueue.main.async {
                            if self.headerArray.count > 0{
                                self.setHeaderScroll()
                                self.setUpScrollView()
                                Constants.appDel.stopLoader()
                                self.setindexforScroll()
                            }
                        }
                    }else{
                        PKSAlertController.alert(Constants.appName, message: response?["message"] as? String ?? "Some Error Occurred, Please try again Later.")
                    }
                    
                })
            }
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
    
    
    //MARK:- Scroll
    func setindexforScroll(){
        let btn = UIButton.init(type: UIButtonType.custom)
        btn.tag = selectedIndex
        self.HeaderlabelSelected(sender: btn)
    }
    
    func setHeaderScroll()
    {
        
        var scrollContentCount = 0;
        var frame : CGRect = CGRect.zero
        var labelx : CGFloat = 2.0
        
        for arrayIndex in 0 ..< headerArray.count
        {
            frame.origin.x = labelx
            // set scorllview properties
            frame.origin.y = 0
            frame.size.width=ScreenSize.SCREEN_WIDTH / 3.5;
            frame.size.height=45
            
            let accountLabelButton = UIButton.init(type: UIButtonType.custom)
            accountLabelButton.backgroundColor = UIColor.clear
            accountLabelButton.addTarget(self, action:#selector(HeaderlabelSelected), for: UIControlEvents.touchUpInside)
            
            accountLabelButton.tag = arrayIndex
            
            accountLabelButton.titleLabel?.adjustsFontSizeToFitWidth = true
            accountLabelButton.titleLabel?.lineBreakMode = NSLineBreakMode.byClipping
            accountLabelButton.titleLabel?.minimumScaleFactor = 0.5
            accountLabelButton.titleLabel?.layer.masksToBounds  = true
            
            //accountLabelButton.backgroundColor = arrayIndex.quotientAndRemainder(dividingBy: 2).remainder == 0 ? UIColor.red : UIColor.green
            accountLabelButton.titleLabel?.numberOfLines = 1
            let heading = headerArray[arrayIndex]
            
            let   indicatorLabel = UILabel.init(frame: CGRect.init(x: labelx, y: 45, width: frame.size.width, height: 3))
            indicatorLabel.text = "";
            indicatorLabel.tag = 1000+arrayIndex
            
            headerScroll.addSubview(accountLabelButton)
            headerScroll.addSubview(indicatorLabel)
            
            if arrayIndex == 0
            {
                accountLabelButton.setAttributedTitle(NSAttributedString(string: "  \(heading.uppercased())  ",  attributes: [NSAttributedStringKey.font : UIFont.init(name: FONTS.Quicksand_Medium, size: 16.5)!, NSAttributedStringKey.foregroundColor : UIColor.black]), for: UIControlState.normal)
                indicatorLabel.backgroundColor = Colors.MAMYBlue
                
                accountLabelButton.alpha=1.0;
            }
            else
            {
                accountLabelButton.setAttributedTitle(NSAttributedString(string: "  \(heading.uppercased())  ",  attributes: [NSAttributedStringKey.font : UIFont.init(name: FONTS.Quicksand_Medium, size: 16.5)!, NSAttributedStringKey.foregroundColor : UIColor.init(red: 76.0/255.0, green: 82.0/255.0, blue: 84.0/255.0, alpha: 1.0)]), for: UIControlState.normal)
                accountLabelButton.alpha=0.6;
                indicatorLabel.backgroundColor = UIColor.clear
                
            }
            
            accountLabelButton.titleLabel?.textAlignment = NSTextAlignment.center
            frame.size.width =  ((accountLabelButton.titleLabel?.attributedText?.size().width)!+2)
            accountLabelButton.frame = frame
            indicatorLabel.frame = CGRect.init(x: labelx, y: 45, width: frame.size.width, height: 3)
            labelx=labelx+frame.size.width;
            scrollContentCount = scrollContentCount + 1;
        }
        
        headerScroll.contentSize = CGSize.init(width: labelx+2 , height: 50)
        automaticallyAdjustsScrollViewInsets = false;
    }
    
    @objc func HeaderlabelSelected(sender: UIButton)
    {
        let buttonArray = NSMutableArray()
        let indicatorLblArray=NSMutableArray()
        let _ : CGFloat = sender.frame.origin.x
        
        for view in headerScroll.subviews
        {
            if view.isKind(of: UIButton.classForCoder())
            {
                let labelButton = view as! UIButton
                buttonArray.add(labelButton)
                let heading = headerArray[labelButton.tag]
                
                if labelButton.tag == sender.tag {
                    labelButton.setAttributedTitle(NSAttributedString(string: "  \(heading.uppercased())  ",  attributes: [NSAttributedStringKey.font : UIFont.init(name: FONTS.Quicksand_Medium, size: 16.5)!, NSAttributedStringKey.foregroundColor :UIColor.black]), for: UIControlState.normal)
                    labelButton.alpha = 1.0
                }
                else
                {
                    labelButton.setAttributedTitle(NSAttributedString(string: "  \(heading.uppercased())  ",  attributes: [NSAttributedStringKey.font : UIFont.init(name: FONTS.Quicksand_Medium, size: 16.5)!, NSAttributedStringKey.foregroundColor : UIColor.init(red: 76.0/255.0, green: 82.0/255.0, blue: 84.0/255.0, alpha: 1.0)]), for: UIControlState.normal)
                    labelButton.alpha = 0.6
                }
            }
            
            if view.isKind(of: UILabel.classForCoder()){
                let indicatorllbbll  = view as! UILabel
                indicatorLblArray.add(indicatorllbbll)
                if indicatorllbbll.tag == sender.tag+1000{
                    indicatorllbbll.backgroundColor = Colors.MAMYBlue
                }else{
                    indicatorllbbll.backgroundColor = UIColor.clear

                }
            }
        }
        
        if sender.tag <= headerArray.count{
        let indicatorLabel = indicatorLblArray[sender.tag] as? UILabel
        
        UIView.animate(withDuration: 0.2, animations: {
            self.detailScroll.setContentOffset(CGPoint.init(x: ScreenSize.SCREEN_WIDTH * CGFloat(sender.tag), y: 0), animated: true)
            self.headerScroll.scrollRectToVisible((indicatorLabel?.frame)!, animated: false)
        }, completion: nil)
        }
    }
    
    // MARK: -  Main Scrollview
    func setUpScrollView() {//
        
        for  arrayIndex in headerArray {
            
            let index : Int = headerArray.index(of: arrayIndex)!
            let arrayindexVC = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "BlogTrendingCollectionVC") as! BlogTrendingCollectionVC
            arrayindexVC.typeClass = arrayIndex
            arrayindexVC.delegate = self
            
            let postDict = self.dataArray[index] as! Dictionary<String,Any>
            let postArray = postDict["post"] as! Array<Any>
            
            arrayindexVC.collectionArray = postArray
            ControllerArray.insert(arrayindexVC, at: index)
        }
        setViewControllers(viewControllers: ControllerArray as NSArray, animated: false)
    }
    
    //  Add and remove view controllers
    
    func setViewControllers(viewControllers : NSArray, animated : Bool) {
        if self.childViewControllers.count > 0 {
            for vC in self.childViewControllers {
                vC.willMove(toParentViewController: nil)
                vC.removeFromParentViewController()
            }
        }
        
        for vC in viewControllers {
            
            self.addChildViewController(vC as! UIViewController)
            (vC as! UIViewController).didMove(toParentViewController: self)
        }
        
        //TODO animations
        if ((detailScroll) != nil) {
            reloadPages()
        }
    }
    
    func reloadPages() {
        var scrollContentCount = 0
        
        for arrayIndex in 0 ..< headerArray.count {
            // set scorllview properties
            var frame : CGRect = CGRect.init(x: 0, y: 0, width: self.detailScrollContentwidth.constant  , height: self.detailScrollContentHeight.constant)
            frame.origin.x = ScreenSize.SCREEN_WIDTH * CGFloat(arrayIndex)
            frame.origin.y = 0;
            frame.size = CGSize.init(width: ScreenSize.SCREEN_WIDTH, height: detailScroll.frame.size.height)
            
            detailScroll.isPagingEnabled = true
            let contentWidth : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(scrollContentCount+1)
            detailScrollContentwidth.constant = contentWidth
            
            let vC = self.childViewControllers[arrayIndex]
            self.addView(view: vC.view, contentView: contentView, frame: frame)
            
            scrollContentCount = scrollContentCount + 1
        }
        
        detailScroll.contentSize = CGSize.init(width: detailScrollContentwidth.constant-SCREEN_WIDTH, height: detailScroll.frame.size.height)
        detailScroll.delegate = self
    }
    
    // adding views to scrollview
    func addView(view : UIView, contentView : UIView, frame : CGRect)  {
        view.frame = frame
        view.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(view)
        
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: contentView, attribute:  NSLayoutAttribute.leading, multiplier: 1.0, constant: frame.origin.x))
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute:  NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: frame.size.width))
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: contentView, attribute:  NSLayoutAttribute.top, multiplier: 1.0, constant:0))
        
        contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute:  NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: frame.size.height))
    }
    
    
    
    //MARK:- Scroll View Delegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    // self.mainScroll.setContentOffset(CGPoint.init(x: 0, y: -40), animated: false)
        if scrollView == detailScroll{
                // update the new position acquired
                self.lastContentOffset = scrollView.contentOffset
                let tag = Int(scrollView.contentOffset.x / SCREEN_WIDTH)
                var btn = UIButton.init(type: UIButtonType.custom)
                btn.tag = tag
                for view in headerScroll.subviews{
                    if let btnx = view as? UIButton{
                        if btnx.tag == tag{
                            btn = btnx
                            break
                        }
                    }
                }
            if tag < headerArray.count {
                 self.HeaderlabelSelected(sender: btn)
            }
            
           
        }
        

    }
    
    //MARK:- Delegate Method
    func didselectArticle(forCategory: String, atindex: Int) {
        
        let index : Int = headerArray.index(of: forCategory)!
        let postDict = self.dataArray[index] as! Dictionary<String,Any>
        let postArray = postDict["post"] as! Array<Any>
        
        let detailvc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ArticleDetailVC") as! ArticleDetailVC
        let diction = postArray[atindex] as! Dictionary <String, Any>
        let title = "\(diction["ID"] as! NSNumber)"
        detailvc.postID = title
        
        if (title.count) == 0{
            // ignore
        }
        else{
            detailvc.postID = title
            DispatchQueue.main.async {
                self.present(detailvc, animated: true, completion: nil)
            }
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }

}

