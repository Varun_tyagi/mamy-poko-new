//
//  SearchViewController.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 07/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import AppsFlyerLib

let KPostId = "post_id"
let KTitle = "title"
let KsearchHistory = "searchHistory"
let KSearchPlistName = "SearchList"

class SearchViewController: UIViewController {

    @IBOutlet weak var searchTbl: UITableView!
    @IBOutlet weak var searchTxt: UITextField!
    var isHistory = true
    var isPresent = false
    var trendingArray : [[String:Any]] = []
    var historyArray : [[String:Any]] = []

    override func viewDidLoad() {
        super.viewDidLoad()
		self.BlockSideMenuSwipe()
		setupView()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        view.endEditing(true)
    }
	
	override func viewWillAppear(_ animated: Bool) {
//		if let statusbar = UIApplication.shared.value(forKey: "statusBarManager") as? UIView {
//			statusbar.backgroundColor = UIColor.white
//		}
		if #available(iOS 13.0, *) {
			let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
			statusBar.backgroundColor = UIColor.clear
			UIApplication.shared.keyWindow?.addSubview(statusBar)
		} else {
			 UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
		}

	}
	
    func setupView()
    {
		
        searchTxt.becomeFirstResponder()
        searchTbl.tableFooterView = UIView()
        if let dict =   readPlist(namePlist: KSearchPlistName, key: KsearchHistory) as? [[String:Any]] {
            historyArray = dict
            searchTbl.reloadData()
        }
    }

    func readPlist(namePlist: String, key: String) -> Any?{
        let fileManager = FileManager.default
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let path = documentDirectory.appending("/\(namePlist).plist")
        var output:Any!
        if(fileManager.fileExists(atPath: path)){
            if let dict = NSMutableDictionary(contentsOfFile: path){
                output = dict.object(forKey: key)
            }
        }
        return output
    }
    func writePlist(namePlist: String, key: String, data: Any){
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let path = documentDirectory.appending("/\(namePlist).plist")
        let dictNew:NSDictionary = [key:data]
        let someData = NSDictionary(dictionary: dictNew)
        _ = someData.write(toFile: path, atomically: true)
    }

    func noDuplicates(_ arrayOfDicts: [[String: Any]]) -> [[String: Any]] {
        var uniqueArray = [[String: Any]]()
        for item in arrayOfDicts {
            let exists =  uniqueArray.contains{ element in
                return (element[KPostId] as? Int) == (item[KPostId] as? Int)
            }
            if !exists {
                uniqueArray.append(item)
            }
        }
        return uniqueArray
    }
    //MARK: - Action
    
    @objc func clearHistoryAction(_ sender : UIButton){
        historyArray.removeAll()
        searchTbl.reloadData()
        writePlist(namePlist: KSearchPlistName, key: KsearchHistory, data: [])
    }

    @IBAction func backAction(_ sender: Any) {
        if isPresent{
            dismiss(animated: true, completion: nil)
            return
        }
self.navigationController?.popViewController(animated: true)
    }
//    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }

}
extension SearchViewController {
    //"webservices/posts/search_result/"+strSearch

    fileprivate  func getTrendingFromAPI(_ searchKey:String) {
        if Helper.isConnectedToNetwork() {
            Constants.appDel.startLoader()
            
            Analytics.logEvent(AnalyticsEventSearch, parameters: [
                AnalyticsParameterItemID: "id-\(searchKey)",
                AnalyticsParameterItemName: searchKey,
                AnalyticsParameterContentType: "SearchKey"
                ])
            AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.SearchView,
                                                 withValues: [
                                                    AFEventParamContent: AppsFlyerConstant.SearchView,
                                                    AFEventParamContentId: "32"
                ])
            
            if searchKey.count == 0 {
                Constants.appDel.stopLoader()
            }
            let utf8str = searchKey.data(using: String.Encoding.utf8)
            var tipCatEncoded : String = ""
            
            if let base64Encoded = utf8str?.base64EncodedString()
            {

                tipCatEncoded = base64Encoded
                if let base64Decoded = NSData(base64Encoded: base64Encoded, options:   NSData.Base64DecodingOptions(rawValue: 0))
                    .map({ NSString(data: $0 as Data, encoding: String.Encoding.utf8.rawValue) })
                {

                }
            }
            let url_str = Constants.BASEURL + MethodName.GET_SearchResult + "\(tipCatEncoded)"

            Server.getRequestWithURL(urlString: url_str, completionHandler: { (response) in
                Constants.appDel.stopLoader()
                let message =  response?["message"] as? String
                if !(response == nil) {
                  if response?["status"] as? Int ==  1  || response?["status"] as? String == "1" {
                    let   data = response? ["data"] as? [[String:Any]]
                    DispatchQueue.main.async {
                        if let theJSONData = try? JSONSerialization.data(
                            withJSONObject: data ?? [[:]],
                            options: []) {
                            _ = String(data: theJSONData, encoding: .ascii)
                            if (data?.isEmpty)!{
                                self.trendingArray.removeAll()
                            }else{
                                self.trendingArray.removeAll()
                                self.trendingArray = data!
                               self.isHistory=false
                            }

                            self.searchTbl.reloadData()

                        }
                    }
                }else{
                    PKSAlertController.alert(Constants.appName, message: message ?? "Something went wrong! Please try again")
                }
            }else{
                
                }
                DispatchQueue.main.async {
                    self.searchTbl.reloadData()
                }

            })
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
}
extension SearchViewController : UITextFieldDelegate {

        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            if string.isEmpty{
                isHistory=true
                searchTbl.reloadData()
            }

            return true
        }

        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            if (textField.text?.isEmpty)!{
                return false
            }
            else{
                getTrendingFromAPI(textField.text!)
            }
            if !(trendingArray.count == 0){
            isHistory = false
            searchTbl.reloadData()
            }
            self.view.endEditing(true)
            return true
        }
    }
extension SearchViewController : UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isHistory ? historyArray.count : trendingArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell")
        let cellData  = isHistory ? historyArray[indexPath.row] : trendingArray[indexPath.row]
        cell?.imageView?.image = isHistory ?  #imageLiteral(resourceName: "history") : #imageLiteral(resourceName: "trending")
        cell?.textLabel?.text = (cellData["title"] as? String ) ?? ""
        return cell!
    }

}
extension SearchViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !isHistory{
        let dictToSave = trendingArray[indexPath.row]
            historyArray.append(dictToSave)
        }

        writePlist(namePlist: KSearchPlistName, key: KsearchHistory, data: noDuplicates(historyArray))

        let detailvc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ArticleDetailVC") as! ArticleDetailVC
        let cellData  = isHistory ? historyArray[indexPath.row] : trendingArray[indexPath.row]

        detailvc.postID = "\(cellData[KPostId] ?? "0")"
        DispatchQueue.main.async {
            self.present(detailvc, animated: true, completion: nil)
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return isHistory ? (historyArray.count==0 ? 0 : 44) : 44
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if isHistory{
            let view =  UIButton.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.size.width, height: 44))
            let btnClearHistory = UIButton.init(frame: CGRect.init(x: 10, y: 0, width: tableView.frame.size.width, height: 44))
            btnClearHistory.setTitle("Clear History", for: .normal)
            btnClearHistory.setImage( #imageLiteral(resourceName: "close"), for: .normal)
            btnClearHistory.setTitleColor(UIColor.darkGray, for: .normal)
        btnClearHistory.contentHorizontalAlignment = .left
            btnClearHistory.titleEdgeInsets = UIEdgeInsetsMake(0, 30, 0, 0)
            btnClearHistory.addTarget(self, action: #selector(clearHistoryAction(_:)), for: .touchUpInside)
            view.addSubview(btnClearHistory)
            return view
        }
        return UIView()
    }
}





