//
//  HomeVC.swift
//  MamyPoko
//
//  Created by Surbhi on 02/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import Foundation
import UIKit
import HTagView
import Kingfisher
import FirebaseAnalytics
import AppsFlyerLib
import Mixpanel
let HomePageDataRefreshNotification = "HomePageDartaRefresh"

class HomeVC : UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UINavigationControllerDelegate, UITextFieldDelegate {
    
    //MARK:- Decleration IBOutlet
    @IBOutlet weak var homeTabel: UITableView!
	@IBOutlet weak var profileButton: RoundButton!
	@IBOutlet weak var myworldLabel: UILabel!

    @IBOutlet weak var homeTblHeaderView: UIView!
    
    @IBOutlet weak var profileImageButton : RoundButton!
	
	@IBOutlet weak var wishPokoChanBannerImg: UIImageView!
	//@IBOutlet weak
    var trendingCollection: UICollectionView!
    //@IBOutlet weak
    var myTrendingCollection: UICollectionView!
	
	var intrestTagView: HTagView!
    var babyFormUrl:String = ""
    @IBOutlet weak var planningBabyTrendingCollection: UICollectionView!

    var isPokoChainBirthday = true
    
    @IBOutlet weak var heightBirthdayBashBanner: NSLayoutConstraint!  // 110 or 0
    //Pragnency Details / status
    var firstDeliveryBtn: UIButton!
    var expectedDeliveryDateTxt, lastPeriodDateTxt: RoundTextField!
    var changePragStatusBtn:UIButton?
    
    //@IBOutlet weak
    var myUploadedPictures: UICollectionView!
	var intrestArray:IntrestModel = []
    var myInterestTrendingArray = [Any]()
    var trendingTopicsArray = [Any]()
    var myWorldDictionary = [String : Any]()
    var babyGrowthDict = [String : Any]()
    var babyMonthlyGrowthDict: [String:Any] = [:]
    var parentalStatus = String()
    var myWorldPostDict = [String : Any]()
    var uploadedImgArray = [Any]()
    var imagePicker = UIImagePickerController()
    var flagBabyArrived = false

    //ImagePickerView
    @IBOutlet weak var imageUploadView: UIView!
    @IBOutlet weak var imageTitle: UITextField!
    @IBOutlet weak var imageSelectedView: UIImageView!

    var imageSelected : UIImage!
    var imageSelectedData : NSData!
    var flagRemainingWeekGreater = false
    var  userData : UserDataModel? //  for user profile
    
    var datePicker:UIDatePicker = UIDatePicker()
    let toolBar = UIToolbar()
    var anotherAlert = UIAlertController()
    
    //MARK:- View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
	
		wishPokoChanBannerImg.loadGif(name: "poko_chan_wish_gif_ios")
        Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
			AnalyticsParameterItemID: "id-\(AppsFlyerConstant.MyWorldVisit)",
            AnalyticsParameterItemName: "\(AppsFlyerConstant.MyWorldVisit)",
            AnalyticsParameterContentType: "My_World"
            ])
		
		profileImageButton.cornerRadius = 20
		profileImageButton.borderColor = UIColor.clear

		NotificationCenter.default.addObserver(self, selector: #selector(ChangeParantalStatusPopup), name: NSNotification.Name(rawValue: "pregnancy_after_36_weeks_users"), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(BabyGrowthTrackerNotificationAction(_:)), name: NSNotification.Name(rawValue: "baby_month_change"), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(BabyGrowthTrackerNotificationAction(_:)), name: NSNotification.Name(rawValue: "baby_growth_details"), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(BabyGrowthTrackerNotificationAction(_:)), name: NSNotification.Name(rawValue: "vaccination_reminder"), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(BabyGrowthTrackerNotificationAction(_:)), name: NSNotification.Name(rawValue: "vaccination_done_reminder"), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(BabyGrowthTrackerNotificationAction(_:)), name: NSNotification.Name(rawValue: "prompt_vaccination_tracker"), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(BabyGrowthTrackerNotificationAction(_:)), name: NSNotification.Name(rawValue: "preg_nb_product_notification"), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(BabyGrowthTrackerNotificationAction(_:)), name: NSNotification.Name(rawValue: "baby_size_switch_notification"), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(BabyGrowthTrackerNotificationAction(_:)), name: NSNotification.Name(rawValue: "offer_registration"), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(BabyGrowthTrackerNotificationAction(_:)), name: NSNotification.Name(rawValue: "BlogDetailPage"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callApiGetProfile), name: NSNotification.Name(rawValue: HomePageDataRefreshNotification), object: nil)
        callApiGetProfile()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
	if #available(iOS 13.0, *) {
		let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
		statusBar.backgroundColor = Colors.MAMYBlue
		 UIApplication.shared.keyWindow?.addSubview(statusBar)
	} else {
		 UIApplication.shared.statusBarView?.backgroundColor = Colors.MAMYBlue
	}
	
}

	override func viewDidAppear(_ animated: Bool) {
		self.AllowSideMenuSwipe()
		AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.MyWorldVisit,
											 withValues: [
												AFEventParamContent: AppsFlyerConstant.MyWorldVisit,
												AFEventParamContentId: "25"
			])
	}
    
    
    //MARK:- Custom Actions

    fileprivate func CheckBirthdayBashPopup(){
        if isPokoChainBirthday {
            if UserDefaults.standard.value(forKey: Constants.UserDefaultsConstant.isFirstLogin) as? Bool ==  nil {
                let popupVc = Constants.mainStoryboard3.instantiateViewController(withIdentifier: "BirthdayBashPopUpVC") as! BirthdayBashPopUpVC
                popupVc.participateCallBack = {(dataReturned) -> ()in
                    UserDefaults.standard.set(false, forKey: Constants.UserDefaultsConstant.isFirstLogin)
                    if dataReturned == KparticipateAction{
                        self.birthdayBashAction(UIButton())
                    }
                }
                self.addChildVC(popupVc)
            }
        }
    }
	//MARK:- API Methods
    @objc func callApiGetProfile()
    {
        if Helper.isConnectedToNetwork() {
            Constants.appDel.startLoader()
            let url_str = Constants.BASEURL + MethodName.UserProfile + "\(UserDefaults.standard.value(forKey: Constants.USERID) ?? "0")"
            Server.getRequestWithURL(urlString: url_str, completionHandler: { (response) in
                Constants.appDel.stopLoader()
                DispatchQueue.main.async {
                    if !(response==nil){
                        if (response?.count)! > 1 && response?["status"] as! Int == 1 || response?["status"] as? String == "1"{
                            let data = response?["data"] as Any
                            var userDataDict = data as! [String:Any]
                            userDataDict["name"] =  userDataDict["user_name"]
                            
                            Helper.saveDataInNsDefault(object: userDataDict as AnyObject, key: Constants.PROFILE)
                            NotificationCenter.default.post(name: NSNotification.Name.init(rawValue:  Constants.KUpdateProfileNotification), object: nil)
                           
                            self.userData = try? UserDataModel.init(userDataDict.toJsonString()!)
                            self.parentalStatus = userDataDict["parental_status"] as? String ?? "Pregnancy"

							Helper.GetProfileImageForButton(butName: self.profileImageButton)
							
							self.myworldLabel.isHidden = false
							
							if self.parentalStatus == "planning-baby" {
								self.myworldLabel.text = "My Planning"
                                self.GetPostsForHomeVC()
                            }
							else if self.parentalStatus == "mother"{
								self.myworldLabel.text = "My Baby"
								self.GetPostsForHomeVC()
							}
                            else{
								self.myworldLabel.text = "My Pregnancy"
                                self.GetPostsForHomeVC()
                            }
                        }
                        else{
                            PKSAlertController.alert(Constants.appName, message: response?["message"] as! String)
                        }
                    }
                    else{
                        PKSAlertController.alert(Constants.appName, message: "Some error has occured! Please try again")
                    }
                }
            })
        }
        else{
            PKSAlertController.alertForNetwok()
        }
    }
    
    func babyArrivedTrackPostAPI(_ babyDetails:String = ""){
        self.view.endEditing(true)
        if Helper.isConnectedToNetwork() {
            let urlstr = Constants.BASEURL + MethodName.BabyTrackDetails
            let userID = UserDefaults.standard.string(forKey: Constants.USERID)
            
            if userID == nil || userID?.count == 0 {
                PKSAlertController.alert(Constants.appName, message: "Invalid User ID")
            }
            else{
                Constants.appDel.startLoader()
               
                let paramStr = babyDetails
                Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
                    let responseDict = response as Dictionary<String, Any>
                    Constants.appDel.stopLoader()
                    if responseDict["status"] as? Int == 1  || responseDict["status"] as? String == "1" {
                        DispatchQueue.main.async {
                            self.callApiGetProfile()
                        }
                    }
                    else if responseDict["status"] as! Int == 0 {
                        DispatchQueue.main.async {
                            PKSAlertController.alert("Error", message: responseDict["message"] as? String ?? KSomeErrorTryAgain)
                        }
                    }
                    else{
                        PKSAlertController.alert("Error", message: responseDict["message"] as? String ?? KSomeErrorTryAgain)
                        
                    }
                }
            }
            
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
    func babyHasNotArrivedPostAPI(_ babyDetails:String = ""){
        self.view.endEditing(true)
        if Helper.isConnectedToNetwork() {
            
            let urlstr = Constants.BASEURL + MethodName.BabyHasnotArrived
            let userID = UserDefaults.standard.string(forKey: Constants.USERID)
            
            if userID == nil || userID?.count == 0 {
                PKSAlertController.alert(Constants.appName, message: "Invalid User ID")
            }
            else{
                Constants.appDel.startLoader()
                let paramStr = "user_id=\(userID!)&delivery=\(self.flagBabyArrived==true ? 1 : 2)&delivery_date=\( self.flagBabyArrived==true ? "" :  self.anotherAlert.textFields![0].text ?? "")"
            Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
                    let responseDict = response as Dictionary<String, Any>
                    Constants.appDel.stopLoader()
                    if responseDict["status"] as? Int == 1  || responseDict["status"] as? String == "1" {
                        DispatchQueue.main.async {
                            if self.flagBabyArrived{
                            // hit another service for baby arrived
                        		self.babyArrivedTrackPostAPI(babyDetails)
                            }
							else{
                                self.callApiGetProfile()
                            }
                        }
                    }
                    else if responseDict["status"] as! Int == 0 {
                        DispatchQueue.main.async {
                            PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                        }
                    }
                }
            }
            
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
    
    func BottomDataPostAPI(){
        if Helper.isConnectedToNetwork() {
            
            let urlstr = Constants.BASEURL + MethodName.homeScreenPtracker
            let userID = UserDefaults.standard.string(forKey: Constants.USERID)
            
            if userID == nil || userID?.count == 0 {
                PKSAlertController.alert(Constants.appName, message: "Invalid User ID")
            }
            else{
                Constants.appDel.startLoader()
                let paramStr = "user_id=\(userID!)"
                Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
                    let responseDict = response as Dictionary<String, Any>
                    Constants.appDel.stopLoader()
                    if responseDict["status"] as? Int == 1 || responseDict["status"] as? String == "1"{
                         self.intrestArray = []
                        self.myWorldDictionary = responseDict
                        if let areaOfIntrest = responseDict["area_of_interest"] as? [[String:Any]] {
                            if areaOfIntrest.count > 0{
                                self.intrestArray = try! IntrestModel.init(areaOfIntrest.json)
                            }
                        }
                        if let babyURl = responseDict["pokochain"] as? String{
                            if  babyURl.count > 0 {
                                self.babyFormUrl = babyURl
    UserDefaults.standard.set(true, forKey: Constants.UserDefaultsConstant.isBirthdayBashSubmitted)
                            }
                        }
                        
                        
                        if response["quiz_status"] as? Int == 1 || response["quiz_status"] as? String == "1"
                        {
            UserDefaults.standard.set(true, forKey: Constants.UserDefaultsConstant.isQuizSubmitted)
                        }else{
            UserDefaults.standard.set(false, forKey: Constants.UserDefaultsConstant.isQuizSubmitted)
                        }
                        
                        if self.parentalStatus.lowercased() == "planning-baby" {
                            self.trendingTopicsArray = responseDict["posts"]  as? Array ?? []
                        }
                        else if self.parentalStatus.lowercased() == "mother"{
                            self.babyMonthlyGrowthDict = responseDict["posts"]  as? [String:Any] ?? [:]
                            self.myWorldPostDict = responseDict["posts"] as! Dictionary<String, Any>
                            self.babyGrowthDict = responseDict["babygrowh"] as? Dictionary<String, Any> ?? [:]
                            self.uploadedImgArray = (self.myWorldPostDict["images"] as? Array<Any>) ?? []
                        }
                        else{
                            self.myWorldPostDict = responseDict["posts"] as! Dictionary<String, Any>
                            self.babyGrowthDict = responseDict["babygrowh"] as? Dictionary<String, Any> ?? [:]
                            self.uploadedImgArray = (self.myWorldPostDict["images"] as? Array<Any>) ?? []
                        }

                        DispatchQueue.main.async {
                            
                            if self.parentalStatus == "planning-baby" {//|| self.parentalStatus.lowercased() == "mother"{
                                self.homeTabel.isHidden = true
                                self.planningBabyTrendingCollection.isHidden = false
                                self.planningBabyTrendingCollection.reloadData()
                            }
                            else if self.parentalStatus.lowercased() == "mother"{
                                self.planningBabyTrendingCollection.isHidden = true
                                self.homeTabel.isHidden = false
                                self.view.layoutIfNeeded()
                                self.homeTabel.reloadData()
                                if self.trendingCollection != nil {
                                self.trendingCollection.reloadData()
                                }
                                if self.myTrendingCollection != nil {
                                self.myTrendingCollection.reloadData()
                                }
                                self.myUploadedPictures.reloadData()
                                self.setUpViewForRemainingDays()
                                self.CheckBirthdayBashPopup()
                            }
                            else {
                                self.planningBabyTrendingCollection.isHidden = true
                                self.homeTabel.isHidden = false
                                self.view.layoutIfNeeded()
                                self.homeTabel.reloadData()
                                if self.trendingCollection != nil {
                                    self.trendingCollection.reloadData()
                                }
                                if self.myTrendingCollection != nil {
                                    self.myTrendingCollection.reloadData()
                                }
                                self.myUploadedPictures.reloadData()
                                self.setUpViewForRemainingDays()
                                self.CheckBirthdayBashPopup()

                            }
                        }
                    }
                    else if responseDict["status"] as? Int == 0 || responseDict["status"] as? String == "0" {
                        DispatchQueue.main.async {
                            PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                        }
                    }
                }
            }
        }
        else{
            PKSAlertController.alertForNetwok()
        }
    }
    
    
    func GetPostsForHomeVC(){
        if Helper.isConnectedToNetwork() {
            Constants.appDel.startLoader()
            
            let userID = UserDefaults.standard.string(forKey: Constants.USERID)
            
            if userID == nil || userID?.count == 0 {
                PKSAlertController.alert(Constants.appName, message: "Invalid User ID")
            }
            else{
                let url_str = Constants.BASEURL + MethodName.homeScreenPosts + "\(userID!)"
                Server.getRequestWithURL(urlString: url_str, completionHandler: { (response) in
                    Constants.appDel.stopLoader()
                    if !(response == nil) {
                    if (response?.count)! > 1 && response?["status"] as! Int == 1 {
                        let data = response?["data"] as! Dictionary <String, Any>
                        self.trendingTopicsArray = data["trending_topics"] as! Array
                        self.myInterestTrendingArray = data["trending_interest"] as! Array
                        DispatchQueue.main.async {
                            self.myUploadedPictures.reloadData()
                            self.BottomDataPostAPI()
                        }
                    }else{
                        PKSAlertController.alert(Constants.appName, message: response?["message"] as! String)
                    }
                    }else{
                        //PKSAlertController.alert(Constants.appName, message: response?["message"] as! String)
                        DispatchQueue.main.async {
                            self.myUploadedPictures.reloadData()
                            self.BottomDataPostAPI()
                        }
                    }

                })
            }
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
    
    
    func UploadImageToServerwithData(_ imageUser:UIImage ,imageData : String, titleSTR : String){
        DispatchQueue.main.async {
        self.view.endEditing(true)
        }
        if Helper.isConnectedToNetwork() {
            
            let urlstr = Constants.BASEURL + MethodName.UploadBumpImage
            let userID = UserDefaults.standard.string(forKey: Constants.USERID)
            
            if userID == nil || userID?.count == 0 {
                PKSAlertController.alert(Constants.appName, message: "Invalid User ID")
            }
            else{
                Constants.appDel.startLoader()
                let base64Str = "\(imageUser.base64(format: ImageFormat.png) ?? "")"
                let parameters = [
                    "user_id": "\(userID ?? "0")",
                    "title":"\(titleSTR)",
                    "image":base64Str
                ]

                Server.postImageUpload(urlstr, imageUser , parameters){ (response) in
                    let responseDict = response as Dictionary<String, Any>
                    Constants.appDel.stopLoader()
                    let message = responseDict["message"] as! String
                    if responseDict["status"] as? Int == 1  || responseDict["status"] as? String == "1"
                    {
                        DispatchQueue.main.async {
                            self.CancelImageUploadPressed()
                            let alert = UIAlertController(title: "Success", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            let acceptButton = UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction) in
                                self.BottomDataPostAPI()
                            })
                            alert.addAction(acceptButton)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    else if  responseDict["status"] as? Int == 0  || responseDict["status"] as? String == "0" {
                        DispatchQueue.main.async {
                            PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                        }
                    }
                    else{
                        PKSAlertController.alert("Error", message: responseDict["message"] as! String)

                    }
                }

            }
            
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
    
    func RemoveImagefromSeverForItem(itemID : Int){
        if Helper.isConnectedToNetwork() {
            
            let urlstr = Constants.BASEURL + MethodName.RemoveBumpImage
            let userID = UserDefaults.standard.string(forKey: Constants.USERID)
            
            if userID == nil || userID?.count == 0 {
                PKSAlertController.alert(Constants.appName, message: "Invalid User ID")
            }
            else{
                Constants.appDel.startLoader()
                let paramStr = "user_id=\(userID!)&image_id=\(itemID)"
                
                Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
                    let responseDict = response as Dictionary<String, Any>
                    let message = responseDict["message"] as! String
                    Constants.appDel.stopLoader()
                    
                    if responseDict["status"] as! Int == 1 {
                        
                        self.CancelImageUploadPressed()
                        
                        let alert = UIAlertController(title: "Success", message: message, preferredStyle: UIAlertControllerStyle.alert)
                        let acceptButton = UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction) in
                            DispatchQueue.main.async {
                                self.BottomDataPostAPI()
                            }
                        })
                        alert.addAction(acceptButton)
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    else{
                        PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                        
                    }
                }
            }
            
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
    
    //MARK:- Set Up If Remaining Days are 0
   @objc  func ChangeParantalStatusPopup() {
        // pop up
        let alert = UIAlertController.init(title: Constants.appName, message: "Waiting for your little one to be welcomed into this world? This truly is going to be an amazing conclusion to that 10 month journey of yours!", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction.init(title: "My Child has arrived", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) in
            self.flagBabyArrived = true
            AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.ChildHasArrivedClicked,
                                                 withValues: [
                                                    AFEventParamContent: AppsFlyerConstant.ChildHasArrivedClicked,
                                                    AFEventParamContentId: "32"
                ])
            DispatchQueue.main.async {
                let popvc =  Constants.mainStoryboard2.instantiateViewController(withIdentifier: "BabyArrivedPopViewController") as! BabyArrivedPopViewController
                popvc.didSaveBabyData = { [weak self](item) in
                    if self != nil {
                        // hit service for baby not arrived with flag arrived = true delicvery type = 2
                        DispatchQueue.main.async {
                            popvc.removeChildVC()
                            if !(item.isEmpty){
                                self?.babyHasNotArrivedPostAPI(item)
                            }
                        }
                    }
                }
                self.addChildVC(popvc)
//                self.addChildViewController(popvc)
//                popvc.view.frame = self.view.frame
//                self.view.addSubview(popvc.view)
//                popvc.didMove(toParentViewController: self)
            }
        }))
        alert.addAction(UIAlertAction.init(title: "My Child hasn't arrived", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) in
            AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.ChildHasNotArrivedClicked,
                                                 withValues: [
                                                    AFEventParamContent: AppsFlyerConstant.ChildHasNotArrivedClicked,
                                                    AFEventParamContentId: "33"
                ])
            self.anotherAlert = UIAlertController.init(title: Constants.appName, message: "Waiting for your little one to be welcomed into this world? This truly is going to be an amazing conclusion to that 10 month journey of yours!", preferredStyle: UIAlertControllerStyle.alert)
            
            self.anotherAlert.addTextField(configurationHandler: { (textField) in
                textField.text = ""
                textField.placeholder = "Expected delivery date"
                self.doDatePicker()
                textField.inputView = self.datePicker
                textField.inputAccessoryView = self.toolBar
            })
            
            self.anotherAlert.addAction(UIAlertAction.init(title: "Save & Continue", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) in
                let textField = self.anotherAlert.textFields![0] // Force unwrapping because we know it exists.
                if textField.text != ""{
                    // Hit the API
                    DispatchQueue.main.async {
                        self.babyHasNotArrivedPostAPI()
                    }
                    
                }
            }))
            self.anotherAlert.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
            self.present(self.anotherAlert, animated: true, completion: nil)
            
        }))
        alert.addAction(UIAlertAction.init(title: "Later", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func setUpViewForRemainingDays(){
        var remainingDay : String = ""
        if let remDay = self.myWorldPostDict["Remaining_days"] as? String{
            remainingDay = String(remDay)
        }
        else{
            if let remDay = self.myWorldPostDict["Remaining_days"] as? Int {
                remainingDay = String(remDay)
            }
        }
        
        if remainingDay == "0 days" || remainingDay == "0"{
            ChangeParantalStatusPopup()
        }
        else{
            // Nothing
            if !remainingDay.isEmpty{
                if Int(remainingDay.components(separatedBy: " ").first!)! <= 4  && remainingDay.components(separatedBy: " ")[1] == "Weeks"{
                    self.flagRemainingWeekGreater=true
                }
                else if  remainingDay.components(separatedBy: " ")[1] == "days"{
                    self.flagRemainingWeekGreater=true
                }
                else{
                    self.flagRemainingWeekGreater=false
                }
            }
        }
        
        if self.parentalStatus.lowercased() == "mother"{
            var monthNum = "1"
            if let monNo =  self.babyMonthlyGrowthDict["month"] as? String  {
                monthNum = monNo
            }
            if  let monnum = self.babyMonthlyGrowthDict["month"] as? Int {
                monthNum =  "\(monnum)"
            }
            
            let monthInt = (monthNum as NSString).intValue
            if monthInt >= 0 && monthInt <= 2 {
                // o - 3 months
                AppsFlyerTracker.shared().trackEvent("BabyAgeGroup0to3Months",
                                                     withValues: [
                                                        AFEventParamContent: "BabyAgeGroup0to3Months+" ,
                                                        AFEventParamContentId: "51"
                    ])
            }
            else if monthInt >= 3 && monthInt <= 5 {
                // 4 - 6 months
                AppsFlyerTracker.shared().trackEvent("BabyAgeGroup4to6Months",
                                                     withValues: [
                                                        AFEventParamContent: "BabyAgeGroup4to6Months+" ,
                                                        AFEventParamContentId: "52"
                    ])
            }
            else if monthInt >= 6 && monthInt <= 8 {
                // 7 - 9 months
                AppsFlyerTracker.shared().trackEvent("BabyAgeGroup7to9Months",
                                                     withValues: [
                                                        AFEventParamContent: "BabyAgeGroup7to9Months+" ,
                                                        AFEventParamContentId: "53"
                    ])
            }
            else if monthInt >= 9 && monthInt < 12 {
                // 10 - 12 months
                AppsFlyerTracker.shared().trackEvent("BabyAgeGroup10to12Months",
                                                     withValues: [
                                                        AFEventParamContent: "BabyAgeGroup10to12Months+" ,
                                                        AFEventParamContentId: "54"
                    ])
            }
            else {
                AppsFlyerTracker.shared().trackEvent("BabyAgeGroup1Year+",
                                                     withValues: [
                                                        AFEventParamContent: "BabyAgeGroup1Year+" ,
                                                        AFEventParamContentId: "55"
                    ])
            }

        }
    }
    
    
    func doDatePicker(){
        // DatePicker
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: self.view.frame.size.height - 220, width:self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        datePicker.datePickerMode = .date
        
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        // SET MiNIMUM & MAximum DAte
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let currentDate: NSDate = NSDate()
        let components: NSDateComponents = NSDateComponents()
        
        datePicker.minimumDate = Date.init()
        
        components.day = +30
        let maxDate : Date = gregorian.date(byAdding: components as DateComponents, to: currentDate as Date, options: NSCalendar.Options.init(rawValue: 0))!
        datePicker.maximumDate = maxDate
        datePicker.date = Date.init()
        datePicker.addTarget(self, action: #selector(self.setDate(_sender:)), for: .valueChanged)

        // ToolBar
        
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        toolBar.isUserInteractionEnabled = true
        self.toolBar.isHidden = false
        
    }
    
    
    @objc func doneClick() {
  let textField = self.anotherAlert.textFields![0]
        textField.endEditing(true)
        view.endEditing(true)
    }
    
    @objc func cancelClick() {
        
        let textField = self.anotherAlert.textFields![0]
        textField.endEditing(true)
        view.endEditing(true)
    }
    @objc func ViewMoreBabyMonthlyTip(_ sender : UIButton){
        let monthlyTipVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "MonthlyTipsViewController") as! MonthlyTipsViewController
        var monthNum = "1"
        if let monNo =  self.babyMonthlyGrowthDict["month"] as? String  {
            monthNum = monNo
        }
        if  let monnum = self.babyMonthlyGrowthDict["month"] as? Int {
            monthNum =  "\(monnum)"
        }
        monthlyTipVc.monthHeadrStr = self.babyMonthlyGrowthDict["head"] as? String  ?? ""
        monthlyTipVc.monthNumerStr = monthNum
        self.navigationController?.pushViewController(monthlyTipVc, animated: true)
    }
    
    //MARK:- Date Picker
    @objc func setDate(_sender : UIDatePicker){
        let dateFormatter: DateFormatter = DateFormatter()
        // Set date format
        dateFormatter.dateFormat = "dd-MM-yyyy"
        // Apply date format
        let selectedDate: String = dateFormatter.string(from: _sender.date)
        
        let textField = self.anotherAlert.textFields![0]
        textField.text = "\(selectedDate)"
    }
	
  
    
    //MARK:- Collection View Delegates
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        if collectionView == planningBabyTrendingCollection && self.trendingTopicsArray.count > 0{
            return section  > 0 ? CGSize.init(width: SCREEN_WIDTH, height: 0) : CGSize.init(width: SCREEN_WIDTH, height: 50)

          //  return section  > 0 ? CGSize.init(width: SCREEN_WIDTH, height: 0) :  isPokoChainBirthday ? CGSize.init(width: SCREEN_WIDTH, height: 180) : CGSize.init(width: SCREEN_WIDTH, height: 50)
        }else{
            return CGSize.init(width: 0, height: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize.init(width: 0, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionHeader && collectionView == planningBabyTrendingCollection && self.trendingTopicsArray.count > 0{
            let hview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "TopicInterestHeaderCell", for: indexPath) as! TopicInterestHeaderCell
          //  hview.birthdayBashBtn.addTarget(self, action: #selector(self.birthdayBashAction(_:)), for: UIControlEvents.touchUpInside)
           // hview.birthdayBashBtnHeight.constant = isPokoChainBirthday ? 100 : 0
            hview.birthdayBashBtnHeight.constant =  0
            
            return hview
        }
        else{
            let hview = UICollectionReusableView.init(frame: CGRect.init(x: 0, y: 0, width: 0, height: 0))
            return hview
        }
    }
    
    //MARK:- Collection Flow Layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == planningBabyTrendingCollection {
            let ratio : CGFloat = 375/314
            let ht = (ScreenSize.SCREEN_WIDTH - 20)/ratio
            
            if self.intrestArray.count > 0{
                if indexPath.section==0{
                    return CGSize.init(width: (ScreenSize.SCREEN_WIDTH - 20), height: intrestArray.count > 10 ? 300 : 260)
                }else if indexPath.section > 0 {
                    return CGSize.init(width: (ScreenSize.SCREEN_WIDTH - 20), height: ht)
                }
            }else{
            
            if indexPath.section>0 {
                if changePragStatusBtn != nil{
            if changePragStatusBtn?.isSelected==false {
                return CGSize.init(width: (ScreenSize.SCREEN_WIDTH - 20), height: 100)
            }else{
                return CGSize.init(width: (ScreenSize.SCREEN_WIDTH - 20), height: 420)
            }
            }else{
                return CGSize.init(width: (ScreenSize.SCREEN_WIDTH - 20), height: 100)
                }
            }
            }
                return CGSize.init(width: (ScreenSize.SCREEN_WIDTH - 20), height: ht)
        }
        else if collectionView == trendingCollection{
            return CGSize.init(width: 225, height: 240)
        }
        else if collectionView == myTrendingCollection{
            return CGSize.init(width: (ScreenSize.SCREEN_WIDTH / 3 - 10), height: 30)
        }
        else if collectionView == myUploadedPictures{
            return CGSize.init(width: 100, height: 100)
        }
        else{
            return CGSize.init(width: 100, height: 100)
        }
    }
    
    
    //MARK:- CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == planningBabyTrendingCollection{
            // return 2  // if pragnency status needed
            if self.intrestArray.count > 0{
                return 2
            }
            return 1
        }
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == trendingCollection || collectionView ==  planningBabyTrendingCollection {
            if self.intrestArray.count > 0{
                if section==0{
                    return 1
                }else if section > 0 {
                    return self.trendingTopicsArray.count
                }
            }
            if section>0{
                return 1
            }
            return self.trendingTopicsArray.count
        }
        else if collectionView == myTrendingCollection {
            return self.myInterestTrendingArray.count
        }
        else{
            return self.uploadedImgArray.count
        }
    }
	
    fileprivate func trendingTopicsPostCollectionPlanning(_ collectionView: UICollectionView, _ indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrendingTopicsHomeCollectionCell2", for: indexPath) as! TrendingTopicsHomeCollectionCell
        cell.bindData(self.trendingTopicsArray,indexPath)
        return cell
}

    fileprivate func planningBabyConditionCollection(_ collectionView: UICollectionView, _ indexPath: IndexPath) -> UICollectionViewCell {
        if changePragStatusBtn?.isSelected==true {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlanningbabyCollectionViewCell", for: indexPath) as! PlanningbabyCollectionViewCell
            
            self.lastPeriodDateTxt = cell.lastPeriodDateTxt
            self.expectedDeliveryDateTxt = cell.expectedDeliveryDateTxt
            self.firstDeliveryBtn = cell.firstDeliveryBtn
            cell.savePragDetailBtn.addTarget(self, action: #selector(savePragnencyAction(_:)), for: .touchUpInside)
            cell.firstDeliveryBtn.addTarget(self, action: #selector(firstDeliveryAction(_:)), for: .touchUpInside)
            cell.lastPeriodDateTxt.delegate=self
            cell.expectedDeliveryDateTxt.delegate=self
            self.lastPeriodDateTxt.delegate = self
            self.expectedDeliveryDateTxt.delegate = self
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlanBabyBtnCollectionViewCell", for: indexPath) as! PlanBabyBtnCollectionViewCell
                    self.changePragStatusBtn=cell.changePragStatusBtn
                    cell.changePragStatusBtn.addTarget(self, action: #selector(changePlanningstatus(_:)), for: .touchUpInside)
                    return cell
                }
}

    //MARK: - My intrest
    func BindMyInterstCellData(_ cell: MyIntrestCollectionViewCell) {
        self.intrestTagView =   cell.intrestTagView
        self.intrestTagView.delegate = self
        self.intrestTagView.dataSource = self
        cell.bindData(self.intrestArray)
        cell.saveIntrestBtn.addTarget(self, action: #selector(saveIntrestAction(_:)), for: .touchUpInside)
    }
    func BindMyInterstCellDataTbl(_ cell: MyIntrestTableViewCell) {
        self.intrestTagView =   cell.intrestTagView
        self.intrestTagView.delegate = self
        self.intrestTagView.dataSource = self
        cell.bindData(self.intrestArray)
        cell.saveIntrestBtn.addTarget(self, action: #selector(saveIntrestAction(_:)), for: .touchUpInside)
    }
    
func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		if collectionView == planningBabyTrendingCollection {
            
            if self.intrestArray.count > 0{
                if indexPath.section == 0 {
                    let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "MyIntrestCollectionViewCell" , for: indexPath) as! MyIntrestCollectionViewCell
                    BindMyInterstCellData(cell)
                    cell.saveIntrestBtn.addTarget(self, action: #selector(saveIntrestAction(_:)), for: .touchUpInside)
                    return cell
                }else if indexPath.section == 1 {
                    return trendingTopicsPostCollectionPlanning(collectionView, indexPath)

                }else{
                    return planningBabyConditionCollection(collectionView, indexPath)
                }
            }else{
                if indexPath.section == 0 {
                    return trendingTopicsPostCollectionPlanning(collectionView, indexPath)
                }
                else {
                    return planningBabyConditionCollection(collectionView, indexPath)
                }
            }
			
		}
		else if collectionView == trendingCollection {
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrendingTopicsHomeCollectionCell", for: indexPath) as! TrendingTopicsHomeCollectionCell
			cell.bindData(self.trendingTopicsArray, indexPath)
			return cell
		}
		else if collectionView == myTrendingCollection {
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myTrendingCell", for: indexPath)
			
			cell.layer.cornerRadius = 15
			cell.layer.masksToBounds = true
			cell.clipsToBounds = true
			
			let backImage = cell.viewWithTag(301) as! UIImageView
			let titleLabel = cell.viewWithTag(302) as! UILabel
			
			let diction = self.myInterestTrendingArray[indexPath.item] as! Dictionary <String, Any>
			let poster = diction["image"] as?  String
          
			if poster != nil && (poster?.count)! > 0 {
				let posterurl = URL(string: poster ?? "")
				backImage.kf.setImage(with: posterurl)
			}
			titleLabel.text = diction["name"] as?  String ?? ""
			
			return cell
		}
		else{
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BumpImageUploadCell", for: indexPath) as! BumpImageUploadCell
			let diction = self.uploadedImgArray[indexPath.item] as! Dictionary<String,Any>
			
			cell.crossButton.tag = indexPath.item
			cell.crossButton.addTarget(self, action: #selector(CrossPhotoPressed(sender:)), for: .touchUpInside)
			
			let imgStr = diction["image"] as? String
			if imgStr != nil || (imgStr!.count) > 0 {
				cell.bckImage.backgroundColor = UIColor.clear
				let imgStrurl = URL(string: imgStr!)
				cell.bckImage.kf.setImage(with: imgStrurl)
			}
			else{
				cell.bckImage.backgroundColor = UIColor.clear
				cell.bckImage.image = UIImage.init(named: "AppIcon")
			}
			return cell
		}
	}
	
    fileprivate func openPostCollection(_ indexPath: IndexPath) {
        let detailvc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ArticleDetailVC") as! ArticleDetailVC
        let diction = self.trendingTopicsArray[indexPath.item] as! Dictionary <String, Any>
        let title = "\(diction["ID"] as! NSNumber)"
        detailvc.postID = title
        detailvc.modalPresentationStyle = .fullScreen
        if (title.count) == 0{
            // ignore
        }
        else{
            detailvc.postID = title
            DispatchQueue.main.async {
                    self.present(detailvc, animated: true, completion: nil)
                }
            }
}

func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		if collectionView == planningBabyTrendingCollection {
            if intrestArray.count > 0 {
                if indexPath.section == 1 {
                openPostCollection(indexPath)
                }
            }else{
            if indexPath.section > 0 {
				return
			}
            openPostCollection(indexPath)
            }
		}
		else if collectionView == trendingCollection{
			let detailvc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ArticleDetailVC") as! ArticleDetailVC
			let diction = self.trendingTopicsArray[indexPath.item] as! Dictionary <String, Any>
			let title = "\(diction["ID"] as! NSNumber)"
			detailvc.postID = title
			detailvc.modalPresentationStyle = .fullScreen
			if (title.count) == 0{
				// ignore
			}
			else{
				detailvc.postID = title
				DispatchQueue.main.async {
					self.present(detailvc, animated: true, completion: nil)
				}
			}
		}
		else if collectionView == myTrendingCollection{
			let detailvc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "BlogVc") as! BlogVc
			detailvc.selectedType = "MyInterest"
			detailvc.selectedIndex = indexPath.item
			DispatchQueue.main.async {
				self.present(detailvc, animated: true, completion: nil)
			}
		}else if collectionView == myUploadedPictures{
			let galleryVc =  Constants.mainStoryboard2.instantiateViewController(withIdentifier: "GalleryViewController") as! GalleryViewController
			galleryVc.galleryArray = self.uploadedImgArray as! [[String : Any]]
			DispatchQueue.main.async {
				self.present(galleryVc, animated: true, completion: nil)
				
			}
		}
	}
	//MARK:- Collection Target Actions
	
    @IBAction func firstDeliveryAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    @IBAction func savePragnencyAction(_ sender: UIButton) {
        self.view.endEditing(true)
        savePragnencyDetails() // check ProfileApiHelper.Swift for method defination
    }
	
    //MARK: - Save pragnency Details
    func savePragnencyDetails(){
        if Helper.isConnectedToNetwork() {
            
            let urlstr = Constants.BASEURL + MethodName.RegisterStep2
            let lastDate = "\(self.lastPeriodDateTxt!.text!)"
            let expectedDate = "\(self.expectedDeliveryDateTxt!.text!)"
            let isFirstDelivery = firstDeliveryBtn.isSelected == true ? "YES" : "NO"
            
            if lastDate.isEmpty == true && expectedDate.isEmpty == true{
                PKSAlertController.alert(Constants.appName, message: "Please Select Last Date of Period or Expected Delivery Date.")
            }
            else{
                Constants.appDel.startLoader()
                let userid = UserDefaults.standard.string(forKey: Constants.USERID)
                
                let paramStr = "user_id=\(userid!)&category=pregnancy&last_date_of_period=\(lastDate)&delivery_date=\(expectedDate)&baby_name=&baby_gender=&baby_dob=&baby_weight=&baby_weight_unit=&baby_height=&baby_height_unit=&head_circumfrence=&head_circumfrence_unit=&medical_history=&description=&first_delivery=\(isFirstDelivery)"
                
                Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
                    let responseDict = response
                    Constants.appDel.stopLoader()
                    
                    if responseDict["status"] as? Int == 3 || responseDict["status"] as? String == "3" {
                        
                        DispatchQueue.main.async {
                            Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
                                AnalyticsParameterItemID: "id-\(AppsFlyerConstant.DataUpdatedAfter40WeekPregnancy)",
                                AnalyticsParameterItemName: AppsFlyerConstant.DataUpdatedAfter40WeekPregnancy,
                                AnalyticsParameterContentType: "Updates"
                                ])
                            AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.DataUpdatedAfter40WeekPregnancy,
                                                                 withValues: [
                                                                    AFEventParamContent: AppsFlyerConstant.DataUpdatedAfter40WeekPregnancy,
                                                                    AFEventParamContentId: "37"
                                ])

                            PKSAlertController.alert(Constants.appName, message: "Please choose your interst", buttons: ["Ok"], tapBlock: { (alert, index) in
                                let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
								destViewController.selectedButton = 1
                                self.navigationController?.pushViewController(destViewController, animated: true)
                            })
                        }
                    }
                    else{
                        PKSAlertController.alert("Error", message: responseDict["message"] as? String ?? "Some error has occured! Please try again")
                    }
                }
            }
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
    //MARK:- Save Intrest
    //MARK:- Interest Action
    @IBAction func saveIntrestAction(_ sender: UIButton) {
        
        if  intrestTagView?.selectedIndices.count == 0 {
            PKSAlertController.alert(Constants.appName, message: "Please select atleast one interest")
        }
        else{
            self.registerInterests() // chaeck ProfileApiHelper.Swift for method defination
        }
        self.view.endEditing(true)
    }
    //MARK;- Register Intrest
    func registerInterests() {
        if Helper.isConnectedToNetwork() {
            Constants.appDel.startLoader()
            let urlstr = Constants.BASEURL + MethodName.RegisterStep3
            let userid = UserDefaults.standard.string(forKey: Constants.USERID)
            var selectedcategories :  [String] = []
            for tag in intrestTagView.selectedIndices {
                selectedcategories.append("\(intrestArray[tag].alias ?? "")")
            }
            
            let paramStr = "user_id=\(userid!)&area_of_interest=\(selectedcategories)"
            
            Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
                let responseDict = response as Dictionary<String, Any>
                Constants.appDel.stopLoader()
                
                if responseDict["status"] as! NSNumber == NSNumber.init(value: 1) {
                    
                    DispatchQueue.main.async {
//						PKSAlertController.alert("Success", message: "Interest has been updated")
                        self.BottomDataPostAPI()
                    }
                    
                }
                else{
                    PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                }
            }
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
    @objc func changePlanningstatus(_ sender: UIButton){
        self.changePragStatusBtn?.isSelected=true
        sender.isSelected=true
    planningBabyTrendingCollection.reloadData()
    }
	
    //MARK:- Action Methods
    @IBAction func quizAction(_ sender: Any) {
      
        if  UserDefaults.standard.value(forKey: Constants.UserDefaultsConstant.isQuizSubmitted) as? Bool == true {
        // go to tghank you page
            //QuizThankyouViewController
            let quizVC = Constants.mainStoryboard3.instantiateViewController(withIdentifier: "QuizThankyouViewController") as! QuizThankyouViewController
                   self.navigationController?.pushViewController(quizVC, animated: true)
        }else{
        let quizVC = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "QuizViewController") as! QuizViewController
        self.navigationController?.pushViewController(quizVC, animated: true)
        }
    }
    
    @IBAction func birthdayBashAction(_ sender: UIButton) {
        
//        if let isSubmitted = UserDefaults.standard.value(forKey: Constants.UserDefaultsConstant.isBirthdayBashSubmitted) as? Bool {
//            if isSubmitted == true {
//				Mixpanel.initialize(token: "bbabd4dc11ec9c7fefd7569a73409213")
//                Mixpanel.mainInstance().track(event: "poko_chan_bday_thankyou_from_banner")
//
//                let destViewController = Constants.mainStoryboard3.instantiateViewController(withIdentifier: "ThankYouVC") as! ThankYouVC
//                destViewController.isSavedImageRequired = true
//                destViewController.babyUrl = self.babyFormUrl
//                self.navigationController?.pushViewController(destViewController, animated: true)
//                return
//            }
//        }
//		Mixpanel.initialize(token: "bbabd4dc11ec9c7fefd7569a73409213")
//		Mixpanel.mainInstance().track(event: "poko_chan_bday_form_from_banner")
//            let destViewController = Constants.mainStoryboard3.instantiateViewController(withIdentifier: "PokoChansVC") as! PokoChansVC
//        destViewController.userData =  self.userData
//            self.navigationController?.pushViewController(destViewController, animated: true)
		
		
		
		// open only winner
		Mixpanel.initialize(token: "bbabd4dc11ec9c7fefd7569a73409213")
		 Mixpanel.mainInstance().track(event: "poko_chan_bday_thankyou_winner")
		 let searchVc = Constants.mainStoryboard3.instantiateViewController(withIdentifier: "PokoWinnerViewController") as! PokoWinnerViewController
		searchVc.isShowMenu=false
		 self.navigationController?.pushViewController(searchVc, animated: true)
    }
	
	@IBAction func wishPokoChan(_ sender: UIButton){
		let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
//						destViewController.isSavedImageRequired = true
						destViewController.isShowMenu = false
						destViewController.isWishPokoChain = true
		                destViewController.isComingfromWishBanner = true
						self.navigationController?.pushViewController(destViewController, animated: true)
	
//		if let isSubmitted = UserDefaults.standard.value(forKey: Constants.UserDefaultsConstant.isWishPokoChanSubmitted) as? Bool {
//			if isSubmitted == true {
//				Mixpanel.initialize(token: "bbabd4dc11ec9c7fefd7569a73409213")
//				Mixpanel.mainInstance().track(event: "poko_chan_bday_thankyou_from_banner")
//
//				let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
//				destViewController.isSavedImageRequired = true
//				destViewController.isShowMenu = false
//				destViewController.isWishPokoChain = true
//				self.navigationController?.pushViewController(destViewController, animated: true)
//				return
//			}
//		}
		
	}
    
	@IBAction func ViewProfilePressed(){
		let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
		destViewController.selectedButton = 0
		destViewController.isHereChangeStatus = false
        destViewController.userData =  self.userData
        
        self.navigationController?.pushViewController(destViewController, animated: true)
//        if Constants.appDel.returnTopViewController().isKind(of: HomeVC.classForCoder()){
//            let home = Constants.appDel.returnTopViewController() as! HomeVC
//            home.hideSideMenuView()
//            home.navigationController?.pushViewController(destViewController, animated: true)
//        }
//        else{
//            sideMenuController()?.setContentViewController(destViewController)
//        }
	}

    @IBAction func SideMenuPressed(){
        toggleSideMenuView()
    }

	@IBAction func SearchButtonPressed(){
        let searchVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(searchVc, animated: true)
    }
	
    @IBAction func UploadImagePressed(){
        if self.imageTitle.text?.count == 0 {
            PKSAlertController.alert("Error", message: "Enter remark title !")
        }
        else{
            let strBase64 = self.imageSelectedData.base64EncodedString(options: .endLineWithCarriageReturn)
            self.UploadImageToServerwithData(self.imageSelected, imageData: strBase64, titleSTR: self.imageTitle.text ?? "")
        }
    }

    @IBAction func CancelImageUploadPressed(){
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.2, animations: {
                self.imageUploadView.alpha = 0.0
                self.imageUploadView.isHidden = true
                self.imageTitle.text = ""
            }) { (val) in
                self.imageTitle.text = ""
                self.view.sendSubview(toBack: self.imageUploadView)
                self.view.bringSubview(toFront: self.homeTabel)
            }
        }
    }
	
	//MARK:- Objective C Functions
	@objc func ViewMoreForPregnancyTrackerPressed(){
		let detailvc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "PregnancyTrackerDetailVC") as! PregnancyTrackerDetailVC
		
		let dictSub = self.babyGrowthDict["post"] as? Dictionary<String,Any> ?? [:]
		let weekstr = dictSub["week"] as? String ?? "1"
		let weekStrHead = self.babyGrowthDict["passedTime"] as? String ?? ""
		
		detailvc.weekNumerStr = weekstr
		detailvc.weekHeadrStr = weekStrHead
		detailvc.modalPresentationStyle = .fullScreen
		DispatchQueue.main.async {
			self.present(detailvc, animated: true, completion: nil)
		}
	}
	
	@objc func showActionSheet() {
		let actionSheet = UIAlertController.init(title: "Mamy Poko", message: "Upload Photo Using", preferredStyle: UIAlertControllerStyle.actionSheet)
		actionSheet.addAction(UIAlertAction.init(title: "Camera", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) in
			self.TakePhotoPressed()
		}))
		actionSheet.addAction(UIAlertAction.init(title: "Gallery", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
			self.UploadPhotoPressed()
		}))
		
		actionSheet.addAction(UIAlertAction.init(title: "Photo Library", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
			self.UploadPhotoPressed()
		}))
		
		actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
		self.present(actionSheet, animated: true, completion: nil)
	}
	
	@objc func UploadPhotoPressed(){
		
		if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
			imagePicker.delegate = self
			imagePicker.sourceType = .savedPhotosAlbum;
			imagePicker.allowsEditing = false
			self.present(imagePicker, animated: true, completion: nil)
		}
		else if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
			imagePicker.delegate = self
			imagePicker.sourceType = .photoLibrary;
			imagePicker.allowsEditing = false
			self.present(imagePicker, animated: true, completion: nil)
		}
	}
	
	@objc func TakePhotoPressed(){
		if UIImagePickerController.isSourceTypeAvailable(.camera){
			imagePicker.delegate = self
			imagePicker.sourceType = .camera;
			imagePicker.allowsEditing = false
			self.present(imagePicker, animated: true, completion: nil)
		}
	}
	

	@objc func CrossPhotoPressed(sender : UIButton){
		let sendertag = sender.tag
		self.RemoveImagefromSeverForItem(itemID: sendertag)
	}
	

    @objc func BabyGrowthTrackerAction(_ sender: UIButton){
                let babyGrowthVC = Constants.mainStoryboard.instantiateViewController(withIdentifier: "BabyGrowthDataViewController") as! BabyGrowthDataViewController
                self.navigationController?.pushViewController(babyGrowthVC, animated: true)
    }
	
	
    @objc func BabyGrowthVaccinationAction(_ sender: UIButton){
        let babyGrowthVC = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "VaccinationScheduleVC") as! VaccinationScheduleVC
        self.navigationController?.pushViewController(babyGrowthVC, animated: true)
    }
	
	
    //MARK:- Textfield Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK:- Touch Up
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let headerView = homeTabel.tableHeaderView {
            
            if isPokoChainBirthday {
                headerView.frame.size.height = 220 - 74
            }else{
                headerView.frame.size.height = 66
            }
            
        }
    }
}

//MARK:- TableView Data Source
extension HomeVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        self.heightBirthdayBashBanner.constant = isPokoChainBirthday ? 80 : 0
        
        if section == 0{
            return homeTblHeaderView
        }else{
            return UIView()
        }
    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        if section == 0{
//            return isPokoChainBirthday ? 180 : 66
//        }else{
//            return 0
//        }
//    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if intrestArray.count > 0 {
            return 5
        }else{
            return 4
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    fileprivate func ManageTableCellForRow(_ indexPath: IndexPath, _ tableView: UITableView) -> UITableViewCell {
        if indexPath.section == 0 {
            tableView.estimatedRowHeight = 145
            tableView.rowHeight = UITableViewAutomaticDimension
            
            if self.parentalStatus.lowercased().contains("mother"){
                let cell = tableView.dequeueReusableCell(withIdentifier: "BabyGrowthMonthlyTableViewCell", for: indexPath) as! BabyGrowthMonthlyTableViewCell
                cell.bindData(self.babyMonthlyGrowthDict)
                cell.viewMoreBtn.addTarget(self, action: #selector(ViewMoreBabyMonthlyTip(_:)), for: .touchUpInside)
                return cell
            }
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "WeekTrackerHomeTableCell", for: indexPath) as! WeekTrackerHomeTableCell
                cell.bindData(self.myWorldPostDict,self.myWorldDictionary , self.babyGrowthDict , userData)
                cell.viewMoreBtn.addTarget(self, action: #selector(ViewMoreForPregnancyTrackerPressed), for: .touchUpInside)
                cell.changeParantalStatusBtn.isHidden = !self.flagRemainingWeekGreater
                cell.changeParantalStatusBtn.addTarget(self, action:
                    #selector(ChangeParantalStatusPopup), for: UIControlEvents.touchUpInside)
                return cell
            }
        }
        else  if indexPath.section == 1 {
            if self.parentalStatus.lowercased().contains("mother"){
                let cell = tableView.dequeueReusableCell(withIdentifier: "BabyGrowthTrackButtonTableViewCell", for: indexPath) as! BabyGrowthTrackButtonTableViewCell
                cell.babyGrowthBtn.addTarget(self, action: #selector(BabyGrowthTrackerAction(_:)), for: .touchUpInside)
                cell.vaccinationBtn.addTarget(self, action: #selector(BabyGrowthVaccinationAction(_:)), for: .touchUpInside)
                tableView.estimatedRowHeight = 160
                tableView.rowHeight = UITableViewAutomaticDimension
                return cell
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "babyBumptableCell", for: indexPath) as! babyBumptableCell
            
            cell.uploadPhotoBtn.addTarget(self, action: #selector(showActionSheet), for: .touchUpInside)
            
            cell.bindData(self.uploadedImgArray)
            self.view.layoutIfNeeded()
            
            self.myUploadedPictures = cell.pictureCollection
            self.myUploadedPictures.reloadData()
            self.myUploadedPictures.delegate = self
            self.myUploadedPictures.dataSource = self
            
            tableView.estimatedRowHeight = 215
            tableView.rowHeight = UITableViewAutomaticDimension
            return cell
        }
        else  if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TrandingArticleTableViewCell", for: indexPath) as! TrandingArticleTableViewCell
            
            cell.trendingCollectionHeight.constant = trendingTopicsArray.count > 0  ? 250 : 0
            self.trendingCollection = cell.trendingArticleCollection
            self.trendingCollection.reloadData()
            self.trendingCollection.delegate = self
            self.trendingCollection.dataSource = self
            tableView.estimatedRowHeight = 280
            tableView.rowHeight = UITableViewAutomaticDimension
            return cell
            
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TrendingIntrestTableViewCell", for: indexPath) as! TrendingIntrestTableViewCell
            
            cell.intrestCollectionHeight.constant = self.myInterestTrendingArray.count > 0 ? CGFloat((myInterestTrendingArray.count/2)*60 + 60 + (myInterestTrendingArray.count/2)*5 + 5) :  0
            
            self.myTrendingCollection = cell.intrestCollection
            self.myTrendingCollection.reloadData()
            self.myTrendingCollection.delegate = self
            self.myTrendingCollection.dataSource = self
            tableView.estimatedRowHeight = 215
            tableView.rowHeight = UITableViewAutomaticDimension
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if intrestArray.count > 0 {
            if indexPath.section == 0 {
                let cell  = tableView.dequeueReusableCell(withIdentifier: "MyIntrestTableViewCell") as! MyIntrestTableViewCell
                BindMyInterstCellDataTbl(cell)
                cell.saveIntrestBtn.addTarget(self, action: #selector(saveIntrestAction(_:)), for: .touchUpInside)
                return cell
            }else{
                let newIndexPath = IndexPath.init(row: indexPath.row, section: indexPath.section-1)
                return  ManageTableCellForRow(newIndexPath, tableView)
            }
            
        }else{
            return   ManageTableCellForRow(indexPath, tableView)
        }
    }
    
   
}
//MARK: - Intrest View Data source
extension HomeVC:HTagViewDataSource{
    func numberOfTags(_ tagView: HTagView) -> Int {
        return intrestArray.count
    }
    func tagView(_ tagView: HTagView, titleOfTagAtIndex index: Int) -> String {
        return intrestArray[index].name!
    }
    func tagView(_ tagView: HTagView, tagTypeAtIndex index: Int) -> HTagType {
        return .select
    }
    func tagView(_ tagView: HTagView, tagWidthAtIndex index: Int) -> CGFloat {
        return .HTagAutoWidth
    }
}
//MARK: - Intrest View  Delegate
extension HomeVC:HTagViewDelegate{
    func tagView(_ tagView: HTagView, tagSelectionDidChange selectedIndices: [Int]) {
        
    }
    
    func tagView(_ tagView: HTagView, didCancelTagAtIndex index: Int) {
        tagView.reloadData()
    }
}
//MARK:- Image Picker Delegate
extension HomeVC : UIImagePickerControllerDelegate {
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
		
		let imseSelect = info["UIImagePickerControllerOriginalImage"] as! UIImage
		self.imageSelected =  Helper.resizeImage(imseSelect, newWidth: 800)
		
		self.imageSelectedData = NSData(data: UIImagePNGRepresentation(self.imageSelected)!)
		
		UIView.animate(withDuration: 0.5, animations: {
			self.imageUploadView.alpha = 1.0
			self.imageUploadView.isHidden = false
		}) { (val) in
			self.view.sendSubview(toBack: self.homeTabel)
			self.view.bringSubview(toFront: self.imageUploadView)
			self.imageSelectedView.image = self.imageSelected
			self.imageTitle.becomeFirstResponder()
			self.dismiss(animated: true, completion: nil)
		}
	}
}

//MARK:- Notification Implementation Target Method
extension HomeVC {
	
	@objc func BabyGrowthTrackerNotificationAction(_ sender: NSNotification){
		if sender.name == NSNotification.Name.init(rawValue: "baby_month_change"){
			let babyGrowthVC = Constants.mainStoryboard.instantiateViewController(withIdentifier: "BabyGrowthDataViewController") as! BabyGrowthDataViewController
			self.navigationController?.pushViewController(babyGrowthVC, animated: false)
			
		}
		if sender.name == NSNotification.Name.init(rawValue: "baby_growth_details"){
			let babyGrowthVC = Constants.mainStoryboard.instantiateViewController(withIdentifier: "BabyGrowthDataViewController") as! BabyGrowthDataViewController
			self.navigationController?.pushViewController(babyGrowthVC, animated: false)
			
		}
		if sender.name == NSNotification.Name.init(rawValue: "vaccination_reminder"){
			let babyGrowthVC = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "VaccinationScheduleVC") as! VaccinationScheduleVC
			self.navigationController?.pushViewController(babyGrowthVC, animated: true)
		}
		if sender.name == NSNotification.Name.init(rawValue: "vaccination_done_reminder"){
			let babyGrowthVC = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "VaccinationScheduleVC") as! VaccinationScheduleVC
			self.navigationController?.pushViewController(babyGrowthVC, animated: true)
		}
		if sender.name == NSNotification.Name.init(rawValue: "prompt_vaccination_tracker"){
			let babyGrowthVC = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "VaccinationScheduleVC") as! VaccinationScheduleVC
			self.navigationController?.pushViewController(babyGrowthVC, animated: true)
		}
		
		if sender.name == NSNotification.Name.init(rawValue: "preg_nb_product_notification"){
			let  destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "OpenInAppBrowser") as! OpenInAppBrowser

			let url = sender.object as? URL
			if url?.absoluteString != nil && (url?.absoluteString.count)! > 0 {
				destViewController.pageUrl = (url?.absoluteString)!
				self.navigationController?.pushViewController(destViewController, animated: true)
			}
		}
		
		if sender.name == NSNotification.Name.init(rawValue: "baby_size_switch_notification"){
			let  destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "OpenInAppBrowser") as! OpenInAppBrowser

			let url = sender.object as? URL
			if url?.absoluteString != nil && (url?.absoluteString.count)! > 0 {
				destViewController.pageUrl = (url?.absoluteString)!
				self.navigationController?.pushViewController(destViewController, animated: true)
			}
		}
		
		if sender.name == NSNotification.Name.init(rawValue: "BlogDetailPage"){
			
			let detailvc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ArticleDetailVC") as! ArticleDetailVC
			detailvc.modalPresentationStyle = .fullScreen
			let blogid = sender.object as? String
			detailvc.postID = blogid!
			if (blogid!.count) > 0{
				detailvc.postID = blogid!
				DispatchQueue.main.async {
					self.navigationController?.pushViewController(detailvc, animated: true)
				}
			}

		}
		
		if sender.name == NSNotification.Name.init(rawValue: "offer_registration"){
			let  destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
			destViewController.selectedButton = 0
			destViewController.isHereChangeStatus = false
			self.navigationController?.pushViewController(destViewController, animated: true)

		}
	}
}
