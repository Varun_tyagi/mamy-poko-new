//
//  ContentNetwork.swift
//  MamyPoko
//
//  Created by Arnab Maity on 31/01/20.
//  Copyright © 2020 Varun Tyagi. All rights reserved.
//

import Foundation
import UIKit

class ContentNetwork{
	class func diapersService(_ productType : String,_ completionHandler: @escaping (Dictionary<String,Any>) -> Void){
		if Helper.isConnectedToNetwork() {
			let urlstr = Constants.BASEURL + MethodName.getProducts
			let userID = UserDefaults.standard.string(forKey: Constants.USERID)
			
			
			let paramStr = "type=\(productType)"
			
			if userID == nil || userID?.count == 0 {
				PKSAlertController.alert(Constants.appName, message: "Invalid User ID")
			}
			else{
				Constants.appDel.startLoader()
				
				Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
					let responseDict = response as Dictionary<String, Any>
					Constants.appDel.stopLoader()
					completionHandler(responseDict)
				}
			}
		}
	}
    
    
    class func GetDiapersServices(_ completionHandler: @escaping (Dictionary<String,Any>) -> Void){
        if Helper.isConnectedToNetwork() {
            let urlstr = Constants.BASEURL + MethodName.getProductsCategoryApi
            let userID = UserDefaults.standard.string(forKey: Constants.USERID)
            
            if userID == nil || userID?.count == 0 {
                PKSAlertController.alert(Constants.appName, message: "Invalid User ID")
            }
            else{
                Constants.appDel.startLoader()
                
                Server.postRequestWithURL(urlstr, paramString: "") { (response) in
                    let responseDict = response as Dictionary<String, Any>
                    Constants.appDel.stopLoader()
                    completionHandler(responseDict)
                    
                }
            }
            
        }
    }
    
    
    class func diapersDetailsService(_ productType : String,_ Alisa: String ,_ completionHandler: @escaping (Dictionary<String,Any>) -> Void){
        if Helper.isConnectedToNetwork() {
            let urlstr = Constants.BASEURL + MethodName.productDetails
            let userID = UserDefaults.standard.string(forKey: Constants.USERID)
            
            
           let paramStr = "type=\(productType)&product_alias=\(Alisa)"
            
            if userID == nil || userID?.count == 0 {
                PKSAlertController.alert(Constants.appName, message: "Invalid User ID")
            }
            else{
                Constants.appDel.startLoader()
                
                Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
                    let responseDict = response as Dictionary<String, Any>
                    Constants.appDel.stopLoader()
                    completionHandler(responseDict)
                }
            }
        }
    }
    
}
