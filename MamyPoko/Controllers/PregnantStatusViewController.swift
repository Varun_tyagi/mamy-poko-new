//
//  PregnantViewController.swift
//  MamyPoko
//
//  Created by Surbhi Varma on 07/06/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit
import AppsFlyerLib
import FirebaseAnalytics


class PregnantStatusViewController: UIViewController {

	@IBOutlet weak var pragnencyTypeDescLbl: UILabel!
	@IBOutlet weak var pragnencyTypeLbl: UILabel!
	@IBOutlet weak var lastPeriodDateTxt: CustomPickerTextField!
	
	@IBOutlet weak var expectedDeliveryDateTxt: CustomPickerTextField!
	
	@IBOutlet weak var firstDeliveryBtn: UIButton!
	
	var pragTypeStr = ""
	var pragTypeDescStr = ""
	
	var datePicker = UIDatePicker()
	var datePicker2 = UIDatePicker()
	var toolBar : UIToolbar!


	override func viewDidLoad() {
        super.viewDidLoad()

		setupView()
		
		setupDatePicker()
	}
	
	
	//Mark:Action
	
	@IBAction func isFirstDeliveryAction(_ sender: UIButton) {
		if (sender.isSelected == true)
		{
			sender.setBackgroundImage(UIImage(named: "box"), for: UIControlState.normal)
			
			sender.isSelected = false;
		}
		else
		{
			sender.setBackgroundImage(UIImage(named: "checkBox"), for: UIControlState.normal)
			
			sender.isSelected = true;
		}
	}
	
	@IBAction func SubmitAction(_ sender: UIButton) {
		self.savePragnencyDetails()
	}
	
	
}
extension PregnantStatusViewController{
	//Mark: Custom Methods
	
	func setupView(){
		
		self.pragnencyTypeLbl.text = self.pragTypeStr
		self.pragnencyTypeDescLbl.text = self.pragTypeDescStr
		
		lastPeriodDateTxt.setLeftPaddingPoints()
		expectedDeliveryDateTxt.setLeftPaddingPoints()
		lastPeriodDateTxt.layer.cornerRadius = 25
		lastPeriodDateTxt.layer.masksToBounds = true
		lastPeriodDateTxt.layer.borderWidth = 1
		lastPeriodDateTxt.layer.borderColor = UIColor(red: 163/255.0, green: 175/255.0, blue: 177/255.0, alpha: 1.0).cgColor
		expectedDeliveryDateTxt.layer.cornerRadius = 25
		expectedDeliveryDateTxt.layer.masksToBounds = true
		expectedDeliveryDateTxt.layer.borderWidth = 1
		expectedDeliveryDateTxt.layer.borderColor = UIColor(red: 163/255.0, green: 175/255.0, blue: 177/255.0, alpha: 1.0).cgColor
	}
	func setupDatePicker()  {
		
		datePicker.datePickerMode = UIDatePickerMode.date
		datePicker.addTarget(self, action: #selector(self.setDate(_sender:)), for: .valueChanged)
		datePicker.timeZone = TimeZone.current
		toolBar = UIToolbar()
		toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(dismissPicker))
		
		
		datePicker2.datePickerMode = UIDatePickerMode.date
		datePicker2.addTarget(self, action: #selector(self.setDate(_sender:)), for: .valueChanged)
		datePicker2.timeZone = TimeZone.current
		
		datePicker.setBabyDobValidation()
		datePicker2.setExpectedDateValidation()
		
		self.lastPeriodDateTxt?.inputView = datePicker
		self.expectedDeliveryDateTxt?.inputView = datePicker2
		
	}
	
	@objc func setDate(_sender : UIDatePicker){
		
		let dateFormatter: DateFormatter = DateFormatter()
		dateFormatter.dateFormat = "dd-MM-yyyy"
		let selectedDate: String = dateFormatter.string(from: _sender.date)
		
		if _sender == datePicker {
			
			datePicker.setBabyDobValidation()
			
			if _sender.date > datePicker.minimumDate! && _sender.date < datePicker.maximumDate! {
				self.lastPeriodDateTxt?.text = "\(selectedDate)"
			}
			else{
				self.lastPeriodDateTxt?.text = ""
				datePicker.date = datePicker.maximumDate!
			}
		}
		else if _sender==datePicker2{
			
			datePicker2.setExpectedDateValidation()
			
			if _sender.date > datePicker2.minimumDate! && _sender.date < datePicker2.maximumDate! {
				self.expectedDeliveryDateTxt?.text = "\(selectedDate)"
			}
			else{
				self.expectedDeliveryDateTxt?.text = ""
				datePicker2.date = datePicker2.minimumDate!
			}
		}
	}
	@objc func dismissPicker() {
		lastPeriodDateTxt.endEditing(true)
		expectedDeliveryDateTxt.endEditing(true)
		lastPeriodDateTxt.resignFirstResponder()
		expectedDeliveryDateTxt.resignFirstResponder()
		
	}
	//MARK:- Webservice
	//MARK: - Save pragnency Details
	func savePragnencyDetails(){
		if Helper.isConnectedToNetwork() {
			
			let urlstr = Constants.BASEURL + MethodName.RegisterStep2
			let lastDate = "\(self.lastPeriodDateTxt!.text!)"
			let expectedDate = "\(self.expectedDeliveryDateTxt!.text!)"
			let isFirstDelivery = firstDeliveryBtn.isSelected == true ? "YES" : "NO"
			
			if lastDate.isEmpty == true && expectedDate.isEmpty == true{
				PKSAlertController.alert(Constants.appName, message: "Please Select Last Date of Period or Expected Delivery Date.")
			}
			else{
				Constants.appDel.startLoader()
				let userid = UserDefaults.standard.string(forKey: Constants.USERID)
				
				let paramStr = "user_id=\(userid!)&category=pregnancy&last_date_of_period=\(lastDate)&delivery_date=\(expectedDate)&baby_name=&baby_gender=&baby_dob=&baby_weight=&baby_weight_unit=&baby_height=&baby_height_unit=&head_circumfrence=&head_circumfrence_unit=&medical_history=&description=&first_delivery=\(isFirstDelivery)"
				
				Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
					let responseDict = response
					Constants.appDel.stopLoader()
					
					if responseDict["status"] as? Int == 3 || responseDict["status"] as? String == "3" {
						
						DispatchQueue.main.async {
							Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
								AnalyticsParameterItemID: "id-\(AppsFlyerConstant.DataUpdatedAfter40WeekPregnancy)",
								AnalyticsParameterItemName: AppsFlyerConstant.DataUpdatedAfter40WeekPregnancy,
								AnalyticsParameterContentType: "Updates"
								])
							AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.DataUpdatedAfter40WeekPregnancy,
																 withValues: [
																	AFEventParamContent: AppsFlyerConstant.DataUpdatedAfter40WeekPregnancy,
																	AFEventParamContentId: "37"
								])
							
							PKSAlertController.alert(Constants.appName, message: "Please choose your interst", buttons: ["Ok"], tapBlock: { (alert, index) in
								let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
								destViewController.selectedButton = 1
								self.navigationController?.pushViewController(destViewController, animated: true)
							})
						}
					}
					else if responseDict["status"] as? Int == 1 || responseDict["status"] as? String == "1"{
						let responseDictionary = responseDict["data"] as! Dictionary<String, Any>
						UserDefaults.standard.set(responseDictionary, forKey: Constants.PROFILE)
						UserDefaults.standard.set(3, forKey: Constants.RegisterStatus)
						// TAKE to HOME SCreen
						DispatchQueue.main.async {
						let root = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SideMenuNavigation")
						UserDefaults.standard.set("1", forKey: firstLogin)
						Constants.appDelegate.window?.rootViewController = root
						UIApplication.shared.keyWindow?.rootViewController = root
					}
					}
					else{
						PKSAlertController.alert("Error", message: responseDict["message"] as? String ?? "Some error has occured! Please try again")
					}
				}
			}
		}else{
			PKSAlertController.alertForNetwok()
		}
	}
}
