//
//  ProductCatelogViewController.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 05/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//see

import UIKit
import FirebaseAnalytics
import AppsFlyerLib

class ProductCatelogViewController: UIViewController {

    var productCatArray : [ProductCatalogModel] = []
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var productTbl: UITableView!
	@IBOutlet weak var profileImageButton : RoundButton!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		profileImageButton.cornerRadius = 20
		profileImageButton.borderColor = UIColor.clear

		NotificationCenter.default.addObserver(self, selector: #selector(OpenNotificationAction), name: NSNotification.Name(rawValue: "ProductDetailToOpen"), object: nil)

        setupInitialView()
        Analytics.logEvent(AnalyticsEventViewItem, parameters: [
			AnalyticsParameterItemID: "id-\(AppsFlyerConstant.ProductCatelogView)",
			AnalyticsParameterItemName: "\(AppsFlyerConstant.ProductCatelogVC)",
            AnalyticsParameterContentType: "Visit"
            ])
        AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.ProductCatalog,
                                             withValues: [
                                                AFEventParamContent: AppsFlyerConstant.ProductCatalog,
                                                AFEventParamContentId: "30"
            ])

    }

	override func viewDidAppear(_ animated: Bool) {
		self.AllowSideMenuSwipe()
	}
	
    func setupInitialView()  {

		Helper.GetProfileImageForButton(butName: self.profileImageButton)

        productCatArray.append(ProductCatalogModel(name:"MAMYPOKO DIAPERS" , image: "catalogDiper", tagLine: "Start Early, Start right with MamyPoko Pants!",btnName:"Diapers",pageUrl:"https://www.mamypoko.co.in/products/diapers?source=app"))
        productCatArray.append(ProductCatalogModel(name:"BABY WIPES" , image: "catalogWipe", tagLine: "Pure and Soft Wipes for your loved one.\nSuitable for new-born's sensitive skin.",btnName:"Wipes",pageUrl:"https://www.mamypoko.co.in/products/wipes?source=app"))
        productTbl.reloadData()

        //table
        productTbl.tableFooterView = UIView()
    }

	//MARK:- ACTIONS
	
	@IBAction func ViewProfilePressed(){

		let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
		destViewController.isHereChangeStatus = false
		destViewController.selectedButton = 0
		if Constants.appDel.returnTopViewController().isKind(of: HomeVC.classForCoder()){
			let home = Constants.appDel.returnTopViewController() as! HomeVC
			home.hideSideMenuView()
			home.navigationController?.pushViewController(destViewController, animated: true)
		}
		else{
			sideMenuController()?.setContentViewController(destViewController)
		}
	}
	
    @IBAction func menuAction(_ sender: UIButton) {
        toggleSideMenuView()
    }
    @IBAction func searchAction(_ sender: Any) {
        let searchVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(searchVc, animated: true)
    }
    @objc func gotoCatalogDetails( _ sender : UIButton){
        let  destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
        destViewController.prodCatlog = sender.accessibilityElements?.first as? ProductCatalogModel
       self.navigationController?.pushViewController(destViewController, animated: true)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }

}
extension ProductCatelogViewController : UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productCatArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCatalogTableViewCell") as! ProductCatalogTableViewCell
           
        let product = productCatArray[indexPath.row]
        cell.viewAllBtn.accessibilityElements = [product]
        cell.viewAllBtn.addTarget(self, action: #selector(gotoCatalogDetails(_:)), for: .touchUpInside)
        cell.bindData(product, indexPath)
        return cell
    }
	
}

extension ProductCatelogViewController {
	@objc func OpenNotificationAction(_ sender: NSNotification){
		if sender.name == NSNotification.Name.init(rawValue: "ProductDetailToOpen"){
			let  destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
			let url = sender.object as? String
			if url != nil && (url?.count)! > 0 {
				NotificationCenter.default.removeObserver(self, name: NSNotification.Name.init(rawValue: "ProductDetailToOpen"), object: nil)
				
				destViewController.isComingFromRegister = false
				destViewController.isProductDetailNotification = true
				destViewController.pageUrl = url!
				self.navigationController?.pushViewController(destViewController, animated: true)
			}
		}
	}
}

