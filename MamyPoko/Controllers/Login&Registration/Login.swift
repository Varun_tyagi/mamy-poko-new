//
//  Login.swift
//  
//
//  Created by Surbhi Varma on 06/06/19.
//

import UIKit
import IQKeyboardManagerSwift
class Login: UIViewController {
	
	@IBOutlet weak var loginUsingPassword: UIButton!

	@IBOutlet weak var textfieldHeight: NSLayoutConstraint!
	
	@IBOutlet weak var mobileNumber: RoundTextField!
	
	@IBOutlet weak var password: RoundTextField!
	
	var isOTPView = true
	
	override func viewDidLoad() {
        super.viewDidLoad()

		self.mobileNumber?.setLeftPaddingPoints()
		self.password?.setLeftPaddingPoints()

		IQKeyboardManager.shared.enable = true
		self.hideKeyboardWhenTappedAround()
    }
	// Mark: Action
	
	
	
	
	@IBAction func loginUsingPasswordPressed(_ sender: UIButton) {
		
		if loginUsingPassword.isSelected == true{
			self.loginUsingPassword.setTitle("or Login using Password", for: .normal)
			self.textfieldHeight.constant = 0
		}else{
			self.textfieldHeight.constant = 50
			self.loginUsingPassword.setTitle("Login With Number", for: .normal)
			
			
		}
		sender.isSelected = !sender.isSelected
		UIView.animate(withDuration: 0.3) {
			self.view.layoutIfNeeded()
		}
	}
	
	
}
