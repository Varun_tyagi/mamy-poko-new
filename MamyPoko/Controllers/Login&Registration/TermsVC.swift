//
//  TermsVC.swift
//  MamyPoko
//
//  Created by Surbhi on 21/06/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit
import WebKit
import FirebaseAnalytics
import AppsFlyerLib


class TermsVC: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var webView = UIWebView()
    var isComingFromRegister = false
    var isPrivacyFromMenu  = false
    var isDisclaimerFromMenu  = false
    var isPokoChan = false
    var isTermsFromMenu = false
	var isProductDetailNotification = false
    var isShowMenu = true
    var type : String  = ""
    var isComingFromTip = false
    var isComingFromBabyGrowth = false
	var isComingfromWishBanner = false
    var isWishPokoChain = false
	var isSavedImageRequired = false
	var isComingFromPokoChanform = false
    @IBOutlet weak var heightHeaderLbl: NSLayoutConstraint!
    var prodCatlog : ProductCatalogModel?

    @IBOutlet weak var menuBtn: UIButton!
    var pageUrl = "https://www.mamypoko.co.in/terms-and-conditions?source=app"

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
		
			if #available(iOS 13.0, *) {
				let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
				statusBar.backgroundColor = Colors.MAMYBlue
				 UIApplication.shared.keyWindow?.addSubview(statusBar)
			} else {
				 UIApplication.shared.statusBarView?.backgroundColor = Colors.MAMYBlue
			 }
		

		setupView()
		if isShowMenu{
			self.AllowSideMenuSwipe()
		}
		else
		{
			self.BlockSideMenuSwipe()
		}
    }
    
    func setupView()  {

        heightHeaderLbl.constant = prodCatlog==nil ? 0 : 60

		if isProductDetailNotification {
			isShowMenu = false
			menuBtn.setImage(isShowMenu ? #imageLiteral(resourceName: "menuIcon") : #imageLiteral(resourceName: "BackIcon"), for: .normal)
			let url = URL(string: pageUrl)!
			self.webView?.loadRequest(URLRequest.init(url: url))
			return
		}
		
        if (prodCatlog != nil){
            pageUrl = (prodCatlog?.pageUrl)!

            isShowMenu =  prodCatlog==nil ? true : false
            headerLbl.text = prodCatlog==nil ? "" : "MamyPoko \(prodCatlog?.btnName ?? "")"
            Analytics.logEvent(AnalyticsEventViewItem, parameters: [
                AnalyticsParameterItemID: "id-\(prodCatlog?.btnName ?? "")",
                AnalyticsParameterItemName: "\(prodCatlog?.btnName ?? "")",
                AnalyticsParameterContentType: "\(prodCatlog?.btnName ?? "")"
                ])
            AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.ProductCatalog,
                                                 withValues: [
                                                    AFEventParamContent: AppsFlyerConstant.ProductCatalog,
                                                    AFEventParamContentId: "22"
                ])
        }
        else if isComingFromBabyGrowth == true {
            menuBtn.setImage( #imageLiteral(resourceName: "BackIcon"), for: .normal)

        }
		else if isComingFromPokoChanform == true{
			pageUrl = "https://www.mamypoko.co.in/pokochan-tnc"
		}
        else{
            if !isComingFromRegister &&  isPrivacyFromMenu{
                pageUrl = "https://www.mamypoko.co.in/privacy-policy?source=app"
    
                AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.PrivacyPolicyOpen,
                                                     withValues: [
                                                        AFEventParamContent: AppsFlyerConstant.PrivacyPolicyOpen,
                                                        AFEventParamContentId: "22"
                    ])
            }
            else  if !isComingFromRegister &&  isDisclaimerFromMenu{
                pageUrl = "https://www.mamypoko.co.in/disclaimer?source=app"
            }
            else if !isComingFromRegister && isPokoChan{
                pageUrl = "https://www.mamypoko.co.in/about-pokochan?source=app"
            }
            else if !isComingFromRegister && isWishPokoChain{
                //Change to live
                //https://www.mamypoko.co.in/
                pageUrl = "\(Constants.BASEURL)pokochan-birthday?source=app"
            }
            else if !isComingFromRegister && isTermsFromMenu{
                pageUrl = "https://www.mamypoko.co.in/terms-and-conditions?source=app"
            }
            else{
				
                if type == "Disclaimer"{
                    pageUrl = "https://www.mamypoko.co.in/disclaimer?source=app"
                }
                else if type == "Disclaimer"{
                    pageUrl = "https://www.mamypoko.co.in/privacy-policy?source=app"
                }
                else if  isComingFromTip == true && type == "tip"{
                    pageUrl = "https://www.mamypoko.co.in/tnc"
                }
                else{
                    pageUrl = "https://www.mamypoko.co.in/terms-and-conditions?source=app"
                }
               
                
                isShowMenu = false
            }
        menuBtn.setImage(isShowMenu ? #imageLiteral(resourceName: "menuIcon") : #imageLiteral(resourceName: "BackIcon"), for: .normal)
        }
		
        let url = URL(string: pageUrl)!
        self.webView?.loadRequest(URLRequest.init(url: url))
	

    }
    
    //MARK:- Action
    @IBAction func BackButtonPressed(){
        if isComingFromPokoChanform{
            self.navigationController?.popViewController(animated: true)
        }
		if isComingfromWishBanner{
			self.navigationController?.popViewController(animated: true)
		}
        if isComingFromBabyGrowth{
            self.navigationController?.popViewController(animated: true)
        }
        if (prodCatlog != nil){
            self.navigationController?.popViewController(animated: true)
            return
        }
		if (isProductDetailNotification){
			self.navigationController?.popViewController(animated: true)
			return
		}
        if isShowMenu{
            toggleSideMenuView()
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func searchAction(_ sender: Any) {
        let searchVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(searchVc, animated: true)
    }
    
	//MARK:- Webview Delegates
	
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if request.description.contains("/products/diapers/"){
            
            let requestArr = request.description.components(separatedBy: "/products/diapers/")
            let catStr = requestArr.last?.replacingOccurrences(of: "?source=app", with: "")

            Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
                AnalyticsParameterItemID: "id-DiaperCategory",
                AnalyticsParameterItemName: "DiaperCategory",
                AnalyticsParameterContentType: "DiaperCategory"
                ])

            if catStr!.description.contains("new-born"){
                AppsFlyerTracker.shared().trackEvent("DiaperCategory_NewBorn",
                                                     withValues: [
                                                        AFEventParamContent: "DiaperCategory_NewBorn" ,
                                                        AFEventParamContentId: "42"
                    ])
            }
            else if catStr!.description.contains("extra-absorb"){
                AppsFlyerTracker.shared().trackEvent("DiaperCategory_ExtraAbsorb",
                                                     withValues: [
                                                        AFEventParamContent: "DiaperCategory_ExtraAbsorb" ,
                                                        AFEventParamContentId: "43"
                    ])

            }
            else if catStr!.description.contains("kids-pants"){
                AppsFlyerTracker.shared().trackEvent("DiaperCategory_KidsPants",
                                                     withValues: [
                                                        AFEventParamContent: "DiaperCategory_KidsPants" ,
                                                        AFEventParamContentId: "44"
                    ])
            }
            else if catStr!.description.contains("standard"){
                AppsFlyerTracker.shared().trackEvent("DiaperCategory_Standard",
                                                     withValues: [
                                                        AFEventParamContent: "DiaperCategory_Standard" ,
                                                        AFEventParamContentId: "45"
                    ])
            }
            else if catStr!.description.contains("airfit"){
                AppsFlyerTracker.shared().trackEvent("DiaperCategory_Airfit",
                                                     withValues: [
                                                        AFEventParamContent: "DiaperCategory_Airfit" ,
                                                        AFEventParamContentId: "46"
                    ])
            }
            else if catStr!.description.contains("mamypoko-pants-xxxl"){
                AppsFlyerTracker.shared().trackEvent("DiaperCategory_MamypokoPantsXXXL",
                                                     withValues: [
                                                        AFEventParamContent: "DiaperCategory_MamypokoPantsXXXL" ,
                                                        AFEventParamContentId: "47"
                    ])
            }
            
        }
        else if request.description.contains("/products/wipes/"){
            
            let requestArr = request.description.components(separatedBy: "/products/wipes/")
            let catStr = requestArr.last?.replacingOccurrences(of: "?source=app", with: "")
            
            Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
                AnalyticsParameterItemID: "id-WipesCategory",
                AnalyticsParameterItemName: "WipesCategory",
                AnalyticsParameterContentType: "WipesCategory"
                ])

            if catStr!.description.contains("wipes-with-green-tea-essence"){
                AppsFlyerTracker.shared().trackEvent("WipesCategory_WipesWithGreenTeaEssence",
                                                     withValues: [
                                                        AFEventParamContent: "WipesCategory_WipesWithGreenTeaEssence" ,
                                                        AFEventParamContentId: "48"
                    ])
            }
            else if catStr!.description.contains("pure-soft-wipes-pop-up-box"){
                AppsFlyerTracker.shared().trackEvent("WipesCategory_PureAndSoftWipesUpBox",
                                                     withValues: [
                                                        AFEventParamContent: "WipesCategory_PureAndSoftWipesUpBox" ,
                                                        AFEventParamContentId: "49"
                    ])
                
            }
            else if catStr!.description.contains("pure-soft-wipes"){
                AppsFlyerTracker.shared().trackEvent("WipesCategory_PureAndSoftWipes",
                                                     withValues: [
                                                        AFEventParamContent: "WipesCategory_PureAndSoftWipes" ,
                                                        AFEventParamContentId: "50"
                    ])
            }

        }
		
        return true
    }
	
	
	func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
//		print("ERROR \(error)")
	}
}
