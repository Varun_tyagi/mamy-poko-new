//
//  TopicOfInterestVC.swift
//  MamyPoko
//
//  Created by Surbhi on 28/06/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAnalytics
import AppsFlyerLib

class TopicOfInterestVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    //MARK:- Declerations
    @IBOutlet weak var topicCollection: UICollectionView!
    
    @IBOutlet weak var saveButton: BorderButton!
    
    var selectedcategories = [String]()
    var categoryitems = [Any]()
    var layout: FlowLayout!
    
    //MARK:- View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
		self.BlockSideMenuSwipe()
        self.hideKeyboardWhenTappedAround()
        self.layout = FlowLayout.init()
        self.layout.alignment = FlowAlignment.center
        self.layout.estimatedItemSize = CGSize.init(width: 90, height: 50)
        self.layout.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5 )
        self.topicCollection.collectionViewLayout = self.layout
        self.topicCollection.setCollectionViewLayout(self.layout, animated: true)
        self.getItemsFromAPI()
    }
	override func viewWillAppear(_ animated: Bool) {
      if #available(iOS 13.0, *) {
			let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
			statusBar.backgroundColor = UIColor.white
			 UIApplication.shared.keyWindow?.addSubview(statusBar)
		} else {
			 UIApplication.shared.statusBarView?.backgroundColor = UIColor.white
		 }
	}
    
    //MARK:- Collection View Delegates

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.init(width: SCREEN_WIDTH, height: 300)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize.init(width: SCREEN_WIDTH, height: 351)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionHeader{
            let hview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerCell", for: indexPath) as! TopicInterestHeaderCell
            return hview
        }
        else if kind == UICollectionElementKindSectionFooter{
            let hview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "footercell", for: indexPath) as! TopicInterestFooterCell
            hview.saveButton.addTarget(self, action: #selector(SaveButtonPressed(_:)), for: .touchUpInside)
            return hview
        }
        else{
            let hview = UICollectionReusableView.init(frame: CGRect.init(x: 0, y: 0, width: 0, height: 0 ))
            return hview
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let dict = self.categoryitems[indexPath.item] as! Dictionary<String, Any>
        var strName = dict["name"] as! String
        strName = strName.replacingOccurrences(of: "&amp;", with: "&")
        let wide = Helper.getWidthForText(strName, fon: UIFont.init(name: FONTS.Quicksand_Regular, size: 18)!, height: 40)
        return CGSize.init(width: wide, height: 40)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.categoryitems.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TagCell", for: indexPath) as! TagCell
        cell.textLabel.numberOfLines = 1;
        cell.textLabel.backgroundColor = UIColor.clear
        
        cell.layer.borderColor = UIColor.gray.cgColor
        cell.layer.borderWidth = 1.0
        cell.textLabel.textColor = UIColor.init(red: 77.0/255.0, green: 83.0/255.0, blue: 85.0/255.0, alpha: 1.0)
            
        cell.isHighlighted = false
        let dict = self.categoryitems[indexPath.item] as! Dictionary<String, Any>
        var strName = dict["name"] as! String
        strName = strName.replacingOccurrences(of: "&amp;", with: "&")
        let stralias = dict["alias"] as! String

        if selectedcategories.count > 0{
            for str in selectedcategories{
                if str == stralias{
                    cell.layer.borderColor = Colors.MamyDarkBlue.cgColor
                    cell.layer.borderWidth = 2.0
                    cell.textLabel.textColor = Colors.MamyDarkBlue
                }
            }
        }
    
        cell.textLabel.text = strName
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict = self.categoryitems[indexPath.item] as! Dictionary<String, Any>
        let strName = dict["alias"] as! String

        if selectedcategories.count == 0 {
            selectedcategories.append(strName)
        }
        else if selectedcategories.count > 0{
            if selectedcategories.contains(strName){
                let index = selectedcategories.index(of: strName)
                selectedcategories.remove(at: index!)
            }
            else{
                selectedcategories.append(strName)
            }
        }
        
        self.topicCollection.reloadData()
    }
    
    //MARK:- Button Action
    @objc func  SaveButtonPressed(_ sender: Any) {
        if selectedcategories.count == 0 {
            PKSAlertController.alert(Constants.appName, message: "Please select atleast one interest")
        }
        else{
            self.registerInterests()
        }
    }
    
    //MARK:- API
    
    func getItemsFromAPI() {
        if Helper.isConnectedToNetwork() {
            Constants.appDel.startLoader()
            let url_str = Constants.BASEURL + MethodName.GET_Categories
            Server.getRequestWithURL(urlString: url_str, completionHandler: { (response) in
                Constants.appDel.stopLoader()
                if (response?.count)! > 1 && response?["status"] as! Int ==  1 {
                    self.categoryitems = response!["data"] as! [Any]
                    DispatchQueue.main.async {
                        if  self.categoryitems.count > 0{
                            self.topicCollection.reloadData()
							
                        }
                    }
                }else{
                    PKSAlertController.alert(Constants.appName, message: response?["message"] as! String)
                }
            })
        }else{
            PKSAlertController.alertForNetwok()
        }
    }

    
    func registerInterests() {
        if Helper.isConnectedToNetwork() {
            Constants.appDel.startLoader()
            let urlstr = Constants.BASEURL + MethodName.RegisterStep3
            let userid = UserDefaults.standard.string(forKey: Constants.USERID)
//            let param = selectedcategories.joined(separator: ",")
            let paramStr = "user_id=\(userid!)&area_of_interest=\(selectedcategories)"
            
            Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
                let responseDict = response as Dictionary<String, Any>
                Constants.appDel.stopLoader()
                Analytics.logEvent(AnalyticsEventViewItem, parameters: [
                    AnalyticsParameterItemID: "id-Registration_Interest_Submit",
                    AnalyticsParameterItemName: "Registration_Interest_Submit",
                    AnalyticsParameterContentType: "Registration_Interest"
                    ])
                AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.RegisterIntrestSubmit,
                                                     withValues: [
                                                        AFEventParamContent: AppsFlyerConstant.RegisterIntrestSubmit,
                                                        AFEventParamContentId: "21"
                    ])
                if responseDict["status"] as! Int == 1 {
                    let responseDictionary = responseDict["data"] as! Dictionary<String, Any>
                    _ = responseDict["message"] as! String
                    
                    DispatchQueue.main.async {
                        UserDefaults.standard.set(responseDictionary, forKey: Constants.PROFILE)
                        UserDefaults.standard.set(3, forKey: Constants.RegisterStatus)
						
                        // Go to Home Screen
                            // TAKE to HOME SCreen
                            let root = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SideMenuNavigation")
                            Constants.appDelegate.window?.rootViewController = root
                            UIApplication.shared.keyWindow?.rootViewController = root
                    }

				}
                else{
                    PKSAlertController.alert("Error", message: responseDict["message"] as? String ?? KSomeErrorTryAgain)
                }
            }
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
}
