//
//  RegisterViewController.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 27/04/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift


protocol SignInRegisterationDelegate {
    // Classes that adopt this protocol MUST define
    // this method -- and hopefully do something in
    // that definition.
    func registerationStep1Complete()
    func registerationOTPVerificationDone()
    func registerationStep3Complete()

}

class RegisterViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var registerView = UIView()
    @IBOutlet weak var txt_name = UITextField()
    @IBOutlet weak var txt_mobile = UITextField()
    @IBOutlet weak var txt_mail = UITextField()
    @IBOutlet weak var txt_password = UITextField()
    @IBOutlet weak var txt_city = UITextField()
    @IBOutlet weak var txt_Gender = UITextField()
    @IBOutlet weak var txt_pincode = UITextField()
    @IBOutlet weak var txt_DOB = UITextField()
    @IBOutlet weak var txt_confirm_password = UITextField()
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var termsButton: UIButton!
    @IBOutlet weak var termsCheckButton: UIButton!
    @IBOutlet weak var termsLabel = UILabel()
    @IBOutlet weak var termstext: UITextView!

    @IBOutlet weak var OTPView = UIView()
    @IBOutlet weak var otp_txt1 = UITextField()
    @IBOutlet weak var otp_txt2 = UITextField()
    @IBOutlet weak var otp_txt3 = UITextField()
    @IBOutlet weak var otp_txt4 = UITextField()
    @IBOutlet weak var otp_txt5 = UITextField()
    @IBOutlet weak var otp_txt6 = UITextField()

    @IBOutlet weak var OtpValidateButton: UIButton!
    @IBOutlet weak var resentOTPButton: UIButton!

    @IBOutlet weak var registerTable = UITableView()
    @IBOutlet weak var otpTable = UITableView()

    
    var genderPicker : UIPickerView!
    var datePicker : UIDatePicker!
    
    var delegate: SignInRegisterationDelegate?
    
    var isWaitingForOTP : Bool = false
    var isSocialDetailsAvailable: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
		self.BlockSideMenuSwipe()
        self.hideKeyboardWhenTappedAround()
        genderPicker = UIPickerView()
        genderPicker.tag = 1
        genderPicker.delegate = self
        genderPicker.dataSource = self
        self.txt_Gender?.inputView = genderPicker

		
		IQKeyboardManager.shared.enable = true
		IQKeyboardManager.shared.enableAutoToolbar = true
		IQKeyboardManager.shared.previousNextDisplayMode = IQPreviousNextDisplayMode.default

		
        datePicker = UIDatePicker()
        datePicker.tag = 100
        datePicker.datePickerMode = UIDatePickerMode.date
        datePicker.addTarget(self, action: #selector(self.setDate(_sender:)), for: .valueChanged)
        datePicker.timeZone = TimeZone.current
        self.txt_DOB?.inputView = datePicker
        let toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(dismissPicker))
        self.txt_DOB?.inputAccessoryView = toolBar

        self.otp_txt1?.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        self.otp_txt2?.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        self.otp_txt3?.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        self.otp_txt4?.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        self.otp_txt5?.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        self.otp_txt6?.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        
        
        // SET MiNIMUM & MAximum DAte
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let currentDate: NSDate = NSDate()
        let components: NSDateComponents = NSDateComponents()
        
        components.year = -18
        let minDate : Date = gregorian.date(byAdding: components as DateComponents, to: currentDate as Date, options: NSCalendar.Options.init(rawValue: 0))!
        datePicker.minimumDate = minDate
        datePicker.date = minDate

        components.year = -150
        let maxDate : Date = gregorian.date(byAdding: components as DateComponents, to: currentDate as Date, options: NSCalendar.Options.init(rawValue: 0))!
        datePicker.maximumDate = maxDate

        
        if isWaitingForOTP == false{
            self.registerTable?.isHidden = false
            registerView!.isHidden = false
            self.otpTable?.isHidden = true
            self.OTPView!.isHidden = true
        }
        else{
            self.registerView!.isHidden = true
            self.registerTable?.isHidden = true
            self.otpTable?.isHidden = false
            self.OTPView!.isHidden = false
        }
		
		
		self.txt_name?.setLeftPaddingPoints()
		self.txt_mail?.setLeftPaddingPoints()
		self.txt_DOB?.setLeftPaddingPoints()
		self.txt_Gender?.setLeftPaddingPoints()
		self.txt_pincode?.setLeftPaddingPoints()
		self.txt_city?.setLeftPaddingPoints()
		self.txt_mobile?.setLeftPaddingPoints()
		self.txt_password?.setLeftPaddingPoints()
		self.txt_confirm_password?.setLeftPaddingPoints()

        // to set some initial data
        setupInitialData()
    }
	
	override func viewWillAppear(_ animated: Bool) {
//		if let statusbar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
//			statusbar.backgroundColor = UIColor.white
//		}
	}

    func setupInitialData()  {
        if isSocialDetailsAvailable{
            guard let userData = UserDefaults.standard.value(forKey: Constants.UserDefaultsConstant.userDetailsSocial) as? [String:Any] else{
                return
            }
            if let userName = userData[Constants.SocialUserData.userName]  as? String{
                txt_name?.text = userName
            }
            if let email = userData[Constants.SocialUserData.userEmail]  as? String{
                txt_mail?.text = email
            }
            if let gender = userData[Constants.SocialUserData.userGender]  as? String{
            txt_Gender?.text =  gender.lowercased().contains("male") ? "Male" :  gender.lowercased().contains("female") ? "Female" : ""
            }

        }

        setEyeButton(txt_password!)
        setEyeButton(txt_confirm_password!)
    }
    func setEyeButton(_ textField: UITextField) {
        let eyeView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40))
        let eyebtn = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40))
        eyebtn.setImage(#imageLiteral(resourceName: "eyeClose"), for: .normal)
        eyebtn.setImage(#imageLiteral(resourceName: "eyeOpen"), for: .selected)
        eyebtn.accessibilityElements = [textField]
        eyebtn.addTarget(self, action: #selector(toggleSecureField(_:)), for: .touchUpInside)
        eyeView.addSubview(eyebtn)

        textField.rightView = eyeView
        textField.isSecureTextEntry=true
        textField.rightViewMode = .always

    }

    @objc func toggleSecureField( _ sender: UIButton){
        guard   let textField = sender.accessibilityElements?.first as? UITextField else{
            return
        }
        if sender.isSelected {
            sender.isSelected = false
            textField.isSecureTextEntry = true
        }else{
            sender.isSelected = true
            textField.isSecureTextEntry = false
        }


    }
    

    // MARK: - Textfield Delegates
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
        
        if textField == self.txt_mobile {
            let currentCharacterCount = textField.text?.count ?? 0
            let set = NSCharacterSet(charactersIn: "0123456789").inverted
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            
            
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 10 && string.rangeOfCharacter(from: set) == nil
        }
        
        if textField == self.txt_pincode {
            let currentCharacterCount = textField.text?.count ?? 0
            let set = NSCharacterSet(charactersIn: "0123456789").inverted
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 6 && string.rangeOfCharacter(from: set) == nil
        }
        
        if textField == self.txt_name {
            let set = NSCharacterSet(charactersIn: "ABCDEFGHIJKLMONPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ").inverted
            return string.rangeOfCharacter(from: set) == nil
        }
        
        if textField == self.txt_password {
            let set = NSCharacterSet(charactersIn: "ABCDEFGHIJKLMONPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789").inverted
            
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 10 //&& string.rangeOfCharacter(from: set) == nil
        }
        if textField == self.txt_confirm_password {
            let set = NSCharacterSet(charactersIn: "ABCDEFGHIJKLMONPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789").inverted
            
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 10 //&& string.rangeOfCharacter(from: set) == nil
        }

        
        if self.isWaitingForOTP == true {
            
            let currentCharacterCount = textField.text?.count ?? 0
            let set = NSCharacterSet(charactersIn: "0123456789").inverted
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 1 && string.rangeOfCharacter(from: set) == nil
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.resignKeyboard()
        
        if self.isWaitingForOTP == true {
            if textField == self.otp_txt1 {
                self.otp_txt2?.becomeFirstResponder()
            }
            else if textField == self.otp_txt2 {
                self.otp_txt3?.becomeFirstResponder()
            }
            else if textField == self.otp_txt3 {
                self.otp_txt4?.becomeFirstResponder()
            }
            else if textField == self.otp_txt4 {
                self.otp_txt5?.becomeFirstResponder()
            }
            else if textField == self.otp_txt5 {
                self.otp_txt6?.becomeFirstResponder()
            }
        }
        else{
            if textField == self.txt_name{
                self.txt_mail?.becomeFirstResponder()
            }
            if textField == self.txt_mail{
                if self.txt_pincode?.text!.isEmpty == true {
                    self.txt_pincode?.becomeFirstResponder()
                }
            }
            if textField == self.txt_mobile{
                self.txt_password?.becomeFirstResponder()
            }
            
            if textField == self.txt_password{
                self.txt_confirm_password?.becomeFirstResponder()
            }
        }
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        self.resignKeyboard()

        if textField == self.txt_name{
            self.txt_mail?.becomeFirstResponder()
        }
        else if textField == self.txt_mail{
            if self.txt_pincode?.text!.isEmpty == true {
                self.txt_pincode?.becomeFirstResponder()
            }
        }
        else if textField == self.txt_pincode{
            self.txt_mobile?.becomeFirstResponder()
            //HiT API
            if textField.text?.isEmpty == false{
                self.getCityForPinCode(pin : (self.txt_pincode?.text)!)
            }
        }
        else if textField == self.txt_mobile{
            self.txt_password?.becomeFirstResponder()
        }
        else if textField == self.txt_password{
            self.txt_confirm_password?.becomeFirstResponder()
        }
        
        if textField == self.otp_txt1 {
            self.otp_txt2?.becomeFirstResponder()
        }
        else if textField == self.otp_txt2 {
            self.otp_txt3?.becomeFirstResponder()
        }
        else if textField == self.otp_txt3 {
            self.otp_txt4?.becomeFirstResponder()
        }
        else if textField == self.otp_txt4 {
            self.otp_txt5?.becomeFirstResponder()
        }
        else if textField == self.otp_txt5 {
            self.otp_txt6?.becomeFirstResponder()
        }
    }
    
   @objc func textFieldDidChange(textField: UITextField){
        
        let text = textField.text
        
        if text?.utf16.count==1{
            switch textField{
            case self.otp_txt1:
                self.otp_txt2?.becomeFirstResponder()
            case self.otp_txt2:
                self.otp_txt3?.becomeFirstResponder()
            case self.otp_txt3:
                self.otp_txt4?.becomeFirstResponder()
            case self.otp_txt4:
                self.otp_txt5?.becomeFirstResponder()
            case self.otp_txt5:
                self.otp_txt6?.becomeFirstResponder()
            case self.otp_txt6:
                self.resignKeyboard()
            default:
                break
            }
        }else{
            switch textField{
            case self.otp_txt1:
                self.otp_txt1?.becomeFirstResponder()
            case self.otp_txt2:
                self.otp_txt1?.becomeFirstResponder()
            case self.otp_txt3:
                self.otp_txt2?.becomeFirstResponder()
            case self.otp_txt4:
                self.otp_txt3?.becomeFirstResponder()
            case self.otp_txt5:
                self.otp_txt4?.becomeFirstResponder()
            case self.otp_txt6:
                self.otp_txt5?.becomeFirstResponder()
            default:
                break
            }
        }
    }
    
    func resignKeyboard(){
        
        if self.isWaitingForOTP == true {
            self.otp_txt1?.resignFirstResponder()
            self.otp_txt2?.resignFirstResponder()
            self.otp_txt3?.resignFirstResponder()
            self.otp_txt4?.resignFirstResponder()
            self.otp_txt5?.resignFirstResponder()
            self.otp_txt6?.resignFirstResponder()
        
        }
        else{
            self.view.endEditing(true)
//            self.txt_name?.resignFirstResponder()
//            self.txt_mail?.resignFirstResponder()
//            self.txt_pincode?.resignFirstResponder()
//            self.txt_mobile?.resignFirstResponder()
//            self.txt_password?.resignFirstResponder()
//            self.txt_confirm_password?.resignFirstResponder()
//            self.txt_Gender?.resignFirstResponder()
//            self.txt_DOB?.resignFirstResponder()
        }
    }
    
    func clearKeyboard(){
        
        if self.isWaitingForOTP == true {
            self.otp_txt1?.text = ""
            self.otp_txt2?.text = ""
            self.otp_txt3?.text = ""
            self.otp_txt4?.text = ""
            self.otp_txt5?.text = ""
            self.otp_txt6?.text = ""
            
        }
        else{
            self.txt_name?.text = ""
            self.txt_mail?.text = ""
            self.txt_pincode?.text = ""
            self.txt_mobile?.text = ""
            self.txt_password?.text = ""
            self.txt_confirm_password?.text = ""
            self.txt_Gender?.text = ""
            self.txt_DOB?.text = ""
        }
    }
    
    //MARK:- Date Picker
    @objc func setDate(_sender : UIDatePicker){
        let dateFormatter: DateFormatter = DateFormatter()
        // Set date format
        dateFormatter.dateFormat = "dd-MM-yyyy"
        // Apply date format
        let selectedDate: String = dateFormatter.string(from: _sender.date)
  
        // SET MiNIMUM & MAximum DAte
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let currentDate: NSDate = NSDate()
        let components: NSDateComponents = NSDateComponents()
        
        components.year = -18
        let minDate : Date = gregorian.date(byAdding: components as DateComponents, to: currentDate as Date, options: NSCalendar.Options.init(rawValue: 0))!
        datePicker.minimumDate = minDate

        components.year = -150
        let maxDate : Date = gregorian.date(byAdding: components as DateComponents, to: currentDate as Date, options: NSCalendar.Options.init(rawValue: 0))!
        datePicker.maximumDate = maxDate

        
        if _sender.date < minDate{
            self.txt_DOB?.text = "\(selectedDate)"
        }
        else{
            self.txt_DOB?.text = ""
            datePicker.date = minDate
        }
        
    }
    
    @objc func dismissPicker() {
        
        view.endEditing(true)
        
    }
    
    //MARK:- Gender Picker
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if row == 0 {
            return "Select Gender"
        }
        else if row == 1{
            return "Male"
        }
        else{
            return "Female"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 3
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        
        
        if row == 0{
            self.txt_Gender?.text = ""
            self.view.endEditing(true)
        }
        else if row == 1{
            self.txt_Gender?.text = "Male"
            self.view.endEditing(true)
        }
        else if row == 2{
            self.txt_Gender?.text = "Female"
            self.view.endEditing(true)
        }
        self.resignKeyboard()
    }

    
    //MARK:- Action
    @IBAction func nextButtonForStep1Presed(){

        self.registerForStepOne()
    }

    @IBAction func nextButtonForOTPPresed(){
        
        self.registerForStepTwo()
    }
    
    @IBAction func resendOTPPresed(){
        self.resendOTPAPI()
    }

    @IBAction func TermsButtonAccept(){
        if termsCheckButton.isSelected == true {
            termsCheckButton.isSelected = false
        }
        else{
            termsCheckButton.isSelected = true
        }
    }
    
    //MARL: - Tap gesture
    //MARK:- PINCODE AND CITY API
    func getCityForPinCode(pin : String){
          DispatchQueue.main.async {
        self.view.endEditing(true)
         }
        if Helper.isConnectedToNetwork() {
            Constants.appDel.startLoader()
            let url_str = Constants.BASEURL + MethodName.Pincode + "\(pin)"
            Server.getRequestWithURL(urlString: url_str, completionHandler: { (response) in
                Constants.appDel.stopLoader()
                 if !(response==nil){
                if (response?.count)! > 1 && response?["status"] as? Int ==  1 {
                    let data = response?["data"] as! String
                    DispatchQueue.main.async {
                        self.txt_city?.text = "\(data)"
                        self.txt_mobile?.becomeFirstResponder()
                    }
                }else{
                   // self.presentAlert(withTitle: Constants.appName, message: (response?["message"] as? String) ?? " Invalid pincode, please try again.")
                      DispatchQueue.main.async {
                    self.view.endEditing(true)
                    let alertController = UIAlertController(title: Constants.appName, message: (response?["message"] as? String) ?? " Invalid pincode, please try again.", preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: "OK", style: .default) { action in
                        self.txt_pincode?.text = ""
                        self.txt_pincode?.becomeFirstResponder()
                    }
                    alertController.addAction(OKAction)
                  
                super.present(alertController, animated: true, completion: nil)
                    }
                  //  PKSAlertController.alert(Constants.appName, message: response?["message"] as! String)
                }
            }else{
                PKSAlertController.alert(Constants.appName, message: "Some error has occured! Please try again")
            }
            })
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
    
    func registerForStepOne(){
        if Helper.isConnectedToNetwork() {

            let urlstr = Constants.BASEURL + MethodName.Register

            if self.CheckDataValidation() {
                Constants.appDel.startLoader()
                let gender = self.txt_Gender!.text! == "Male" ? "M" : "F"
                let password = self.txt_password!.text!
                let confirmPassword = self.txt_confirm_password!.text!
                
                let paramStr = "user_name=\(self.txt_name!.text!)&user_email=\(self.txt_mail!.text!)&user_phone=\(self.txt_mobile!.text!)&user_dob=\(self.txt_DOB!.text!)&user_city=\(self.txt_city!.text!)&pincode=\(self.txt_pincode!.text!)&gender=\(gender)&device_type=iOS&device_id=\(AppDelegate.getUniqueDeviceIdentifierAsString())&device_token=\(AppDelegate.getDeviceToken())&password=\(password)&confrm_password=\(confirmPassword)"

                Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
                    let responseDict = response as Dictionary<String, Any>
                    Constants.appDel.stopLoader()
                    
                    if responseDict["status"] as? Int ==  1   || responseDict["status"] as? String == "1" {
                      
                        DispatchQueue.main.async {
//UserDefaults.standard.set(["name":self.txt_name?.text], forKey: Constants.PROFILE)
                         Helper.saveDataInNsDefault(object: ["name":self.txt_name?.text] as AnyObject, key: Constants.PROFILE)
                            UIView.animate(withDuration: 0.2, animations: {
                                
                                self.registerView!.alpha = 0.5
                                self.otpTable!.isHidden = false
                                self.OTPView!.alpha = 1.0
                            }) { (value) in
                                
                                self.registerView!.isHidden = true
                                self.registerTable!.isHidden = true
                                self.otpTable!.isHidden = false
                                self.OTPView!.isHidden = false
                                self.isWaitingForOTP = true;
                                self.otp_txt1?.becomeFirstResponder()
                            }
                        }
                    }
                    else{
                        PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                    }
                }
            }

        }else{
            PKSAlertController.alertForNetwok()
        }
    }
    
    
    func CheckDataValidation()-> (Bool){
        let userName = "\(self.txt_name!.text!)"
        let userPhone = "\(String(describing: self.txt_mobile!.text!))"
        let userDOB = "\(String(describing: self.txt_DOB!.text!))"
        let userPin = "\(String(describing: self.txt_pincode!.text!))"
        let gender = "\(String(describing: self.txt_Gender!.text!))"
        let password = "\(String(describing: self.txt_password!.text!))"
        let confirmPassword = "\(String(describing: self.txt_confirm_password!.text!))"

        if userName.isEmpty == true {
            PKSAlertController.alert(Constants.appName, message: "Enter your name !")
            return false
        }
        
        if self.txt_mail!.text!.isEmpty == true {
            PKSAlertController.alert(Constants.appName, message: "Enter email id !")
            return false
        }
        else if Helper.isValidEmail(self.txt_mail!.text!) == false {
            PKSAlertController.alert(Constants.appName, message: "Enter valid email id !")
            return false
        }
        
        if gender.isEmpty == true{
            PKSAlertController.alert(Constants.appName, message: "Select Gender !")
            return false
        }
        
        if userDOB.isEmpty == true {
            PKSAlertController.alert(Constants.appName, message: "Enter DOB !")
            return false
        }
        
        if userPin.isEmpty == true {
            PKSAlertController.alert(Constants.appName, message: "Enter pincode !")
            return false
        }
        else if !Helper.isValidPinCode(pinnumber: userPin){
            PKSAlertController.alert(Constants.appName, message: "Enter valid pincode !")
            return false
        }
        
        if userPhone.isEmpty == true {
            PKSAlertController.alert(Constants.appName, message: "Enter mobile no !")
            return false
        }
        else if !Helper.isValidPhoneNumber(phoneNumber: userPhone){
            PKSAlertController.alert(Constants.appName, message: "Enter valid mobile no !")
            return false
        }
        
        if password.isEmpty == true {
            PKSAlertController.alert(Constants.appName, message: "Enter password !")
            return false
        }
        if self.txt_Gender!.text!.isEmpty == true{
            PKSAlertController.alert(Constants.appName, message: "Select gender !")
            return false
        }
        if self.txt_DOB!.text!.isEmpty == true{
            PKSAlertController.alert(Constants.appName, message: "Select date of birth !")
            return false
        }

        else if !Helper.isValidPasword(pass: self.txt_password!.text!){
            
            if self.txt_password!.text!.count < 6 {
                PKSAlertController.alert(Constants.appName, message: "Enter min 6 character password !")
                return false
            }
            else{
                PKSAlertController.alert(Constants.appName, message: "Enter at least 1 alphabet and 1 number !")
                return false
            }
        }
        
        if confirmPassword.isEmpty == true {
            PKSAlertController.alert(Constants.appName, message: "Enter confirm password !")
            return false
        }
        else if confirmPassword != password {
            PKSAlertController.alert(Constants.appName, message: "Enter same password !")
            return false
        }
        
        if termsCheckButton.isSelected == false{
            PKSAlertController.alert(Constants.appName, message: "Accept the terms and conditions !")
            return false
        }
        
        return true
    }
    
    
    func registerForStepTwo(){
        if Helper.isConnectedToNetwork() {
            
            let urlstr = Constants.BASEURL + MethodName.OTPVerify

            let otp1 = "\(self.otp_txt1!.text!)"
            let otp2 = "\(self.otp_txt2!.text!)"
            let otp3 = "\(self.otp_txt3!.text!)"
            let otp4 = "\(self.otp_txt4!.text!)"
            let otp5 = "\(self.otp_txt5!.text!)"
            let otp6 = "\(self.otp_txt6!.text!)"

            
            if otp1.isEmpty == true || otp2.isEmpty == true || otp3.isEmpty == true || otp4.isEmpty == true || otp5.isEmpty == true || otp6.isEmpty == true{
                PKSAlertController.alert(Constants.appName, message: "Enter valid 6 digit OTP !")
            }
            else{
                Constants.appDel.startLoader()
                let otpString = "\(otp1)\(otp2)\(otp3)\(otp4)\(otp5)\(otp6)"
                let paramStr = "otp_val=\(otpString)&user_mobile=\(self.txt_mobile!.text!)"

                Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
                    let responseDict = response as Dictionary<String, Any>
                    Constants.appDel.stopLoader()
                    
                    if responseDict["status"] as! Int ==  2 {
                        let responseDictionary = responseDict["data"] as! Dictionary<String, Any>
                        let userID = "\(responseDictionary["user_id"] as! NSNumber)"
                        let gender = responseDictionary["gender"] as! String
                        let message = responseDict["message"] as! String

                        DispatchQueue.main.async {
                            UserDefaults.standard.set(1, forKey: Constants.RegisterStatus)
                            UserDefaults.standard.set(userID, forKey: Constants.USERID)
                            UserDefaults.standard.set(gender, forKey: Constants.GENDER)

                            let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            let acceptButton = UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction) in
                                self.delegate?.registerationOTPVerificationDone()
                                NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: Constants.refreshGenderNotification), object: nil)
                            })
                            alert.addAction(acceptButton)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    else if responseDict["status"] as! Int ==  0 {
                        DispatchQueue.main.async {
                            self.resignKeyboard()
                            self.clearKeyboard()
                            PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                        }
                    }
                    else{
                        PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                        
                    }
                }
            }

        }else{
            PKSAlertController.alertForNetwok()
        }
    }
    
    
    func resendOTPAPI(){
        if Helper.isConnectedToNetwork() {
            Constants.appDel.startLoader()
            let urlstr = Constants.BASEURL + MethodName.ResendOTP
            let paramStr = "user_mobile=\(self.txt_mobile!.text!)"
        
            Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
                let responseDict = response as Dictionary<String, Any>
                Constants.appDel.stopLoader()
                
                if responseDict["status"] as! Int ==  1  {
                    DispatchQueue.main.async {
                        UIView.animate(withDuration: 0.2, animations: {
                            
                        }) { (value) in
                            self.isWaitingForOTP = true;
                            self.clearKeyboard()
                        }
                    }
                }
                else{
                    PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                }
            }
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
    
}


extension UIToolbar {
    
    func ToolbarPiker(mySelect : Selector) -> UIToolbar {
        
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: mySelect)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([ spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        return toolBar
    }
    
}
extension UIViewController {
    
    func presentAlert(withTitle title: String, message : String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction.init(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        DispatchQueue.main.async {
         UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
        }
        //self.present(alertController, animated: true, completion: nil)
    }
}
