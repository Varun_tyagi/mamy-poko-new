//
//  RegisterVC.swift
//  MamyPoko
//
//  Created by Surbhi on 28/05/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import Foundation
import UIKit
import IQKeyboardManagerSwift
class RegisterVC: UIViewController ,UIScrollViewDelegate, SignInRegisterationDelegate{
    
    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var mainScrollContentView: UIView!
    @IBOutlet weak var mainContentHeight: NSLayoutConstraint!
    @IBOutlet weak var headerScroll: UIScrollView!
    @IBOutlet weak var detailScroll: UIScrollView!
    @IBOutlet weak var detailScrollContentView: UIView!
    @IBOutlet weak var detailContentHeight: NSLayoutConstraint!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!

    static let OTPViewHeight = 360 //+ 185
    static let RegisterViewHeight = 515  // +185 remaining height
    let detailedscrollheight = SCREEN_HEIGHT - 185.0
    var page = 0
    
    var StepButton = UIButton()
    private var indicatorLabel : UILabel!
    private var ControllerArray = [AnyObject]()
    private var headerArray = ["Step1","Step2"]
    private var headerlabelArray = ["Basic Information","Parental Status"]
    
    var signIn =   Constants.mainStoryboard.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController //RegisterViewController()
    var signUp = RegisterViewControllerStep2()
    let unselectColor = UIColor(red: 182.0/255.0, green:  197.0/255.0, blue: 203.0/255.0, alpha: 1.0)
    var registerStatus : Int = UserDefaults.standard.integer(forKey: Constants.RegisterStatus)


    //Terms Attributiom
    var layoutManager = NSLayoutManager()
    var textContainer = NSTextContainer()
    var textStorage = NSTextStorage()

    
    //MARK:- View Methods
	override func viewWillAppear(_ animated: Bool) {
//		if let statusbar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
//			statusbar.backgroundColor = UIColor.white
//		}
	}

    override func viewDidLoad() {
        super.viewDidLoad()
		self.BlockSideMenuSwipe()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
		
        self.hideKeyboardWhenTappedAround()
        Constants.appDel.startLoader()
        self.setHeaderScroll()
        self.setUpScrollView()
        
        if registerStatus == 0 {
            Constants.appDel.stopLoader()
        }
        else if registerStatus == 1{
            self.registerationOTPVerificationDone()
            Constants.appDel.stopLoader()
        }
        else{
            Constants.appDel.stopLoader()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        Constants.appDel.stopLoader()
    }
    
    //MARK:- HEADER SCROLL
    func setHeaderScroll() {
        var scrollContentCount = 0;
        var frame : CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
        var labelx : CGFloat = 0.0
        let width  : CGFloat = SCREEN_WIDTH/2
        let firstLabelWidth : CGFloat = SCREEN_WIDTH/2
        
        for arrayIndex in 0 ..< headerArray.count {
            frame.origin.x = labelx
            // set scorllview properties
            frame.origin.y = 0;
            frame.size.width=width;
            frame.size.height=35;
            
            StepButton = UIButton.init(type: UIButtonType.custom)
            StepButton.backgroundColor = .clear
            StepButton.frame = frame;
            StepButton.tag = arrayIndex;
            
            StepButton.titleLabel?.textAlignment = NSTextAlignment.center
//            StepButton.addTarget(self , action: #selector(HeaderlabelSelected), for: UIControlEvents.touchUpInside)
            
            indicatorLabel = UILabel.init(frame: CGRect(x: labelx,y: 35,width: firstLabelWidth,height: 35))
            indicatorLabel.backgroundColor = .clear
            indicatorLabel.textColor = UIColor.black
            indicatorLabel.tag = arrayIndex+100;

            headerScroll.addSubview(StepButton)
            headerScroll.addSubview(indicatorLabel)
            StepButton.alpha=1.0;
            
            if arrayIndex == 0 {
                
                if self.detailedscrollheight < 470{
                    detailContentHeight.constant = 470.0
                }
                
                detailScroll.contentSize = CGSize.init(width: detailScroll.contentSize.width, height: 515.0)
               
                
                let AttStr1 = NSAttributedString(string: "\(headerArray[arrayIndex])",  attributes: [NSAttributedStringKey.font : UIFont.init(name: FONTS.AdiraDisplaySSi, size: 30.0)!, NSAttributedStringKey.foregroundColor : Colors.appThemeColorText])
                let AttStr2 = NSAttributedString(string: headerlabelArray[arrayIndex],  attributes: [NSAttributedStringKey.font : UIFont.init(name: FONTS.QuicksandBook_Regular, size: 17.0)!, NSAttributedStringKey.foregroundColor : UIColor.init(red: 76.0/255.0, green: 82.0/255.0, blue: 84.0/255.0, alpha: 1.0)])

                StepButton.setAttributedTitle(AttStr1, for:  UIControlState.normal)
                indicatorLabel.attributedText = AttStr2
                indicatorLabel.textAlignment = .center

                if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS {
                    let AttStr1 = NSAttributedString(string: "\(headerArray[arrayIndex])\n",  attributes: [NSAttributedStringKey.font : UIFont.init(name: FONTS.AdiraDisplaySSi, size: 25.0)!, NSAttributedStringKey.foregroundColor : Colors.appThemeColorText])
                    let AttStr2 = NSAttributedString(string: headerlabelArray[arrayIndex],  attributes: [NSAttributedStringKey.font : UIFont.init(name: FONTS.QuicksandBook_Regular, size: 15.0)!, NSAttributedStringKey.foregroundColor : UIColor.init(red: 76.0/255.0, green: 82.0/255.0, blue: 84.0/255.0, alpha: 1.0)])
                    StepButton.setAttributedTitle(AttStr1, for:  UIControlState.normal)
                    indicatorLabel.attributedText = AttStr2
                }
            }
            else {
                
                if self.detailedscrollheight < 647.0 {
                    detailContentHeight.constant = 647.0
                }
                
                let AttStr1 = NSAttributedString(string: "\(headerArray[arrayIndex])",  attributes: [NSAttributedStringKey.font : UIFont.init(name: FONTS.AdiraDisplaySSi, size: 30.0)!, NSAttributedStringKey.foregroundColor : unselectColor])
                let AttStr2 = NSAttributedString(string: headerlabelArray[arrayIndex],  attributes: [NSAttributedStringKey.font : UIFont.init(name: FONTS.QuicksandBook_Regular, size: 17.0)!, NSAttributedStringKey.foregroundColor : unselectColor])
                
                StepButton.setAttributedTitle(AttStr1, for:  UIControlState.normal)
                indicatorLabel.attributedText = AttStr2
                indicatorLabel.textAlignment = .center
                
                if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS {
                    let AttStr1 = NSAttributedString(string: "\(headerArray[arrayIndex])\n",  attributes: [NSAttributedStringKey.font : UIFont.init(name: FONTS.AdiraDisplaySSi, size: 25.0)!, NSAttributedStringKey.foregroundColor : unselectColor])
                    let AttStr2 = NSAttributedString(string: headerlabelArray[arrayIndex],  attributes: [NSAttributedStringKey.font : UIFont.init(name: FONTS.QuicksandBook_Regular, size: 15.0)!, NSAttributedStringKey.foregroundColor : unselectColor])
                    StepButton.setAttributedTitle(AttStr1, for:  UIControlState.normal)
                    indicatorLabel.attributedText = AttStr2
                }
                StepButton.alpha=0.7;
            }
            
            labelx = labelx + width;
            scrollContentCount = scrollContentCount + 1;
        }
        
        // set scroll content size
        DispatchQueue.main.async(execute: {
            self.headerScroll.contentSize = CGSize(width: frame.width,height: 70)
        })
        automaticallyAdjustsScrollViewInsets = false;
        detailScroll.delegate=self;
    }
    
    
    @objc func HeaderlabelSelected(sender: UIButton) {
        
        detailScroll.tag = sender.tag
        if sender.tag == 0 && signIn.isWaitingForOTP == false {
            if detailedscrollheight < 470{
                self.detailContentHeight.constant = 470.0
            }
        }
        else{
            self.detailContentHeight.constant = 647
        }
        
        let buttonArray = NSMutableArray()
        let labelArray = NSMutableArray()

        let xAxis : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(sender.tag)
        
        
        for view in headerScroll.subviews {
            if view.isKind(of: UIButton.self) {
                let labelButton = view as! UIButton
                buttonArray.add(labelButton)
                
                page = sender.tag
                
                var attributeselected1 = [NSAttributedStringKey.font : UIFont.init(name: FONTS.AdiraDisplaySSi, size: 30.0)!, NSAttributedStringKey.foregroundColor : Colors.appThemeColorText]
                var attributeselected2 = [NSAttributedStringKey.font : UIFont.init(name: FONTS.AdiraDisplaySSi, size: 30.0)!, NSAttributedStringKey.foregroundColor : unselectColor]

                if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS {
                    attributeselected1 = [NSAttributedStringKey.font : UIFont.init(name: FONTS.AdiraDisplaySSi, size: 25.0)!, NSAttributedStringKey.foregroundColor : Colors.appThemeColorText]
                    attributeselected2 = [NSAttributedStringKey.font : UIFont.init(name: FONTS.AdiraDisplaySSi, size: 25.0)!, NSAttributedStringKey.foregroundColor : unselectColor]
                }

                
                if labelButton.tag == sender.tag {
                    labelButton.alpha = 1.0
                    
                    sender.setAttributedTitle(NSAttributedString.init(string:  headerArray[sender.tag], attributes: attributeselected1), for: UIControlState.normal)
                }
                else {
                    labelButton.alpha = 0.7
                    labelButton.setAttributedTitle(NSAttributedString.init(string:  headerArray[labelButton.tag], attributes: attributeselected2), for: UIControlState.normal)
                }
                

            }
            else if view.isKind(of: UILabel.self){
                let labelindicator = view as! UILabel
                labelArray.add(labelindicator)
                
                
                var attributeselected1 = [NSAttributedStringKey.font : UIFont.init(name: FONTS.QuicksandBook_Regular, size: 17.0)!, NSAttributedStringKey.foregroundColor : UIColor.init(red: 76.0/255.0, green: 82.0/255.0, blue: 84.0/255.0, alpha: 1.0)]
                var attributeselected2 = [NSAttributedStringKey.font : UIFont.init(name: FONTS.QuicksandBook_Regular, size: 17.0)!, NSAttributedStringKey.foregroundColor : unselectColor]
                
                if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS {
                    attributeselected1 = [NSAttributedStringKey.font : UIFont.init(name: FONTS.QuicksandBook_Regular, size: 15.0)!, NSAttributedStringKey.foregroundColor : UIColor.init(red: 76.0/255.0, green: 82.0/255.0, blue: 84.0/255.0, alpha: 1.0)]
                    attributeselected2 = [NSAttributedStringKey.font : UIFont.init(name: FONTS.QuicksandBook_Regular, size: 15.0)!, NSAttributedStringKey.foregroundColor : unselectColor]
                }
                
                
                
                if (labelindicator.tag - 100) == sender.tag {
                    labelindicator.alpha = 1.0
                    labelindicator.attributedText = NSAttributedString.init(string: headerlabelArray[sender.tag], attributes: attributeselected1)
                    labelindicator.textAlignment = .center
                }
                else {
                    labelindicator.alpha = 0.7
                    labelindicator.textColor = UIColor.darkGray
                    labelindicator.attributedText = NSAttributedString.init(string: headerlabelArray[(labelindicator.tag - 100)], attributes: attributeselected2)
                    labelindicator.textAlignment = .center
                }
            }
        }
        
        
        UIView.animate(withDuration: 0.2, animations: {
            self.detailScroll.setContentOffset(CGPoint.init(x: xAxis, y: 0), animated: true)
            self.headerScroll.scrollRectToVisible(self.indicatorLabel.frame, animated: false)

        }, completion: nil)
    }
    
    // MARK: -  Main Scrollview
    func setUpScrollView() {
        for arrayIndex in headerArray {
            if (arrayIndex == "Step1")  {
                signIn.delegate = self
                ControllerArray.append(signIn)
            }
            if (arrayIndex == "Step2") {
                signUp = Constants.mainStoryboard.instantiateViewController(withIdentifier: "RegisterViewControllerStep2") as! RegisterViewControllerStep2
                signUp.delegate = self
                ControllerArray.append(signUp)
            }
        }
        
        setViewControllers(viewControllers: ControllerArray as NSArray, animated: false)
    }
    
    
    //  Add and remove view controllers
    func setViewControllers(viewControllers : NSArray, animated : Bool) {
        if self.childViewControllers.count > 0 {
            for vC in self.childViewControllers {
                vC.willMove(toParentViewController: nil)
                vC.removeFromParentViewController()
            }
        }
        
        for vC in viewControllers {
            self.addChildViewController(vC as! UIViewController)
            
        }
        
        //TODO animations
        if ((detailScroll) != nil) {
            reloadPages()
        }
        automaticallyAdjustsScrollViewInsets = false;

        if self.childViewControllers.count > 1 {
            let attributedString = NSMutableAttributedString.init(string: "I accept the T&C, Privacy Policy and Disclaimer")
            let linkRange = NSMakeRange(13, 34)

            let linkRange1 = NSMakeRange(0, 12)
            
            let linkAttributes : [NSAttributedStringKey : Any] = [
                NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor.init(red: 255.0/255.0, green: 64.0/255.0, blue: 129.0/255.0, alpha: 1.0),
                NSAttributedStringKey(rawValue: NSAttributedStringKey.underlineColor.rawValue): UIColor.init(red: 255.0/255.0, green: 64.0/255.0, blue: 129.0/255.0, alpha: 1.0),
                NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont.systemFont(ofSize: 18.0)]
            
            let linkAttributes1 : [NSAttributedStringKey : Any] = [
                NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor.init(red: 132.0/255.0, green: 132.0/255.0, blue: 132.0/255.0, alpha: 1.0),
                NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): UIFont.systemFont(ofSize: 18.0)]
            
            attributedString.addAttributes(linkAttributes, range: linkRange)
            attributedString.addAttributes(linkAttributes1, range: linkRange1)
            
            
            
            
            // Assign attributedText to UILabel
            signIn.termsLabel?.attributedText = attributedString;
            signIn.termstext.attributedText = attributedString;

            signIn.termstext?.isUserInteractionEnabled = true
            signIn.termstext?.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(TermsButtonPressed(tapGesture:)                                                       )))
            //(UIGestureRecognizer.init(target: self, action: #selector(TermsButtonPressed)))
            
            layoutManager = NSLayoutManager.init()
            textContainer = NSTextContainer.init(size: CGSize.zero)
            textStorage = NSTextStorage.init(attributedString: attributedString)
            
            // Configure layoutManager and textStorage
            layoutManager.addTextContainer(textContainer)
            textStorage.addLayoutManager(layoutManager)
            
            // Configure textContainer
            textContainer.lineFragmentPadding = 0.0
            textContainer.lineBreakMode = NSLineBreakMode.byWordWrapping
            textContainer.maximumNumberOfLines = 0
            
            textContainer.size = signIn.termstext?.bounds.size ?? CGSize.zero;
        }
        
    }
    
    func reloadPages() {
        var scrollContentCount = 0;
        
        for arrayIndex in 0 ..< headerArray.count {
//            signIn.termsButton?.addTarget(self, action: #selector(TermsButtonPressed), for: .touchUpInside)
            
            // set scorllview properties
            var frame : CGRect = CGRect(x: 0, y: 0, width: 0, height: 0) //(0, 0, 0, 0)
            frame.origin.x = ScreenSize.SCREEN_WIDTH * CGFloat(arrayIndex)
            frame.origin.y = 0;
            frame.size = ScreenSize.SCREEN.size
            if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS{
                frame.size = CGSize.init(width: ScreenSize.SCREEN_WIDTH, height: SCREEN_HEIGHT-165)
            }
            
            detailScroll.isPagingEnabled = true
            let contentWidth : CGFloat = ScreenSize.SCREEN_WIDTH * CGFloat(scrollContentCount+1)
            widthConstraint.constant = contentWidth;
            
            let vC = self.childViewControllers[arrayIndex]
            addView(view: vC.view, contentView: detailScrollContentView, frame: frame)
            
            scrollContentCount = scrollContentCount + 1;
        }
        
        DispatchQueue.main.async(execute: {
            self.detailScroll.contentSize = CGSize(width: self.widthConstraint.constant, height:self.detailScroll.frame.size.height) //self.detailScroll.frame.size.height)//(self.widthConstraint.constant, self.detailScroll.frame.size.height)
        })
        detailScroll.delegate = self
    }
    
    // adding views to scrollview
    func addView(view : UIView, contentView : UIView, frame : CGRect) {
        view.frame = frame
        view.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(view)
        
        
        DispatchQueue.main.async(execute: {
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: contentView, attribute:  NSLayoutAttribute.leading, multiplier: 1.0, constant: frame.origin.x))
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute:  NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: frame.size.width))
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: contentView, attribute:  NSLayoutAttribute.top, multiplier: 1.0, constant:0))
            contentView.addConstraint(NSLayoutConstraint.init(item: view, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute:  NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: frame.size.height))
        })
    }
    
    //MARK:- Registration View Delegates
    func registerationStep1Complete() {
        if self.detailedscrollheight < 360{
            self.detailContentHeight.constant = 360
        }
        
        
        self.reloadPages()
        self.signUp.viewWillLayoutSubviews()
        self.signUp.tblView.reloadData()
    }
    
    func registerationOTPVerificationDone() {
        
        let newbutton = StepButton
        newbutton.tag = 1
        self.HeaderlabelSelected(sender: newbutton)
        self.reloadPages()

    }
    
    func registerationStep3Complete() {
        // Hide current screen and display new Screen
        let topiInterest = Constants.mainStoryboard.instantiateViewController(withIdentifier: "TopicOfInterestVC") as! TopicOfInterestVC
        DispatchQueue.main.async {
            self.present(topiInterest, animated: true, completion: nil)
        }

    }
    
    //MARK:- Terms Button Pressed on Step1 register Screen
    @objc func TermsButtonPressed(tapGesture : UITapGestureRecognizer){
        
        let textView = tapGesture.view as! UITextView
        let layoutManager = textView.layoutManager

        var locationOfTouchInLabel: CGPoint = tapGesture.location(in: tapGesture.view)
        locationOfTouchInLabel.x -= (textView.textContainerInset.left)
        locationOfTouchInLabel.y -= (textView.textContainerInset.top)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInLabel, in: (textView.textContainer), fractionOfDistanceBetweenInsertionPoints: nil)
        
        guard indexOfCharacter < textView.textStorage.length else {
            return
        }
        
        let linkRangeTC = NSMakeRange(13, 4)
        let linkRangePP = NSMakeRange(18, 15)
        let linkRangeDis = NSMakeRange(34, 10)

       
        // TERMS VC
        let termsvc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
        termsvc.isComingFromRegister = true

        
        //Checking Range
        if NSLocationInRange(indexOfCharacter, linkRangeTC){
            termsvc.type = "T&C"
            DispatchQueue.main.async {
                self.present(termsvc, animated: true, completion: nil)
            }
        }
        else if NSLocationInRange(indexOfCharacter, linkRangePP){
            termsvc.type = "Privacy"
            DispatchQueue.main.async {
                self.present(termsvc, animated: true, completion: nil)
            }
        }
        else if NSLocationInRange(indexOfCharacter, linkRangeDis){
            termsvc.type = "Disclaimer"
            termsvc.isDisclaimerFromMenu=true
            DispatchQueue.main.async {
                self.present(termsvc, animated: true, completion: nil)
            }
        }
        else if indexOfCharacter>32 && indexOfCharacter < 46{
            termsvc.type = "Disclaimer"
            termsvc.isDisclaimerFromMenu=true
            DispatchQueue.main.async {
                self.present(termsvc, animated: true, completion: nil)
            }
        }else{
            
        }


    }
    
}


