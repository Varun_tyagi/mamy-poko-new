//
//  RegisterViewControllerStep2.swift
//  MamyPoko
//
//  Created by Surbhi on 27/06/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import IQKeyboardManagerSwift
import AppsFlyerLib

let refreshGenderNotification = Constants.refreshGenderNotification

class RegisterViewControllerStep2: UIViewController {
    
    //MARK:- Declerations
    
    @IBOutlet weak var categoryPregnantView: UIView!
    @IBOutlet weak var tblView: UITableView!

    var delegate: SignInRegisterationDelegate?
    var selectedSection = [Int]()
    //MARK:- IBOutlets from Planning a Baby
    var firstdeliveryBtn = UIButton()
    var diabetesbtn = UIButton()
    var thyroidbtn = UIButton()
    var otehrbtn = UIButton()
    var textV = UITextView()
    var textVHeight = NSLayoutConstraint()
    var isOtherBtnSelected : Bool = false
    
    var firstpregnancyBTN : UIButton!
    var lastDOPtxt : RoundTextField!
    var expectedDeliverTXT : RoundTextField!
    var datePicker : UIDatePicker!
    var datePicker2 : UIDatePicker!
    var toolBar : UIToolbar!
    var gender =  "f"
    static let pregnantViewColor = UIColor.init(red: 212.0/255.0, green: 220.0/255.0, blue: 223.0/255.0, alpha: 1.0)
    // baby details
    var babyNameTxt, babyGenderTxt, babyDOBTxt: RoundTextField!
    var weightTxt, heightTxt, circumferenceTxt: RoundTextField!
    var lbsButton,kgButton, cmButton, inchButton, cm_cButton,inch_cButton: UIButton!
    var babyDetails: BabyDetails? = nil

    //MARK:- View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
       gender =  UserDefaults.standard.value(forKey: Constants.GENDER)  as? String ?? "f"
        //Registration_First_Step_Complete
        Analytics.logEvent(AnalyticsEventViewItem, parameters: [
            AnalyticsParameterItemID: "id-Registration_First_Step_Complete",
            AnalyticsParameterItemName: "Registration_First_Step_Complete",
            AnalyticsParameterContentType: "Registration_First_Step"
            ])
        AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.RegisterFirstStepComplete,
                                             withValues: [
                                                AFEventParamContent: AppsFlyerConstant.RegisterFirstStepComplete,
                                                AFEventParamContentId: "18"
            ])

        NotificationCenter.default.addObserver(self, selector: #selector(refreshGenderNotification(sender:)), name: NSNotification.Name.init(rawValue: Constants.refreshGenderNotification), object: nil)
        
        self.hideKeyboardWhenTappedAround()
        // SET MiNIMUM & MAximum DAte
        datePicker = UIDatePicker()
        datePicker.tag = 100
        datePicker.datePickerMode = UIDatePickerMode.date
        datePicker.addTarget(self, action: #selector(self.setDate(_sender:)), for: .valueChanged)
        datePicker.timeZone = TimeZone.current
        toolBar = UIToolbar()
        toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(dismissPicker))
        
        datePicker2 = UIDatePicker()
        datePicker2.tag = 200
        datePicker2.datePickerMode = UIDatePickerMode.date
        datePicker2.addTarget(self, action: #selector(self.setDate(_sender:)), for: .valueChanged)
        datePicker2.timeZone = TimeZone.current

        datePicker.setLastPeriodValidation()
        datePicker2.setExpectedDateValidation()
        self.babyDetails = BabyDetails()
        self.babyDetails?.babyWeightUnit = "kg."
        self.babyDetails?.headCircumfrenceUnit="cm."
        self.babyDetails?.babyHeightUnit="cm."
    }
    
    @objc func refreshGenderNotification(sender: Notification){
        gender =  UserDefaults.standard.value(forKey: Constants.GENDER)  as? String ?? "f"
        self.tblView.reloadData()
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
       
    }
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(true)
		self.BlockSideMenuSwipe()
		IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
		IQKeyboardManager.shared.previousNextDisplayMode = IQPreviousNextDisplayMode.default
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        IQKeyboardManager.shared.enableAutoToolbar = false
		IQKeyboardManager.shared.previousNextDisplayMode = IQPreviousNextDisplayMode.default
    }
    
    
    //MARK:- Planning a Baby Section Button Actions
    @objc func OtherButtonPressed() {
        self.isOtherBtnSelected = self.isOtherBtnSelected==true ? false : true
        otehrbtn.isSelected = self.isOtherBtnSelected==true ? false : true
        self.tblView.reloadData()
    }
    
    @objc func DiabitiesButtonPressed() {
        self.diabetesbtn.isSelected =  !self.diabetesbtn.isSelected //== true ? false : true
    }
    @objc func ThyroidButtonPressed() {
        self.thyroidbtn.isSelected = !self.thyroidbtn.isSelected
    }
    @objc func isFirstDeliveryPressed(_ gestureRecognizer: UITapGestureRecognizer) {
        firstdeliveryBtn.isSelected = !firstdeliveryBtn.isSelected
    }
    //MARK:- Button Actions Im Pregnant
    @objc func isFirstPregnancyPressed(_ gestureRecognizer: UITapGestureRecognizer) {
        firstpregnancyBTN.isSelected = !firstpregnancyBTN.isSelected
    }
    
     //MARK:- BABY DETAILS
    @IBAction func KGButtonPressed(_ sender: UIButton) {
        kgButton.isSelected = true
        lbsButton.isSelected = false
        self.babyDetails?.babyWeightUnit = "kg."
    }
    @IBAction func LBSButtonPressed(_ sender: UIButton) {
        kgButton.isSelected = false
        lbsButton.isSelected = true
        self.babyDetails?.babyWeightUnit = "lb."
    }
    @IBAction func CMButtonPressed(_ sender: UIButton) {
        cmButton.isSelected = true
        inchButton.isSelected = false
        self.babyDetails?.babyHeightUnit = "cm."
    }
    @IBAction func InchButtonPressed(_ sender: UIButton) {
        cmButton.isSelected = false
        inchButton.isSelected = true
        self.babyDetails?.babyHeightUnit = "in."
    }
    @IBAction func CM_cButtonPressed(_ sender: UIButton) {
        cm_cButton.isSelected = true
        inch_cButton.isSelected = false
        self.babyDetails?.headCircumfrenceUnit = "cm."
    }
    @IBAction func Inch_cButtonPressed(_ sender: UIButton) {
        cm_cButton.isSelected = false
        inch_cButton.isSelected = true
        self.babyDetails?.headCircumfrenceUnit = "in."
    }
    @IBAction func SaveBabyDetailsPressed(_ sender: UIButton) {
        // hit service for baby details // check ProfileApiHelper.Swift for method defination
        self.view.endEditing(true)
        if  (self.babyNameTxt.text?.isEmpty)!{
            PKSAlertController.alert(Constants.appName, message: "Enter baby name.")
            return
        }else if  (self.babyGenderTxt.text?.isEmpty)!{
            PKSAlertController.alert(Constants.appName, message: "Select gender!")
            return
        }
        else if  (self.babyDOBTxt.text?.isEmpty)!{
            PKSAlertController.alert(Constants.appName, message: "Enter DOB!")
            return
        }else if  (self.weightTxt.text?.isEmpty)!{
            PKSAlertController.alert(Constants.appName, message: "Enter weight!")
            return
        }else if  (self.heightTxt.text?.isEmpty)!{
            PKSAlertController.alert(Constants.appName, message: "Enter height!")
            return
        }else if  (self.circumferenceTxt.text?.isEmpty)!{
            PKSAlertController.alert(Constants.appName, message: "Enter head circumference!")
            return
        }
       registerForBabyDetails()
       
    }
     //MARK:- Save Details and Call API
    
    @objc func saveButtonPressed(_ gestureRecognizer: UITapGestureRecognizer) {
        if isOtherBtnSelected &&  self.textV.text.isEmpty{
            PKSAlertController.alert(Constants.appName, message: "Please specify medical history")
        }
        if selectedSection.contains(1) {
            self.registerForStepThree_Pregnant()
        }
        else if selectedSection.contains(0) {
            self.registerForStepThree_Planning()
        }else if selectedSection.contains(2){
            
        }
    }
    
    //MARK:- Date Picker
    @objc func setDate(_sender : UIDatePicker){
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let selectedDate: String = dateFormatter.string(from: _sender.date)
        if _sender.tag == 100 {
            datePicker.setLastPeriodValidation()
            if _sender.date > datePicker.minimumDate! && _sender.date < datePicker.maximumDate! {
                self.lastDOPtxt?.text = "\(selectedDate)"
            }
            else{
                self.lastDOPtxt?.text = ""
                datePicker.date = datePicker.minimumDate!
            }
        }
        else if _sender.tag == 200{
            datePicker2.setExpectedDateValidation()
            if _sender.date > datePicker2.minimumDate! && _sender.date < datePicker2.maximumDate! {
                self.expectedDeliverTXT?.text = "\(selectedDate)"
            }
            else{
                self.expectedDeliverTXT?.text = ""
                datePicker2.date = datePicker2.minimumDate!
            }
        }
    }
    @objc func dismissPicker() {
        view.endEditing(true)
    }
    
    //MARK:- API
    func registerForStepThree_Pregnant(){
        if Helper.isConnectedToNetwork() {
            let urlstr = Constants.BASEURL + MethodName.RegisterStep2
            let lastDate = "\(self.lastDOPtxt!.text!)"
            let expectedDate = "\(self.expectedDeliverTXT!.text!)"
            let isFirstDelivery = firstpregnancyBTN.isSelected == true ? "YES" : "NO"

            if lastDate.isEmpty == true && expectedDate.isEmpty == true{
                PKSAlertController.alert(Constants.appName, message: "Please Select Last Date of Period or Expected Delivery Date.")
            }
            else{
                Constants.appDel.startLoader()
                let userid = UserDefaults.standard.string(forKey: Constants.USERID)

                let paramStr = "user_id=\(userid!)&category=pregnancy&last_date_of_period=\(lastDate)&delivery_date=\(expectedDate)&baby_name=&baby_gender=&baby_dob=&baby_weight=&baby_weight_unit=&baby_height=&baby_height_unit=&head_circumfrence=&head_circumfrence_unit=&medical_history=&description=&first_delivery=\(isFirstDelivery)"

                Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
                    let responseDict = response as Dictionary<String, Any>
                    Constants.appDel.stopLoader()
                    Analytics.logEvent(AnalyticsEventViewItem, parameters: [
                        AnalyticsParameterItemID: "id-Registration_IAm_Pregnant_Click",
                        AnalyticsParameterItemName: "Registration_IAm_Pregnant_Click",
                        AnalyticsParameterContentType: "Registration_IAm_Pregnant"
                        ])
                    AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.RegisterIAmPregnentClick,
                                                         withValues: [
                                                            AFEventParamContent: AppsFlyerConstant.RegisterIAmPregnentClick,
                                                            AFEventParamContentId: "19"
                        ])
                    self.MakeDecisionForStepThreeAndHome(responseDict)
                }
            }
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
    
    func registerForStepThree_Planning(){
        if Helper.isConnectedToNetwork() {
            let urlstr = Constants.BASEURL + MethodName.RegisterStep2
            let isFirstDelivery = firstdeliveryBtn.isSelected == true ? "YES" : "NO"
            var medicalhistoryArr = [String]()
            var description : String = ""
            if diabetesbtn.isSelected {
                medicalhistoryArr.append("sugar")
            }
            if thyroidbtn.isSelected {
                medicalhistoryArr.append("thyroid")
            }
            if otehrbtn.isSelected {
                medicalhistoryArr.append("other")
                description = textV.text
            }
            
            Constants.appDel.startLoader()
            let userid = UserDefaults.standard.string(forKey: Constants.USERID)
            
            let paramStr = "user_id=\(userid!)&category=planning-baby&last_date_of_period=&delivery_date=&baby_name=&baby_gender=&baby_dob=&baby_weight=&baby_weight_unit=&baby_height=&baby_height_unit=&head_circumfrence=&head_circumfrence_unit=&medical_history=\(medicalhistoryArr)&description=\(description)&first_delivery=\(isFirstDelivery)"
            
            Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
                let responseDict = response as Dictionary<String, Any>
                Constants.appDel.stopLoader()
                Analytics.logEvent(AnalyticsEventViewItem, parameters: [
                    AnalyticsParameterItemID: "id-Registration_Planning_Baby_Click",
                    AnalyticsParameterItemName: "Registration_Planning_Baby_Click",
                    AnalyticsParameterContentType: "Registration_Planning_Baby"
                    ])
                AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.RegisterPlanningBabyClick,
                                                     withValues: [
                                                        AFEventParamContent: AppsFlyerConstant.RegisterPlanningBabyClick,
                                                        AFEventParamContentId: "20"
                    ])
               self.MakeDecisionForStepThreeAndHome(responseDict)
            }
        }
        else{
            PKSAlertController.alertForNetwok()
        }
    }
    //MARK: - Post babay details
    fileprivate func MakeDecisionForStepThreeAndHome(_ responseDict: [String : Any]) {
        DispatchQueue.main.async {
            Constants.appDel.stopLoader()

            if responseDict["status"] as? Int == 3  || responseDict["status"] as? String == "3"{
                UserDefaults.standard.set(2, forKey: Constants.RegisterStatus)
                self.delegate?.registerationStep3Complete()
                
            }
            else if responseDict["status"] as? Int == 1 || responseDict["status"] as? String == "1"{
                let responseDictionary = responseDict["data"] as! Dictionary<String, Any>
                UserDefaults.standard.set(responseDictionary, forKey: Constants.PROFILE)
                UserDefaults.standard.set(3, forKey: Constants.RegisterStatus)
                // TAKE to HOME SCreen
                let root = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SideMenuNavigation")
                UserDefaults.standard.set("1", forKey: firstLogin)
                Constants.appDelegate.window?.rootViewController = root
                UIApplication.shared.keyWindow?.rootViewController = root
            }
            else{
    PKSAlertController.alert("Error", message: responseDict["message"] as? String ?? KSomeErrorTryAgain)
            }
        }
    }
    
    func registerForBabyDetails(){
        if Helper.isConnectedToNetwork() {
            Constants.appDel.startLoader()
            self.view.endEditing(true)
        let paramStr = "user_id=\( UserDefaults.standard.string(forKey: Constants.USERID)!)&category=mother&last_date_of_period= &delivery_date= &baby_name=\(babyNameTxt.text ?? "")&baby_gender=\((babyGenderTxt.text?.lowercased().contains("male"))! ? "M" : "F")&baby_dob=\(babyDOBTxt.text ?? "")&baby_weight=\(weightTxt.text ?? "")&baby_weight_unit=\(lbsButton.isSelected==true ? "lb." : "kg.")&baby_height=\(heightTxt.text ?? "")&baby_height_unit=\(cmButton.isSelected ? "cm." : "in.")&head_circumfrence=\(circumferenceTxt.text ?? "")&head_circumfrence_unit=\(cm_cButton.isSelected ? "cm." : "in.")&medical_history=&description=&first_delivery=NO"
            
            Server.postRequestWithURL(Constants.BASEURL + MethodName.RegisterStep2, paramString: paramStr) { (response) in
                let responseDict = response as Dictionary<String, Any>
                self.MakeDecisionForStepThreeAndHome(responseDict)
        }
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
}
extension RegisterViewControllerStep2 : UITableViewDelegate,UITableViewDataSource{
    //MARK:- TableView Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  selectedSection.contains(section) ? 2 : 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    fileprivate func PragnencyStatusCell(_ tableView: UITableView, _ indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PregnancyStatusCell", for: indexPath) as! PregnancyStatusCell
        
        cell.statusLabel.text = "I'm Planning a Baby"
         if indexPath.section == 1{
			if gender == "M"{
				cell.statusLabel.text = "My wife is Pregnant"
			}
			else{
				cell.statusLabel.text = "I'm Pregnant"
			}
        }else if indexPath.section == 2 {
			if gender == "M"{
				cell.statusLabel.text = "I'm a Father"
			}
			else{
				cell.statusLabel.text = "I'm a Mother"
			}
        }
        
        if self.selectedSection.contains(indexPath.section){
            cell.addButton.setTitle("-", for: .normal)
            cell.addButton.setTitleColor(UIColor.white, for: .normal)
            cell.statusLabel.textColor = UIColor.white
            cell.categoryPregnantView.backgroundColor = Colors.MamyDarkBlue
        }
        else{
            cell.addButton.setTitle("+", for: .normal)
            cell.addButton.setTitleColor(UIColor.black, for: .normal)
            cell.statusLabel.textColor = UIColor.black
            cell.categoryPregnantView.backgroundColor = #colorLiteral(red: 0.8666666667, green: 0.862745098, blue: 0.8745098039, alpha: 1)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            return PragnencyStatusCell(tableView, indexPath)
        }
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlanningBabyCell", for: indexPath) as! PlanningBabyCell
            
            self.firstdeliveryBtn = cell.isFirstdeliveryBtn
            self.diabetesbtn = cell.diabitiesBtn
            self.thyroidbtn = cell.thyroidBtn
            self.otehrbtn = cell.otherBtn
            self.textV = cell.otherTextview
            self.textVHeight = cell.textviewHeight
            
            self.otehrbtn.addTarget(self, action: #selector(OtherButtonPressed), for: .touchUpInside)
            self.diabetesbtn.addTarget(self, action: #selector(DiabitiesButtonPressed), for: .touchUpInside)
            self.thyroidbtn.addTarget(self, action: #selector(ThyroidButtonPressed), for: .touchUpInside)
            self.firstdeliveryBtn.addTarget(self, action: #selector(isFirstDeliveryPressed(_:)), for: .touchUpInside)
            
            self.textVHeight.constant =  otehrbtn.isSelected == true ? 92 : 0
            
            cell.saveDetailBtn.addTarget(self, action: #selector(saveButtonPressed(_:)), for: .touchUpInside)
            
            return cell
        }
        else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PregnancyDetailCell", for: indexPath) as! PregnancyDetailCell
            
            self.lastDOPtxt = cell.lastPeriodDOBText
            self.expectedDeliverTXT = cell.expectedDeliveryText
            self.firstpregnancyBTN = cell.isFirstPregnancyBtn
            self.firstpregnancyBTN.addTarget(self, action: #selector(isFirstPregnancyPressed(_:)), for: .touchUpInside)
            
            self.lastDOPtxt.delegate = self
            self.expectedDeliverTXT.delegate = self
            self.lastDOPtxt?.inputView = datePicker
            self.lastDOPtxt?.inputAccessoryView = toolBar
            self.expectedDeliverTXT?.inputView = datePicker2
            self.expectedDeliverTXT?.inputAccessoryView = toolBar
            
            cell.saveDetailBtn.addTarget(self, action: #selector(saveButtonPressed(_:)), for: .touchUpInside)
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyBabyDetailTableViewCell", for: indexPath) as! MyBabyDetailTableViewCell
            cell.BindRegistgerData(nil, babyDetails: self.babyDetails, vc: self)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.selectedSection.count > 0 && indexPath.row == 0{
            if self.selectedSection.contains(indexPath.section){
                self.selectedSection.removeAll()
            }
            else{
                self.selectedSection.removeAll()
                self.selectedSection.append(indexPath.section)
            }
        }
        else{
            self.selectedSection.append(indexPath.section)
        }
        self.tblView.reloadData()
    }
    
}
extension RegisterViewControllerStep2 : UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == babyGenderTxt {
            babyDOBTxt.resignFirstResponder()
            babyNameTxt.resignFirstResponder()
            self.view.endEditing(true)
            CustomPicker.show(data: [["Male", "Female"]]) {  [weak self] (selections: [Int : String]) -> Void in
                if let name = selections[0] {
                    self?.babyGenderTxt.text = name
                }
            }
            return false
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
}
}
