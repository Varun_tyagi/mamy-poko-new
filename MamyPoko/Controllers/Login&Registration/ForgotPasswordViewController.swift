//
//  ForgotPasswordViewController.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 04/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var emailTxt: RoundTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
		self.BlockSideMenuSwipe()
        self.hideKeyboardWhenTappedAround()
        setupinititialView()
    }


    @IBAction func cancelAction(_ sender: Any) {
        self.remove()
    }

    @IBAction func okAction(_ sender: Any) {
        view.endEditing(true)
        forgotPasswordService()
    }
    func forgotPasswordService(){
        if Helper.isConnectedToNetwork() {

            let urlstr = Constants.BASEURL + MethodName.ForgetPass

            if self.CheckDataValidation(){

                Constants.appDel.startLoader()

                let paramStr = "email=\(self.emailTxt!.text!)"
                Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
                    let responseDict = response as [String:Any]
                    Constants.appDel.stopLoader()

                    if responseDict["status"] as! Int ==  1 {
                        DispatchQueue.main.async {
                            if let msg  = responseDict["message"] as? String {
                                self.remove()

            PKSAlertController.alert(Constants.appName, message: msg)
                            }else{
PKSAlertController.alert(Constants.appName, message: "Your Temporary Password has been sent to registered email id.")
                            }
                        }
                    }
                    else{
                        PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                    }
                }
            }
        }else{
            PKSAlertController.alertForNetwok()
        }
    }

    func CheckDataValidation()-> (Bool){

        if self.emailTxt!.text!.isEmpty == true {
            PKSAlertController.alert(Constants.appName, message: "Enter email id !")
            return false
        }
        else if Helper.isValidEmail(self.emailTxt!.text!) == false {
            PKSAlertController.alert(Constants.appName, message: "Enter valid email id !")
            return false
        }

        return true
    }


}
extension ForgotPasswordViewController {
    func setupinititialView()  {
        Helper.setImageOntextField(self.emailTxt!, imagename: "", mode: true, frame: CGRect(x:0, y:0, width:15,height: (self.emailTxt!.frame.height)))

    }
}
extension ForgotPasswordViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
}

extension UIViewController {
    func add(_ child: UIViewController) {
        addChildViewController(child)
        view.addSubview(child.view)
        child.didMove(toParentViewController: self)
    }
    func remove() {
        guard parent != nil else {
            return
        }
        willMove(toParentViewController: nil)
        removeFromParentViewController()
        view.removeFromSuperview()
    }
}
