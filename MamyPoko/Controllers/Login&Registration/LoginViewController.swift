//
//  LoginViewController.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 25/04/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

let chagnePasswordHeight = 265
let firstLogin = "firstLogin"

import UIKit
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import FirebaseAnalytics
import AppsFlyerLib

class LoginViewController: UIViewController {

    @IBOutlet weak var txt_Email:UITextField!
    @IBOutlet weak var txt_Password:UITextField!
    @IBOutlet weak var rememberMeButton:UIButton!

    //MARK:- Declerations for Social
    var emailID = ""
    var authType = ""
    var socialId = ""

	override func viewWillAppear(_ animated: Bool) {
        if let statusbar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
            statusbar.backgroundColor = UIColor.white
        }
	}
    override func viewDidLoad() {
        super.viewDidLoad()
		
		self.BlockSideMenuSwipe()
        self.hideKeyboardWhenTappedAround()
        Helper.setImageOntextField(self.txt_Email, imagename: "", mode: true, frame: CGRect(x:0, y:0, width:15,height: self.txt_Email.frame.height))
        Helper.setImageOntextField(self.txt_Password, imagename: "", mode: true, frame: CGRect(x:0, y:0, width:15,height: self.txt_Email.frame.height))
        setEyeButton(txt_Password)
        
      
    }
    func setEyeButton(_ textField: UITextField) {
        let eyeView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40))
        let eyebtn = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40))
        eyebtn.setImage(#imageLiteral(resourceName: "eyeClose"), for: .normal)
        eyebtn.setImage(#imageLiteral(resourceName: "eyeOpen"), for: .selected)
        eyebtn.accessibilityElements = [textField]
        eyebtn.addTarget(self, action: #selector(toggleSecureField(_:)), for: .touchUpInside)
        eyeView.addSubview(eyebtn)

        textField.rightView = eyeView
        textField.isSecureTextEntry=true
        textField.rightViewMode = .always

    }

    @objc func toggleSecureField( _ sender: UIButton){
        guard   let textField = sender.accessibilityElements?.first as? UITextField else{
            return
        }
        if sender.isSelected {
            sender.isSelected = false
            textField.isSecureTextEntry = true
        }else{
            sender.isSelected = true
            textField.isSecureTextEntry = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    
    func resignKeyboard(){
        self.txt_Email?.resignFirstResponder()
        self.txt_Password?.resignFirstResponder()
    }
    
    //MARK:- FACEBOOK
    @IBAction func LoginWithFB(_ sender: Any) {
        if Helper.isConnectedToNetwork() {
            Constants.appDel.startLoader()
            Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
                AnalyticsParameterItemID: "id-Social_FB_Click",
                AnalyticsParameterItemName: "Social_FB_Click",
                AnalyticsParameterContentType: "Facebook"
                ])
            AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.SocialFbClick,
                                                 withValues: [
                                                    AFEventParamContent: AppsFlyerConstant.SocialFbClick,
                                                    AFEventParamContentId: "15"
                ])
            DispatchQueue.main.async()
            {
				let logManager = LoginManager.init()
				logManager.loginBehavior = LoginBehavior.browser
                logManager.logOut()
				logManager.logIn(permissions: ["public_profile", "email"], from: self, handler: { (result, error) in
					if error != nil {
						Constants.appDel.stopLoader()
						PKSAlertController.alert(Constants.appName, message: "Some error Occurred !")
					}
					else if result?.isCancelled == true {
						Constants.appDel.stopLoader()
					}
					else {
						if result?.token?.tokenString != "" || result?.token != nil {
							self.fetchUserInfo()
						}
					}
				})
//                logManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) in
//                    if error != nil {
//                        Constants.appDel.stopLoader()
//                        PKSAlertController.alert(Constants.appName, message: "Some error Occurred !")
//                    }
//                    else if result?.isCancelled == true {
//                        Constants.appDel.stopLoader()
//                    }
//                    else {
//                        if result?.token.tokenString != "" || result?.token != nil {
//                            self.fetchUserInfo()
//                        }
//                    }
//                }
            }
        }
        else {
            PKSAlertController.alert(Constants.appName, message: "Please check your internet connection.")
        }
    }
    
    
    func fetchUserInfo()
    {
        if Helper.isConnectedToNetwork() {
          //  let req = FBSDKGraphRequest.init(graphPath: "me" , parameters: ["fields": "id, name, picture.width(500).height(500), email, gender,first_name, last_name"])
            DispatchQueue.main.async()
                {
					GraphRequest.init(graphPath: "me" , parameters: ["fields": "id, name, picture.width(500).height(500), email, gender,first_name, last_name"]).start {(connection, result, error) in
                        if(error == nil) {
                            let resDict = result as! [String:Any]
                            self.authType = "facebook"
                            self.checkUserAuthonticationWithUserInfo(result: resDict)
                        }
                        else {
                           Constants.appDel.stopLoader()
                            PKSAlertController.alert(Constants.appName, message: "Some error occured")
                        }
                    }
                }
        }
        else {
            Constants.appDel.stopLoader()
            PKSAlertController.alert(Constants.appName, message: "Please check your internet connection.")
        }
    }
//
    func checkUserAuthonticationWithUserInfo(result : [String:Any]) {

        if Helper.isConnectedToNetwork() {
            let fbDetails = result
            var email = result["email"] as? String
            if email == nil {
                email = ""
            }
            self.socialId = fbDetails["id"] as! String
            self.emailID = email!
            UserDefaults.standard.set(fbDetails, forKey: Constants.UserDefaultsConstant.userDetailsSocial)
            self.callApiSocialRegister()
        }
        else {
            Constants.appDel.stopLoader()
            PKSAlertController.alert(Constants.appName, message: "Please check your internet connection.")
        }
    }
    
    //MARK:- GOOGLE
    @IBAction func LoginWithGoogle(_ sender: Any) {
        GIDSignIn.sharedInstance().delegate=self
        GIDSignIn.sharedInstance().uiDelegate=self
        GIDSignIn.sharedInstance().signIn()
        Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
            AnalyticsParameterItemID: "id-Social_Gmail_Click",
            AnalyticsParameterItemName: "Social_Gmail_Click",
            AnalyticsParameterContentType: "Google"
            ])
        AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.SocialGmailClick,
                                             withValues: [
                                                AFEventParamContent: AppsFlyerConstant.SocialGmailClick,
                                                AFEventParamContentId: "16"
            ])
    }
    


    //MARK:- Normal Login
    
    @IBAction func LoginWithUsername(_ sender: Any) {
        if Helper.isConnectedToNetwork(){
            
            if txt_Email.text!.replacingOccurrences(of: " ", with: "").isEmpty {
                //Error
                PKSAlertController.alert(Constants.appName, message: "Enter your email !")
            }
            else if txt_Password.text!.replacingOccurrences(of: " ", with: "").isEmpty{
                //error
                PKSAlertController.alert(Constants.appName, message: "Enter password !")
            }
            else if self.CheckDataValidation() == false {
                
            }
            else{
                Constants.appDel.startLoader()
                let urlstr = Constants.BASEURL + MethodName.Login
                let email = self.txt_Email.text!
                let password = self.txt_Password!.text!
                self.view.endEditing(true)
                let paramStr = "email=\(email)&password=\(password)&device_type=ios&device_id=\(AppDelegate.getUniqueDeviceIdentifierAsString())&device_token=\(AppDelegate.getDeviceToken())"
                
                Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
                    let responseDict = response as Dictionary<String, Any>
                    Constants.appDel.stopLoader()
                    let message = responseDict["message"] as! String
                    Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
                        AnalyticsParameterItemID: "id-Login_Success",
                        AnalyticsParameterItemName: "Login_Success",
                        AnalyticsParameterContentType: "Login"
                        ])
                    AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.LoginSuccess,
                                                         withValues: [
                                                            AFEventParamContent: AppsFlyerConstant.LoginSuccess,
                                                            AFEventParamContentId: "17"
                        ])
                    if responseDict["status"] as? Int == 1 || responseDict["status"] as? String == "1"  {
                        let responseDC = responseDict["data"] as! Dictionary<String, Any>
                        let userID = "\(responseDC["user_id"] ?? "")"
                        UserDefaults.standard.set(0, forKey: Constants.UserDefaultsConstant.isSocialLogin)
                        UserDefaults.standard.set(userID, forKey: Constants.USERID)
                        UserDefaults.standard.set(3, forKey: Constants.RegisterStatus)
                        UserDefaults.standard.synchronize()
                        Helper.saveDataInNsDefault(object: responseDC as AnyObject, key: Constants.PROFILE)

                        DispatchQueue.main.async {
                            // TAKE to HOME SCreen
                            let root = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SideMenuNavigation")
                            UserDefaults.standard.set("1", forKey: firstLogin)
                            Constants.appDelegate.window?.rootViewController = root
                            UIApplication.shared.keyWindow?.rootViewController = root
                        }
                    }
                    else if responseDict["status"] as? Int ==  2 || responseDict["status"] as? String == "2" {
                        let responseDC = responseDict["data"] as! Dictionary<String, Any>
                        let userID = "\(responseDC["user_id"] ?? "")"
                        UserDefaults.standard.set(0, forKey: Constants.UserDefaultsConstant.isSocialLogin)
                        UserDefaults.standard.set(userID, forKey: Constants.USERID)
                        UserDefaults.standard.set(1, forKey: Constants.RegisterStatus)
                        UserDefaults.standard.synchronize()
                        Helper.saveDataInNsDefault(object: responseDC as AnyObject, key: Constants.PROFILE)
                        
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "Success", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            let acceptButton = UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction) in
                                let view = Constants.mainStoryboard.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
                                view.registerStatus = 1
                                self.present(view, animated: true, completion: nil)

                            })
                            alert.addAction(acceptButton)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    else if responseDict["status"] as? Int ==  3  || responseDict["status"] as? String == "3" {
                        let responseDC = responseDict["data"] as! Dictionary<String, Any>
                        let userID = "\(responseDC["user_id"] ?? "")"
                        UserDefaults.standard.set(0, forKey: Constants.UserDefaultsConstant.isSocialLogin)
                        UserDefaults.standard.set(userID, forKey: Constants.USERID)
                        UserDefaults.standard.set(2, forKey: Constants.RegisterStatus)
                        UserDefaults.standard.synchronize()
                        Helper.saveDataInNsDefault(object: responseDC as AnyObject, key: Constants.PROFILE)
                        
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "Success", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            let acceptButton = UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction) in
                                self.TopicOfInterestSelection()
                            })
                            alert.addAction(acceptButton)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    else{
                        
                        PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                    }
                }
            }
        }
        else{
            PKSAlertController.alertForNetwok()
        }
    }
    
    
    func CheckDataValidation()-> (Bool){
        let userName = "\(self.txt_Email!.text!)"
        let password = "\(self.txt_Password!.text!)"
        
        if userName.isEmpty == true {
            PKSAlertController.alert(Constants.appName, message: "Enter email id !")
            return false
        }
        else if Helper.isValidEmail(userName) == false {
            PKSAlertController.alert(Constants.appName, message: "Enter valid email id !")
            return false
        }
        else if password.isEmpty == true {
            PKSAlertController.alert(Constants.appName, message: "Enter password !")
            return false
        }
        else if password.count < 6 {
                PKSAlertController.alert(Constants.appName, message: "Enter min 6 character password !")
                return false
        }
        return true
    }
    
    //MARK:- Go To Topic Of Interest Page
    func TopicOfInterestSelection() {
        // Hide current screen and display new Screen
        let topiInterest = Constants.mainStoryboard.instantiateViewController(withIdentifier: "TopicOfInterestVC") as! TopicOfInterestVC
        DispatchQueue.main.async {
            self.present(topiInterest, animated: true, completion: nil)
        }
        
    }
    
    
    //MARK:- CREATE NEW ACCOUNT
    @IBAction func CreateNewAccount(_ sender: Any) {

        let view = Constants.mainStoryboard.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        self.present(view, animated: true, completion: nil)

    }
    
    //MARK:- FORGET PASSWORD
    @IBAction func ForgetPassword(_ sender: Any) {

        let forgotVC = Constants.mainStoryboard.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.add(forgotVC)
        
    }
    
    //MARK:- Remember ME
    @IBAction func RememberMePressed(_ sender: Any) {
        
        if self.rememberMeButton.isSelected == true{
            self.rememberMeButton.isSelected = false
        }
        else{
            self.rememberMeButton.isSelected = true
        }
    }
    
    
    //MARK:- Social API
    func callApiSocialRegister()
    {
        if Helper.isConnectedToNetwork(){
            Constants.appDel.startLoader()
            let urlstr = Constants.BASEURL + MethodName.SocialLogin
            
            let paramStr = "social_id=\(self.socialId)&email_id=\(self.emailID)&auth_type=\(self.authType)&social_type=\(self.authType)&device_token=\(AppDelegate.getDeviceToken())"
            Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
                let responseDict = response as [String:Any]
                Constants.appDel.stopLoader()
                let message = responseDict["message"] as! String
                Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
                    AnalyticsParameterItemID: "id-Login_Success",
                    AnalyticsParameterItemName: "Login_Success",
                    AnalyticsParameterContentType: "Login"
                    ])
                AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.LoginSuccess,
                                                     withValues: [
                                                        AFEventParamContent: AppsFlyerConstant.LoginSuccess,
                                                        AFEventParamContentId: "17"
                    ])
                if responseDict["status"] as! Int ==  1 {
                    UserDefaults.standard.set(1, forKey: Constants.UserDefaultsConstant.isSocialLogin)
                    let responseDC = responseDict["data"] as! Dictionary<String, Any>
                    let userID = "\(responseDC["user_id"] ?? "")"
                    UserDefaults.standard.set(userID, forKey: Constants.USERID)
                    UserDefaults.standard.set(3, forKey: Constants.RegisterStatus)
                    UserDefaults.standard.synchronize()
                    Helper.saveDataInNsDefault(object: responseDC as AnyObject, key: Constants.PROFILE)
                    DispatchQueue.main.async {
                        // TAKE to HOME SCreen
                        let root = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SideMenuNavigation")
                        UserDefaults.standard.set("1", forKey: firstLogin)

                            Constants.appDelegate.window?.rootViewController = root
                        UIApplication.shared.keyWindow?.rootViewController = root
                    }
                }
                else if responseDict["status"] as! Int ==  3{
                    UserDefaults.standard.set(1, forKey: Constants.UserDefaultsConstant.isSocialLogin)
                    let responseDC = responseDict["data"] as! Dictionary<String, Any>
                    let userID = "\(responseDC["user_id"] ?? "")"
                    UserDefaults.standard.set(userID, forKey: Constants.USERID)
                   // UserDefaults.standard.set(3, forKey: Constants.RegisterStatus)
                    UserDefaults.standard.synchronize()
                    Helper.saveDataInNsDefault(object: responseDC as AnyObject, key: Constants.PROFILE)
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "Success", message: message, preferredStyle: UIAlertControllerStyle.alert)
                        let acceptButton = UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction) in
                            self.TopicOfInterestSelection()
                        })
                        alert.addAction(acceptButton)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else if responseDict["status"] as! Int ==  5{
                    DispatchQueue.main.async {
                        let view = Constants.mainStoryboard.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
                        view.signIn.isSocialDetailsAvailable = true
                        self.present(view, animated: true, completion: nil)
                    }
                }


                else{
                    
                    PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                }
            }
        }
        else{
            PKSAlertController.alertForNetwok()
        }
    }
}

extension LoginViewController : GIDSignInUIDelegate{
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        guard error == nil else {
//            print("Error while trying to redirect : \(error)")
            return
        }

       

    }
    // Present a view that prompts the user to sign in with Google

    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    // Dismiss the "Sign in with Google" view

    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }

}
extension LoginViewController : GIDSignInDelegate{

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if (error) != nil {
            NotificationCenter.default.post(
                name: Notification.Name(rawValue: "ToggleAuthUINotification"), object: nil, userInfo: nil)
        } else {
            var  userDict: [String:Any] = [:]
            userDict[Constants.SocialUserData.userName] = user.profile.name ?? ""
            userDict[Constants.SocialUserData.userEmail] = user.profile.email ?? ""
            userDict[Constants.SocialUserData.socialId] = user.userID ?? ""
            userDict[Constants.SocialUserData.userGender] =  ""
            UserDefaults.standard.set(userDict, forKey: Constants.UserDefaultsConstant.userDetailsSocial)
            self.authType = "gmail"
            self.checkUserAuthonticationWithUserInfo(result: userDict)
        }
    }

    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // [START_EXCLUDE]
        NotificationCenter.default.post(
            name: Notification.Name(rawValue: "ToggleAuthUINotification"),
            object: nil,
            userInfo: ["statusText": "User has disconnected."])
        // [END_EXCLUDE]
    }

}

extension LoginViewController : UITextFieldDelegate{
    //MARK:- Textfield Delegates

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
//        if textField == self.txt_Password {
//            let set = NSCharacterSet(charactersIn: "ABCDEFGHIJKLMONPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789").inverted
//
//            let currentCharacterCount = textField.text?.count ?? 0
//            if (range.length + range.location > currentCharacterCount){
//                return false
//            }
//            let newLength = currentCharacterCount + string.count - range.length
//            return newLength <= 10 && string.rangeOfCharacter(from: set) == nil
//        }
        
        
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.resignKeyboard()
        if textField == self.txt_Email{
            self.txt_Password?.becomeFirstResponder()
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        self.resignKeyboard()
        if textField == self.txt_Email{
            self.txt_Password?.becomeFirstResponder()
        }
    }
    
}


