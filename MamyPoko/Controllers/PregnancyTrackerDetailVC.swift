//
//  PregnancyTrackerDetailVC.swift
//  MamyPoko
//
//  Created by Surbhi on 05/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAnalytics
import AppsFlyerLib

class PregnancyTrackerDetailVC : UIViewController, UITableViewDelegate, UITableViewDataSource{
	
	
    
    @IBOutlet weak var pregnancyTrackerTable: UITableView!
    var weekNumerStr = String()
    var weekHeadrStr = String()

    var weekDIct = [String: Any]()
    var disclaimerdict = [String : Any]()

	var threeDImage:Bool = true
	var fetusImageCollection: UICollectionView?
	
	
	
	
	//MARK:- View Methods
    override func viewDidLoad() {
        super .viewDidLoad()
        self.getPregnancyWeeklyTips(week: self.weekNumerStr)
		
		self.BlockSideMenuSwipe()
		 self.pregnancyTrackerTable.reloadData()
        Analytics.logEvent(AnalyticsEventViewItem, parameters: [
			AnalyticsParameterItemID: "id-\(AppsFlyerConstant.PragnencyTrackerVisit)",
			AnalyticsParameterItemName: "\(AppsFlyerConstant.PregnancyTrackerDetail)",
            AnalyticsParameterContentType: "Visit"
            ])
        
        Analytics.logEvent(AnalyticsEventViewItem, parameters: [
			AnalyticsParameterItemID: "id-\(AppsFlyerConstant.Pregnancy_Week_Details_Page)",
            AnalyticsParameterItemName: "\(AppsFlyerConstant.Pregnancy_Week_Details_Page)",
            AnalyticsParameterContentType: "Pregnancy_Week"
            ])
        
        AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.PragnencyTrackerVisit,
                                             withValues: [
                                                AFEventParamContent: AppsFlyerConstant.PragnencyTrackerVisit,
                                                AFEventParamContentId: "26"
            ])
		
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(true)
		
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		if #available(iOS 13.0, *) {
			let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
			statusBar.backgroundColor = Colors.MAMYBlue
			 UIApplication.shared.keyWindow?.addSubview(statusBar)
		} else {
			 UIApplication.shared.statusBarView?.backgroundColor = Colors.MAMYBlue
		 }
		
//		fetusImageCollection?.dataSource = self
//			  fetusImageCollection?.delegate = self
	}
    
    @IBAction func searchAction(_ sender: Any) {
        let searchVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        searchVc.isPresent = true
        present(searchVc, animated: false, completion: nil)

    }
    
 
    
    //MARK:- API
    
    func getPregnancyWeeklyTips(week : String){
        if Helper.isConnectedToNetwork() {
            
            let urlstr = Constants.BASEURL + MethodName.WeeklyTips
            let userID = UserDefaults.standard.string(forKey: Constants.USERID)
            let weekStr = week

            if userID == nil || userID?.count == 0 {
                PKSAlertController.alert(Constants.appName, message: "Invalid User ID")
            }
            else{
                Constants.appDel.startLoader()
                let paramStr = "user_id=\(userID!)&week=\(weekStr)"
                
                Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
					Constants.appDel.stopLoader()
                    let responseDict = response as Dictionary<String, Any>
                    if responseDict["status"] as! NSNumber == NSNumber.init(value: 1) {
                        
                        let arr = responseDict["data"] as! [Any]
                        self.weekDIct = arr[0] as! Dictionary<String, Any>
                        
                        if responseDict.keys.contains("disclaimer") == true{
                            self.disclaimerdict = responseDict["disclaimer"] as? Dictionary <String, Any> ?? [:]
                        }

                        
                        DispatchQueue.main.async {
                            
                            self.pregnancyTrackerTable.reloadData()
							self.fetusImageCollection?.reloadData()
							
						}
                    }
                    else if responseDict["status"] as! NSNumber == NSNumber.init(value: 0) {
                        DispatchQueue.main.async {
                            PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                        }
                    }
                    else{
                        PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                        
                    }
                }
            }
            
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
    

    //MARK:- TableView
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.disclaimerdict.count > 0 {
            return 3
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "pregnancyTrackerDetailCell", for: indexPath) as! pregnancyTrackerDetailCell
			
			
			
            if weekDIct.count > 0{
                let titleSTr = self.weekDIct["title"] as? String
                let imageStr = self.weekDIct["image"] as? String
                
                if (titleSTr?.count)! == 0{
                    cell.titleHeight.constant = 0
                }
                else{
                    cell.titleHeight.constant = 40
                    cell.sizeLabel.text = titleSTr!
                }
                
                
                if  imageStr != nil && (imageStr?.count)! > 0 {
                    let imageStrurl = URL(string: imageStr!)
                    cell.sizeImage.kf.setImage(with: imageStrurl)
                    cell.sizeImageHeight.constant = 125
                    cell.sizeImage.isHidden = true
                }
                else{
                    // placeholder Image
                    cell.sizeImage.isHidden = true
                    cell.sizeImageHeight.constant = 0
                }
				
					if threeDImage{

						self.fetusImageCollection = cell.foetusImgCollection
						self.fetusImageCollection?.delegate = self
						self.fetusImageCollection?.dataSource = self
						self.fetusImageCollection?.isHidden=false

						self.fetusImageCollection?.reloadData()
						
						cell.weekLabel.isHidden=true
						cell.sizeImage.isHidden=true
						cell.sizeLabel.isHidden=true
			
					}else{
						self.fetusImageCollection?.isHidden=true
						cell.weekLabel.isHidden=false
						cell.sizeImage.isHidden=false
						cell.sizeLabel.isHidden=false
					}
				
				
            }
            
           

            cell.weekLabel.text = self.weekHeadrStr
            cell.weekNumberLabel.text = "Week \(self.weekNumerStr)"
            cell.prevButton.addTarget(self, action: #selector(PreviousPressed(sender:)), for: .touchUpInside)
            cell.nextButton.addTarget(self, action: #selector(NextPressed(sender:)), for: .touchUpInside)
            
            tableView.estimatedRowHeight = 315
            tableView.rowHeight = UITableViewAutomaticDimension
            
            
            return cell
        }
        else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "pregnancyTrackerDetailCell_text", for: indexPath)
            let detailText = cell.contentView.viewWithTag(401) as! UILabel
            
            if weekDIct.count > 0{
                let contentStr = self.weekDIct["content"] as! String
                
                var descriptionAlign = "<style type='text/css'>html,body {margin: 0;padding: 0;width: 100%;height: 100%; font-family: Quicksand,Adira Display SSi; font-size: 18px;}html {display: table;}body {display: table-cell;vertical-align: middle;padding: 0px;text-align: left;-webkit-text-size-adjust: none;} h2 {font-size: 22px; color: #333; margin: 0;}h2 .head-yel {color: #f2b413;}h4 {font-size: 22px;}p {margin: 0px 0 0;} </style>"
                descriptionAlign.append(contentStr)
                
                detailText.attributedText = descriptionAlign.htmlToAttributedString
            }
            
            tableView.estimatedRowHeight = 100
            tableView.rowHeight = UITableViewAutomaticDimension

            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DoctorProfileCell", for: indexPath) as! DoctorProfileCell

            cell.coverView.dropShadow(scale: true)
            let userImg = self.disclaimerdict["profile_image"] as?  String
            if userImg != nil || (userImg?.count)! > 0 || userImg != ""{
                let userImgurl = URL(string: userImg!)
                cell.doctorImage.kf.setImage(with: userImgurl)
                
            }
            else{
                // placeholder Image
                cell.doctorImage.image = #imageLiteral(resourceName: "userPlaceholderProfile")
            }

            let userName = self.disclaimerdict["doctor_name"] as?  String
            cell.nameLabel.text = userName

            let userExperience = self.disclaimerdict["doctor_exp"] as?  String
            cell.experienceLabel.text = userExperience

            let userDegree = self.disclaimerdict["doctor_degree"] as?  String
            let userProfile = self.disclaimerdict["doctor_profile"] as?  String

            cell.degreeLabel.text = userProfile! + userDegree!.htmlToString

            let contentSTR = self.disclaimerdict["disc_heading"] as?  String
			cell.contentLabel.text = contentSTR?.htmlToString

            cell.viewProfileButton.addTarget(self, action: #selector(OpenDoctorProfile), for: .touchUpInside)

            tableView.estimatedRowHeight = 300
            tableView.rowHeight = UITableViewAutomaticDimension

            return cell
        }
    }
    
   
    //MARK:- Action Methods
    @IBAction func BackButtonPressed(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func NextPressed(sender : UIButton){
        let num = Int(self.weekNumerStr)
        if num == 40 {
            //Do nothing
        }
        else{
            self.weekNumerStr = "\(num! + 1)"
            self.getPregnancyWeeklyTips(week: self.weekNumerStr)
        }
        
    }
    
    @objc func PreviousPressed(sender : UIButton){
        let num = Int(self.weekNumerStr)
        if num == 1 {
            //Do nothing
        }
        else{
            self.weekNumerStr = "\(num! - 1)"
            self.getPregnancyWeeklyTips(week: self.weekNumerStr)
        }
    }
    
    @objc func OpenDoctorProfile(){
        DispatchQueue.main.async {
            let searchVc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "PanelistProfileVC") as! PanelistProfileVC
            searchVc.disclaimerdict = self.disclaimerdict
            self.present(searchVc, animated: false, completion: nil)
        }
    }


    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }

	
	
	@IBAction func threeDAction(_ sender: Any) {
		threeDImage = true
		self.pregnancyTrackerTable.reloadData()
		
		
	}
	
	@IBAction func fruitAction(_ sender: Any) {
		threeDImage = false
		self.pregnancyTrackerTable.reloadData()
	}
	
}

extension PregnancyTrackerDetailVC:UICollectionViewDataSource,UICollectionViewDelegate{
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		if  let fetousImagesArr = weekDIct["3d_image_data"] as? [[String:Any]] {
			return fetousImagesArr.count
		}
        return 1
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "_DImageCollectionCell", for: indexPath) as! _DImageCollectionCell
        
		if let fetousArr = weekDIct["3d_image_data"] as? [[String:Any]] {
			let fetDict = fetousArr[indexPath.row]
			cell.imgFoetus.sd_setImage(with: URL.init(string: fetDict["image_3d"] as? String ?? "")) { (img, err, cash, uri) in
			DispatchQueue.main.async {
				
				}
			}
		}

		
		return cell
	}

}
extension PregnancyTrackerDetailVC:UICollectionViewDelegateFlowLayout{

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
		return CGSize(width: 430, height: 224)
	}
//	func collectionView(_ collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                        sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        return CGSize(width: 390, height: 224)
//    }
}

