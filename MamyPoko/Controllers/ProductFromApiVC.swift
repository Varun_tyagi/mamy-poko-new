//
//  ProductFromApiVC.swift
//  MamyPoko
//
//  Created by Arnab Maity on 14/01/20.
//  Copyright © 2020 Varun Tyagi. All rights reserved.
//

import UIKit
import SDWebImage

class ProductFromApiVC: UIViewController {
	
	@IBOutlet weak var headerView: UIView!
	
	@IBOutlet weak var productTbleView: UITableView!
	
	@IBOutlet weak var menuBtn: UIButton!
	
	@IBOutlet weak var searchBtn: UIButton!
	
	@IBOutlet weak var profileImageButton: RoundButton!
	
	var products = [ProductCategoryCatalogModel]()
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		profileImageButton.cornerRadius = 20
		profileImageButton.borderColor = UIColor.clear
		
		getProductApi()
		
		
		if #available(iOS 13.0, *) {
			let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
			statusBar.backgroundColor = UIColor.clear
			UIApplication.shared.keyWindow?.addSubview(statusBar)
		} else {
			UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
		}

		// Do any additional setup after loading the view.
	}
	
//	override func viewWillAppear(_ animated: Bool) {
//		super.viewWillAppear(true)
//		if #available(iOS 13.0, *) {
//			let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
//			statusBar.backgroundColor = UIColor.clear
//			UIApplication.shared.keyWindow?.addSubview(statusBar)
//		} else {
//			UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
//		}
//	}
	
	@IBAction func menuAction(_ sender: Any) {
		toggleSideMenuView()
	}
	
	@IBAction func searchAction(_ sender: Any) {
		let searchVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
		self.navigationController?.pushViewController(searchVc, animated: true)
	}
	
	@IBAction func profileAction(_ sender: Any) {
		
		let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
		destViewController.isHereChangeStatus = false
		destViewController.selectedButton = 0
		if Constants.appDel.returnTopViewController().isKind(of: HomeVC.classForCoder()){
			let home = Constants.appDel.returnTopViewController() as! HomeVC
			home.hideSideMenuView()
			home.navigationController?.pushViewController(destViewController, animated: true)
		}
		else{
			sideMenuController()?.setContentViewController(destViewController)
		}
	}
	
	@objc func gotoCatalogDetails( _ sender : UIButton){
		
		let btn = sender
		
		if btn.isSelected == true {
			btn.backgroundColor = UIColor.white
			btn.setTitleColor(UIColor.black, for: .normal)
			btn.isSelected = false
			print("Hello mamy Poko")
			//productDetail()
			
		} else {
			btn.backgroundColor = UIColor.init(red: 94/255, green: 190/255, blue: 235/255, alpha: 1.0)//Choose your colour here
			btn.setTitleColor(UIColor.white, for: .normal) //To change button Title colour .. check your button Tint color is clear_color..
			btn.isSelected = true
			print("Hello mamy Pokos")
			//productDetail()
			let  destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "NewProductsVC") as! NewProductsVC
			destViewController.productType = products[sender.tag].type ?? ""
			// destViewController.prodCatlog = sender.accessibilityElements?.first as? ProductCatalogModel
			self.navigationController?.pushViewController(destViewController, animated: true)
		}
		
	}
	
	
	
	
	
}

extension ProductFromApiVC{
	
	//MARK:-Api
    func getProductApi() {
        ContentNetwork.GetDiapersServices { (response) in
            print(response)
            let responseDict = response as Dictionary<String, Any>
            Constants.appDel.stopLoader()
            if responseDict["status"] as? Int == 1  || responseDict["status"] as? String == "1" {
                
                DispatchQueue.main.async {
                    
                    
                    
                    let productContentModel = try? ProductModel((response.json))
                    self.products = productContentModel?.data ?? []
                    self.productTbleView.reloadData()
                }
            }
            else if responseDict["status"] as! Int == 0 {
                DispatchQueue.main.async {
                    PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                }
            }
            else{
                PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                
            }
            
        }
    }
    
}

extension ProductFromApiVC:UITableViewDelegate,UITableViewDataSource{
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return products.count
	}
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		//		let cell = tableView.dequeueReusableCell(withIdentifier: "ProductApiCell" forin ) as! ProductFromApiCell
		let cell = tableView.dequeueReusableCell(withIdentifier: "ProductApiCell", for: indexPath) as! ProductFromApiCell
		//let product = products[indexPath.row]
		cell.catalogDesLbl.text = products[indexPath.row].type
		let imgData = self.products[indexPath.row].image
		cell.catalogImg.sd_setImage(with: URL.init(string: imgData ?? ""))
		cell.viewAllBtn.accessibilityElements = [products]
		cell.viewAllBtn.addTarget(self, action: #selector(gotoCatalogDetails(_:)), for: .touchUpInside)
		cell.viewAllBtn.tag = indexPath.row
		return cell
	}
}
