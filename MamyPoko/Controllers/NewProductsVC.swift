//
//  NewProductsVC.swift
//  MamyPoko
//
//  Created by Arnab  maity on 19/01/20.
//  Copyright © 2020 Varun Tyagi. All rights reserved.
//

import UIKit

class NewProductsVC: UIViewController{
	
	
	@IBOutlet weak var table: UITableView!
	
	var mamyProducts: UICollectionView!
	
	var categoryProduct = [GetproductModels]()
	//var categoryProduct: ProductCatalogCategoryModels = []
	var productType = ""
	
	override func viewDidLoad() {
		super.viewDidLoad()
		productDetail()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		if #available(iOS 13.0, *) {
			let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
			statusBar.backgroundColor = UIColor.clear
			UIApplication.shared.keyWindow?.addSubview(statusBar)
		} else {
			UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
		}
	}
	//MARK:- ACTIONS
	
	@IBAction func menuAction(_ sender: Any) {
		toggleSideMenuView()
	}
	
	@IBAction func searchAction(_ sender: Any) {
		let searchVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
		self.navigationController?.pushViewController(searchVc, animated: true)
	}
	
	@IBAction func profileAction(_ sender: Any) {
		
		let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
		destViewController.isHereChangeStatus = false
		destViewController.selectedButton = 0
		if Constants.appDel.returnTopViewController().isKind(of: HomeVC.classForCoder()){
			let home = Constants.appDel.returnTopViewController() as! HomeVC
			home.hideSideMenuView()
			home.navigationController?.pushViewController(destViewController, animated: true)
		}
		else{
			sideMenuController()?.setContentViewController(destViewController)
		}
	}
	
	@objc func btnAction(_ sender: UIButton!) {

		let btn = sender
		if btn!.isSelected == true {
			btn!.backgroundColor = UIColor.white
			btn!.setTitleColor(UIColor.black, for: .normal)
			btn!.isSelected = false
			print("Hello mamy Poko")
		} else {
			btn!.backgroundColor = UIColor.init(red: 94/255, green: 190/255, blue: 235/255, alpha: 1.0)//Choose your colour here
			btn!.setTitleColor(UIColor.white, for: .normal) //To change button Title colour .. check your button Tint color is clear_color..
			btn!.isSelected = true
			let  destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "NewProductDiapersVC") as! NewProductDiapersVC
			destViewController.productType = categoryProduct[sender.tag].type ?? ""
			destViewController.product_Alisa = categoryProduct[sender.tag].alias ?? ""
			// destViewController.prodCatlog = sender.accessibilityElements?.first as? ProductCatalogModel
			self.navigationController?.pushViewController(destViewController, animated: true)
			print("Hello mamy Pokos")
		}
		
	}
}
extension NewProductsVC{
	func productDetail(){
  	ContentNetwork.diapersService(productType, {(response) in
			print(response)
		let responseDict = response as Dictionary<String, Any>
		Constants.appDel.stopLoader()


								if responseDict["status"] as? Int == 1  || responseDict["status"] as? String == "1" {

									DispatchQueue.main.async {
										let productContentModel = try? GetProductModel ((response.json))
										self.categoryProduct = productContentModel?.data ?? []
										self.table.reloadData()
									}
								}
								else if responseDict["status"] as! Int == 0 {
									DispatchQueue.main.async {
										PKSAlertController.alert("Error", message: responseDict["message"] as! String)
									}
								}
								else{
									PKSAlertController.alert("Error", message: responseDict["message"] as! String)

								}
	})
	}
}
extension NewProductsVC: UITableViewDelegate, UITableViewDataSource{
	//MARK:- TableView Delegates and DataSource
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "DataCell", for: indexPath) as! DataCell
		
		self.mamyProducts = cell.collection
		
		self.mamyProducts.delegate = self
		self.mamyProducts.dataSource = self
		self.mamyProducts.reloadData()
		return cell
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 377
	}
	
}

extension NewProductsVC: UICollectionViewDelegate, UICollectionViewDataSource {
	//MARK:- CollectionView Delegates and DataSource
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return categoryProduct.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DataCollectionCell", for: indexPath) as! DataCollectionCell
		
		let imgData = self.categoryProduct[indexPath.row].productImage
		cell.productImg.sd_setImage(with: URL.init(string: imgData ?? ""))
		cell.productDescLbl.text = self.categoryProduct[indexPath.row].alias
		cell.productSizeLbl.text = self.categoryProduct[indexPath.row].price
		//cell.btn.accessibilityElements = [categoryProduct]
		cell.btn.addTarget(self, action: #selector(btnAction(_:)), for: .touchUpInside)
		cell.btn.tag = indexPath.row
		return cell
	}
	
}
