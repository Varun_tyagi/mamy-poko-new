//
//  NewPregnancyTrackerDetailVC.swift
//  MamyPoko
//
//  Created by Arnab Maity on 07/10/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit

class NewPregnancyTrackerDetailVC: UIViewController {

	@IBOutlet weak var imgFoetus: UIImageView!
	@IBOutlet weak var lblDetail: UILabel!
	//////////////////////////////Next
	@IBOutlet weak var btnNext: UIButton!
	//////////////////////////////Previous
	@IBOutlet weak var btnPrevious: UIButton!
	//////////////////////////////Fruit
	@IBOutlet weak var btnFruit: UIButton!
	//////////////////////////////3D
	@IBOutlet weak var btn3D: UIButton!
	
	@IBOutlet weak var lblFoetusWeekday: UILabel!
	//3d outlets

	
	//friut iboutlets
	
	@IBOutlet weak var lblFruitWeekDayTopHeading: UILabel!
	
	
	@IBOutlet weak var lblFruitBabySizeSubtitle: UILabel!
	/*****************************************************************************/
	@IBOutlet weak var lblFruitName: UILabel!
	@IBOutlet weak var imgFruit: UIImageView!
	//friut iboutlets

    override func viewDidLoad() {
        super.viewDidLoad()
		setUp3D()

    }
    
	/***************************Functions**************************************************/
	/*****************************************************************************/

	@IBAction func btn3DClicked(_ sender: UIButton) {
	  setUp3D()
	  
	}
	@IBAction func btnFruitClicked(_ sender: UIButton) {
	  setUpFruitSize()
	}
	//////////////////////////////Fruit

	

	//////////////////////////////Next
	//show next image
	@IBAction func leftSwipe(_ sender: Any) {
	  print("left swipe")
	}
	
	//show prev image
	@IBAction func rightSwipe(_ sender: Any) {
	  print("right swipe")
	}


}
extension NewPregnancyTrackerDetailVC{
	fileprivate func setUp3D(){
	  btn3D.backgroundColor = UIColor.systemTeal
	  btnFruit.backgroundColor = UIColor.lightGray
	  
	  imgFruit.isHidden = true
	  lblFruitName.isHidden = true
	  lblFruitBabySizeSubtitle.isHidden = true
	  lblFruitWeekDayTopHeading.isHidden = true
	  
	  lblFoetusWeekday.isHidden  = false
	  imgFoetus.image = UIImage(named: "sampleImage")

	}
	
	fileprivate func setUpFruitSize(){
	  
	  btnFruit.backgroundColor = UIColor.systemTeal
	  btn3D.backgroundColor = UIColor.lightGray
	  
	  imgFruit.isHidden = false
	  lblFruitName.isHidden = false
	  lblFruitBabySizeSubtitle.isHidden = false
	  lblFruitWeekDayTopHeading.isHidden = false
	  
	  lblFoetusWeekday.isHidden  = true
	  imgFoetus.image = UIImage(named: "bgfruit")
	}
	//////////////////////////////3D
}
