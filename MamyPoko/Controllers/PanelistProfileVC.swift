//
//  PanelistProfileVC.swift
//  MamyPoko
//
//  Created by Surbhi on 24/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import Foundation
import FirebaseAnalytics
import AppsFlyerLib

class PanelistProfileVC : UIViewController, UITableViewDelegate, UITableViewDataSource {
    //MARK:- IBOutlets
    @IBOutlet weak var detailTable: UITableView!

    
    //MARK:- Declerations
    var disclaimerdict = [String:Any]()
    
    //MARK:- View Methods
    override func viewDidLoad() {
		self.BlockSideMenuSwipe()
        self.hideKeyboardWhenTappedAround()
        let userName = self.disclaimerdict["doctor_name"] as?  String

        Analytics.logEvent(AnalyticsEventViewItem, parameters: [
			AnalyticsParameterItemID: "id-\(AppsFlyerConstant.PanelistProfileView)-\(userName!)",
			AnalyticsParameterItemName: "\(AppsFlyerConstant.PanelistProfileVC)",
            AnalyticsParameterContentType: "Visit"
            ])
        AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.PanelistDetailView,
                                             withValues: [
                                                AFEventParamContent: AppsFlyerConstant.PanelistDetailView,
                                                AFEventParamContentId: "29"
            ])
    }
	override func viewWillAppear(_ animated: Bool) {
		
	}
	
    override func didReceiveMemoryWarning() {
        
    }
    

    //MARK:- TableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DoctorProfileCell", for: indexPath) as! DoctorProfileCell
            
            cell.coverView.dropShadow(scale: true)
            let userImg = self.disclaimerdict["profile_image"] as?  String
            if userImg != nil || (userImg?.count)! > 0 {
                let userImgurl = URL(string: userImg!)
                cell.doctorImage.kf.setImage(with: userImgurl)
            }
            else{
                // placeholder Image
            }
            
            let userName = self.disclaimerdict["doctor_name"] as?  String
            cell.nameLabel.text = userName
            
            let userExperience = self.disclaimerdict["doctor_exp"] as?  String
            cell.experienceLabel.text = userExperience
			
			let userDegree = self.disclaimerdict["doctor_degree"] as?  String
            let userProfile = self.disclaimerdict["doctor_profile"] as?  String
            
			cell.degreeLabel.text = userProfile! + userDegree!.htmlToString
			
            let contentSTR = self.disclaimerdict["doctor_verified"] as?  String
            cell.contentLabel.text = contentSTR?.htmlToString
            
            tableView.estimatedRowHeight = 200
            tableView.rowHeight = UITableViewAutomaticDimension
            
            return cell

        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleDetailCellText", for: indexPath)
            let detailtextSectionLabel = cell.contentView.viewWithTag(511) as! UILabel

            let contentStr = self.disclaimerdict["doctor_description"] as! String

            var descriptionAlign = "<style type='text/css'>html,body {margin: 0;padding: 0;width: 100%;height: 100%; font-family: Helvetica Neue,Helvetica; font-size: 17px;}html {display: table;}body {display: table-cell;vertical-align: middle;padding: 0px;text-align: justify;-webkit-text-size-adjust: none;} h2 {font-size: 19px; color: #333; margin: 0;}h2 .head-yel {color: #f2b413;}h4 {font-size: 19px;}p {margin: 0px 0 0;} </style>"
            descriptionAlign.append(contentStr)

            detailtextSectionLabel.attributedText = descriptionAlign.htmlToAttributedString

            tableView.estimatedRowHeight = 100
            tableView.rowHeight = UITableViewAutomaticDimension

            return cell
        }
    }
    
    //MARK:- Action Methods
    @IBAction func BackButtonPressed(){
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func searchAction(_ sender: Any) {
        let searchVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        searchVc.isPresent = true
        present(searchVc, animated: false, completion: nil)
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }


}
