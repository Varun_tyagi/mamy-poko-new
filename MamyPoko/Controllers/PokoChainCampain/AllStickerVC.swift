//
//  AllStickerVC.swift
//  MamyPoko
//
//  Created by Arnab Maity on 25/07/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit
import SDWebImage
import AppsFlyerLib
import FirebaseAnalytics


struct StickerPackModel {
	var stickerArray:[StickerModel]?
}
struct  StickerModel {
	var name:String?
	var imageUrl:String?
}
class AllStickerVC: UIViewController {
	
	@IBOutlet weak var stickersTblView: UITableView!
	
	var tblSelectedIndex:Int = 0
	// Properties
	
	private var needsFetchStickerPacks = true
	private var stickerPacks: [StickerPack] = []
	private var selectedIndex: IndexPath?
	
	var stickerPack: StickerPack? {
		didSet {
			
			mainStickerCollection?.reloadData()
		}
	}

	var myIndexs = 0
	var mainStickerCollection : UICollectionView? = nil
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
        Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
			AnalyticsParameterItemID: "id-\(AppsFlyerConstant.StickerList)",
            AnalyticsParameterItemName: "\(AppsFlyerConstant.StickerList)",
            AnalyticsParameterContentType: "Sticker PAck"
            ])
        AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.StickerList,
                                             withValues: [
                                                AFEventParamContent: AppsFlyerConstant.StickerList,
                                                AFEventParamContentId: "47"
            ])
		stickersTblView.reloadData()
		if #available(iOS 13.0, *) {
			let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
			statusBar.backgroundColor = Colors.MAMYBlue
			 UIApplication.shared.keyWindow?.addSubview(statusBar)
		} else {
			 UIApplication.shared.statusBarView?.backgroundColor = Colors.MAMYBlue
		 }
		
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		if let selectedIndex = self.selectedIndex {
			stickersTblView.deselectRow(at: selectedIndex, animated: true)
		}
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		if needsFetchStickerPacks {
			needsFetchStickerPacks = false
			self.fetchStickerDataFRomServer()
		}
	}
	//Action
	@IBAction func menuBtn(_ sender: Any) {
		toggleSideMenuView()
		
	}
	@IBAction func searchBtn(_ sender: Any) {
		let searchVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
		self.navigationController?.pushViewController(searchVc, animated: true)
	}
	
}
extension AllStickerVC{
	
	private func fetchStickerDataFRomServer(){
		if Helper.isConnectedToNetwork(){
			//let urlstr = "http://35.154.227.98/index.php/webservices/pokochan/getContent"
			let urlstr = Constants.BASEURL + MethodName.getSticker
			Constants.appDel.startLoader()
			Server.getRequestWithURL(urlString: urlstr) { (responseDict) in
				
				Constants.appDel.stopLoader()
				let message  = responseDict?["message"] as? String ?? KSomeErrorTryAgain
				if responseDict?["status"] as? Int == 1  || responseDict?["status"] as? String == "1"
				{
					
			self.fetchStickerPacks(responseDict?["data"] as? [String:Any] ?? [:])
				
				}
					
					
				else if  responseDict?["status"] as? Int == 0  || responseDict?["status"] as? String == "0" {
					
					DispatchQueue.main.async {
						PKSAlertController.alert("Error", message: message)
					}
				}
				else{
					
					PKSAlertController.alert("Error", message: message)
					
				}
			}
			
		}
			
		else{
			PKSAlertController.alertForNetwok()
		}
	}
	
	private func fetchStickerPacks(_ responseDict:[String:Any]) {
//		print(responseDict)
		 StickerPackManager.fetchStickerPacks(fromJSON: responseDict) { stickerPacks in
			DispatchQueue.main.async {
				self.stickerPacks.removeAll()
				self.stickerPacks = stickerPacks
				self.stickersTblView.reloadData()
			}
			}
		
	}
	private func show(stickerPack: StickerPack, animated: Bool) {
		// "stickerPackNotAnimated" still animates the push transition on iOS 8 and earlier.
		let identifier = animated ? "stickerPackAnimated" : "stickerPackNotAnimated"
		performSegue(withIdentifier: identifier, sender: stickerPack)
	}
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let vc = segue.destination as? StickerVC,
			let stickerPack = sender as? StickerPack {
			vc.title = stickerPack.name
			vc.stickerPack = stickerPack
			vc.navigationItem.hidesBackButton = stickerPacks.count <= 1
		}
	}
}
extension AllStickerVC:UITableViewDelegate{

	@objc func onClickPackAction(_ sender:UIButton) {
		let stickerPAck = stickerPacks[sender.tag]
		
		DispatchQueue.main.async {
			self.checkAllStieckerDownloaded(sender, stickerPAck)
		}
		if !(stickerPAck.trayImageUrl?.contains("Done"))!{
			Constants.appDel.startLoader()
			UIImageView().sd_setImage(with: URL.init(string: stickerPAck.trayImageUrl ?? "")) { (trayImg, err, cashe, uri) in
				if let trayImage = trayImg{
					var  newImg = trayImage
					if newImg.size.width > 512 || newImg.size.height > 512{
						newImg = newImg.sd_resizedImage(with: CGSize.init(width: 512, height: 512), scaleMode: SDImageScaleMode.aspectFit)!
					}
					stickerPAck.trayImage = ImageData.init(data: UIImagePNGRepresentation(newImg)!, type: ImageDataExtension.png)
					
					DispatchQueue.main.async {
					self.checkAllStieckerDownloaded(sender, stickerPAck)
					}
				}
			}
		//	var isAllDownloaded = true
			for (index,sticker) in stickerPAck.stickers.enumerated(){
				let  newStcker = sticker
				if sticker.isDownloaded == false{
				//	isAllDownloaded=false
					UIImageView().sd_setImage(with: URL.init(string: sticker.imageUrl ?? "")) { (Img, err, cashe, uri) in
						if let stickerImage = Img{
							newStcker.isDownloaded=true
							var  newImg = stickerImage
							if newImg.size.width > 512 || newImg.size.height > 512{
								newImg = newImg.sd_resizedImage(with: CGSize.init(width: 512, height: 512), scaleMode: SDImageScaleMode.aspectFit)!
							}
						
							newStcker.imageData = ImageData.init(data: UIImagePNGRepresentation(newImg)!, type: ImageDataExtension.png)
							//print(newStcker.imageData?.data.count as Any)
							stickerPAck.stickers[index] = newStcker
							DispatchQueue.main.async {
								self.checkAllStieckerDownloaded(sender, stickerPAck)
							}
							
						}
					}
				}
			}

			
			
			return
		}
		
		self.checkAllStieckerDownloaded(sender, stickerPAck)

		var isAllDownloaded = true
		for sticker in stickerPAck.stickers{
			if sticker.isDownloaded == false{
				isAllDownloaded=false
			}
		}
		if isAllDownloaded{
			let vc = Constants.mainStoryboard3.instantiateViewController(withIdentifier: "StickerVC") as! StickerVC
			vc.stickerPack = stickerPacks[sender.tag]
			self.navigationController?.pushViewController(vc, animated: true)
		}
		
	}
	func checkAllStieckerDownloaded(_ sender:UIButton,_ stickerPAck:StickerPack ){
		
		var isAllDownloaded = true
		for sticker in stickerPAck.stickers{
			if sticker.isDownloaded == false{
				isAllDownloaded=false
			}
		}
		if isAllDownloaded{
			Constants.appDel.stopLoader()
			stickerPAck.trayImageUrl = "Done"
			sender.setImage(#imageLiteral(resourceName: "NextIcon"), for: UIControlState.normal)
		}else{
			sender.setImage(#imageLiteral(resourceName: "plusDownloadSticker"), for: UIControlState.normal)
			//sender.setImage(#imageLiteral(resourceName: "downloadSticker"), for: UIControlState.normal)

			self.onClickPackAction(sender)
		}
	}
	
}
extension AllStickerVC:UITableViewDataSource{
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return stickerPacks.count

	}
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 1
	}
	
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell=tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! StickersTableViewCell
		cell.stikcerKbLabel.text = ""
		
		cell.stickerCollection.tag = indexPath.section
		cell.stickerCollection.accessibilityElements = [stickerPacks[indexPath.section]]
		cell.stickerCollection.delegate = self
		cell.stickerCollection.dataSource = self
		self.mainStickerCollection = cell.stickerCollection
		self.mainStickerCollection!.reloadData()
		
		cell.nxtButton.tag = indexPath.section
		let stickerPAck = stickerPacks[indexPath.section]
		cell.stickerLabel.text = stickerPAck.name

		if !(stickerPAck.trayImageUrl?.contains("Done"))!{
			cell.nxtButton.setImage(#imageLiteral(resourceName: "plusDownloadSticker"), for: UIControlState.normal)
		//cell.nxtButton.setImage(#imageLiteral(resourceName: "downloadSticker"), for: UIControlState.normal)
		}else{
		cell.nxtButton.setImage(#imageLiteral(resourceName: "NextIcon"), for: UIControlState.normal)
		}
		
		cell.nxtButton.addTarget(self, action: #selector(self.onClickPackAction(_:)), for: UIControlEvents.touchUpInside)
		return cell
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 130
	}

}
extension AllStickerVC:UICollectionViewDelegate{
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		
	}
}
extension AllStickerVC:UICollectionViewDataSource{
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		if let stickerPAck = collectionView.accessibilityElements?.first as? StickerPack{
			return stickerPAck.stickers.count
		}else{
			return 0
		}
	}
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellid", for: indexPath) as! StickerCollectionViewCell

		if let stickerPAck = collectionView.accessibilityElements?.first as? StickerPack{
			let  sticker = stickerPAck.stickers[indexPath.row]
			cell.sticker = sticker
			cell.sticker?.collectionIndex = collectionView.tag
			cell.sticker?.cellIndex = indexPath.row
			cell.stickerCallbackDelegate=self
			
			
		}

			return cell
			}
	
		}


	
extension AllStickerVC: UpdateStickerPackDelegate{
	func updateStickerFromCell(_ sticker: Sticker?) {
		self.stickerPacks[sticker?.collectionIndex ?? 0 ].stickers[sticker?.cellIndex ?? 0] = sticker!
		
	}
	
	
}
