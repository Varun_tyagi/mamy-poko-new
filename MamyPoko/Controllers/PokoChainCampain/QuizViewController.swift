//
//  QuizViewController.swift
//  MamyPoko
//
//  Created by Arnab Maity on 23/10/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit
import Alamofire
import FirebaseAnalytics
import AppsFlyerLib

class QuizViewController: UIViewController {

  @IBOutlet weak var btnNext: UIButton!
  @IBOutlet weak var quizCollection: UICollectionView!
  
  @IBOutlet weak var tblView: UITableView!
    
    var currentIndexPath: IndexPath = IndexPath.init(row: 0, section: 0)
  
    var answerToPost : [[String : Any]] = []
  
	
	var quizArr = [QuestionModel]()
	
	override func viewDidLoad() {
        super.viewDidLoad()
   
        
        Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
                  AnalyticsParameterItemID: "id-\(AppsFlyerConstant.Quiz)",
                  AnalyticsParameterItemName: "\(AppsFlyerConstant.Quiz)",
                  AnalyticsParameterContentType: "Quiz Thank You"
                  ])
              AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.Quiz,
                                                   withValues: [
                                                      AFEventParamContent: AppsFlyerConstant.Quiz,
                                                      AFEventParamContentId: "58"
                  ])
    fetchQuizQuestion()

    }
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
        let layout = UICollectionViewFlowLayout()
           layout.itemSize = quizCollection.frame.size
           quizCollection.collectionViewLayout = layout
        
           layout.scrollDirection = .horizontal
	}
	//MARKS:- Action
	
	@IBAction func optionBtnPressed(_ sender: UIButton) {
        var  questionDetail = quizArr[self.currentIndexPath.row]
        
        switch sender.tag {
		 case 0:
            questionDetail.selectedAns = questionDetail.optionA ?? ""
		 case 1:
        questionDetail.selectedAns = questionDetail.optionB ?? ""
		 case 2:
            questionDetail.selectedAns = questionDetail.optionC ?? ""
		 case 3:
            questionDetail.selectedAns = questionDetail.optionD ?? ""
        default:
            questionDetail.selectedAns =  ""

        }
        quizArr[self.currentIndexPath.row] = questionDetail
        self.quizCollection.reloadData()
        
	}
	
 

  func scrollToNextCell(){
   
        var newIndexPath = currentIndexPath
            newIndexPath.row = currentIndexPath.row + 1
        if newIndexPath.row < quizArr.count {
             currentIndexPath = newIndexPath
            self.quizCollection.scrollToItem(at: newIndexPath, at: UICollectionViewScrollPosition.centeredHorizontally, animated: false)
        }

  }
	
    @IBAction func backAction(_ sender: Any) {
       let homeVc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        var navArr = self.navigationController?.viewControllers
        if (navArr?.contains(homeVc))!{
            self.navigationController?.popViewController(animated: true)
        }
        else{
            navArr?.append(homeVc)
            self.navigationController?.viewControllers = navArr!
            self.navigationController?.popToViewController(homeVc, animated: true)
        }
    }
    
	@IBAction func nextAction(_ sender: UIButton) {
        if quizArr[currentIndexPath.row].selectedAns == nil ||  quizArr[currentIndexPath.row].selectedAns == "" {
                   PKSAlertController.alert("Choose Answer")
                   return
               }
   
       if currentIndexPath == IndexPath.init(row: quizArr.count - 1 , section: 0){
        answerToPost.removeAll()
        for ques in self.quizArr{
            answerToPost.append(["question_id":ques.questionID ?? "","question":ques.question ?? "","user_answer":ques.selectedAns ?? "","right_ans":ques.rightAnsName ?? ""])
        }
        var  dict_data = ["data" : answerToPost, "user_id":UserDefaults.standard.string(forKey: Constants.USERID) ?? ""] as [String : Any]
        dict_data = ["data" : dict_data.json, "user_id":UserDefaults.standard.string(forKey: Constants.USERID) ?? ""]
        sendQuizAnswers(dict_data: dict_data)
            return
       }
       
            scrollToNextCell()
	 
	}
}
extension QuizViewController:UICollectionViewDataSource, UICollectionViewDelegate{
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return quizArr.count
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "quizCollectionCell", for: indexPath) as! QuizQuestionCollectionViewCell
        
        cell.bindData(quizArr[indexPath.row])
        

        cell.optionA.addTarget(self, action: #selector(self.optionBtnPressed(_:)), for: UIControlEvents.touchUpInside)
        cell.optionB.addTarget(self, action: #selector(self.optionBtnPressed(_:)), for: UIControlEvents.touchUpInside)
        cell.optionC.addTarget(self, action: #selector(self.optionBtnPressed(_:)), for: UIControlEvents.touchUpInside)
        cell.optionD.addTarget(self, action: #selector(self.optionBtnPressed(_:)), for: UIControlEvents.touchUpInside)

		return cell
	}
   
   

	
}

extension QuizViewController: UICollectionViewDelegateFlowLayout{

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: SCREEN_WIDTH, height: 270)
    }
    
}

extension QuizViewController{
	private func fetchQuizQuestion(){
		if Helper.isConnectedToNetwork(){

			let urlstr = Constants.BASEURL + MethodName.quiz
			Constants.appDel.startLoader()
			Server.getRequestWithURLNew(urlString: urlstr) { (data) in
				
				Constants.appDel.stopLoader()
					let quizData = try? JSONDecoder().decode(QuizDataModel.self, from: data ?? Data())
				if quizData?.status! == 1{
					self.quizArr = quizData!.data!
					DispatchQueue.main.async {
						self.quizCollection.reloadData()
					}
				}
					
					
				else if  quizData?.status! == 0{
					
					DispatchQueue.main.async {
						PKSAlertController.alert("Error", message: "Something Went wrong")
					}
				}
				
			}
			
		}
			
		else{
			PKSAlertController.alertForNetwok()
		}
	}

    private func sendQuizAnswers(dict_data : [String:Any]){
    if Helper.isConnectedToNetwork(){

      let urlstr = Constants.BASEURL + MethodName.answer
      
   Constants.appDel.startLoader()

        Server.postMultiPartdataQuiz(param: dict_data, urlStr: urlstr) { (response) in
            DispatchQueue.main.async {
            Constants.appDel.stopLoader()
                if response["status"] as? String == "1" || response["status"] as? Int == 1  {
                    // go to home page
                    self.navigationController?.popViewController(animated: true)
                }else{
                      self.navigationController?.popViewController(animated: true)
                    PKSAlertController.alert("Error", message: "Something Went wrong")
                }

            }
        }
       
       
    }
      
    else{
      PKSAlertController.alertForNetwok()
    }
  }
}



