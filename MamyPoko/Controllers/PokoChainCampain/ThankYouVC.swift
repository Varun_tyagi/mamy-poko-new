//
//  ThankYouVC.swift
//  MamyPoko
//
//  Created by Surbhi Varma on 17/06/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit
import FacebookShare
import FBSDKShareKit
import Social
import FirebaseAnalytics
import AppsFlyerLib
import Mixpanel
class ThankYouVC: UIViewController {

    @IBOutlet weak var leaderBoardHeight: NSLayoutConstraint!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var forSubmitingEntryLbl: UILabel!
    @IBOutlet weak var thankYouLbl: UILabel!
    @IBOutlet weak var blurrView: UIView!
    
    @IBOutlet weak var shareView: UIView!
    @IBOutlet weak var topBabyconstraint: NSLayoutConstraint! //45
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var bottomBabyConstraint: NSLayoutConstraint! // -93
    
    @IBOutlet weak var pokoChainImvView: UIImageView!
    @IBOutlet weak var subImageView: UIImageView!
	
	@IBOutlet weak var HeadingLbl: UILabel!
	
    @IBOutlet weak var topPokoChanView: NSLayoutConstraint!
    @IBOutlet weak var facebookBtnOutlet: UIButton!
	
	@IBOutlet weak var instagramBtnOutlet: UIButton!
	
	@IBOutlet weak var twitterBtnOutlet: UIButton!
	
	@IBOutlet weak var pokoChainView: UIView!
    @IBOutlet weak var profileImageButton : RoundButton!

	// Properties
	
	var documentIC: UIDocumentInteractionController!
	var postDatadict = [String : Any]()
	var selectedImage = UIImage()
    var isPopup=false
    var isSavedImageRequired = false
    var babyUrl:String = ""
	override func viewDidLoad() {
        super.viewDidLoad()
		
        Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
            AnalyticsParameterItemID: "id-\(AppsFlyerConstant.ThankYouBirthdayBash)",
            AnalyticsParameterItemName: "\(AppsFlyerConstant.ThankYouBirthdayBash)",
            AnalyticsParameterContentType: "Birthday Bash Thank You"
            ])
        AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.ThankYouBirthdayBash,
                                             withValues: [
                                                AFEventParamContent: AppsFlyerConstant.SubmitBirthdayBash,
                                                AFEventParamContentId: "43"
            ])
        tblView.tableFooterView = UIView()
        if ScreenSize.SCREEN_WIDTH > 400.0{
            topBabyconstraint.constant = 25
            bottomBabyConstraint.constant = -80
        }else if ScreenSize.SCREEN_WIDTH < 340{
			topBabyconstraint.constant = 48
			bottomBabyConstraint.constant = -110
		}
        
        if ScreenSize.SCREEN_HEIGHT < 670{
            leaderBoardHeight.constant = 50
            topPokoChanView.constant = 20

        }else{
            topPokoChanView.constant = 38
        }
        
        self.setupInitialView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
		if #available(iOS 13.0, *) {
			let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
			statusBar.backgroundColor = Colors.MAMYBlue
			 UIApplication.shared.keyWindow?.addSubview(statusBar)
		} else {
			 UIApplication.shared.statusBarView?.backgroundColor = Colors.MAMYBlue
		 }
        
    }
    fileprivate func getPokoChainWithFrameImage() -> UIImage{
        pokoChainView.backgroundColor = UIColor.white
        let img =   pokoChainView.takeScreenshot()
        pokoChainView.backgroundColor = UIColor.clear
        return img
    }
	// Mark: Actions
    @IBAction func ViewProfilePressed(){
        let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        destViewController.selectedButton = 0
        destViewController.isHereChangeStatus = false
		self.navigationController?.pushViewController(destViewController, animated: true)
//        if Constants.appDel.returnTopViewController().isKind(of: HomeVC.classForCoder()){
//            let home = Constants.appDel.returnTopViewController() as! HomeVC
//            home.hideSideMenuView()
//            home.navigationController?.pushViewController(destViewController, animated: true)
//        }
//        else{
//            sideMenuController()?.setContentViewController(destViewController)
//        }
    }
    @IBAction func facebookBtnPressed(_ sender: Any){
		Mixpanel.initialize(token: "bbabd4dc11ec9c7fefd7569a73409213")
		Mixpanel.mainInstance().track(event: "poko_chan_bday_thankyou_fb_share")
	   facebook(self.getPokoChainWithFrameImage())
    }
	
	
	@IBAction func instagramBtnPressed(_ sender: Any) {
		Mixpanel.initialize(token: "bbabd4dc11ec9c7fefd7569a73409213")
		Mixpanel.mainInstance().track(event: "poko_chan_bday_thankyou_insta_share")
		InstagramManager.sharedManager.postImageToInstagramWithCaption(imageInstagram:self.getPokoChainWithFrameImage(), instagramCaption: "#POKOChan #MamyPoko", view: self.view)

	}
	
	@IBAction func whatsappBtnPressed(_ sender: Any) {
		Mixpanel.initialize(token: "bbabd4dc11ec9c7fefd7569a73409213")
		Mixpanel.mainInstance().track(event: "poko_chan_bday_thankyou_whatsup_share")
		whatsapp(self.getPokoChainWithFrameImage())
	}
	
	@IBAction func menuBtnPressed(_ sender: Any) {
        toggleSideMenuView()
	}
	
	@IBAction func searchBtn(_ sender: Any) {
		let searchVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
		self.navigationController?.pushViewController(searchVc, animated: true)
	}
    
    @IBAction func leaderBoardACtion(_ sender: UIButton) {
		Mixpanel.initialize(token: "bbabd4dc11ec9c7fefd7569a73409213")
		Mixpanel.mainInstance().track(event: "poko_chan_bday_thankyou_winner")
        let searchVc = Constants.mainStoryboard3.instantiateViewController(withIdentifier: "PokoWinnerViewController") as! PokoWinnerViewController
       searchVc.isShowMenu=false
        self.navigationController?.pushViewController(searchVc, animated: true)
    }
    
}
extension ThankYouVC{
    fileprivate func setupInitialView(){
        Helper.GetProfileImageForButton(butName: self.profileImageButton)
        if babyUrl.count>0 {
            self.subImageView.sd_setImage(with: URL.init(string: self.babyUrl)) { (img, erre, cash, uri) in
                
            }
        }
        if isSavedImageRequired  {
            if let imgDAta = UserDefaults.standard.value(forKey: Constants.UserDefaultsConstant.babyImagePathBirthdayBashPath) as? Data , let babyImg =  UIImage.init(data: imgDAta) {
                self.selectedImage = babyImg
            }
        }
        
        self.subImageView.image = self.selectedImage
        
    }
}
extension ThankYouVC:UIDocumentInteractionControllerDelegate{
    fileprivate func facebook(_ pokoImage: UIImage){
		let shareToFacebook : SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
//		shareToFacebook.add(pokoImage)
		shareToFacebook.setInitialText("#PokoChan#MamyPoko")
		shareToFacebook.add(pokoImage)
		self.present(shareToFacebook, animated: true, completion: nil)
	}
    fileprivate func whatsapp(_ pokoImage: UIImage){
		let msg = "#PokoChan#MamyPoko"
		let urlWhats = "whatsapp://send?text=\(msg)"
		if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
			if let whatsappURL = URL(string: urlString) {
				if UIApplication.shared.canOpenURL(whatsappURL) {
					
					
						if let imageData = UIImageJPEGRepresentation(pokoImage, 1.0) {
							let tempFile = NSURL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent("Documents/whatsAppTmp.wai")
							do {
								try imageData.write(to: tempFile!, options: .atomic)
								
								self.documentIC = UIDocumentInteractionController(url: tempFile!)
								self.documentIC.uti = "net.whatsapp.image"
								self.documentIC.presentOpenInMenu(from: CGRect.zero, in: self.view, animated: true)
							}
							catch {
								//print(error)
							}
						}
				} else {
					PKSAlertController.alert("Error", message: "Please Install WhatsApp on your device")
					// Cannot open whatsapp
				}
			}
		}
	}
}
