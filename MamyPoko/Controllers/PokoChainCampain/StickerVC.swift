//
//  StickerVC.swift
//  MamyPoko
//
//  Created by Arnab Maity on 26/07/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import AppsFlyerLib

class StickerVC: UIViewController {

	@IBOutlet weak var groupStickerCollectionView: UICollectionView!
	
	//Properties

	var stickerPack: StickerPack? = nil
	
	var selectedPackModel:StickerPackModel? =  nil
	
	override func viewDidLoad() {
        super.viewDidLoad()
		
        Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
			AnalyticsParameterItemID: "id-\(AppsFlyerConstant.StickerPAckDetail)",
            AnalyticsParameterItemName: "\(AppsFlyerConstant.StickerPAckDetail)",
            AnalyticsParameterContentType: "Sticker PAck"
            ])
        AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.StickerPAckDetail,
                                             withValues: [
                                                AFEventParamContent: AppsFlyerConstant.StickerPAckDetail,
                                                AFEventParamContentId: "48"
            ])
        groupStickerCollectionView.reloadData()
		groupStickerCollectionView.dataSource = self
		
if #available(iOS 13.0, *) {
	let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
	statusBar.backgroundColor = Colors.MAMYBlue
	 UIApplication.shared.keyWindow?.addSubview(statusBar)
} else {
	 UIApplication.shared.statusBarView?.backgroundColor = Colors.MAMYBlue
 }
        // Do any additional setup after loading the view.
    }
	//Action
	
	@IBAction func backAction(_ sender: UIButton) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func addToWhatsAppAction(_ sender: Any) {
		
        Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
            AnalyticsParameterItemID: "id-Download_Sticker_PAck",
            AnalyticsParameterItemName: "Download_Sticker_PAck",
            AnalyticsParameterContentType: "Sticker PAck"
            ])
        AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.DownloadStickerPack,
                                             withValues: [
                                                AFEventParamContent: AppsFlyerConstant.DownloadStickerPack,
                                                AFEventParamContentId: "49"
            ])
		let loadingAlert: UIAlertController = UIAlertController(title: "Sending to WhatsApp", message: "\n\n", preferredStyle: .alert)
		loadingAlert.addSpinner()
		present(loadingAlert, animated: true, completion: nil)
		
		stickerPack?.sendToWhatsApp { completed in
			loadingAlert.dismiss(animated: true, completion: nil)
		}
	}
	
}
extension StickerVC:UICollectionViewDataSource{
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return stickerPack?.stickers.count ?? 0
	}
	

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)as! GroupStickerCollectionViewCell

		cell.sticker = stickerPack?.stickers[indexPath.row]
		
		return cell
	}
}
