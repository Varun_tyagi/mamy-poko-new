//
//  BirthdayBashPopUpVC.swift
//  MamyPoko
//
//  Created by Arnab Maity on 28/06/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit
import Mixpanel
let KparticipateAction = "ParticipateAction"
let KSkipAction = "SkipAavtion"

typealias ParticipateCallBack = (_ infoToReturn :String) ->()

class BirthdayBashPopUpVC: UIViewController {

    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var heightPopupView: NSLayoutConstraint!
    var isBabyForm = false
    var participateCallBack:ParticipateCallBack?
    
	@IBOutlet weak var termsAndAgreementLabel: UILabel!
    @IBOutlet weak var profileImageButton : RoundButton!

	override func viewDidLoad() {
        super.viewDidLoad()
		initialView()
//        if isBabyForm{
//            self.skipBtn.isHidden=true
//            heightPopupView.constant = 550
//            self.termsAndAgreementLabel.text = """
//            Photos should include only the baby and not their mothers.
//
//            Only 1 entry is allowed per user.
//
//            Entries with proper information i.e., Baby’s Name, Email, Mobile, City will be considered while selecting images for videos.
//
//            Blurred, hazy photos will not be considered.
//
//            Black and white photos will not be considered.
//
//            Images with diaper products other than MamyPoko will not be accepted and considered for the video.
//            """
//        }else{
//            self.skipBtn.isHidden=false
//            heightPopupView.constant = 500
//            self.termsAndAgreementLabel.text = """
//            It’s time to #SayCheeswWithPokoChan! Share with us an endearing picture of your baby wearing the new Poko Chan edition diapers and get a chance to win exciting prizes. Update to the latest MamyPoko app to participate!
//            """
//        }
    }

    @IBAction func skipAction(_ sender: UIButton) {
        self.removeChildVC()
        self.participateCallBack?(KSkipAction)

    }
    @IBAction func participateAction(_ sender: UIButton) {
         self.removeChildVC()
		Mixpanel.initialize(token: "bbabd4dc11ec9c7fefd7569a73409213")
		Mixpanel.mainInstance().track(event: "poko_chan_bday_home")
        if !self.isBabyForm{
            self.participateCallBack?(KparticipateAction)
        }
        
    }
	func initialView(){
		let text = "It’s time to #SayCheeseWithPokoChan! Share with us an endearing picture of your baby wearing the new Poko Chan edition diapers and get a chance to win exciting prizes. Update to the latest MamyPoko app to participate!"
		let linkTextWithColor = "#SayCheeseWithPokoChan!"
		
		let range = (text as NSString).range(of: linkTextWithColor)
		
		let attributedString = NSMutableAttributedString(string:text)
		attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.init(red: 3.0/255.0, green: 73.0/255.0, blue: 154.0/255.0, alpha: 1.0) , range: range)
		
		self.termsAndAgreementLabel.attributedText = attributedString
	}
}
