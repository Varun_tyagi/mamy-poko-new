//
//  SampleNotificationVC.swift
//  MamyPoko
//
//  Created by Arnab Maity on 19/07/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit

class SampleNotificationVC: UIViewController {

	@IBOutlet weak var nameTxt: RoundTextField!
	
	@IBOutlet weak var mobileTxt: RoundTextField!
	
	@IBOutlet weak var pinCodetxt: UITextField!
	
	@IBOutlet weak var cityTxt: UITextField!
	
	
	override func viewDidLoad() {
        super.viewDidLoad()
		
		pinCodetxt.delegate = self
		mobileTxt.delegate = self
		setupInitialView()
		
if #available(iOS 13.0, *) {
	let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
	statusBar.backgroundColor = Colors.MAMYBlue
	 UIApplication.shared.keyWindow?.addSubview(statusBar)
} else {
	 UIApplication.shared.statusBarView?.backgroundColor = Colors.MAMYBlue
 }
    }
	//Marks: Action
	
	@IBAction func ViewProfilePressed(){
		let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
		destViewController.selectedButton = 0
		destViewController.isHereChangeStatus = false
		self.navigationController?.pushViewController(destViewController, animated: true)
	}
	
	@IBAction func backAction(_ sender: Any) {
		let home =  Constants.mainStoryboard2.instantiateViewController(withIdentifier:"HomeVC" ) as! HomeVC
		self.navigationController?.pushViewController(home, animated: true)
	}
	
	
	
	@IBAction func menuBtn(_ sender: Any) {
		
		toggleSideMenuView()
		
	}

	@IBAction func submitAction(_ sender: UIButton) {
		if CheckDataValidation(){
		
//			self.registerBabyDetails()
			let searchVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
			self.navigationController?.pushViewController(searchVc, animated: true)
		}
	}
	
}
extension SampleNotificationVC{
	
	// Marks: Web Service
	func getCityForPinCode(pin : String){
		DispatchQueue.main.async {
			self.view.endEditing(true)
		}
		if Helper.isConnectedToNetwork() {
			Constants.appDel.startLoader()
			let url_str = Constants.BASEURL + MethodName.Pincode + "\(pin)"
			Server.getRequestWithURL(urlString: url_str, completionHandler: { (response) in
				Constants.appDel.stopLoader()
				if !(response==nil){
					if (response?.count)! > 1 && response?["status"] as? Int ==  1 {
						let data = response?["data"] as! String
						DispatchQueue.main.async {
							self.cityTxt?.text = "\(data)"
							self.mobileTxt?.becomeFirstResponder()
						}
					}else{
						// self.presentAlert(withTitle: Constants.appName, message: (response?["message"] as? String) ?? " Invalid pincode, please try again.")
						DispatchQueue.main.async {
							self.view.endEditing(true)
							let alertController = UIAlertController(title: Constants.appName, message: (response?["message"] as? String) ?? " Invalid pincode, please try again.", preferredStyle: .alert)
							let OKAction = UIAlertAction(title: "OK", style: .default) { action in
								self.pinCodetxt?.text = ""
								self.pinCodetxt?.becomeFirstResponder()
							}
							alertController.addAction(OKAction)
							
							super.present(alertController, animated: true, completion: nil)
						}
						//  PKSAlertController.alert(Constants.appName, message: response?["message"] as! String)
					}
				}else{
					PKSAlertController.alert(Constants.appName, message: "Some error has occured! Please try again")
				}
			})
		}else{
			PKSAlertController.alertForNetwok()
		}
	}
	
	fileprivate func setupInitialView(){
		
		nameTxt.setLeftPaddingPoints()
		
		mobileTxt.setLeftPaddingPoints()
		pinCodetxt.setLeftPaddingPoints()
		cityTxt.setLeftPaddingPoints()
	
	}
	
	func CheckDataValidation()-> (Bool){
		let name = "\(self.nameTxt!.text!.trim())"
		let mobileNumber = "\(self.mobileTxt!.text!.trim())"
		let pinCode = "\(self.pinCodetxt!.text!.trim())"
	
		
		if name.isEmpty == true {
			PKSAlertController.alert(Constants.appName, message: "Enter your child name!")
			return false
		}
		
		if mobileNumber.isEmpty == true {
			PKSAlertController.alert(Constants.appName, message: "Enter your mobile!")
			return false
		}
		
		if pinCode.isEmpty == true {
			PKSAlertController.alert(Constants.appName, message: "Enter your pin code")
			return false
		}
		
		return true
	}
}
extension SampleNotificationVC:UITextFieldDelegate{
	func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
		return true
	}
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		if textField == self.pinCodetxt {
			let currentCharacterCount = textField.text?.count ?? 0
			let set = NSCharacterSet(charactersIn: "0123456789").inverted
			if (range.length + range.location > currentCharacterCount){
				return false
			}
			
			let newLength = currentCharacterCount + string.count - range.length
			return newLength <= 6 && string.rangeOfCharacter(from: set) == nil
		}
		if textField == self.mobileTxt {
			let currentCharacterCount = textField.text?.count ?? 0
			let set = NSCharacterSet(charactersIn: "0123456789").inverted
			if (range.length + range.location > currentCharacterCount){
				return false
			}
			
			let newLength = currentCharacterCount + string.count - range.length
			return newLength <= 15 && string.rangeOfCharacter(from: set) == nil
		}
		return true
	}
	func textFieldDidEndEditing(_ textField: UITextField) {
		if textField == self.pinCodetxt{
			self.mobileTxt?.becomeFirstResponder()
			//HiT API
			if textField.text?.isEmpty == false{
				self.getCityForPinCode(pin : (self.pinCodetxt?.text)!)
			}
		}
	}
	
}
