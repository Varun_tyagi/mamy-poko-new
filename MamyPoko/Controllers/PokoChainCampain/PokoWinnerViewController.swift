//
//  PokoWinnerViewController.swift
//  MamyPoko
//
//  Created by varun tyagi on 15/06/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import AppsFlyerLib
import AAPlayer
import YouTubePlayer

class PokoWinnerViewController: UIViewController {
    
    @IBOutlet weak var noPArticipantLbl: UILabel!
    
    //HEADER Items
     var weakTxt: RoundTextField? = nil
     var searchTxt: RoundTextField? = nil
     var  previousWinnerCollection: UICollectionView? = nil
     var winnerPlayer: YouTubePlayerView? = nil
    //END
    
    @IBOutlet weak var profileImageButton : RoundButton!

    @IBOutlet var winnerCollection: UICollectionView!
    
    @IBOutlet weak var menuBtn: UIButton!

    var isShowMenu = true
    var videoId:String = "dPRBiPj0jGA"
    var selectedWeek:String = ""
    var participantArr:[PokoData] = []
    //MARK:- View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
            AnalyticsParameterItemID: "id-\(AppsFlyerConstant.WinnerBirthdayBash)",
            AnalyticsParameterItemName: "\(AppsFlyerConstant.WinnerBirthdayBash)",
            AnalyticsParameterContentType: "Birthday Bash Winner"
            ])
        AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.WinnerBirthdayBash,
                                             withValues: [
                                                AFEventParamContent: AppsFlyerConstant.WinnerBirthdayBash,
                                                AFEventParamContentId: "44"
            ])
        
        self.hideKeyboardWhenTappedAround()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
if #available(iOS 13.0, *) {
	let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
	statusBar.backgroundColor = Colors.MAMYBlue
	 UIApplication.shared.keyWindow?.addSubview(statusBar)
} else {
	 UIApplication.shared.statusBarView?.backgroundColor = Colors.MAMYBlue
 }
        setupView()
       
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if let player = self.winnerPlayer{
           // player.pausePlayback()
            player.stop()
            self.winnerPlayer=nil
        }
    }
    
   
    
    //MARK:- Action
    @IBAction func ViewProfilePressed(){
        let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        destViewController.selectedButton = 0
        destViewController.isHereChangeStatus = false
		self.navigationController?.pushViewController(destViewController, animated: true)
//        if Constants.appDel.returnTopViewController().isKind(of: HomeVC.classForCoder()){
//            let home = Constants.appDel.returnTopViewController() as! HomeVC
//            home.hideSideMenuView()
//            home.navigationController?.pushViewController(destViewController, animated: true)
//        }
//        else{
//            sideMenuController()?.setContentViewController(destViewController)
//        }
    }
   
    @IBAction func searchTxtAction(_ sender: RoundTextField) {
        if sender.text == "" {
             participantArr = PokoWinnerModel.sharedWinnerModel?.data ?? []
        }else{
            participantArr = PokoWinnerModel.sharedWinnerModel?.data?.filter({ (partic) -> Bool in
                return (partic.childName?.lowercased().contains(sender.text?.lowercased() ?? ""))!
            }) ?? []
        }
       // winnerCollection.reloadSections(IndexSet.init(integer: 0))
        self.searchTxt?.becomeFirstResponder()
    }
    @IBAction func BackButtonPressed(){
        if isShowMenu{
            toggleSideMenuView()
        }else{
            //self.dismiss(animated: true, completion: nil)
            self.navigationController?.popViewController(animated: true)
        }
       // self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func searchAction(_ sender: Any) {
        let searchVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(searchVc, animated: true)
    }
    
    
}
extension PokoWinnerViewController {
    fileprivate func setupView(){
        Helper.GetProfileImageForButton(butName: self.profileImageButton)

        menuBtn.setImage(isShowMenu ? #imageLiteral(resourceName: "menuIcon") : #imageLiteral(resourceName: "BackIcon"), for: .normal)
        self.callApiGetWinner()
    }
    
    fileprivate func callApiGetWinner()
    {
        if Helper.isConnectedToNetwork() {
            Constants.appDel.startLoader()
            var  url_str = Constants.BASEURL + MethodName.getBirthdayBashWinner
            if selectedWeek.count > 0 {
                url_str = url_str + "?week_id=\(selectedWeek)"
            }
            Server.getRequestWithURL(urlString: url_str, completionHandler: { (response) in
                Constants.appDel.stopLoader()
                response?.printJson()
                DispatchQueue.main.async {
                    if !(response==nil){
                        if (response?.count)! > 1 && response?["status"] as! Int == 1 || response?["status"] as? String == "1"{
                            if let pokoWinnerModel = try? PokoWinnerModel(response!.json){
                                PokoWinnerModel.sharedWinnerModel = pokoWinnerModel
                                self.participantArr = pokoWinnerModel.data ?? []
								if self.participantArr.count == 0 &&  PokoWinnerModel.sharedWinnerModel?.winnerVideos?.count == 0 {
                        self.noPArticipantLbl.isHidden=false
                                }else{
                            self.noPArticipantLbl.isHidden=true
                                }
                                self.videoId = pokoWinnerModel.winnerVideos?.first?.videoURL ?? ""

                                DispatchQueue.main.async {
                                    self.winnerCollection.reloadData()
                                }
                            }else{
                        PKSAlertController.alert(Constants.appName, message: response?["message"] as? String ?? KSomeErrorTryAgain)

                            }
                        }
                        else{
                            PKSAlertController.alert(Constants.appName, message: response?["message"] as? String ?? KSomeErrorTryAgain)
                        }
                    }
                    else{
                        PKSAlertController.alert(Constants.appName, message: KSomeErrorTryAgain)
                    }
                }
            })
        }
        else{
            PKSAlertController.alertForNetwok()
        }
    }
}
extension PokoWinnerViewController:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView==previousWinnerCollection ? PokoWinnerModel.sharedWinnerModel?.winnerVideos?.count ?? 0 : participantArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == winnerCollection {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ParticipantCollectionViewCell", for: indexPath) as! ParticipantCollectionViewCell
            cell.participant =  participantArr[indexPath.row]
            //PokoWinnerModel.sharedWinnerModel?.data![indexPath.row]
        
        return cell
            
        }else{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PreviousWinnerCollectionViewCell", for: indexPath) as! PreviousWinnerCollectionViewCell
            cell.winner = PokoWinnerModel.sharedWinnerModel?.winnerVideos![indexPath.row]

            return cell
        }
    }
    
   func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView{

        switch kind {
            
        case UICollectionElementKindSectionHeader:
            
            if collectionView == winnerCollection {
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "PreviousWinnerCollectionReusableView", for: indexPath) as! PreviousWinnerCollectionReusableView
                self.winnerPlayer = headerView.winnerVideoPlayerView
                self.weakTxt = headerView.weekTxt
                self.searchTxt = headerView.searchTxt
                self.previousWinnerCollection = headerView.previousWinnerCollection
                self.previousWinnerCollection?.delegate = self
                self.previousWinnerCollection?.dataSource = self
                self.previousWinnerCollection?.reloadData()
                self.weakTxt?.delegate=self
                
				self.winnerPlayer?.loadVideoURL(URL.init(string: "https://www.youtube.com/watch?v=\(videoId.trim())")!)
                self.winnerPlayer?.loadVideoID(self.videoId.trim())
                
                self.searchTxt = headerView.searchTxt
                self.searchTxt?.addTarget(self, action: #selector(self.searchTxtAction(_:)), for: UIControlEvents.editingChanged)
                self.searchTxt?.delegate=self
                
                
                if PokoWinnerModel.sharedWinnerModel?.winnerVideos?.count == 0 {
                    headerView.playerHeight.constant = 0
                headerView.previousVideoTxtHeight.constant = 0
                headerView.previousWinnerCollectionHeight.constant = 0
                }else{
                    headerView.playerHeight.constant = 200
                    headerView.previousVideoTxtHeight.constant = 80
                    headerView.previousWinnerCollectionHeight.constant = 180
                }
                return headerView
            }else{
                let headerVie = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "winnerReusableView", for: indexPath)
                return headerVie
            }
            
        default:
            assert(false, "Unexpected element kind")
        }
        return  UICollectionReusableView()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == winnerCollection{

            let cell = collectionView.cellForItem(at: indexPath) as! ParticipantCollectionViewCell
         
            let detailVc = Constants.mainStoryboard3.instantiateViewController(withIdentifier: "ShareBabyViewController") as! ShareBabyViewController
            detailVc.participant = participantArr[indexPath.row]
            detailVc.selectedImage = cell.participantImgView.image!
            self.navigationController?.pushViewController(detailVc, animated: true)
          
            
        }else{
            if let winnerVideo = PokoWinnerModel.sharedWinnerModel?.winnerVideos![indexPath.row]{
                self.winnerPlayer?.stop()
				self.videoId = winnerVideo.videoURL ?? ""
				
				self.winnerPlayer?.loadVideoURL(URL.init(string: "https://www.youtube.com/watch?v=\(videoId.trim())")!)
				self.winnerPlayer?.loadVideoID(self.videoId.trim())
            }
        }
    }
  
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if collectionView == winnerCollection{
            if PokoWinnerModel.sharedWinnerModel?.winnerVideos?.count == 0 {
                return CGSize.init(width: collectionView.frame.size.width, height: 601-180-200-80)
            }
        return CGSize.init(width: collectionView.frame.size.width, height: 601)
        }else{
            return CGSize.zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == winnerCollection{
            return CGSize.init(width: collectionView.frame.size.width/3-18, height:130)
        }else{
            return CGSize.init(width: 150, height: 180)
        }
    }

}

extension PokoWinnerViewController : UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.weakTxt {
            self.view.endEditing(true)
            var weekArr:[String] = []
            for weekModel in  PokoWinnerModel.sharedWinnerModel?.weeks ?? [] {
                weekArr.append("Week \(weekModel.week ?? "1")")
            }
            CustomPicker.show(data: [weekArr]) {  [weak self] (selections: [Int : String]) -> Void in
                if let name = selections[0] {
                    self?.selectedWeek = name.components(separatedBy: " ").last ?? ""
                    self?.weakTxt?.text = name
                    self?.callApiGetWinner()
                }
            }
            return false
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField==self.searchTxt{
            self.winnerCollection.reloadData()
        }
        self.view.endEditing(true)
    }
}
