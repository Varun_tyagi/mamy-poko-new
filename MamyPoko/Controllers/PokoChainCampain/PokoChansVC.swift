//
//  PokoChansVC.swift
//  MamyPoko
//
//  Created by Surbhi Varma on 17/06/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit
import AppsFlyerLib
import FirebaseAnalytics
import Mixpanel
import FRHyperLabel
class PokoChansVC: UIViewController {

	@IBOutlet weak var childNameTxt: RoundTextField!
	@IBOutlet weak var selectGender: RoundTextField!
	@IBOutlet weak var dateOfBirth: RoundTextField!
	@IBOutlet weak var uploadImage: RoundTextField!
	@IBOutlet weak var mobileNumberTxt: RoundTextField!
	@IBOutlet weak var submitBtnOutlet: UIButton!
    @IBOutlet weak var profileImageButton : RoundButton!

	
	@IBOutlet weak var termsAndCondition: FRHyperLabel!
	
	
	 var  userData : UserDataModel? //  for user profile
	
	var isComingFromTC = true
	
    @IBOutlet weak var checkTncBtn: UIButton!
    // Properties
	
	var datePicker = UIDatePicker()
	var toolBar : UIToolbar!
    var imagePicker = UIImagePickerController()
    var imageSelected : UIImage!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		
		self.TNClabel()
        Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
            AnalyticsParameterItemID: "id-\(AppsFlyerConstant.SubmitBirthdayBash)",
            AnalyticsParameterItemName: "\(AppsFlyerConstant.SubmitBirthdayBash)",
            AnalyticsParameterContentType: "Birthday Bash Submit"
            ])
        AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.ThankYouBirthdayBash,
                                             withValues: [
                                                AFEventParamContent: AppsFlyerConstant.SubmitBirthdayBash,
                                                AFEventParamContentId: "42"
            ])
        
        self.setupInitialView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
if #available(iOS 13.0, *) {
	let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
	statusBar.backgroundColor = Colors.MAMYBlue
	 UIApplication.shared.keyWindow?.addSubview(statusBar)
} else {
	 UIApplication.shared.statusBarView?.backgroundColor = Colors.MAMYBlue
 }
    }
    
	//Marks: Action
    
    @IBAction func termsCondtiionAcvtion(_ sender: UIButton) {
        
        let popupVc = Constants.mainStoryboard3.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
        self.addChildVC(popupVc)
        
    }
    
    
    @IBAction func checkTncAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func ViewProfilePressed(){
        let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        destViewController.selectedButton = 0
        destViewController.isHereChangeStatus = false
		self.navigationController?.pushViewController(destViewController, animated: true)
		
//        if Constants.appDel.returnTopViewController().isKind(of: HomeVC.classForCoder()){
//            let home = Constants.appDel.returnTopViewController() as! HomeVC
//          //  home.hideSideMenuView()
//            home.navigationController?.pushViewController(destViewController, animated: true)
//        }
//        else{
//            sideMenuController()?.setContentViewController(destViewController)
//        }
    }
	@IBAction func submitBtnPressed(_ sender: Any) {
      if CheckDataValidation(){
            self.registerBabyDetails()
        }
		Mixpanel.initialize(token: "bbabd4dc11ec9c7fefd7569a73409213")
		Mixpanel.mainInstance().track(event: "pkochan_bday_form_submit")

	}
	
	@IBAction func searchBtnPressed(_ sender: Any) {
		let searchVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
		self.navigationController?.pushViewController(searchVc, animated: true)
	}
	
	@IBAction func menuBtn(_ sender: Any) {
        toggleSideMenuView()

	}

	@IBAction func uploadBtnPressed(_ sender: Any) {
		self.showActionSheet()
	}
	
	
}
extension PokoChansVC{
	// Setup Initial View

	fileprivate func setupInitialView(){
		
		childNameTxt.setLeftPaddingPoints()
		selectGender.setLeftPaddingPoints()
		dateOfBirth.setLeftPaddingPoints()
		mobileNumberTxt.setLeftPaddingPoints()
		uploadImage.setLeftPaddingPoints()
        
		self.mobileNumberTxt.text = userData?.userPhone ?? ""
		self.childNameTxt.text = userData?.babyDetails?.babyName ?? ""
		self.dateOfBirth.text = userData?.babyDetails?.babyDob ?? ""
        
        if self.dateOfBirth.text!.count > 0 {
    let dateBaby = userData?.babyDetails?.babyDob?.toDateTime(dateFormate: "dd-MM-yyyy")
            self.dateOfBirth.text = dateBaby?.dateString("yyyy-MM-dd")
        }
        
		self.selectGender.text = userData?.babyDetails?.babyGender ?? ""
        Helper.GetProfileImageForButton(butName: self.profileImageButton)

        self.setupDatePicker()
		
//        let popupVc = Constants.mainStoryboard3.instantiateViewController(withIdentifier: "BirthdayBashTermsViewController") as! BirthdayBashTermsViewController
//        self.addChildVC(popupVc)
        
	}
	fileprivate func TNClabel(){
		//		let str = "I agree with T&C and the Rules of the Contest"
		
		let firstStr = NSAttributedString.init(string: "I agree with", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)])
		
		let TNCAttStr =  NSAttributedString.init(string: "T&C", attributes: [NSAttributedString.Key.foregroundColor: UIColor.blue,NSAttributedString.Key.underlineStyle: 1, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)])
		
		let andTheStr = NSAttributedString.init(string: "and the", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)])
		
		let RuleAttStr =  NSAttributedString.init(string: "Rules of the Contest", attributes: [NSAttributedString.Key.foregroundColor: UIColor.blue,NSAttributedString.Key.underlineStyle: 1, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)])
		
		
		
		let finalAttString = NSMutableAttributedString()
		finalAttString.append(firstStr)
		finalAttString.append(TNCAttStr)
		finalAttString.append(andTheStr)
		finalAttString.append(RuleAttStr)
		
		termsAndCondition.attributedText = finalAttString
		
		let handler = {
			(hyperLabel: FRHyperLabel!, substring: String!) -> Void in
			
			if substring == "T&C"{
				let popupVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
				popupVc.isShowMenu = false
				popupVc.isComingFromPokoChanform = true
				self.navigationController?.pushViewController(popupVc, animated: true)
			}else{
				self.getGameApi()
//				let popupVc = Constants.mainStoryboard3.instantiateViewController(withIdentifier: "BirthdayBashTermsViewController") as! BirthdayBashTermsViewController
//				self.addChildVC(popupVc)
			}
		}
		//Step 3: Add link substrings
		self.termsAndCondition.setLinksForSubstrings(["T&C"], withLinkHandler: handler)
		self.termsAndCondition.setLinksForSubstrings(["Rules of the Contest"], withLinkHandler: handler)
	}
    //API
    func registerBabyDetails() {
        if Helper.isConnectedToNetwork()
        {
            
            let urlstr = Constants.BASEURL + MethodName.registerBabyBirthdayBash
           
                Constants.appDel.startLoader()
               // let base64Str = "\(imageSelected.base64(format: ImageFormat.png) ?? "")"
                let parameters = [
                    "child_name" : self.childNameTxt.text ?? "",
                    "gender":self.selectGender.text == "Male" ? "Male":"Female",
                    "dob":self.dateOfBirth.text ?? "",
					"mobile_no": self.mobileNumberTxt.text ?? "",
                   // "image":base64Str,
                    "user_id":UserDefaults.standard.string(forKey: Constants.USERID) ?? ""
                ]
          //  let parameters = "child_name=\(self.childNameTxt.text ?? "")&gender=\(self.selectGender.text == "Male" ? "Male":"Female")&dob=\(self.dateOfBirth.text ?? "")&mobile_no=\(self.mobileNumberTxt.text ?? "")&image=\(base64Str)"
			
            Server.postMultiPartdata(param: parameters as [String : Any], urlStr: urlstr, image: self.imageSelected, imageName: "babyImg") { (responseDict) in
                Constants.appDel.stopLoader()
                _ = responseDict["message"] as? String ?? KSomeErrorTryAgain
                if responseDict["status"] as? Int == 1  || responseDict["status"] as? String == "1"
                {
					
                    DispatchQueue.main.async {
                     //   let alert = UIAlertController(title: "Success", message: message, preferredStyle: UIAlertControllerStyle.alert)
                     //   let acceptButton = UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction) in
                            //// Go to home page
                UserDefaults.standard.set(true, forKey: Constants.UserDefaultsConstant.isBirthdayBashSubmitted)
            let searchVc = Constants.mainStoryboard3.instantiateViewController(withIdentifier: "ThankYouVC") as! ThankYouVC
                searchVc.selectedImage = self.imageSelected
           self.navigationController?.pushViewController(searchVc, animated: true)
                       // })
                       // alert.addAction(acceptButton)
                      //  self.present(alert, animated: true, completion: nil)
                    }
                }
                else if  responseDict["status"] as? Int == 0  || responseDict["status"] as? String == "0" {
                    DispatchQueue.main.async {
                        PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                    }
                }
                else{
                    PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                    
                }
            }
            
        }
        
        else{
            PKSAlertController.alertForNetwok()
        }

    }

	// getgameplayApi
	func getGameApi(){
		if Helper.isConnectedToNetwork(){
			 let urlstr = Constants.BASEURL + MethodName.getgameplay
			 Constants.appDel.startLoader()
			Server.getRequestWithURL(urlString: urlstr) { (responseDict) in
				 Constants.appDel.stopLoader()
				let message  = responseDict?["message"] as? String ?? KSomeErrorTryAgain
				if responseDict?["status"] as? Int == 1  || responseDict?["status"] as? String == "1"
				{

					DispatchQueue.main.async {
                        if let data = responseDict?["data"] as? [String:Any] , let descText = data["description"] as? String{

                            let popupVc = Constants.mainStoryboard3.instantiateViewController(withIdentifier: "BirthdayBashTermsViewController") as! BirthdayBashTermsViewController
                            popupVc.TncStr = descText
                            self.addChildVC(popupVc)
                        }
						
						
					}
				}
				else if  responseDict?["status"] as? Int == 0  || responseDict?["status"] as? String == "0" {
					DispatchQueue.main.async {
						PKSAlertController.alert("Error", message: message)
					}
				}
				else{
					PKSAlertController.alert("Error", message: message)
					
				}
			}
			
		}
			
		else{
			PKSAlertController.alertForNetwok()
		}
	}

	//Custom Methods
	func setupDatePicker()  {
		datePicker.datePickerMode = UIDatePickerMode.date
		datePicker.addTarget(self, action: #selector(self.setDate(_sender:)), for: .valueChanged)
		datePicker.timeZone = TimeZone.current
		toolBar = UIToolbar()
		toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(dismissPicker))
		datePicker.setBirthdayBashBabyDobValidation()
		self.dateOfBirth?.inputView = datePicker
	}
	@objc func setDate(_sender : UIDatePicker){
		
		let dateFormatter: DateFormatter = DateFormatter()
		dateFormatter.dateFormat = "yyyy-MM-dd"
		let selectedDate: String = dateFormatter.string(from: _sender.date)
		
		if _sender == datePicker {
			
			datePicker.setBirthdayBashBabyDobValidation()
			
			if _sender.date > datePicker.minimumDate! && _sender.date < datePicker.maximumDate! {
				self.dateOfBirth?.text = "\(selectedDate)"
			}
			else{
				self.dateOfBirth?.text = ""
				datePicker.date = datePicker.maximumDate!
			}
		}
	}
	@objc func dismissPicker() {
		dateOfBirth.endEditing(true)
		dateOfBirth.resignFirstResponder()
	}
	
	// Mark:Validation
	func CheckDataValidation()-> (Bool){
		let childName = "\(self.childNameTxt!.text!.trim())"
		let mobileNumber = "\(self.mobileNumberTxt!.text!.trim())"
		let dateOfBirth = "\(self.dateOfBirth!.text!.trim())"
		let gender = "\(self.selectGender!.text!.trim())"
		
        if childName.isEmpty == true {
            PKSAlertController.alert(Constants.appName, message: "Enter your child name!")
            return false
        }
        if gender.isEmpty == true{
            PKSAlertController.alert(Constants.appName, message: "Select your gender!")
            return false
        }
        if dateOfBirth.isEmpty == true {
            PKSAlertController.alert(Constants.appName, message: "Enter your date of birth!")
            return false
        }
		if mobileNumber.isEmpty == true {
			PKSAlertController.alert(Constants.appName, message: "Enter your mobile!")
			return false
		}
		
        if self.imageSelected==nil{
            PKSAlertController.alert(Constants.appName, message: "Please upload baby image!")
            return false
        }
        
        if self.checkTncBtn.isSelected==false{
    PKSAlertController.alert(Constants.appName, message: "Please select terms & conditions!")
        return false
        
        }
		return true
	}
    
    //MARK:- Picker
    @objc func showActionSheet() {
        let actionSheet = UIAlertController.init(title: "Mamy Poko", message: "Upload Photo Using", preferredStyle: UIAlertControllerStyle.actionSheet)
        actionSheet.addAction(UIAlertAction.init(title: "Camera", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) in
            self.TakePhotoPressed()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Gallery", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.UploadPhotoPressed()
        }))
        
        actionSheet.addAction(UIAlertAction.init(title: "Photo Library", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.UploadPhotoPressed()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @objc func UploadPhotoPressed(){
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    @objc func TakePhotoPressed(){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
}
//MARK:- Image Picker Delegate
extension PokoChansVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let imseSelect = info["UIImagePickerControllerOriginalImage"] as! UIImage
        let newImg = Helper.resizeImage(imseSelect, newWidth: 400)
        DispatchQueue.global().async {
            UserDefaults.standard.set(UIImagePNGRepresentation(newImg), forKey: Constants.UserDefaultsConstant.babyImagePathBirthdayBashPath)
        }
        self.imageSelected = newImg
        self.uploadImage.text = "\(childNameTxt.text ?? "baby").png"
        self.dismiss(animated: true, completion: nil)

    }
}

extension PokoChansVC : UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == selectGender {
            self.view.endEditing(true)
            CustomPicker.show(data: [["Male", "Female"]]) {  [weak self] (selections: [Int : String]) -> Void in
                if let name = selections[0] {
                    self?.selectGender.text = name
                }
            }
            return false
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
}
