//
//  ShareBabyViewController.swift
//  MamyPoko
//
//  Created by Varun Tyagi on 26/06/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit
import FacebookShare
import FBSDKShareKit
import Social
import Mixpanel

class ShareBabyViewController: UIViewController {
    
    @IBOutlet weak var babyAge: UILabel!
    @IBOutlet weak var babyName: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var forSubmitingEntryLbl: UILabel!
    @IBOutlet weak var thankYouLbl: UILabel!
    @IBOutlet weak var blurrView: UIView!
    @IBOutlet weak var profileImageButton : RoundButton!

    @IBOutlet weak var shareView: UIView!
    @IBOutlet weak var topBabyconstraint: NSLayoutConstraint! //45
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var bottomBabyConstraint: NSLayoutConstraint! // -93
    
    @IBOutlet weak var pokoChainImvView: UIImageView!
    @IBOutlet weak var subImageView: UIImageView!
    
    @IBOutlet weak var HeadingLbl: UILabel!
    
    @IBOutlet weak var facebookBtnOutlet: UIButton!
    
    @IBOutlet weak var instagramBtnOutlet: UIButton!
    
    @IBOutlet weak var twitterBtnOutlet: UIButton!
    
    @IBOutlet weak var pokoChainView: UIView!
    
    // Properties
    
    var documentIC: UIDocumentInteractionController!
    var postDatadict = [String : Any]()
    var selectedImage = UIImage()
    var isPopup=false
    var participant:PokoData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tblView.tableFooterView = UIView()
        if ScreenSize.SCREEN_WIDTH > 400.0{
            topBabyconstraint.constant = 25
            bottomBabyConstraint.constant = -80
        }else if ScreenSize.SCREEN_WIDTH < 340{
            topBabyconstraint.constant = 48
            bottomBabyConstraint.constant = -110
        }
        self.setupInitialView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
if #available(iOS 13.0, *) {
	let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
	statusBar.backgroundColor = Colors.MAMYBlue
	 UIApplication.shared.keyWindow?.addSubview(statusBar)
} else {
	 UIApplication.shared.statusBarView?.backgroundColor = Colors.MAMYBlue
 }
    }
    fileprivate func getPokoChainWithFrameImage() -> UIImage{
        pokoChainView.backgroundColor = UIColor.white
        let img =   pokoChainView.takeScreenshot()
        pokoChainView.backgroundColor = UIColor.clear
        return img
    }
    // Mark: Actions
    
    @IBAction func ViewProfilePressed(){
        let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        destViewController.selectedButton = 0
        destViewController.isHereChangeStatus = false
        if Constants.appDel.returnTopViewController().isKind(of: HomeVC.classForCoder()){
            let home = Constants.appDel.returnTopViewController() as! HomeVC
            home.hideSideMenuView()
            home.navigationController?.pushViewController(destViewController, animated: true)
        }
        else{
            sideMenuController()?.setContentViewController(destViewController)
        }
    }
    @IBAction func facebookBtnPressed(_ sender: Any){
		Mixpanel.initialize(token: "bbabd4dc11ec9c7fefd7569a73409213")
		Mixpanel.mainInstance().track(event: "winner_list_page_fb_share")
        facebook(self.getPokoChainWithFrameImage())
    }
    
    
    @IBAction func instagramBtnPressed(_ sender: Any) {
		
		Mixpanel.initialize(token: "bbabd4dc11ec9c7fefd7569a73409213")
		Mixpanel.mainInstance().track(event: "winner_list_page_insta_share")
		InstagramManager.sharedManager.postImageToInstagramWithCaption(imageInstagram:self.getPokoChainWithFrameImage(), instagramCaption: "#PokoChan#MamyPoko", view: self.view)
        
    }
    
    @IBAction func whatsappBtnPressed(_ sender: Any) {
		Mixpanel.initialize(token: "bbabd4dc11ec9c7fefd7569a73409213")
		Mixpanel.mainInstance().track(event: "winner_list_page_whatsup_share")
		
		whatsapp(self.getPokoChainWithFrameImage())
    }
    
    @IBAction func menuBtnPressed(_ sender: Any) {
        //toggleSideMenuView()
        //self.removeChildVC()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func searchBtn(_ sender: Any) {
        let searchVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(searchVc, animated: true)
    }
    
}
extension ShareBabyViewController{
    fileprivate func setupInitialView(){
        self.subImageView.image = self.selectedImage
        self.babyName.text = participant?.childName ?? ""
        self.babyAge.text = "Age : "

        if let dob = participant?.dob?.components(separatedBy: " ").first?.calculateAge() {
            self.babyAge.text = "Age : \(dob.year==0 ? "" : String(dob.year)) years"
            if dob.year<1{
                self.babyAge.text = "Age : \(dob.month==0 ? "" : String(dob.month)) months"
             if dob.month<1{
                    self.babyAge.text = "Age : \(dob.day==0 ? "" : String(dob.day)) days"
            }
            }
        }
        
    }
    
    
}
extension ShareBabyViewController:UIDocumentInteractionControllerDelegate{
    fileprivate func facebook(_ pokoImage: UIImage){
        let shareToFacebook : SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
//        shareToFacebook.add(pokoImage)
        shareToFacebook.setInitialText("#PokoChan#MamyPoko")
        shareToFacebook.add(pokoImage)
        self.present(shareToFacebook, animated: true, completion: nil)
    }
    fileprivate func whatsapp(_ pokoImage: UIImage){
        let msg = "#PokoChan#MamyPoko"
        let urlWhats = "whatsapp://send?text=\(msg)"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
            if let whatsappURL = URL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL) {
                    
                    //if let image = UIImage(named: "Baby_Registration") {
                    if let imageData = UIImageJPEGRepresentation(pokoImage, 1.0) {
                        let tempFile = NSURL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent("Documents/whatsAppTmp.wai")
                        do {
                            try imageData.write(to: tempFile!, options: .atomic)
                            
                            self.documentIC = UIDocumentInteractionController(url: tempFile!)
                            self.documentIC.uti = "net.whatsapp.image"
                            self.documentIC.presentOpenInMenu(from: CGRect.zero, in: self.view, animated: true)
                        }
                        catch {
                            //print(error)
                        }
                    }
                    //}
                    
                } else {
                    // Cannot open whatsapp
                }
            }
        }
    }
}
