//
//  BirthdayBashTermsViewController.swift
//  MamyPoko
//
//  Created by Varun Tyagi on 01/07/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit

class BirthdayBashTermsViewController: UIViewController {
    
    @IBOutlet weak var heightPopupView: NSLayoutConstraint!
    @IBOutlet weak var profileImageButton : RoundButton!

    @IBOutlet weak var termsAndAgreementLabel: UILabel!
    var TncStr = ""
    override func viewDidLoad() {
        super.viewDidLoad()

	self.termsAndAgreementLabel.attributedText = TncStr.htmlToAttributedString

    }
   
    @IBAction func participateAction(_ sender: UIButton) {
        self.removeChildVC()
        
    }
    
}
