//
//  QuizThankyouViewController.swift
//  MamyPoko
//
//  Created by Varun Tyagi on 20/11/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import UIKit


import UIKit
import FacebookShare
import FBSDKShareKit
import Social
import FirebaseAnalytics
import AppsFlyerLib
import Mixpanel
class QuizThankyouViewController: UIViewController {

    @IBOutlet weak var headerView: UIView!
   
    @IBOutlet weak var profileImageButton : RoundButton!

    // Properties
    
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
            AnalyticsParameterItemID: "id-\(AppsFlyerConstant.QuizThankYou)",
            AnalyticsParameterItemName: "\(AppsFlyerConstant.QuizThankYou)",
            AnalyticsParameterContentType: "Quiz Thank You"
            ])
        AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.QuizThankYou,
                                             withValues: [
                                                AFEventParamContent: AppsFlyerConstant.QuizThankYou,
                                                AFEventParamContentId: "59"
            ])
       
        Helper.GetProfileImageForButton(butName: self.profileImageButton)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
            statusBar.backgroundColor = Colors.MAMYBlue
             UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
             UIApplication.shared.statusBarView?.backgroundColor = Colors.MAMYBlue
         }
        
    }
   
    // Mark: Actions
    @IBAction func ViewProfilePressed(){
        let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        destViewController.selectedButton = 0
        destViewController.isHereChangeStatus = false
        self.navigationController?.pushViewController(destViewController, animated: true)

    }
   
    
   
    
    @IBAction func menuBtnPressed(_ sender: Any) {
        //toggleSideMenuView()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func searchBtn(_ sender: Any) {
        let searchVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(searchVc, animated: true)
    }
    
   
    
}
