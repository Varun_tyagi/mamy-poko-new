//  BabyGrowthDataViewController.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 22/10/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.


import UIKit
import Charts
import MBProgressHUD
let KBabyGrowthJsonFileName = "baby_gowth"
let KBabyMeasurementNotification = "getBabyMeasurement"
class BabyGrowthDataViewController: UIViewController {

    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var lineChart: LineChartView!
    
    @IBOutlet weak var chartHeaderLbl: UILabel!
    @IBOutlet weak var tblGrowth: UITableView!
    
    @IBOutlet weak var addToMEasBtn: RoundButton!
    var unitGradient:[HeadCircum] = []

    var  months:[String] = []
    var flagIsMale = false
    override func viewDidLoad() {
        super.viewDidLoad()
		self.BlockSideMenuSwipe()

        SetupInitialView()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
		
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        getMesaurementData()
    }

    //MARK:- Action
    
    @IBAction func percentileLinkPressed(_ sender: Any) {
        //Open Link
        //https://www.iapindia.org/iap-growth-charts-for-height-weight-and-body/?hilite=%27growth%27
       // guard let url = URL(string: "https://www.iapindia.org/iap-growth-charts-for-height-weight-and-body/?hilite=%27growth%27") else { return }
       // UIApplication.shared.open(url)
        let  destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
        destViewController.isDisclaimerFromMenu = false
        destViewController.isComingFromRegister = false
        destViewController.pageUrl = "https://www.iapindia.org/iap-growth-charts-for-height-weight-and-body/?hilite=%27growth%27"
        destViewController.isShowMenu=false
        destViewController.isComingFromBabyGrowth = true
        self.navigationController?.pushViewController(destViewController, animated: true)
    
    }
    
    @IBAction func segmentAction(_ sender: UISegmentedControl) {
        var strBabyValue = ""
       
       unitGradient.removeAll()
        switch segment.selectedSegmentIndex {
        case 0 : // weight
            if  (BabyGrowthModel.sharedBabyGrowthModel?.data?.weightUnit?.lowercased().contains("kg"))! {
                strBabyValue =  flagIsMale ? BabyGrowthType.BoyWeightKg.rawValue : BabyGrowthType.GirlWeightKg.rawValue
            }
            else{
                strBabyValue =  flagIsMale ? BabyGrowthType.BoyWeightLbs.rawValue : BabyGrowthType.GirlWeightLbs.rawValue
            }
           unitGradient = (BabyGrowthModel.sharedBabyGrowthModel?.data?.weights)!
 chartHeaderLbl.text = "Weight(\(BabyGrowthModel.sharedBabyGrowthModel?.data?.weightUnit ?? ""))"
        case 1 : // height
            if  (BabyGrowthModel.sharedBabyGrowthModel?.data?.heightUnit?.lowercased().contains("cm"))! {
                strBabyValue =  flagIsMale ? BabyGrowthType.BoyHeightCm.rawValue : BabyGrowthType.GirlHeightCm.rawValue
            }
            else{
                strBabyValue =  flagIsMale ? BabyGrowthType.BoyHeightInch.rawValue : BabyGrowthType.GirlHeightInch.rawValue
            }
            unitGradient = (BabyGrowthModel.sharedBabyGrowthModel?.data?.heights)!
            chartHeaderLbl.text = "Height(\(BabyGrowthModel.sharedBabyGrowthModel?.data?.heightUnit ?? ""))"

        case 2 : // head
            if  (BabyGrowthModel.sharedBabyGrowthModel?.data?.headCircumUnit?.lowercased().contains("cm"))! {
                strBabyValue =  flagIsMale ? BabyGrowthType.BoyHeadCircumferenceCm.rawValue : BabyGrowthType.GirlHeadCircumferenceCm.rawValue
            }
            else{
                strBabyValue =  flagIsMale ? BabyGrowthType.BoyHeadCircumferenceInch.rawValue : BabyGrowthType.GirlHeadCircumferenceInch.rawValue
            }
            unitGradient = (BabyGrowthModel.sharedBabyGrowthModel?.data?.headCircums)!
            chartHeaderLbl.text = "Head(\(BabyGrowthModel.sharedBabyGrowthModel?.data?.headCircumUnit ?? ""))"

        default :
            break
        }
        
        let BabyGrowthDataArray = KBabyGrowthJsonFileName.readBabyGrowthJson(strBabyValue)
        self.setChart(dataPoints: months, values: BabyGrowthDataArray)

    }
    @IBAction func addMeasurementAction(_ sender: UIButton) {
       self.addChildVC(Constants.mainStoryboard.instantiateViewController(withIdentifier: "AddMeasurementViewController") as! AddMeasurementViewController)
    }
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func searchAction(_ sender: Any) {
        let searchVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(searchVc, animated: true)
    }
    @objc func editMeasurementAction(_ sender : UIButton){
        let measurVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "AddMeasurementViewController") as! AddMeasurementViewController
        measurVc.measurData = sender.accessibilityElements?.first as? MeasurementDatum
        self.addChildVC(measurVc)

    }
    @objc func deleteMeasurementAction(_ sender : UIButton){
        let measurement = sender.accessibilityElements?.first as! MeasurementDatum
        self.deleteMesaurementData(measurement.babyID ?? "")
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension BabyGrowthDataViewController{
    
    private func SetupInitialView(){
        // number of month baby is
        for  i  in 0...216 {
                months.append("\(i)")
        }
        NotificationCenter.default.addObserver(self, selector: #selector(getMesaurementData), name: NSNotification.Name.init(rawValue: KBabyMeasurementNotification), object: nil)
        
    }
    private func setupData(){
        self.flagIsMale =  BabyGrowthModel.sharedBabyGrowthModel?.data?.gender?.lowercased()=="male" ? true :  false
       
        var strBabyValue = ""
        if  (BabyGrowthModel.sharedBabyGrowthModel?.data?.weightUnit?.lowercased().contains("kg"))! {
          strBabyValue =  flagIsMale ? BabyGrowthType.BoyWeightKg.rawValue : BabyGrowthType.GirlWeightKg.rawValue
        }
       else{
            strBabyValue =  flagIsMale ? BabyGrowthType.BoyWeightLbs.rawValue : BabyGrowthType.GirlWeightLbs.rawValue
        }
        let BabyGrowthDataArray = KBabyGrowthJsonFileName.readBabyGrowthJson(strBabyValue)
        
        unitGradient = (BabyGrowthModel.sharedBabyGrowthModel?.data?.weights)!
        chartHeaderLbl.text = "Weight(\(BabyGrowthModel.sharedBabyGrowthModel?.data?.weightUnit ?? ""))"
        segment.selectedSegmentIndex=0
        setChart(dataPoints: months, values: BabyGrowthDataArray)
        tblGrowth.reloadData()
    }
    
   fileprivate func setChart(dataPoints: [String], values: [[Double]]) {
        var dataEntries: [ChartDataEntry] = []
    
        let chartData = LineChartData()

        var  linenumber = 1
        for unitArray in values {
            dataEntries.removeAll()
            var dataEntry = ChartDataEntry()
            for i in 0..<dataPoints.count{
                if i<unitArray.count {
                dataEntry = ChartDataEntry(x: Double(i), y: unitArray[i], data: dataPoints[i] as AnyObject)
                    dataEntries.append(dataEntry)
                    
                }
            }
            
            var  lineColor = UIColor.red
            var  lineText = "5th"
            switch linenumber {
            case 0 :    lineText = "5th";       lineColor = #colorLiteral(red: 0.8666666667, green: 0.1294117647, blue: 0.8549019608, alpha: 1)
            case 1 :    lineText =  "25th";     lineColor =  UIColor.black
            case 2 :    lineText =  "50th";     lineColor = UIColor.blue
            case 3 :    lineText =  "85th";     lineColor = UIColor.green
            case 4 :    lineText =  "95th";     lineColor =  UIColor.yellow
            case 5 :    lineText =  "99th";     lineColor =  UIColor.darkGray
            default :   lineText =  "5th" ;     lineColor =  #colorLiteral(red: 0.8666666667, green: 0.1294117647, blue: 0.8549019608, alpha: 1)
            }
//            lineText = lineText //+" percentile"
			let chartDataSet = LineChartDataSet(entries: dataEntries, label: lineText)
            chartDataSet.circleRadius = 0
            chartDataSet.circleHoleRadius = 0
            chartDataSet.drawValuesEnabled = false
            chartDataSet.lineDashPhase = 5
            chartDataSet.lineDashLengths = [25,25]
            chartDataSet.lineWidth = 1
            chartDataSet.drawFilledEnabled=false
            chartDataSet.colors =  [lineColor]
            
            chartData.dataSets.append(chartDataSet)
            linenumber = linenumber+1
        }
        
        // for gradient line of baby actual growth
        dataEntries.removeAll()
    
        for cord in unitGradient{
            let dataEntry = ChartDataEntry.init(x: Double(cord.x ?? "0")! + 0.0, y: Double(cord.y ?? "0")! + 0.0)
            dataEntries.append(dataEntry)
        }
    
	let chartDataSet = LineChartDataSet(entries: dataEntries, label: "Baby")
        chartDataSet.circleRadius = 4
        chartDataSet.circleHoleRadius = 0
        chartDataSet.circleHoleColor = UIColor.black
        chartDataSet.setCircleColor(UIColor.black)
        chartDataSet.drawValuesEnabled = true
        chartDataSet.lineDashPhase = 3
        chartDataSet.lineDashLengths = [5,5]
        chartDataSet.lineWidth = 1
        chartDataSet.colors = [UIColor.red]
        chartDataSet.fill = Fill.init(color: UIColor.red)
        chartDataSet.drawFilledEnabled=true
        chartData.dataSets.append(chartDataSet)
        
        lineChart.data = chartData
        
        //chart customizations can be used as extension also to make code cleaner
        lineChart.borderLineWidth = 5
        lineChart.chartDescription?.text = "Age(Month.)"
        lineChart.chartDescription?.font = UIFont.systemFont(ofSize: 10.0)
    
    
        lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: months)
        lineChart.xAxis.labelPosition = .bottom
        lineChart.xAxis.drawGridLinesEnabled = true
        lineChart.xAxis.avoidFirstLastClippingEnabled = true
        
        lineChart.rightAxis.drawAxisLineEnabled = true
        lineChart.rightAxis.drawLabelsEnabled = false
        
        lineChart.leftAxis.drawAxisLineEnabled = false
        lineChart.leftAxis.drawBottomYLabelEntryEnabled=true
        lineChart.leftAxis.drawZeroLineEnabled=false
        
        lineChart.pinchZoomEnabled = true
        lineChart.doubleTapToZoomEnabled = true
        lineChart.legend.enabled = true
        lineChart.legend.form = .line
        lineChart.highlightPerTapEnabled=false
        lineChart.highlightPerDragEnabled=false
        lineChart.animate(xAxisDuration: 0.7)
        
    }
}

//MARK: - Webservice
extension BabyGrowthDataViewController{
   @objc private  func getMesaurementData(){
        if Helper.isConnectedToNetwork() {
            Constants.appDel.startLoader()
            let userID = UserDefaults.standard.string(forKey: Constants.USERID)
            if userID == nil || userID?.count == 0 {
                PKSAlertController.alert(Constants.appName, message: "Invalid User ID")
            }
            else{
                Server.postRequestWithURL( Constants.BASEURL + MethodName.BabyGrowthMesaurements, paramString: "user_id=\(userID!)&device=ios") { (response) in
                    Constants.appDel.stopLoader()
                        if (response.count) > 1 && response["status"] as? Int == 1 {
                            let growthData = response["file"] as! [String:Any]
                            let file = "\(KBabyGrowthJsonFileName).json"
                            let babyModel = try? BabyGrowthModel.init((response.toJsonString())!)
                            BabyGrowthModel.sharedBabyGrowthModel = babyModel
                            if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                                let fileURL = dir.appendingPathComponent(file)
                                //writing
    do {   try growthData.toJsonString()?.write(to: fileURL, atomically: false, encoding: .utf8)  }
        catch {
//			print("error in writing growth json file") /* error handling here */
								}
                            }
                            DispatchQueue.main.async {
                                self.setupData()
                            }
                        }else{
                            PKSAlertController.alert("Error", message: response["message"] as? String ?? "Some error has occured! Please try again")
                        }
                }
            }
        }else{
           PKSAlertController.alertForNetwok()
        }
    }
    fileprivate func deleteMesaurementData(_ babyID: String){
       if Helper.isConnectedToNetwork() {
            Constants.appDel.startLoader()
            let userID = UserDefaults.standard.string(forKey: Constants.USERID)
            if userID == nil || userID?.count == 0 {
                PKSAlertController.alert(Constants.appName, message: "Invalid User ID")
            }
            else{
                let url_str = Constants.BASEURL + MethodName.DeleteMeasurement
                Server.postRequestWithURL(url_str, paramString: "baby_id=\(babyID.toBase64())") { (response) in
                    Constants.appDel.stopLoader()
                    if (response.count) > 1 && response["status"] as? Int == 1 {
                       self.getMesaurementData()
                    }else{
                        PKSAlertController.alert("Error", message: response["message"] as? String ?? "Some error has occured! Please try again")
                    }
                }
            }
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
}
extension BabyGrowthDataViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BabyGrowthDataTableViewCell") as! BabyGrowthDataTableViewCell
        
        cell.bindData(indexPath)
        if indexPath.row > 0 {
            let measurDAgta = BabyGrowthModel.sharedBabyGrowthModel?.measurementData![indexPath.row-1]
            cell.editBtn.accessibilityElements=[measurDAgta!]
            cell.deleteBtn.accessibilityElements=[measurDAgta!]
            cell.editBtn.addTarget(self, action: #selector(editMeasurementAction(_:)), for: .touchUpInside)
            cell.deleteBtn.addTarget(self, action: #selector(deleteMeasurementAction(_:)), for: .touchUpInside)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if BabyGrowthModel.sharedBabyGrowthModel != nil &&   (BabyGrowthModel.sharedBabyGrowthModel?.measurementData?.count)! > 0 {
            return (BabyGrowthModel.sharedBabyGrowthModel?.measurementData?.count)! + 1
        }
        return 0
    }
    
}








