//  AddMeasurementViewController.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 01/10/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.

import UIKit
import IQKeyboardManagerSwift
import FirebaseAnalytics
import AppsFlyerLib

class AddMeasurementViewController: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var weightTxt: RoundTextField!
    @IBOutlet weak var headUnitTxt: RoundTextField!
    @IBOutlet weak var headTxt: RoundTextField!
    @IBOutlet weak var heightUnitTxt: RoundTextField!
    @IBOutlet weak var heightTxt: RoundTextField!
    @IBOutlet weak var weightUnitTxt: RoundTextField!
    @IBOutlet weak var dateOfMeasurementTxt: RoundTextField!
    
    var datePicker : UIDatePicker!
    var toolBar : UIToolbar!
    var measurData:MeasurementDatum?
    //MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.previousNextDisplayMode = IQPreviousNextDisplayMode.alwaysHide
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
          IQKeyboardManager.shared.enableAutoToolbar = false
		IQKeyboardManager.shared.previousNextDisplayMode = IQPreviousNextDisplayMode.default
    }
    //MARK:- Action
    @IBAction func closeAction(_ sender: UIButton) {
    self.removeChildVC()
    }
    @IBAction func submitAction(_ sender: Any) {
        if validate(){
            self.addEditMesaurementData()
        }
        
    }
}

//MARK: - Custom Methods
extension AddMeasurementViewController {
    
    func setupView()  {
        
        for textField in  [weightTxt,headUnitTxt,headTxt,dateOfMeasurementTxt,heightTxt,heightUnitTxt,weightUnitTxt]{
            Helper.setImageOntextField(textField!, imagename: "", mode: true, frame: CGRect(x:0, y:0, width:15,height: (textField!.frame.height)))
        }
        setupDatePicker()
        
        if measurData != nil {
            headTxt.text = measurData?.headCircum ?? ""
            headUnitTxt.text = measurData?.headCircumUnit ?? ""
            weightTxt.text = measurData?.weight ?? ""
            weightUnitTxt.text = measurData?.weightUnit ?? ""
            heightTxt.text = measurData?.height ?? ""
            heightUnitTxt.text = measurData?.heightUnit ?? ""
            dateOfMeasurementTxt.text = measurData?.measureDate ?? ""
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMM, yyyy"
            let selectedDate = dateFormatter.date(from: (measurData?.measureDate!)!)
            
            let newDareFor = DateFormatter()
            newDareFor.dateFormat = "dd-MM-yyyy"
            dateOfMeasurementTxt.text = newDareFor.string(from: selectedDate!)
        }
        
    }
    func setupDatePicker()  {
        
        datePicker = UIDatePicker()
        datePicker.tag = 100
        datePicker.datePickerMode = UIDatePickerMode.date
        datePicker.addTarget(self, action: #selector(self.setDate(_sender:)), for: .valueChanged)
        datePicker.timeZone = TimeZone.current
        toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(dismissPicker))
        
        self.dateOfMeasurementTxt.delegate = self
        self.dateOfMeasurementTxt?.inputView = datePicker
        self.dateOfMeasurementTxt?.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Date Picker
    @objc func setDate(_sender : UIDatePicker){
        
        if _sender.tag == 100 {
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let selectedDate: String = dateFormatter.string(from: _sender.date)
            
            // SET MiNIMUM & MAximum DAte
          //  let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
         //   let currentDate: NSDate = NSDate()
            let components2: NSDateComponents = NSDateComponents()
            
            let dateFormattervalid = DateFormatter()
            dateFormattervalid.dateFormat = "MMM dd yyyy"
            let minDate = dateFormattervalid.date(from: BabyGrowthModel.sharedBabyGrowthModel?.data?.dob ?? Date().toDateString(dateFormate: "dd-MM-yyyy")) ?? Date()
            datePicker.minimumDate = minDate
            
            //18 years from dob
            // 2years from dob
            components2.month = 24-Int(BabyGrowthModel.sharedBabyGrowthModel?.data?.month ?? "9")!
            var maxDate = Date()
            if  minDate.dateByAddingYears(2) > Date(){
                maxDate = Date()
            }else{
                 maxDate = minDate.dateByAddingYears(2)
            }
            datePicker.maximumDate = maxDate
            
            if _sender.date > minDate && _sender.date < maxDate {
                self.dateOfMeasurementTxt?.text = "\(selectedDate)"
            }
            else{
                self.dateOfMeasurementTxt?.text = Date().toDateString(dateFormate: "dd-MM-yyyy")
                datePicker.date = Date()
            }
        }
    }
    
    @objc func dismissPicker() {
        view.endEditing(true)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    private func validate()-> Bool{
        if (dateOfMeasurementTxt.text?.isEmpty)!{
            PKSAlertController.alert(Constants.appName, message: "Enter Measurement Date.")
            return false
        }else  if (weightTxt.text?.isEmpty)!{
            PKSAlertController.alert(Constants.appName, message: "Enter weight.")
            return false
        }else  if (weightUnitTxt.text?.isEmpty)!{
            PKSAlertController.alert(Constants.appName, message: "Select weight unit.")
            return false
        }else  if (heightTxt.text?.isEmpty)!{
            PKSAlertController.alert(Constants.appName, message: "Enter height.")
            return false
        }else  if (heightUnitTxt.text?.isEmpty)!{
            PKSAlertController.alert(Constants.appName, message: "Select height unit.")
            return false
        }else  if (headTxt.text?.isEmpty)!{
            PKSAlertController.alert(Constants.appName, message: "Enter head circumference.")
            return false
        }else  if (headUnitTxt.text?.isEmpty)!{
            PKSAlertController.alert(Constants.appName, message: "Select head circumference unit.")
            return false
        }
        
        return true
    }
    //MARK:- Webservice
    fileprivate func addEditMesaurementData(){
        if Helper.isConnectedToNetwork() {
            Constants.appDel.startLoader()
            let userID = UserDefaults.standard.string(forKey: Constants.USERID) ?? ""
            
            if userID.isEmpty ||  userID.count == 0 {
                PKSAlertController.alert(Constants.appName, message: "Invalid User ID")
            }
            else{
                let url_str = Constants.BASEURL + MethodName.AddEditMeasurement
                var paramStr = "user_id=\(userID.toBase64())&height_unit=\(heightUnitTxt.text ?? "cm.")&measure_date=\(dateOfMeasurementTxt.text ?? "")&weight=\(weightTxt.text ?? "")&weight_unit=\(weightUnitTxt.text ?? "kg.")&height=\(heightTxt.text ?? "")&head_circum=\(headTxt.text ?? "")&head_circum_unit=\(headUnitTxt.text ?? "cm.")"
                if self.measurData != nil {
                    paramStr = paramStr+"&baby_id=\(self.measurData?.babyID?.toBase64() ?? "")"
                }
                
                Server.postRequestWithURL(url_str, paramString: paramStr) { (response) in
                    Constants.appDel.stopLoader()
                    if (response.count) > 1 && response["status"] as? Int == 1 {
                        DispatchQueue.main.async {
                            
                            Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
                                AnalyticsParameterItemID: "id-\(AppsFlyerConstant.AddMesurementClicked)",
                                AnalyticsParameterItemName: AppsFlyerConstant.AddMesurementClicked,
                                AnalyticsParameterContentType: "Updates"
                                ])
                            AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.AddMesurementClicked,
                                                                 withValues: [
                                                                    AFEventParamContent: AppsFlyerConstant.AddMesurementClicked,
                                                                    AFEventParamContentId: "38"
                                ])

                            
                            NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: KBabyMeasurementNotification), object: nil)
                            self.closeAction(UIButton())
                            
                        }
                    }else{
                        PKSAlertController.alert("Error", message: response["message"] as? String ?? "Some error has occured! Please try again")
                    }
                }
            }
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
    
}
extension AddMeasurementViewController : UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {

        if textField == weightUnitTxt || textField == headUnitTxt || textField == heightUnitTxt  {
            self.view.endEditing(true)
        let  pickerArray = textField == weightUnitTxt ?  ["lb.", "kg."] : ["cm.", "in."]
        CustomPicker.show(data: [pickerArray]) {   (selections: [Int : String]) -> Void in
            if let name = selections[0] {
            textField.text = name
            }
        }
                return false
        }
            return true
        }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == heightTxt || textField == weightTxt || textField == headTxt{
            if (textField.text?.count)! > 4{
                return false
            }
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}







