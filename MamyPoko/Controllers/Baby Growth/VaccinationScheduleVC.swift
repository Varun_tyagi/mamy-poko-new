//
//  VaccinationScheduleVC.swift
//  MamyPoko
//
//  Created by Varun Tyagi on 15/10/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import Foundation
import Foundation
import UIKit
import FirebaseAnalytics
import SDWebImage
import IQKeyboardManagerSwift

class VaccinationScheduleVC : UIViewController{
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var imageCaptionTxt: UITextField!
    @IBOutlet weak var babyPopupImagView: UIImageView!
    @IBOutlet weak var topUploadImgPopup: NSLayoutConstraint!
    @IBOutlet weak var uploadImgPopup: UIView!
    @IBOutlet weak var bottomVaccTbl: NSLayoutConstraint!
    @IBOutlet weak var heightPendingVaccPopup: NSLayoutConstraint!
    @IBOutlet weak var pendingVaccLbl: UILabel!
    @IBOutlet weak var babyImgCollection: UICollectionView!
    @IBOutlet weak var heightUploadBabyImgView: NSLayoutConstraint!  // 60 /180
    @IBOutlet weak var footerView: UIView!  // 320 // 220
    @IBOutlet weak var vaccinationTbl: UITableView!
    
    @IBOutlet weak var pendingVaccPopupView: RoundView!
    @IBOutlet weak var babyImage: UIImageView!
    @IBOutlet weak var babyName: UILabel!
    @IBOutlet weak var babyDOB: UILabel!
    @IBOutlet weak var nextSchedule: UILabel!
    @IBOutlet weak var note: UILabel!
    
    //Vaccine dose popup
    @IBOutlet weak var  protectionDosePopup: UIView!
    
    @IBOutlet weak var protectionPopupTitleLbl: UILabel!
    
    @IBOutlet weak var protectionPopupProtLbl: UILabel!
    @IBOutlet weak var protectionPopupDoseLbl: UILabel!
    

    let  mUserPic="\(Constants.BASEURL)blog/wp-content/themes/mamypokopants/images/user-img.jpg"

    var vaccineData : VaccineScheduleModel?
    var scheduleArray : [[String: [ScheduleArray]?]?] = [ ]
    var zero_ThreeArr :  [String: [ScheduleArray]?]   = [:]
    var four_SixArr :    [String: [ScheduleArray]?]   = [:]
    var seven_NineArr :  [String: [ScheduleArray]?]   = [:]
    var Ten_TwelveArr :  [String: [ScheduleArray]?]   = [:]
    var isFirstAppear=true
    var hidden:[Bool] = [true,true,true,true,true]
    var vaccinationSectionArray = ["","0 to 3 months","4 to 6 months","7 to 9 months","10 to 12 months"]
    var selectedSection = ""
    fileprivate var imagePicker = ImagePicker()
   
    var vacceinDetailTable : UITableView? = nil
    
     //MARK:- View Methods
    override func viewDidLoad() {
        super .viewDidLoad()
		self.BlockSideMenuSwipe()

        self.setupInitialView()
    }
 
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(true)
        IQKeyboardManager.shared.enableAutoToolbar=true
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        IQKeyboardManager.shared.enableAutoToolbar=false
    }
   
    //MARK: - Action
    @IBAction func backACtion(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func searchAction(_ sender: UIButton) {
        let searchVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(searchVc, animated: true)
    }
    @IBAction func nextbabyScrollAction(_ sender: Any) {
        if babyImgCollection.visibleCells.count==1{
            babyImgCollection.setContentOffset(CGPoint.zero , animated: true)
            return
        }
        babyImgCollection.setContentOffset( CGPoint(x: babyImgCollection.contentOffset.x+80, y: babyImgCollection.contentOffset.y) , animated: true)
    }
    @IBAction func previousbabyScrollAction(_ sender: Any) {
        if babyImgCollection.visibleCells.count==1{
            babyImgCollection.setContentOffset(CGPoint.zero , animated: true)
            return
        }
        babyImgCollection.setContentOffset( CGPoint(x: babyImgCollection.contentOffset.x-80, y: babyImgCollection.contentOffset.y) , animated: true)
    }
    
    @IBAction func closePendVaccPopup(_ sender: Any) {
        heightPendingVaccPopup.constant=10
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func canceLAction(_ sender: Any) {
    topUploadImgPopup.constant=self.view.frame.size.height
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func uploadPhotoServiceAction(_ sender: Any) {
        if (imageCaptionTxt.text?.isEmpty)!{
            PKSAlertController.alert(Constants.appName, message: "Enter Image Caption!")
        }else{
            self.view.endEditing(true)
            UploadBabyImageService()
        }
    }
    
    @IBAction func uploadPhotoAction(_ sender: UIButton) {
        self.imageCaptionTxt.text = ""
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.imagePicker.galleryAsscessRequest()
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender
            alert.popoverPresentationController?.sourceRect = sender.bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func ProtectionDosePopupCloseAction(_ sender: UIButton) {
        UIView.transition(with: self.protectionDosePopup, duration: 0.4, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {
            self.protectionDosePopup.isHidden=true
        }, completion: nil)
    }
    
}
//MARK:- Custom Msthods
extension VaccinationScheduleVC {
    func setupInitialView(){
       babyImage.sd_setImage(with: URL.init(string: mUserPic))
        self.heightPendingVaccPopup.constant=10
        topUploadImgPopup.constant=self.view.frame.size.height
        imagePicker.delegate = self
         self.getVaccinationDetail()
    }
    
    func setupViewData()  {
    self.babyName.text =  self.vaccineData?.babyName ?? ""
    self.babyDOB.text = "Date of Birth \(self.vaccineData?.babyDob ?? "")"
        self.nextSchedule.text = "Due: \(self.vaccineData?.reminderVacc?.remDateTxt ?? "") - \(self.vaccineData?.reminderVacc?.remVacc ?? "")"
        self.note.text = "The immunization schedule is recommended for \(self.vaccineData?.babyName ?? ""), \(self.vaccineData?.babyDob ?? "")."
    }
    
    func getVaccDetailHeaderName(_ section:Int)->(String,String){
        var vaccenData = ""
        if  selectedSection ==  "0 to 3 months" {
            vaccenData = Array(zero_ThreeArr.keys)[section]
        }
        else if selectedSection == "4 to 6 months" {
            vaccenData = Array(four_SixArr.keys.sorted())[section]
        }
        else if selectedSection == "7 to 9 months" {
            vaccenData = Array(seven_NineArr.keys)[section]
        }
        else if selectedSection == "10 to 12 months" {
            switch section {
            case 0:
                vaccenData = "12-to-15-months"
            case 1:
                vaccenData = "15-to-18-months"
            case 2:
                vaccenData = "4-to-6-years"
            case 3:
                vaccenData = "11-to-12-years"
            case 4:
                vaccenData = "16-to-18-years"
            default:
                vaccenData = ""
            }
        }
        else {
            vaccenData = ""
        }
        let vaccDataWithDash=vaccenData
        if vaccenData.count>0{
            vaccenData = vaccenData.replacingOccurrences(of: "-", with: " ")
            vaccenData = vaccenData.replacingOccurrences(of: "at", with: "At")
            vaccenData = vaccenData.replacingOccurrences(of: "birth", with: "Birth")
            vaccenData = vaccenData.replacingOccurrences(of: "to", with: "To")
        }
        return (vaccenData,vaccDataWithDash)
    }
    //MARK: - Webservice
    func getVaccinationDetail(){
        if Helper.isConnectedToNetwork() {
            let urlstr = Constants.BASEURL + MethodName.VaccineSchedule
            guard   let userID = UserDefaults.standard.string(forKey: Constants.USERID) else{
                PKSAlertController.alert(Constants.appName, message: "Invalid User ID")
                return
            }
            Constants.appDel.startLoader()
            Server.postRequestWithURL(urlstr, paramString: "user_id=\(userID)") { (response) in
                DispatchQueue.main.async {
                    let responseDict = response as Dictionary<String, Any>
                    Constants.appDel.stopLoader()
                    if responseDict["status"] as? Int == 1 || responseDict["status"] as? String == "1"{
                        DispatchQueue.main.async {
                            self.vaccineData = try? VaccineScheduleModel.init(response.json)

                            if let scaduleDataArray = self.vaccineData?.scheduleArray{
                               // 0 to 3 month
                                if scaduleDataArray.keys.contains("at-birth"){
                                    self.zero_ThreeArr["at-birth"] = scaduleDataArray["at-birth"]!
                                }
                                if scaduleDataArray.keys.contains("1-to-2-months"){
                                    self.zero_ThreeArr["1-to-2-months"]=scaduleDataArray["1-to-2-months"]
                                }
                                if scaduleDataArray.keys.contains("2-months"){
                                    self.zero_ThreeArr["2-months"]=scaduleDataArray["2-months"]
                                }

                               //4 to 6 months
                                if scaduleDataArray.keys.contains("4-months"){
                                    self.four_SixArr["4-months"]=scaduleDataArray["4-months"]
                                }
                                if scaduleDataArray.keys.contains("6-months"){
                                    self.four_SixArr["6-months"]=scaduleDataArray["6-months"]
                                }

                                //7 to 9 months
                                if scaduleDataArray.keys.contains("6-to-18-months"){
                                    self.seven_NineArr["6-to-18-months"]=scaduleDataArray["6-to-18-months"]
                                }
                                if scaduleDataArray.keys.contains("6-months-or-older"){
                                    self.seven_NineArr["6-months-or-older"]=scaduleDataArray["6-months-or-older"]
                                }
                               
                                //10 to 12 months
                                if scaduleDataArray.keys.contains("12-to-15-months"){
                         self.Ten_TwelveArr["12-to-15-months"]=scaduleDataArray["12-to-15-months"]
                                }
                                if scaduleDataArray.keys.contains("15-to-18-months"){
                                    self.Ten_TwelveArr["15-to-18-months"]=scaduleDataArray["15-to-18-months"]
                                }
                                if scaduleDataArray.keys.contains("4-to-6-years"){
                                    self.Ten_TwelveArr["4-to-6-years"]=scaduleDataArray["4-to-6-years"]
                                }
                                if scaduleDataArray.keys.contains("11-to-12-years"){
                                    self.Ten_TwelveArr["11-to-12-years"]=scaduleDataArray["11-to-12-years"]
                                }
                                if scaduleDataArray.keys.contains("16-to-18-years"){
                                    var newArr:[ScheduleArray] = []
                                    if scaduleDataArray.keys.contains("16-years") && (scaduleDataArray["16-years"]?.count)! > 0{
                                        for  schedul  in scaduleDataArray["16-years"]!{
                                            newArr.append(schedul)
                                        }
                                    }
                                    for  schedul  in scaduleDataArray["16-to-18-years"]!{
                                        newArr.append(schedul)
                                    }
                                    self.Ten_TwelveArr["16-to-18-years"]=newArr
                                }

                            }
    self.scheduleArray = [nil, self.zero_ThreeArr,self.four_SixArr,self.seven_NineArr,self.Ten_TwelveArr]
                            self.setupViewData()
                            self.vaccinationTbl.reloadData()
                            
                            if self.vaccineData?.imageChart != nil && (self.vaccineData?.imageChart?.count)!>0{
                              self.heightUploadBabyImgView.constant=180
                            }else{
                               self.heightUploadBabyImgView.constant=60
                            }
                            
                    if self.isFirstAppear{
                        self.isFirstAppear=false
if self.vaccineData?.pendingVacc != nil && self.vaccineData?.pendingVacc?.pendingVacc != nil {
    self.pendingVaccLbl.text = "Are you done with your \(self.vaccineData?.pendingVacc?.pendingVacc ?? "") which is due on \(self.vaccineData?.pendingVacc?.pendingDateTxt ?? "")"
                             self.heightPendingVaccPopup.constant=70
                            }else{
                             self.heightPendingVaccPopup.constant=10
                            }
                            }
                           
                    UIView.animate(withDuration: 0.5, animations: {
                                self.view.layoutIfNeeded()
                            })
                            self.babyImgCollection.reloadData()
                        }
                    }
                    else if responseDict["status"] as? Int == 0 || responseDict["status"] as? String == "0" {
            PKSAlertController.alert("Error", message: response["message"] as? String ?? KSomeErrorTryAgain)
                    }
                    else{
    PKSAlertController.alert("Error", message: response["message"] as? String ?? KSomeErrorTryAgain)
                    }
                }
            }
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
    fileprivate func UploadBabyImageService() {
        if Helper.isConnectedToNetwork() {
            let urlstr = Constants.BASEURL + MethodName.UploadVaccBabyImage
            Constants.appDel.startLoader()
            let userId = "\(UserDefaults.standard.value(forKey: Constants.USERID) ?? "0")"
            let base64Str = "\(self.babyPopupImagView.image?.base64(format: ImageFormat.png) ?? "")"
            let parameters = [
                "user_id": userId.toBase64(),
                "image":base64Str,
                "device":"app",
                "title":"\(imageCaptionTxt.text ?? "")"
            ]
            Server.postImageUpload(urlstr, self.babyPopupImagView.image! ,parameters) { (responseDict) in
                Constants.appDel.stopLoader()
                if responseDict["status"] as? Int == 1  || responseDict["status"] as? String == "1" {
                    DispatchQueue.main.async {
                        PKSAlertController.alert(Constants.appName, message: responseDict["message"] as? String ?? "Image upload successfully", buttons: ["Ok"], tapBlock: { (alert, index) in
                            self.getVaccinationDetail()
                        })
                    }
                }
                else{
                    PKSAlertController.alert("Error", message: (responseDict["message"] as? String) ?? "Unable to upload image")
                }
            }
        }else{
            PKSAlertController.alertForNetwok()
        }
        topUploadImgPopup.constant=self.view.frame.size.height
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    fileprivate func DeleteBabyImageService(_ imageId:String){
        if Helper.isConnectedToNetwork() {
            let urlstr = Constants.BASEURL + MethodName.RemoveVaccBabyImage
           let userID = UserDefaults.standard.string(forKey: Constants.USERID) ?? "0"
            let parameters = "user_id=\(userID.toBase64())&image_id=\(imageId.toBase64())"
            self.PostService(urlstr: urlstr, parameters: parameters)
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
    fileprivate func UpdateVaccDateService(_ vaccData:ScheduleArray, _ vaccDAte: String , _ aliasName :String){
        if Helper.isConnectedToNetwork() {
            let urlstr = Constants.BASEURL + MethodName.UpdateVaccineDate
            let userID = UserDefaults.standard.string(forKey: Constants.USERID) ?? "0"
            Constants.appDel.startLoader()
            let parameters = "user_id=\(userID.toBase64())&vacc_date=\(vaccDAte)&up_dose=\(vaccData.dose ?? "")&up_vacc=\(vaccData.vaccines ?? "")&up_age=\(aliasName)"
            self.PostService(urlstr: urlstr, parameters: parameters)
        }else{
            PKSAlertController.alertForNetwok()
        }
    }

    fileprivate func VaccDoneService(_ vaccData:ScheduleArray, _ status:String,_ aliasName:String){
        if Helper.isConnectedToNetwork() {
            let urlstr = Constants.BASEURL + MethodName.SubmitVaccine
            let userID = UserDefaults.standard.string(forKey: Constants.USERID) ?? "0"
            let parameters = "user_id=\(userID.toBase64())&dose=\(vaccData.dose ?? "")&vacc=\(vaccData.vaccines ?? "")&vacc_age=\(aliasName)&status=\(status)"
           self.PostService(urlstr: urlstr, parameters: parameters)
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
    
    //Common post service method
    fileprivate func PostService(urlstr:String,parameters:String ){
        Constants.appDel.startLoader()
        Server.postRequestWithURL(urlstr, paramString: parameters) { (response) in
            DispatchQueue.main.async {
                let responseDict = response as Dictionary<String, Any>
                Constants.appDel.stopLoader()
                if responseDict["status"] as? Int == 1 || responseDict["status"] as? String == "1"{
                    DispatchQueue.main.async {
                        self.getVaccinationDetail()
                    }
                }
                else if responseDict["status"] as? Int == 0 || responseDict["status"] as? String == "0" {
                    PKSAlertController.alert("Error", message: response["message"] as? String ?? "Some error has occured! Please try again")
                }
                else{
                    PKSAlertController.alert("Error", message: response["message"] as? String ?? "Some error has occured! Please try again")
                }
            }
        }
    }
}

extension VaccinationScheduleVC : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  tableView==vacceinDetailTable ? 70 : UITableViewAutomaticDimension
    }
    
func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat { return 45 }
    
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            
            if tableView==vacceinDetailTable{
                let  sectionViewDetailView = UIView(frame: CGRect(x: 0, y: 0, width: Int(tableView.frame.size.width), height: 30))
                let  sectionViewDetail = UILabel(frame: CGRect(x: 5, y: 0, width: Int(tableView.frame.size.width-10), height: 30))
                sectionViewDetail.text = getVaccDetailHeaderName(section).0
                sectionViewDetail.backgroundColor=UIColor.lightGray
                sectionViewDetailView.backgroundColor=UIColor.lightGray
                sectionViewDetailView.addSubview(sectionViewDetail)
                return sectionViewDetailView

            }
            if section==0{
                return headerView
            }
            let  sectionView = UIView(frame: CGRect(x: 0, y: 0, width: Int(tableView.frame.size.width), height: 45))
            sectionView.tag = section
            
            let skyBlueView = RoundView(frame: CGRect(x: 0, y: 5, width: Int(tableView.frame.size.width), height: 45))
            skyBlueView.backgroundColor = hidden[section]==false ? UIColor.white : #colorLiteral(red: 0.168627451, green: 0.7019607843, blue: 0.9215686275, alpha: 1)
            skyBlueView.layer.cornerRadius = 10
            skyBlueView.layer.masksToBounds=true
            
            let sectionTitleLbl = UILabel(frame: CGRect(x: 10, y: 5, width: Int(tableView.frame.size.width-20), height: 45))
            sectionTitleLbl.textColor = hidden[section]==false ? UIColor.black : UIColor.white
            sectionTitleLbl.font = UIFont.init(name: FONTS.AdiraDisplaySSi, size: 16.0)
            sectionTitleLbl.text = vaccinationSectionArray[section]
            
            let sectionImg = UIImageView.init(frame: CGRect.init(x: tableView.frame.size.width-30, y: 15, width: 20, height: 20))
            sectionImg.image =  hidden[section]==false ? #imageLiteral(resourceName: "minus") :  #imageLiteral(resourceName: "plus")
            sectionView.addSubview(skyBlueView)
            sectionView.addSubview(sectionTitleLbl)
            sectionView.addSubview(sectionImg)
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction))
            sectionView.isUserInteractionEnabled = true
            sectionView.addGestureRecognizer(tap)
            
            if  hidden[section]==true {
                sectionView.dropShadow()
            }
            sectionView.backgroundColor = hidden[section]==false ? UIColor.groupTableViewBackground : UIColor.clear
            sectionTitleLbl.backgroundColor =  UIColor.clear
            return sectionView
        }
    
        @objc func tapFunction(sender:UITapGestureRecognizer) {
            let section = sender.view!.tag
            hidden=[true,true,true,true,true]
            if selectedSection==vaccinationSectionArray[section]{
                selectedSection=""
            }else{
               selectedSection=vaccinationSectionArray[section]
                hidden[section] = false
            }
            vaccinationTbl.beginUpdates()
            let range = NSMakeRange(0, self.vaccinationTbl.numberOfSections)
            let sections = NSIndexSet(indexesIn: range)
            self.vaccinationTbl.reloadSections(sections as IndexSet, with: UITableViewRowAnimation.automatic)
            vaccinationTbl.endUpdates()
            if vaccinationTbl.contentOffset.y < 0{
                vaccinationTbl.setContentOffset(CGPoint.zero, animated: true)
            }
        }
    
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return (tableView==vacceinDetailTable) ? 30 : (section==0 ? 0 : 54)
        }
    }

extension VaccinationScheduleVC : UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
         if tableView==vaccinationTbl{
        return 5
         }else{
            switch selectedSection{
            case "0 to 3 months" :
                return zero_ThreeArr.count
            case "4 to 6 months" :
                return four_SixArr.count
            case "7 to 9 months" :
                return seven_NineArr.count
            case "10 to 12 months" :
                return Ten_TwelveArr.count
            default :
                return 0
            }
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView==vaccinationTbl{
           return section==0 ? 0 : hidden[section] ? 0 : 1
        }else{
            var vaccenData = ""
            if  selectedSection ==  "0 to 3 months" {
                vaccenData = Array(zero_ThreeArr.keys)[section]
                return zero_ThreeArr[vaccenData]??.count ?? 0
            }
            else if selectedSection == "4 to 6 months" {
                vaccenData = Array(four_SixArr.keys.sorted())[section]
                 return four_SixArr[vaccenData]??.count ?? 0
            }
            else if selectedSection == "7 to 9 months" {
                vaccenData = Array(seven_NineArr.keys)[section]
                return seven_NineArr[vaccenData]??.count ?? 0
            }
            else if selectedSection == "10 to 12 months" {
                switch section {
                case 0:
                    return Ten_TwelveArr["12-to-15-months"]??.count ?? 0
                case 1:
                    return self.Ten_TwelveArr["15-to-18-months"]??.count ?? 0
                case 2:
                    return self.Ten_TwelveArr["4-to-6-years"]??.count ?? 0
                case 3:
                    return self.Ten_TwelveArr["11-to-12-years"]??.count ?? 0
                case 4:
                    return self.Ten_TwelveArr["16-to-18-years"]??.count ?? 0
                default:
                    return 0
                }
            }
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == vaccinationTbl {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainVaccineCell") as! MainVaccineCell
        self.vacceinDetailTable = cell.detailVaccineTable
        self.vacceinDetailTable?.delegate=self
        self.vacceinDetailTable?.dataSource=self
            if self.scheduleArray.count ==  0{  return cell }
            cell.bindData(self.scheduleArray,indexPath)
            self.vacceinDetailTable?.reloadData()
            return cell
        }else {//} if tableView == vacceinDetailTable{
            let cell = tableView.dequeueReusableCell(withIdentifier: "VaccinationDetailTableViewCell") as! VaccinationDetailTableViewCell
            var vaccCellData:ScheduleArray? = nil
           vaccCellData =  cell.bindData(indexPath: indexPath, zero_ThreeArr: zero_ThreeArr, four_SixArr: four_SixArr, seven_NineArr: seven_NineArr, Ten_TwelveArr: Ten_TwelveArr,selectedSection: selectedSection)
            cell.editBtn.accessibilityElements=[vaccCellData!,indexPath]
            cell.doneBtn.accessibilityElements=[vaccCellData!,indexPath]
            cell.vaccProtectionBtn.accessibilityElements = [vaccCellData!,indexPath]

            cell.editBtn.addTarget(self, action: #selector(editBabyVacc(_:)), for: .touchUpInside)
            cell.doneBtn.addTarget(self, action: #selector(doneBabyVacc(_:)), for: .touchUpInside)
            cell.vaccProtectionBtn.addTarget(self, action: #selector(protectionDosePopupVacc(_:)), for: .touchUpInside)
            return cell
        }
    }
    
     @objc func editBabyVacc(_ sender: UIButton){
        let VaccCellData = sender.accessibilityElements?.first as! ScheduleArray
        let indexPath = sender.accessibilityElements?.last as! IndexPath
        let aliasName = getVaccDetailHeaderName(indexPath.section).1
        let minDAte = VaccCellData.dateFrom?.toDateTime(dateFormate: "yyyy-MM-dd")
        CustomDatePicker.selectDate(title: "", hideCancel: true, minDate: minDAte , maxDate: minDAte?.dateByAddingDays(11), didSelectDate: { (selectedDate) in
                  self.UpdateVaccDateService(VaccCellData, selectedDate.toDateString(dateFormate: "dd-MM-yyyy"), aliasName)
        })
    }
     @objc func doneBabyVacc(_ sender: UIButton){
        let VaccCellData = sender.accessibilityElements?.first as! ScheduleArray
        let indexPath = sender.accessibilityElements?.last as! IndexPath
        let aliasName = getVaccDetailHeaderName(indexPath.section).1
        if (VaccCellData.dateFrom?.toDateTime(dateFormate: "yyyy-MM-dd"))! <= Date(){
            self.VaccDoneService(VaccCellData, VaccCellData.request==0 ? "1" : "0",aliasName)
        }else{
           PKSAlertController.alert(Constants.appName, message: "Please Choose valid vaccine for baby")
        }
    }
    @objc func protectionDosePopupVacc(_ sender: UIButton){
        let VaccCellData = sender.accessibilityElements?.first as! ScheduleArray
        
        UIView.transition(with: self.protectionDosePopup, duration: 0.4, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {
            self.protectionDosePopup.isHidden=false
        }, completion: nil)
        self.protectionPopupTitleLbl.text = VaccCellData.vaccines ?? "NA"
        self.protectionPopupDoseLbl.text = VaccCellData.dose ?? "-"
        self.protectionPopupProtLbl.text = VaccCellData.protection ?? "-"
        
    }
}

extension VaccinationScheduleVC : UICollectionViewDataSource,UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.vaccineData?.imageChart?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BabyImageCollectionViewCell", for: indexPath) as! BabyImageCollectionViewCell
        let imgData = self.vaccineData?.imageChart![indexPath.row]
        cell.babyImageView.sd_setImage(with: URL.init(string: imgData?.image ?? ""))
        cell.imageNameLbl.text = imgData?.title ?? ""
        cell.deleteImgBtn.accessibilityElements=[imgData!]
        cell.deleteImgBtn.addTarget(self, action: #selector(deleteBabyImage(_:)), for: .touchUpInside)
        return cell
    }
    @objc func deleteBabyImage(_ sender: UIButton){
    let imgDAta = sender.accessibilityElements?.first as! ImageChart
    self.DeleteBabyImageService(imgDAta.title ?? "")
    }
}

//MARK: - Date Picker & Camera
extension VaccinationScheduleVC{
    fileprivate func presentImagePicker(sourceType: UIImagePickerControllerSourceType) {
        imagePicker.controller.sourceType = sourceType
        DispatchQueue.main.async {
            self.present(self.imagePicker.controller, animated: true, completion: nil)
        }
    }
    func openCamera()
    {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            presentImagePicker(sourceType: .camera)
        }
        else
        {
            PKSAlertController.alert("Warning", message: "You don't have camera")
        }
    }
}
extension VaccinationScheduleVC: ImagePickerDelegate {
    func imagePickerDelegate(didSelect image: UIImage, imageName:String, delegatedForm: ImagePicker) {
        self.babyPopupImagView.image = image
        topUploadImgPopup.constant=0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        imagePicker.dismiss()
    }
    func imagePickerDelegate(didCancel delegatedForm: ImagePicker) {
        imagePicker.dismiss()
    }
    func imagePickerDelegate(canUseGallery accessIsAllowed: Bool, delegatedForm: ImagePicker) {
        if accessIsAllowed { presentImagePicker(sourceType: .photoLibrary) }
    }
    func imagePickerDelegate(canUseCamera accessIsAllowed: Bool, delegatedForm: ImagePicker) {
        if accessIsAllowed {    presentImagePicker(sourceType: .camera) }
    }
}


