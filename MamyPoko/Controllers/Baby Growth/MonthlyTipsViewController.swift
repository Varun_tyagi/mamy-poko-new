//
//  MonthlyTipsViewController.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 24/10/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit
import UIKit
import FirebaseAnalytics
import AppsFlyerLib

class MonthlyTipsViewController : UIViewController{
    
	@IBOutlet weak var babyMonthlyTipTbl: UITableView!
	var monthNumerStr = ""
	var monthHeadrStr = ""
        
    var babyMonthlyGrowthDict: [String:Any] = [:]
    var disclaimerdict: [String : Any] = [:]
    var babyMonthlyTipModel:BabyMonthlyTipModel? = nil
        //MARK:- View Methods
	override func viewDidLoad() {
		super .viewDidLoad()
		self.BlockSideMenuSwipe()

		
		self.getBabyMonthlyTips(self.monthNumerStr)
		
		Analytics.logEvent(AnalyticsEventViewItem, parameters: [
			AnalyticsParameterItemID: "id-MonthlyBabyGrowth",
			AnalyticsParameterItemName: "MonthlyTipsViewController",
			AnalyticsParameterContentType: "Visit"
			])
		AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.MonthlyTipsPage,
											 withValues: [
												AFEventParamContent: AppsFlyerConstant.MonthlyTipsPage,
												AFEventParamContentId: "14"
			])
	}
	
	override func viewWillAppear(_ animated: Bool) {
		
	}
	//MARK:- Action Methods
	@IBAction func BackButtonPressed(){
		self.navigationController?.popViewController(animated: true)
	}

	@IBAction func searchAction(_ sender: Any) {
	let searchVc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
	searchVc.isPresent = true
	present(searchVc, animated: false, completion: nil)
	}

	@objc func NextPressed(sender : UIButton){
		let num = Int(self.monthNumerStr)
		if num != 40 {
			self.monthNumerStr = "\(num! + 1)"
			self.getBabyMonthlyTips(self.monthNumerStr)
		}
	}
	
	@objc func PreviousPressed(sender : UIButton){
		let num = Int(self.monthNumerStr)
		if num != 1 {
			self.monthNumerStr = "\(num! - 1)"
			self.getBabyMonthlyTips(self.monthNumerStr)
		}
	}
	
	@objc func OpenDoctorProfile(){
		DispatchQueue.main.async {
			let searchVc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "PanelistProfileVC") as! PanelistProfileVC
			searchVc.disclaimerdict = self.disclaimerdict
			self.present(searchVc, animated: false, completion: nil)
		}
	}
    
	override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
        
}
//MARK: - Webservice
extension MonthlyTipsViewController{
    func getBabyMonthlyTips(_ month : String){
        if Helper.isConnectedToNetwork() {
            guard   let userID = UserDefaults.standard.string(forKey: Constants.USERID) else{
                PKSAlertController.alert(Constants.appName, message: "Invalid User ID")
                return
            }
            let urlstr = Constants.BASEURL + MethodName.BabyGrowthMonthlyTips+userID
            Constants.appDel.startLoader()
            Server.postRequestWithURL(urlstr, paramString: "user_id=\(userID)&month=\(month)") { (response) in
                Constants.appDel.stopLoader()
                DispatchQueue.main.async {
                    if response["status"] as? Int != 1{
                        PKSAlertController.alert("Error", message: response["message"] as? String ?? "Some error has occured! Please try again")
                    }else{
                        self.babyMonthlyTipModel = try? BabyMonthlyTipModel(response.toJsonString() ?? "{}")
                        let responseDict = response as Dictionary<String, Any>
                        if responseDict["status"] as? Int == 1  {
                            let arr = responseDict["data"] as! [Any]
                            self.babyMonthlyGrowthDict = arr[0] as? [String:Any] ?? [:]
                            if responseDict.keys.contains("disclaimer") == true{
                                self.disclaimerdict = responseDict["disclaimer"] as? [String:Any] ?? [:]
                            }
                        }
                    }
                     self.babyMonthlyTipTbl.reloadData()
                   }
            }
        }else{
            PKSAlertController.alertForNetwok()
        }
    }
}
//MARK:- TableView

extension MonthlyTipsViewController : UITableViewDataSource , UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        if babyMonthlyTipModel==nil{
            return 0
        }
        if  babyMonthlyTipModel?.disclaimer != nil {
            return 3
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "pregnancyTrackerDetailCell", for: indexPath) as! pregnancyTrackerDetailCell
            
            cell.bindCellData(babyMonthlyTipModel!)
            cell.weekLabel.text = self.monthHeadrStr
            cell.weekNumberLabel.text = "Month \(self.monthNumerStr)"
            cell.prevButton.addTarget(self, action: #selector(PreviousPressed(sender:)), for: .touchUpInside)
            cell.nextButton.addTarget(self, action: #selector(NextPressed(sender:)), for: .touchUpInside)
            
            tableView.estimatedRowHeight = 315
            tableView.rowHeight = UITableViewAutomaticDimension
            
            return cell
        }
        else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "pregnancyTrackerDetailCell_text", for: indexPath)
            let detailText = cell.contentView.viewWithTag(401) as! UILabel
            
                let contentStr = self.babyMonthlyTipModel?.data?.first?.content ?? ""
                var descriptionAlign = "<style type='text/css'>html,body {margin: 0;padding: 0;width: 100%;height: 100%; font-family: Quicksand,Adira Display SSi; font-size: 18px;}html {display: table;}body {display: table-cell;vertical-align: middle;padding: 0px;text-align: left;-webkit-text-size-adjust: none;} h2 {font-size: 22px; color: #333; margin: 0;}h2 .head-yel {color: #f2b413;}h4 {font-size: 22px;}p {margin: 0px 0 0;} </style>"
                descriptionAlign.append(contentStr)
            descriptionAlign=descriptionAlign.replacingOccurrences(of: "<h4>", with: "<br><h4>")
                detailText.attributedText = descriptionAlign.htmlToAttributedString
            
            tableView.estimatedRowHeight = 100
            tableView.rowHeight = UITableViewAutomaticDimension
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DoctorProfileCell", for: indexPath) as! DoctorProfileCell
              cell.bindCellData(babyMonthlyTipModel!)
            cell.viewProfileButton.addTarget(self, action: #selector(OpenDoctorProfile), for: .touchUpInside)

            tableView.estimatedRowHeight = 300
            tableView.rowHeight = UITableViewAutomaticDimension
            
            return cell
        }
    }
}




