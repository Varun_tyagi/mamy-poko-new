//
//  BabyGrowthConstant.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 22/10/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import Foundation
import UIKit

enum BabyGrowthType: String {
    
    case   GirlWeightKg                     = "girlWeightKg"
    case   GirlWeightLbs                    = "girlWeightLbs"
    case   GirlHeadCircumferenceCm          = "girlHeadCircumferenceCm"
    case   GirlHeadCircumferenceInch        = "girlHeadCircumferenceInch"
    case   GirlHeightCm                     = "girlHeightCm"
    case   GirlHeightInch                   = "girlHeightInch"
    case   BoyWeightKg                      = "boyWeightKg"
    case   BoyWeightLbs                     = "boyWeightLbs"
    case   BoyHeadCircumferenceCm           = "boyHeadCircumferenceCm"
    case   BoyHeadCircumferenceInch         = "boyHeadCircumferenceInch"
    case   BoyHeightCm                      = "boyHeightCm"
    case   BoyHeightInch                    = "boyHeightInch"
    
    case    Weightlbs = "weightlbs"
    case    Maxm = "maxm"
    case    Minm = "minm"
    case    Unit = "unit"
    case    LBS = "Lbs"
    case    KG = "Kg"
    case    Weightkg =   "weightkg"
    case    HeightCm =    "heightcm"
    case    HeightInch =     "heightinch"
    case    Inch = "Inch"
    case    HeadCm =  "headcm"
    case    HeadInch =   "headinch"
}

enum GraphPoint : String {
    case    Fifth = "5th"
    case    TwentyFifth = "25th"
    case    Fifty = "50th"
    case    EightyFive = "85th"
    case    NinetyFive = "95th"
    case    NinetyNine = "99th"
    
}
