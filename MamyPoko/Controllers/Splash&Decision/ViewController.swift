//
//  ViewController.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 24/04/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import FirebaseMessaging
import AppsFlyerLib

class ViewController: UIViewController {
    var navCtrl: UINavigationController? = nil
    var loggedin : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
	
        (self.navigationController?.isNavigationBarHidden = true)!
        Analytics.logEvent(AnalyticsEventAppOpen, parameters: [
            AnalyticsParameterItemID: "id-Splash",
            AnalyticsParameterItemName: "SplashVC",
            AnalyticsParameterContentType: "cont"
            ])

        AppsFlyerTracker.shared().trackEvent(AppsFlyerConstant.SplashScreen,
                                             withValues: [
                                                AFEventParamContent: AppsFlyerConstant.SplashScreen,
                                                AFEventParamContentId: "5"
            ])
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
            self.makeDecisionForFisrtScreen()
        })
			DeviceTokenManager.submitDeviceToken("Info Screen")
		
    }
	

    override func viewWillAppear(_ animated: Bool) {
if #available(iOS 13.0, *) {
	let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
	statusBar.backgroundColor = UIColor.clear
	 UIApplication.shared.keyWindow?.addSubview(statusBar)
} else {
	 UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
 }//        Constants.appDel.startLoaderSplash()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
//        Constants.appDel.stopLoaderSplash()
    }

    //MARK:- Decision Making
    func makeDecisionForFisrtScreen(){

//        let register = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "NewProductDiapersVC") as! NewProductDiapersVC
//        let rootNav = UINavigationController.init(rootViewController: register)
//        rootNav.isNavigationBarHidden = true
//        Constants.appDelegate.window?.rootViewController = rootNav
//
////        let register = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "AnwserVC") as! AnwserVC
////        let rootNav = UINavigationController.init(rootViewController: register)
////        rootNav.isNavigationBarHidden = true
////        Constants.appDelegate.window?.rootViewController = rootNav
//        return
        
//                let register = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ProductFromApiVC") as! ProductFromApiVC
//                let rootNav = UINavigationController.init(rootViewController: register)
//                rootNav.isNavigationBarHidden = true
//                Constants.appDelegate.window?.rootViewController = rootNav
//                return
//        
//
//		     let register = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "QuizViewController") as! QuizViewController
//
//		       let rootNav = UINavigationController.init(rootViewController: register)
//		        rootNav.isNavigationBarHidden = true
//		       Constants.appDelegate.window?.rootViewController = rootNav
//
//		       return
        if loggedin{
            let root = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SideMenuNavigation")
            Constants.appDelegate.window?.rootViewController = root
            UIApplication.shared.keyWindow?.rootViewController = root
        }
        else{
            
            let registerationType : Int = UserDefaults.standard.integer(forKey: Constants.RegisterStatus)
            if registerationType == 0{
                let root = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                navCtrl = UINavigationController.init(rootViewController: root)
                navCtrl?.navigationBar.isHidden = true
                Constants.appDelegate.window?.rootViewController = navCtrl
            }
            else if registerationType == 1 {
                //let view = Constants.mainStoryboard.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
				let view = Constants.mainStoryboard.instantiateViewController(withIdentifier: "NewRegistrationViewController") as! NewRegistrationViewController
                navCtrl = UINavigationController.init(rootViewController: view)
                navCtrl?.navigationBar.isHidden = true
                Constants.appDelegate.window?.rootViewController = navCtrl
            }
            else if registerationType == 2{
				let topiInterest = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SelectPragnencyTypeViewController") as! SelectPragnencyTypeViewController

               // let topiInterest = Constants.mainStoryboard.instantiateViewController(withIdentifier: "TopicOfInterestVC") as! TopicOfInterestVC
                navCtrl = UINavigationController.init(rootViewController: topiInterest)
                navCtrl?.navigationBar.isHidden = true
                Constants.appDelegate.window?.rootViewController = navCtrl
            }
            else if registerationType == 3{
                let root = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "SideMenuNavigation")
                Constants.appDelegate.window?.rootViewController = root
                UIApplication.shared.keyWindow?.rootViewController = root


            }
        }
        Constants.appDelegate.window?.makeKeyAndVisible()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }

}

//MARK:- Extension UIView Controller Keyboard Hide
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

/*
		        let register = Constants.mainStoryboard3.instantiateViewController(withIdentifier: "PopupScreenVC") as! PopupScreenVC

		        let rootNav = UINavigationController.init(rootViewController: register)
		        rootNav.isNavigationBarHidden = true
		        Constants.appDelegate.window?.rootViewController = rootNav

		        return
        let register = Constants.mainStoryboard3.instantiateViewController(withIdentifier: "ThankYouVC") as! ThankYouVC

        let rootNav = UINavigationController.init(rootViewController: register)
        rootNav.isNavigationBarHidden = true
        Constants.appDelegate.window?.rootViewController = rootNav

        return
*/
