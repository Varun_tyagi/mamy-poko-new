//
//  InfoViewController.swift
//  Smart-E
//
//  Created by Hitesh Dhawan on 01/03/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit
import QuartzCore
import FirebaseMessaging
class InfoViewController: UIViewController, iCarouselDataSource, iCarouselDelegate{

   var items: [UIImage] = [#imageLiteral(resourceName: "first.png"),#imageLiteral(resourceName: "Second.png"),#imageLiteral(resourceName: "Third.png")]
    @IBOutlet var carousel: iCarousel!
    @IBOutlet var pageControl: UIPageControl!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
		

        carousel.isPagingEnabled = true
        carousel.type = .coverFlow2
        view.backgroundColor = .white
		
		
			DeviceTokenManager.submitDeviceToken("Info Screen")

	}
	

	
	
	
	override func viewWillAppear(_ animated: Bool) {
if #available(iOS 13.0, *) {
	let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
	statusBar.backgroundColor = UIColor.clear
	 UIApplication.shared.keyWindow?.addSubview(statusBar)
} else {
	 UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
 }
	}
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return items.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        var itemView: UIImageView
        
        //reuse view if available, otherwise create a new view
        if let view = view as? UIImageView {
            itemView = view
        } else {
            itemView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view!.frame.size.width - 80, height: self.view!.frame.size.width - 80))
            itemView.image = items[index]
            itemView.contentMode = .scaleAspectFill
        }
        return itemView
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == .spacing) {
            return value * 1.1
        }
        return value
    }
    
    func carouselItemWidth(_ carousel: iCarousel) -> CGFloat {
        return self.carousel.frame.size.width
    }
    
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        pageControl.currentPage = carousel.currentItemIndex
    }
    
    //MARK:- Button Action
    @IBAction func LoginButtonPressed(_ sender: Any) {
        let view = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
//        self.navigationController?.pushViewController(view, animated: true)
        self.present(view, animated: true, completion: nil)
        
    }
    @IBAction func RegisterButtonPressed(_ sender: Any) {
        let view = Constants.mainStoryboard.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        self.present(view, animated: true, completion: nil)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // self.dismiss(animated: true, completion: nil)
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }

}


//class InfoViewController: UIViewController {
//
//    @IBOutlet weak var carousel: ScalingCarouselView!
//    var index = 0
//
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        //view.isOpaque = false
//        view.backgroundColor = .white //UIColor.black.withAlphaComponent(0.8)
//        // Do any additional setup after loading the view.
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//
//    //MARK:- Button Action
//    @IBAction func LoginButtonPressed(_ sender: Any) {
//        let view = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
//        self.navigationController?.pushViewController(view, animated: true)
//    }
//    @IBAction func RegisterButtonPressed(_ sender: Any) {
//        let view = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//        self.navigationController?.pushViewController(view, animated: true)
//    }
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//       // self.dismiss(animated: true, completion: nil)
//
//    }
//}

//class Cell: ScalingCarouselCell {
//
//    @IBOutlet weak var imgview: UIImageView!
//    @IBOutlet weak var lblRide:UILabel!
//    @IBOutlet weak var lblRidePrice:UILabel!
//
//}

//typealias CarouselDatasource = InfoViewController
//extension CarouselDatasource: UICollectionViewDataSource {
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return 3
//
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
//
//        if let scalingCell = cell as? Cell {
//            index = indexPath.row
////       let rotation =  CGAffineTransform(rotationAngle: CGFloat(10.0 * Double.pi / 180)) //CGAffineTransform(rotationAngle: CGFloat(Double.pi/1.0))
////       
////       scalingCell.imgview.transform = cell.transform.concatenating(rotation);
////       scalingCell.imgview.image = #imageLiteral(resourceName: "first.png")
//
//            if index == 0{
////                scalingCell.imgview.backgroundColor = UIColor.red
//                scalingCell.imgview.image = #imageLiteral(resourceName: "first.png")
//            }
//            else if index == 1{
////                scalingCell.imgview.backgroundColor = UIColor.blue
//                scalingCell.imgview.image = #imageLiteral(resourceName: "Second.png")
//            }
//            else{
////                scalingCell.imgview.backgroundColor = UIColor.green
//                scalingCell.imgview.image = #imageLiteral(resourceName: "Third.png")
//            }
//        }
//
//        return cell
//    }
//}
//
//typealias CarouselDelegate = InfoViewController
//extension InfoViewController: UICollectionViewDelegate {
//
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        carousel.didScroll()
//
//        // guard let currentCenterIndex = carousel.currentCenterCellIndex?.row else { return }
//        //index = (carousel.currentCenterCellIndex?.row)!
//        // output.text = String(describing: currentCenterIndex)
//    }
//}

