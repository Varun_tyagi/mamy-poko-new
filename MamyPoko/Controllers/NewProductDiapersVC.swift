//
//  NewProductDiapersVC.swift
//  MamyPoko
//
//  Created by Arnab Maity on 24/01/20.
//  Copyright © 2020 Varun Tyagi. All rights reserved.
//

import UIKit

class NewProductDiapersVC: UIViewController {
	
	@IBOutlet weak var headingLbl: UILabel!
	
	@IBOutlet weak var descriptionLbl: UILabel!
	
	@IBOutlet weak var moreBtn: UIButton!
	
	@IBOutlet weak var priceLbl: UILabel!
	
	@IBOutlet weak var buyBtn: UIButton!
	
	@IBOutlet weak var userReview: UILabel!
	
	
	
	@IBOutlet weak var diaperCollectionView: UICollectionView!
	
	//var categoryProduct = [GetproductModels]()
	var categoryProductsDetails: DataClasses?
	
	var productType = ""
	var product_Alisa = ""
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		productDetails()
		
		// Do any additional setup after loading the view.
	}
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		if #available(iOS 13.0, *) {
			let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
			statusBar.backgroundColor = UIColor.clear
			UIApplication.shared.keyWindow?.addSubview(statusBar)
		} else {
			UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
		}
	}
}


extension NewProductDiapersVC {
	func productDetails(){
        
        ContentNetwork.diapersDetailsService(productType, product_Alisa) { (response) in
            print(response)
            let responseDict = response as Dictionary<String, Any>
                                Constants.appDel.stopLoader()
                                if responseDict["status"] as? Int == 1  || responseDict["status"] as? String == "1" {
            
                                    DispatchQueue.main.async {
                                        let prodouctContentModel = try? ProductModelDetails ((response.json))
                                        self.categoryProductsDetails = prodouctContentModel?.data
                                        self.setupData()
                                        self.diaperCollectionView.reloadData()
                                    }
                                }
                                else if responseDict["status"] as! Int == 0 {
                                    DispatchQueue.main.async {
                                        PKSAlertController.alert("Error", message: responseDict["message"] as! String)
                                    }
                                }
                                else{
                                    PKSAlertController.alert("Error", message: responseDict["message"] as! String)
            
                                }
        }
		
//		if Helper.isConnectedToNetwork() {
//			let urlstr = Constants.BASEURL + MethodName.productDetails
//			let userID = UserDefaults.standard.string(forKey: Constants.USERID)
//
//			let paramStr = "type=\(productType)&product_alias=\(product_Alisa)"
//			if userID == nil || userID?.count == 0 {
//				PKSAlertController.alert(Constants.appName, message: "Invalid User ID")
//			}
//			else{
//				Constants.appDel.startLoader()
//
//				Server.postRequestWithURL(urlstr, paramString: paramStr) { (response) in
//					let responseDict = response as Dictionary<String, Any>
//					Constants.appDel.stopLoader()
//					if responseDict["status"] as? Int == 1  || responseDict["status"] as? String == "1" {
//
//						DispatchQueue.main.async {
//							let prodouctContentModel = try? ProductModelDetails ((response.json))
//							self.categoryProductsDetails = prodouctContentModel?.data
//							self.setupData()
//							self.diaperCollectionView.reloadData()
//						}
//					}
//					else if responseDict["status"] as! Int == 0 {
//						DispatchQueue.main.async {
//							PKSAlertController.alert("Error", message: responseDict["message"] as! String)
//						}
//					}
//					else{
//						PKSAlertController.alert("Error", message: responseDict["message"] as! String)
//
//					}
//				}
//			}
//		}
	}
	func setupData(){
		self.descriptionLbl.text = categoryProductsDetails?.metaDescription
		self.headingLbl.text = categoryProductsDetails?.productName
		self.priceLbl.text = categoryProductsDetails?.price
		let reviewCount = "User Review \(String(describing: categoryProductsDetails?.reviewCount!))"
		self.userReview.text = reviewCount
	}
}




extension NewProductDiapersVC:UICollectionViewDelegate,UICollectionViewDataSource{
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return categoryProductsDetails?.productImage?.count ?? 0
	}
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "DiaperCell", for: indexPath) as! NewProductsDiapersCell
		let imgURi = Constants.BASEURL + (self.categoryProductsDetails?.productImage?[indexPath.item] ?? "")
		cell.diaperImage.sd_setImage(with: URL.init(string: imgURi ))
		
		return cell
	}
}
