//
//  MenuTableView.swift
//  TestSwiftApp
//
//  Created by Surbhi on 24/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit
import CoreData
import SafariServices
import SDWebImage
import GoogleSignIn
import FirebaseAnalytics
import AppsFlyerLib
import Mixpanel

class MenuTableView: TableViewControllerSideMenu  {
    
    var menuArray = [MenuItem]()
    var headerView:HeaderViewSide?
    var isTipsOpen = false
    var categoryPass = ""
    //Footer View for Table
    let footerview = UIView()
    
    //MARK:- View controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(ReloadMenuView), name: NSNotification.Name(rawValue: Constants.pushNotificationToUpdate), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(updateDataHeader), name: NSNotification.Name(rawValue: Constants.KUpdateProfileNotification), object: nil)

        
        tableView.frame = UIScreen.main.bounds

        self.SetMenuArray()

        self.navigationController?.isNavigationBarHidden = true
        
        self.view.backgroundColor =  #colorLiteral(red: 0.1058823529, green: 0.6980392157, blue: 0.9450980392, alpha: 1)
        tableView.contentInset = UIEdgeInsets.zero
        tableView.tableHeaderView = UIView.init(frame: CGRect.zero)
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = .never
        }

        // Preserve selection between presentations
        //tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .middle)
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuTableViewCell")
        tableView.showsHorizontalScrollIndicator=false
        var width:CGFloat = 275
        if DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_6P {
            width  = 300
        }
		
		if DeviceType.IS_IPHONE_X {
            footerview.frame = CGRect(x: 0, y: SCREEN_HEIGHT - 80, width: width, height: 80)
             tableView.frame.size.height = SCREEN_HEIGHT - 80
        }
        else if DeviceType.IS_IPHONE_6P {
            footerview.frame = CGRect(x: 0, y: SCREEN_HEIGHT - 155, width: width, height: 65)
            tableView.frame.size.height = SCREEN_HEIGHT - 155
        }
        else {
            footerview.frame = CGRect(x: 0, y: SCREEN_HEIGHT - 65, width: width, height: 55)
            tableView.frame.size.height = SCREEN_HEIGHT - 55

        }
        footerview.backgroundColor =   #colorLiteral(red: 0.1058823529, green: 0.6980392157, blue: 0.9450980392, alpha: 1)
        
        let lblcopyRite = UILabel()
        lblcopyRite.frame = CGRect(x: 20, y: 12, width: 40, height: 30)
        lblcopyRite.text = "© 2019"
        lblcopyRite.font = UIFont.systemFont(ofSize: 11)
        lblcopyRite.textColor = UIColor.white
        lblcopyRite.backgroundColor = .clear
        
        let unicharmImg = UIImageView.init(frame: CGRect(x: 60, y: 12, width: 95, height: 30))
        unicharmImg.image = #imageLiteral(resourceName: "unicharmLogo")
        unicharmImg.contentMode = UIViewContentMode.scaleAspectFit
        
        let fbBtn = UIButton.init(frame: CGRect(x: 160, y: 19, width: 16, height: 16))
        fbBtn.setImage(#imageLiteral(resourceName: "fb"), for: .normal)
        fbBtn.addTarget(self, action: #selector(FBButtonPressed), for: .touchUpInside)
        
        let twBtn = UIButton.init(frame: CGRect(x: 190, y: 19, width: 16, height: 16))
        twBtn.setImage(#imageLiteral(resourceName: "tw"), for: .normal)
        twBtn.addTarget(self, action: #selector(TwitterButtonPressed), for: .touchUpInside)

        let instaBtn = UIButton()
        instaBtn.frame = CGRect(x: 220, y: 19, width: 16, height: 16)
        instaBtn.setImage(#imageLiteral(resourceName: "insta"), for: .normal)
        instaBtn.addTarget(self, action: #selector(InstagramButtonPressed), for: .touchUpInside)

        let ytBtn = UIButton()
        ytBtn.frame = CGRect(x: 250, y: 19, width: 16, height: 16)
        ytBtn.setImage(#imageLiteral(resourceName: "yt"), for: .normal)
        ytBtn.addTarget(self, action: #selector(YouTubeButtonPressed), for: .touchUpInside)

        footerview.addSubview(lblcopyRite)
        footerview.addSubview(unicharmImg)
        footerview.addSubview(fbBtn)
        footerview.addSubview(twBtn)
        footerview.addSubview(instaBtn)
        footerview.addSubview(ytBtn)
        
    }

    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }

    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return  DeviceType.IS_IPHONE_4_OR_LESS ? 60 : 44
    }
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return  footerview
    }

//override func tableView
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_6P
        {
            return 50
        }
        return 45
    }
    
   
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArray.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as! MenuTableViewCell
        if indexPath.row == menuArray.count{
            cell.imgView.image = nil
            cell.lblText.text = ""
            return cell
        }
        cell.lblText.text = menuArray[indexPath.row].rawValue
        cell.imgView?.image = UIImage(named: "\(menuArray[indexPath.row].rawValue)")
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
    
        if menuArray[indexPath.row] == .TipsByMomsTips ||  menuArray[indexPath.row] == .TipsByMomsStory{
           // cell.imgView?.image = #imageLiteral(resourceName: "Tips By Moms")
        }
        cell.arrowImgView.isHidden =  menuArray[indexPath.row] == .SheCaresSheShares ? false : true

        cell.arrowImgView.image =  !isTipsOpen ?  #imageLiteral(resourceName: "whiteArrowDown") : #imageLiteral(resourceName: "whiteArrowUp")
        
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let menuItem =  menuArray[indexPath.row]
        
		if (menuItem == .MyPlanning) ||  (menuItem == .MyBaby) || (menuItem == .MyPregnancy) {
            
            AnalyticsManager.LogFireBaseAnalytics(analyticsType: AnalyticsEventAppOpen, itemId: "id-My_World_From_Left_Slider", itemName: "My_World_From_Left_Slider", ContentType: "My_World")
            AnalyticsManager.LogApsFlyerEvent(eventType: AppsFlyerConstant.MyWorldFromLeftSlider, contentId: "6")
           let  destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            self.goToViewController(destViewController, "HomeVC")

        }
        if menuItem == .ChangePregnancyStatus {
            let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
			destViewController.selectedButton = 2
            destViewController.isHereChangeStatus = true
            self.goToViewController(destViewController, "ProfileVC")
        }
        else if menuItem == .SheCaresSheShares {
            self.isTipsOpen =  !self.isTipsOpen
			
            self.SetMenuArray()

        }
        else if menuItem == .TipsByMomsStory || menuItem == .TipsByMomsTips {
          
AnalyticsManager.LogFireBaseAnalytics(analyticsType: AnalyticsEventAppOpen, itemId: "id-Tips_From_Left_Slider", itemName: "Tips_From_Left_Slider", ContentType: "Tips")
    AnalyticsManager.LogApsFlyerEvent(eventType: AppsFlyerConstant.TipsFromMomLeftMenu, contentId: "7")
    let  destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "TipsByMomsViewController") as! TipsByMomsViewController
            destViewController.tipsIdentifier = menuItem == .TipsByMomsStory ? "story" : "tips"
        self.goToViewController(destViewController, "TipsByMomsViewController")
            
        }
        else if menuItem == .ProductCatalogue {
            AnalyticsManager.LogFireBaseAnalytics(analyticsType: AnalyticsEventAppOpen, itemId: "id-Product_Catalog_From_Left_Slider", itemName: "Product_Catalog_From_Left_Slider", ContentType: "Product_Catalog")
            AnalyticsManager.LogApsFlyerEvent(eventType: AppsFlyerConstant.ProductCatFromLeftSLider, contentId: "8")
            let  destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "ProductCatelogViewController") as! ProductCatelogViewController
           self.goToViewController(destViewController, "ProductCatelogViewController")

        }
			
		else if menuItem == .PokoChanSticker {
            AnalyticsManager.LogFireBaseAnalytics(analyticsType: AnalyticsEventAppOpen, itemId: "poko_chan_From_Left_Slider", itemName: "poko_chan_From_Left_Slider", ContentType: "poko sticker")
            AnalyticsManager.LogApsFlyerEvent(eventType: AppsFlyerConstant.PokoChainStickerFromMenu, contentId: "46")
            
			let detailvc = Constants.mainStoryboard3.instantiateViewController(withIdentifier: "AllStickerVC") as! AllStickerVC
			self.goToViewController(detailvc, "AllStickerVC")
		}
            else if menuItem == .Quiz {
                AnalyticsManager.LogFireBaseAnalytics(analyticsType: AnalyticsEventAppOpen, itemId: "Quiz_From_Left_Slider", itemName: "Quiz_From_Left_Slider", ContentType: "Quiz")
                AnalyticsManager.LogApsFlyerEvent(eventType: AppsFlyerConstant.Quiz, contentId: "58")
                
            if  UserDefaults.standard.value(forKey: Constants.UserDefaultsConstant.isQuizSubmitted) as? Bool == true {
                   // go to tghank you page
                       //QuizThankyouViewController
                       let detailvc = Constants.mainStoryboard3.instantiateViewController(withIdentifier: "QuizThankyouViewController") as! QuizThankyouViewController
                    self.goToViewController(detailvc, "QuizThankyouViewController")
                   }else{
                   let detailvc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "QuizViewController") as! QuizViewController
                   self.goToViewController(detailvc, "QuizViewController")
                   }
            }
        else if menuItem == .WishPokochain  {
            AnalyticsManager.LogFireBaseAnalytics(analyticsType: AnalyticsEventAppOpen, itemId: "id-\(AppsFlyerConstant.WishPokoChainFromMenu)", itemName: "\(AppsFlyerConstant.WishPokoChainFromMenu)", ContentType: "Wish Pokochain")
            AnalyticsManager.LogApsFlyerEvent(eventType: AppsFlyerConstant.WishPokoChainFromMenu, contentId: "9")
            AnalyticsManager.logMixPannel("wish_poko_chan_from_menu")
            let  destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
            destViewController.isWishPokoChain = true
            destViewController.isComingFromRegister = false
			self.goToViewController(destViewController, "TermsVC")
            
        }
        else if menuItem == .SayCheeseWithPokoChan {
		
//				if let isSubmitted = UserDefaults.standard.value(forKey: Constants.UserDefaultsConstant.isBirthdayBashSubmitted) as? Bool {
//					if isSubmitted == true {
//                        AnalyticsManager.LogFireBaseAnalytics(analyticsType: AnalyticsEventAppOpen, itemId: "id-\(AppsFlyerConstant.ThankYouBirthdayBashFromMenu)", itemName: "\(AppsFlyerConstant.ThankYouBirthdayBashFromMenu)", ContentType: "B-Day Thank You")
//                        AnalyticsManager.LogApsFlyerEvent(eventType: AppsFlyerConstant.ThankYouBirthdayBashFromMenu, contentId: "45")
//                        AnalyticsManager.logMixPannel("poko_chan_bday_form_from_menu")
//
//						let destViewController = Constants.mainStoryboard3.instantiateViewController(withIdentifier: "ThankYouVC") as! ThankYouVC
//						destViewController.isSavedImageRequired = true
//                        self.goToViewController(destViewController, "ThankYouVC")
//						return
//					}
//				}
//
//            AnalyticsManager.LogFireBaseAnalytics(analyticsType: AnalyticsEventAppOpen, itemId: "id-\(AppsFlyerConstant.SubmitBirthdayBash)", itemName: "\(AppsFlyerConstant.SubmitBirthdayBash)", ContentType: "Birthday Bash Submit")
//            AnalyticsManager.LogApsFlyerEvent(eventType: AppsFlyerConstant.SubmitBirthdayBash, contentId: "42")
//            AnalyticsManager.logMixPannel("poko_chan_bday_form_from_menu")
//
//    let destViewController = Constants.mainStoryboard3.instantiateViewController(withIdentifier: "PokoChansVC") as! PokoChansVC
//    self.goToViewController(destViewController, "PokoChansVC")
			
			
			// open winner directly
			Mixpanel.initialize(token: "bbabd4dc11ec9c7fefd7569a73409213")
			 Mixpanel.mainInstance().track(event: "poko_chan_bday_thankyou_winner")
			 let searchVc = Constants.mainStoryboard3.instantiateViewController(withIdentifier: "PokoWinnerViewController") as! PokoWinnerViewController
			searchVc.isShowMenu=false
			self.goToViewController(searchVc, "PokoWinnerViewController")
				
        }

        else if menuItem == .PrivacyPolicy {
            let  destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
            destViewController.isPrivacyFromMenu = true
            destViewController.isComingFromRegister = false
            self.goToViewController(destViewController, "TermsVC")
        }
            
        else if menuItem == .Disclaimer {
            
            let  destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
            destViewController.isDisclaimerFromMenu = true
            destViewController.isComingFromRegister = false
            self.goToViewController(destViewController, "TermsVC")
        }
        else if menuItem == .Blog{
            
            AnalyticsManager.LogFireBaseAnalytics(analyticsType: AnalyticsEventAppOpen, itemId: "id-Blog_From_Left_Slider", itemName: "Blog_From_Left_Slider", ContentType: "Blog_Detail")
            AnalyticsManager.LogApsFlyerEvent(eventType: AppsFlyerConstant.BlogFromMenu, contentId: "10")
           
            
            let detailvc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "BlogVc") as! BlogVc
            detailvc.selectedType = "Blog"
           self.goToViewController(detailvc, "BlogVc")
        }
        else if menuItem == .WhatsTrending{
            AnalyticsManager.LogFireBaseAnalytics(analyticsType: AnalyticsEventAppOpen, itemId: "id-Whats_Trending_From_Left_Slider", itemName: "Whats_Trending_From_Left_Slider", ContentType: "Trending")
            AnalyticsManager.LogApsFlyerEvent(eventType: AppsFlyerConstant.TrendingFromMenu, contentId: "11")
            
            let detailvc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "BlogVc") as! BlogVc
            detailvc.selectedType = "MyInterest"
            self.goToViewController(detailvc, "BlogVc")
        }
            //"Baby Growth Tracker","Vaccination Chart"
        else if menuItem == .BabyGrowthTracker {
            AnalyticsManager.LogFireBaseAnalytics(analyticsType: AnalyticsEventAppOpen, itemId: "id-Growth_Tracker_From_Left_Slider", itemName: "Growth_Tracker_From_Left_Slider", ContentType: "Growth Tracker")
            AnalyticsManager.LogApsFlyerEvent(eventType: AppsFlyerConstant.GrowthTrackerFromMenu, contentId: "12")
            
            let detailvc = Constants.mainStoryboard.instantiateViewController(withIdentifier: "BabyGrowthDataViewController") as! BabyGrowthDataViewController
        self.goToViewController(detailvc, "BabyGrowthDataViewController")
            
        }
        else if menuItem == .VaccinationTracker{
            AnalyticsManager.LogFireBaseAnalytics(analyticsType: AnalyticsEventAppOpen, itemId: "id-Vaccine_Schedule_From_Left_Slider", itemName: "Vaccine_Schedule_From_Left_Slider", ContentType: "Vaccine Schedule")
            AnalyticsManager.LogApsFlyerEvent(eventType: AppsFlyerConstant.VaccScheduleFromMenu, contentId: "13")
            
        let detailvc = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "VaccinationScheduleVC") as! VaccinationScheduleVC
        self.goToViewController(detailvc, "VaccinationScheduleVC")
            
        }
		
			
        else if menuItem == .TermsConditions{
            let  destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
            destViewController.isTermsFromMenu = true
            destViewController.isComingFromRegister = false
            self.goToViewController(destViewController, "TermsVC")
            
        }
        else if menuItem == .SayCheeseWithPokoChan{
           
            if let isSubmitted = UserDefaults.standard.value(forKey: Constants.UserDefaultsConstant.isBirthdayBashSubmitted) as? Bool {
                if isSubmitted == true {
    AnalyticsManager.LogFireBaseAnalytics(analyticsType: AnalyticsEventAppOpen, itemId: "id-\(AppsFlyerConstant.ThankYouBirthdayBashFromMenu)", itemName: "\(AppsFlyerConstant.ThankYouBirthdayBashFromMenu)", ContentType: "B-Day Thank You")
    AnalyticsManager.LogApsFlyerEvent(eventType: AppsFlyerConstant.ThankYouBirthdayBashFromMenu, contentId: "45")
    AnalyticsManager.logMixPannel("poko_chan_bday_form_from_menu")
                    
let dest = Constants.mainStoryboard3.instantiateViewController(withIdentifier: "ThankYouVC") as! ThankYouVC
        dest.isSavedImageRequired = true
        self.goToViewController(dest, "ThankYouVC")
            return
                }
            }
            
            AnalyticsManager.LogFireBaseAnalytics(analyticsType: AnalyticsEventAppOpen, itemId: "id-\(AppsFlyerConstant.SubmitBirthdayBash)", itemName: "\(AppsFlyerConstant.SubmitBirthdayBash)", ContentType: "Birthday Bash Submit")
            AnalyticsManager.LogApsFlyerEvent(eventType: AppsFlyerConstant.SubmitBirthdayBash, contentId: "42")
			AnalyticsManager.logMixPannel("poko_chan_bday_form_from_menu")
            
            let destViewController = Constants.mainStoryboard3.instantiateViewController(withIdentifier: "PokoChansVC") as! PokoChansVC
            self.goToViewController(destViewController, "PokoChansVC")
        }
		else if menuItem == .LogOut {
			PKSAlertController.alert(Constants.appName, message: "Are you sure you want to Logout?", buttons: ["Ok","Cancel"], tapBlock: { (alert, index) in
				if index == 0 {
					let root = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
					let   navCtrl = UINavigationController.init(rootViewController: root)
					navCtrl.navigationBar.isHidden = true
					Constants.appDelegate.window?.rootViewController = navCtrl
					Helper.resetDefaults()
                    UserDefaults.standard.set(0, forKey: Constants.RegisterStatus)
				}
			})
        }
        else{
            return
        }
    }
    
    
    func goToViewController( _ destinationVC : AnyObject , _ identifier : String )  {

        if Constants.appDel.returnTopViewController().isKind(of: HomeVC.classForCoder()){
            let home = Constants.appDel.returnTopViewController() as! HomeVC
            home.hideSideMenuView()
            home.navigationController?.pushViewController(destinationVC as! UIViewController, animated: true)
        }
        else{
            sideMenuController()?.setContentViewController(destinationVC as! UIViewController)
        }
    }
    
    
    //MARK:- Extraa funtions
   
    func  SetMenuArray(){
        
        guard let profileDict = Helper.getDataFromNsDefault(key: Constants.PROFILE) as? [String : Any], let  parentalStatus =  profileDict["parental_status"] as? String  else {
            return
        }

        switch parentalStatus.lowercased() {
        case "planning-baby":
            menuArray = menuPlanningArray
        case "mother" :
            menuArray = menuMotherArray
        default:
            menuArray =  menuPragnentArray
        }
        
         if self.isTipsOpen{
            if let tipsMomIndex = menuArray.index(of: MenuItem.SheCaresSheShares){
            menuArray.insert(MenuItem.TipsByMomsStory, at: tipsMomIndex+1)
            menuArray.insert(MenuItem.TipsByMomsTips, at: tipsMomIndex+2)
            }
        }
        self.ReloadMenuView()
    }

    @objc func  ReloadMenuView(){
        tableView.reloadData()
    }
    
    @objc func updateDataHeader(){
        DispatchQueue.main.async {
            self.SetMenuArray()
        }
    }
    
    func ResetDefaults(){
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
		
    }
    override var prefersStatusBarHidden: Bool{
        return false
    }
    
    //MARK:- Social Media Links
    @objc func FBButtonPressed(){
        self.openSocialMedia(FBLink)
    }
    @objc func TwitterButtonPressed(){
        self.openSocialMedia(TwitterLink)
    }
    @objc func InstagramButtonPressed(){
        self.openSocialMedia(InstagramLink)
    }
    @objc func YouTubeButtonPressed(){
        self.openSocialMedia(YouTubeLink)
    }
    
    fileprivate func openSocialMedia(_ linkUrl :String){
        if UIApplication.shared.canOpenURL(URL.init(string: YouTubeLink)!)
        {
UIApplication.shared.open(URL(string : YouTubeLink)!, options: [:], completionHandler: {(status) in })
        }
    }
}

class TableViewControllerSideMenu:UITableViewController{
    override init(style: UITableViewStyle) {
        super.init(style: .plain)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension MenuTableView : ENSideMenuDelegate{
    func sideMenuDidOpen() {    }
}

