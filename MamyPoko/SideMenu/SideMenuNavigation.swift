//
//  SideMenuNavigation.swift
//  TestSwiftApp
//
//  Created by Surbhi on 24/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class SideMenuNavigation: ENSideMenuNavigationController, ENSideMenuDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sideMenu = ENSideMenu(sourceView: self.view, menuViewController: MenuTableView.init(style: .plain) , menuPosition:.left)
        
        if DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_6P {
            sideMenu?.menuWidth = 300.0//300.0
        }
        else
        {
            sideMenu?.menuWidth = 275.0
        }
        
        view.bringSubview(toFront: navigationBar)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - ENSideMenu Delegate
    func sideMenuWillOpen() {
		
    }
    
    func sideMenuWillClose() {
		
    }
    
    func sideMenuDidClose() {
		
    }
    
    func sideMenuDidOpen() {
       
    }
	
	func sideMenuAllowSwipe(){
		sideMenu?.allowRightSwipe = true
	}
	func sideMenuBlockSwipe(){
		sideMenu?.allowRightSwipe = false
	}
}
