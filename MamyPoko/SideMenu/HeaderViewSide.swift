//
//  HeaderViewSide.swift
//  Smart-E
//
//  Created by Hitesh Dhawan on 20/01/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit

class HeaderViewSide: UIView {

    @IBOutlet weak var contentView:UIView!
    @IBOutlet weak var imgViewProfile:UIImageView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblEmail:UILabel!
    @IBOutlet weak var btnView:UIButton!

    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    // MARK: - UI setup
    func xibSetup() {
        let nib = UINib(nibName: "HeaderViewSide", bundle: nil)
        contentView = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        contentView.frame = bounds
        contentView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        addSubview(contentView)
       
        
    }

}
