//
//  MenuTableViewCell.swift
//  Smart-E
//
//  Created by Hitesh Dhawan on 09/01/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var arrowImgView: UIImageView!
    @IBOutlet weak var imgView : UIImageView!
    @IBOutlet weak var lblText : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }    
}
