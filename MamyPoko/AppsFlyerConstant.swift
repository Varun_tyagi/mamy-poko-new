//
//  AppsFlyerConstant.swift
//  MamyPoko
//
//  Created by Varun Tyagi on 06/02/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

import Foundation

struct AppsFlyerConstant{
    static let AppLaunch                    =   "App_Launch"
    static let AppEnterBackground           =   "App_Enter_Background"
    static let AppEnterForground            =   "App_Enter_Forground"
    static let AppTerminate                 =   "App_Terminate"
    static let MyWorldFromLeftSlider        =   "My_World_From_Left_Slider"
    static let TipsFromMomLeftMenu          =   "Tips_From_Left_Slider"
    static let ProductCatFromLeftSLider     =   "Product_Catalog_From_Left_Slider"
    static let PokoChainFromMenu            =   "Pokochain_From_Left_Slider"
    static let WishPokoChainFromMenu        =   "Wish_Pokochain_From_Left_Slider"
    static let BlogFromMenu                 =   "Blog_From_Left_Slider"
    static let TrendingFromMenu             =   "Whats_Trending_From_Left_Slider"
    static let GrowthTrackerFromMenu        =   "Growth_Tracker_From_Left_Slider"
    static let VaccScheduleFromMenu         =   "Vaccine_Schedule_From_Left_Slider"
    static let MonthlyTipsPage              =   "MonthlyBabyTips"
    static let SplashScreen                 =   "Splash_Appear"
    static let SocialFbClick                =   "Social_FB_Click"
    static let SocialGmailClick             =   "Social_Gmail_Click"
    static let LoginSuccess                 =   "Login_Success"
    static let RegisterFirstStepComplete    =   "Registration_First_Step_Complete"
    static let RegisterIAmPregnentClick     =   "Registration_IAm_Pregnant_Click"
    static let RegisterPlanningBabyClick    =   "Registration_Planning_Baby"
    static let RegisterIntrestSubmit        =   "Registration_Interest_Submit"
    static let ProductCatalog               =   "Product_Catalog"
    static let PrivacyPolicyOpen            =   "Privacy_Policy_Visit"
    static let ProfileVisit                 =   "Profile_Page_Visit"
    static let ProfileEditVisit             =   "Profile_Edit_Visit"
    static let ProfileEditSubmit            =   "Profile_Edit_Submit"
    static let MyWorldVisit                 =   "My_World_Dashboard"
    static let ChildHasArrivedClicked       =   "My_Child_has_arrived"
    static let ChildHasNotArrivedClicked    =   "My_Child_has_not_arrived"
    static let PragnencyTrackerVisit        =   "PregnancyTrackerWeekView"
    static let BlogDetail                   =   "Blog_Detail_Page"
    static let BlogComment                  =   "Blog_Comment"
    static let PanelistDetailView           =   "Panelist_Profile_View"
    static let TipsByMomView                =   "Tips_By_Moms_View"
    static let SearchView                   =   "Search_View"
    static let DataUpdatedAfter40WeekPregnancy  =   "UpdateData_After_40Week_Pregnancy"//37
    static let AddMesurementClicked         =   "Baby_Measurement_Added_OrEdited"//38
    static let DiaperCategory               =   "Diaper_Category" //39
	static let ProfileImageView            	=   "Profile_Image_View" //40
	static let ProfileImageEdit            	=   "Profile_Image_Edit" //41
    static let SubmitBirthdayBash           =    "Submit_Birthday_Bash"//42
    static let ThankYouBirthdayBash         =   "ThankYou_Birthday_Bash"//43
    static let WinnerBirthdayBash           =  "Winner_Birthday_Bash"//44
    static let ThankYouBirthdayBashFromMenu  = "ThankYou_BirthdayBash_From_Menu"//45
   
    static let PokoChainStickerFromMenu        = "Poko_Chan_Sticker_From_Left_Slider"//46
    
    
    static let StickerList                          = "Sticker_Pack_List"//47
    static let StickerPAckDetail                    = "Sticker_Pack_Detail"//48
    static let DownloadStickerPack                  = "Download_Sticker_PAck"//49
	static let PregnancyTrackerDetail               = "PregnancyTrackerDetailVC"//50
	static let Pregnancy_Week_Details_Page = "Pregnancy_Week_Details_Page"//51
	static let PanelistProfileView =         "PanelistProfileView"//52
	static let PanelistProfileVC = "PanelistProfileVC" // 53
	static let ProductCatelogView = "ProductCatelogView" // 54
	static let ProductCatelogVC = "ProductCatelogVC" // 55
	static let TipsByMomsView = "TipsByMomsView" // 56
	static let TipsByMomsVC = "TipsByMomsVC" // 57
	
	static let Quiz = "Poko Chan Quiz"// 58
    static let QuizThankYou         =   "ThankYou_Birthday_Bash"//59

	
}
