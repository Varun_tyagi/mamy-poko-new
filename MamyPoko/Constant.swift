    //
    //  Constant.swift
    //
    //
    //  Created by Surbhi on 24/10/16.
    //  Copyright © 2016 Neuronimbus. All rights reserved.
    //

    import Foundation
    import UIKit
   
    let KSomeErrorTryAgain =  "Some error has occured! Please try again"
    let FBLink = "https://www.facebook.com/mamypokopantsindia"
    let InstagramLink = "https://www.instagram.com/mamypokoindia/"
    let TwitterLink = "https://twitter.com/MamyPokoIN"
    let YouTubeLink = "https://www.youtube.com/channel/UCpqgqbPAXIpsX3o0nYE1G-w"
    
    struct ScreenSize {
        static let SCREEN               = UIScreen.main.bounds
        static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }

    struct DeviceType {
        static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPHONE_X         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0

        static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad
    }

    struct iOSVersion {
        static let SYS_VERSION_FLOAT = (UIDevice.current.systemVersion as NSString).floatValue
        static let iOS7 = (iOSVersion.SYS_VERSION_FLOAT < 8.0 && iOSVersion.SYS_VERSION_FLOAT >= 7.0)
        static let iOS8 = (iOSVersion.SYS_VERSION_FLOAT >= 8.0 && iOSVersion.SYS_VERSION_FLOAT < 9.0)
        static let iOS9 = (iOSVersion.SYS_VERSION_FLOAT >= 9.0 && iOSVersion.SYS_VERSION_FLOAT < 10.0)
        static let iOS10 = (iOSVersion.SYS_VERSION_FLOAT >= 10.0 && iOSVersion.SYS_VERSION_FLOAT < 11.0)

    }


    struct DeviceOrientation {
        static let IS_portT = UIDevice.current.orientation.isPortrait
        static let IS_Land = UIDevice.current.orientation.isLandscape

    }

    struct Constants {

        static let pushNotification = "com.neuro.pushNotification"
        static let pushNotificationToUpdate = "sideMenuPushNotification"
        static let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        static let mainStoryboard2: UIStoryboard = UIStoryboard(name: "Main2",bundle: nil)
		static let mainStoryboard3: UIStoryboard = UIStoryboard(name: "Main3",bundle: nil)
        static let appDel = UIApplication.shared.delegate as! AppDelegate
        
   static let BASEURL =   "http://35.154.227.98/"//betalive
    // static let BASEURL =   "https://www.mamypoko.co.in/" //live

        
        static let authUserName = "mamypokopants@gmail.com"
        static let authPass = "Neuro@123"
        static let AuthorisationStr = "Basic bWFteXBva29wYW50c0BnbWFpbC5jb206TmV1cm9AMTIz"
        
        
        static let Device_UUID = UIDevice.current.identifierForVendor?.uuidString.replacingOccurrences(of: "-", with: "")
        static let PROFILE = "data"
        static let PASSWORD = "pwd_Blank"
        static let SelectedMenuIndex = "selectedIndex"
        static let DevToken = "deviceToken"
        static let AUTHID = "auth_id"
        static let AUTHType = "auth_type"
        static let DEVId = "device_id"
        static let USERID = "id"
        static let GENDER = "gender"
        static let PARENTALSTATS = "parental_status"
        static let isLoggedIn = "loggedin"

        static let MOB = "mobile"
        static let NAME = "name"
        static let PROFILEPIC = "profile_pic"
        static let PASS = "password"
        static let STATUS = "success"
        static let EMAIL = "email"
        static let refreshGenderNotification = "refreshGenderNotification"
        static let AuthType = "auth_type"
        static var AuthId = "auth_id"
        static let RUPEE = "₹"

        static let MESS =  "messageCode"
        static let Message =  "message"
        static let USERNAme = "userName"
        static let USERPASSWORD = "USERPASSWORD"

        static let header = "header"
        static let appDelegate = UIApplication.shared.delegate as! AppDelegate

        static let SERVICEGROUPNAME = "serviceGroupname"
        static let SERVICEGROUPID = "serviceGroupId"
        static let REFRENCEID =  "referenceId"
        static let appName = "MamyPoko"

        static let RideStatus = "rideStatus"
        static let RideDetail = "rideDetail"
        static let bookingRideDetails = "bookingRideDetails"
        
        static let WaitingForOTP = "isWaitingForOTP"
        static let RegisterStatus = "registrationStatus" // 0 - only register - mobile not verified
                                                         // 1- mobile verified - personal details missing
                                                         // 2 - personal details verified - topics of interest not selected
                                                         // 3 - entire registration flow complete

        struct UserDefaultsConstant {
            static let userDetailsSocial = "socialDetails"
            static let isSocialLogin = "isSocialLogin"
            static let isBirthdayBashSubmitted = "isBirthdayBashSubmitted"
            static let babyImagePathBirthdayBashPath = "babyImagePath"
			static let isWishPokoChanSubmitted = "isWishPokoChan"
            static let isFirstLogin = "isFirstLogin"
            static let isQuizSubmitted = "isQuizSubmitted"
            //keys    ----  name,email,gender,id

        }
        struct SocialUserData {
            static let userName = "name"
            static let userEmail = "email"
            static let socialId = "id"
            static let userGender = "gender"
        }
        static let  KUpdateProfileNotification = "updateProfileNotification"
        static let KUpdateBumpGalleryNotification  = "updateBumpGalleryNotification"

    }

    struct MethodName {
        static let Pincode                           =  "webservices/user/city_by_pincode/"
        static let Register                          =  "webservices/user/register"
        static let OTPVerify                         =  "webservices/user/otpveryfication"
        static let ResendOTP                         =  "webservices/user/resend_otp"
        static let RegisterStep2                     =  "webservices/user/registerSteptwo"
        static let GET_Categories                    =  "webservices/posts/categories"
        static let RegisterStep3                     =  "webservices/user/registerStepThree"
        static let Login                             =  "webservices/posts/login"//"Login"
        static let ForgetPass                        =  "webservices/user/forgot_password"
        static let SocialLogin                       =  "webservices/posts/social_login"
        
        static let homeScreenPtracker                =  "webservices/posts/save_pregnency_tracker"
        static let homeScreenPosts                   =  "webservices/posts/dashboard/"
        static let UploadBumpImage                   =  "webservices/images/save_gallery_pregnency_tracker"
        static let RemoveBumpImage                   =  "webservices/images/remove_gallery_pregnency_tracker"
        
        static let WeeklyTips                        =  "webservices/posts/weekly_tips/"
        static let ArticleDetails                    =  "webservices/posts/posts_details/"
        static let SaveComment                       =  "webservices/posts/save_comments"

        static let UserProfile                       =  "webservices/posts/user_profile/"
        static let UpdateProfile                     =  "webservices/posts/update_profile_info"
        static let ProfileImgUpload                  =  "webservices/images/profile_image_upload"//"Login"
        static let ChangePass                        =  "webservices/user/change_password"
		static let OfferRegister 					 =  "webservices/posts/add_offer_subscribe"
		
        static let QuestionListing                   =  "webservices/questions/question"
        static let Question                          =  "webservices/posts/questions"
        static let BlogCat                           =  "webservices/posts/blog_categories"
        static let Story                             =  "webservices/posts/story"
        static let MyWorld                           =  "webservices/posts/my_world"
        static let CatPost                           =  "webservices/posts/category_posts/"
        static let RemoveGalleryPregTracker          =  "webservices/images/remove_gallery_pregnency_tracker"
        static let GET_tipsCategory                  =  "webservices/momtips/tips_category"
        static let GET_tipsByCategory                =  "webservices/momtips/tips/" // append id
        static let AddTip                            =  "webservices/momtips/add_tips/"
        static let GET_SearchResult                  = "webservices/posts/search_result/" // append keyword
        static let UpdateBmpImage                    = "webservices/images/update_image_title_pregnency_tracker"
        static let BabyHasnotArrived                 = "webservices/posts/pregnancy_delivery"
        static let BabyTrackDetails                  = "webservices/posts/baby_tracker"  // track baby
        static let VaccineSchedule                   = "webservices/vaccination/vaccination_chart"  // VAccine schedule
        static let BabyGrowthMesaurements            = "webservices/baby_tracker/baby_growth"// Baby growth Mesaurement
        static let DeleteMeasurement                 = "webservices/baby_tracker/baby_tracker_delete"
        static let AddEditMeasurement        = "webservices/baby_tracker/tracker_update"
        static let BabyGrowthMonthlyTips     = "webservices/posts/monthly_tips/?user_id=" // tips for baby growth
        
        static let SubmitVaccine       = "webservices/vaccination/submit_vacc"
        static let UpdateVaccineDate   = "webservices/vaccination/update_vacc_date"
        static let UploadVaccBabyImage = "webservices/vaccination/upload_vacc"
        static let RemoveVaccBabyImage = "webservices/vaccination/vacc_image_remove"
		static let registerDeviceToken = "webservices/posts/user_activity_log"
		static let verifyOtp           = "webservices/user/otpveryfication"
        static let registerBabyBirthdayBash = "webservices/pokochan/register"
      static let getBirthdayBashWinner = "webservices/pokochan/getdata"
		static let getgameplay  = "webservices/pokochan/getgameplay"
		static let getSticker = "webservices/pokochan/getContent"
		 static let Tips_pagination =   "webservices/momtips/tips_pagination"
         static let offers_likes = "webservices/momtips/offers_likes"
		static let quiz = "crm/webservices/quiz/question_get"
      static let answer = "crm/webservices/quiz/answer_post"
		static let getProductsCategoryApi = "webservices/products/getCategory"
		static let getProducts = "webservices/products/getProducts"
		static let productDetails = "webservices/products/product_detail"
    }


    struct Colors {
        static let Apptextcolor =  UIColor(red: 33.0/255.0, green: 33.0/255.0, blue: 33.0/255.0, alpha: 1.0) // also to be used for TextColor
        static let appThemeColorText = UIColor(red: 240.0/255.0, green:  157.0/255.0, blue: 189.0/255.0, alpha: 1.0) // baby Pink Color
        static let appThemeColorTextgray = UIColor(red: 33.0/255.0, green:  33.0/255.0, blue: 33.0/255.0, alpha: 1.0)
        static let ShadowColor = UIColor(red: 85.0/255.0, green:  85.0/255.0, blue: 85.0/255.0, alpha: 1.0)
        static let Lighttextcolor = UIColor(red: 240.0/255.0, green:  240.0/255.0, blue: 245.0/255.0, alpha: 1.0)
        static let MAMYBlue = #colorLiteral(red: 0.1058823529, green: 0.6980392157, blue: 0.9450980392, alpha: 1)
        static let MamyDarkBlue =  UIColor.init(red: 3.0/255.0, green: 74.0/255.0, blue: 154.0/255.0, alpha: 1.0)
     }

    struct FONTS {
        static let Helvetica_Regular = "HelveticaNeue"
        static let Helvetica_Light = "HelveticaNeue-Light"
        static let Helvetica_Medium = "HelveticaNeue-Medium"
        static let Helvetica_Bold = "HelveticaNeue-Bold"
        static let Helvetica_Thin = "HelveticaNeue-Thin"
        static let Helvetica_UltraLight = "HelveticaNeue-UltraLight"
        static let Helvetica_Italic = "HelveticaNeue-Italic"
        static let Helvetica_LightItalic = "HelveticaNeue-LightItalic"
        static let Helvetica_MediumItalic = "HelveticaNeue-MediumItalic"
        static let Helvetica_ThinItalic = "HelveticaNeue-ThinItalic"
        static let Helvetica_UltraLightItalic = "HelveticaNeue-UltraLightItalic"
        static let Helvetica_BoldItalic = "HelveticaNeue-BoldItalic"
        static let Helvetica_CondensedBlack = "HelveticaNeue-CondensedBlack"
        static let Helvetica_CondensedBold = "HelveticaNeue-CondensedBold"


        static let AutumninNovember = "AutumninNovember"
        static let AdiraDisplaySSi = "AdiraDisplaySSi"
        static let QuicksandBook_Regular = "QuicksandBook-Regular"
        static let Quicksand_Light = "Quicksand-Light"
        static let QuicksandLight_Regular = "QuicksandLight-Regular"
        static let Quicksand_Medium = "Quicksand-Medium"
        static let Quicksand_Regular = "Quicksand-Regular"
        static let QuicksandBookOblique_Regular = "QuicksandBookOblique-Regular"
        static let Quicksand_Italic = "Quicksand-Italic"
        static let Quicksand_Bold = "Quicksand-Bold"
        static let QuicksandBold_Regular = "QuicksandBold-Regular"
        static let QuicksandBoldOblique_Regular = "QuicksandBoldOblique-Regular"
        static let AnjaElianceAccent = "Anja-Eliane-accent002"
    }

    
    //MARK:- Menu constants
    enum MenuItem:String {
        case MyPlanning = "My Planning"
        case MyBaby = "My Baby"
        case MyPregnancy = "My Pregnancy"
        case BabyGrowthTracker = "Baby Growth Tracker"
        case VaccinationTracker = "Vaccination Tracker"
        case ChangePregnancyStatus  = "Change Pregnancy Status"
        case ProductCatalogue  = "Product Catalogue"
        case Blog = "Blog"
        case SheCaresSheShares = "She Cares She Shares"
        case TipsByMomsStory = "Stories By Moms"
        case TipsByMomsTips = "Tips By Moms"
        
        case PokoChanSticker = "Poko Chan Sticker"
        case SayCheeseWithPokoChan = "Say Cheese With Poko Chan"
        case WishPokochain = "Wish Poko Chan"
        case Quiz = "Poko Chan Quiz"
        case PrivacyPolicy = "Privacy Policy"
        
        case TermsConditions = "Terms & Conditions"
        case Disclaimer = "Disclaimer"
        case WhatsTrending = "What's Trending"
        case LogOut = "Log Out"
    }
    
    let menuPlanningArray:[MenuItem] = [.MyPlanning,.ChangePregnancyStatus,.ProductCatalogue,.Blog,.SheCaresSheShares,.PokoChanSticker,.Quiz,.PrivacyPolicy,.TermsConditions,.Disclaimer,.LogOut]
    
   
   let menuMotherArray:[MenuItem] = [.MyBaby,.BabyGrowthTracker,.VaccinationTracker,.WhatsTrending,.ProductCatalogue,.Blog,.SheCaresSheShares,.SayCheeseWithPokoChan,.PokoChanSticker,.Quiz,.PrivacyPolicy,.TermsConditions,.Disclaimer,.LogOut]
        
      let  menuPragnentArray:[MenuItem] =  [.MyPregnancy,.WhatsTrending,.ProductCatalogue,.Blog,.SheCaresSheShares, .SayCheeseWithPokoChan,.PokoChanSticker,.Quiz,.PrivacyPolicy,.TermsConditions,.Disclaimer,.LogOut]

// Sample payload
/*
   {
     
     "aps" : {
     "mutable-content": true,
     "content-available": true,
     "media-type":"image",
     "sound": "default",
     "priority" : "high",
     
     "userdata" : { "media-url": "https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png"},
     
     "alert" : {
     "title" : " Notification",
     "body" : "New Notification Look Amazing"
     
     },
     "category" : "attachmentCategory",
     "badge" : 0
     },
     "media-url": "https://cdn.shopifycloud.com/hatchful-web/assets/313d73fa42f04a46213abc6267b4d074.png"
     }
     

     */
