// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let quizDataModel = try QuizDataModel(json)

import Foundation

// MARK: - QuizDataModel
struct QuizDataModel: Codable {
    let status: Int?
    let data: [QuestionModel]?
}

// MARK: QuizDataModel convenience initializers and mutators

extension QuizDataModel {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(QuizDataModel.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - Datum
struct QuestionModel: Codable {
    let questionID, question, optionA, optionB: String?
    let optionC: String?
    let optionD: String?
    let rightAns: RightAns?
    let rightAnsName: String?
    var selectedAns:String?
    enum CodingKeys: String, CodingKey {
        case questionID = "question_id"
        case question
        case optionA = "option_a"
        case optionB = "option_b"
        case optionC = "option_c"
        case optionD = "option_d"
        case rightAns = "right_ans"
        case rightAnsName = "right_ans_name"
        case selectedAns
    }
}



// MARK: Datum convenience initializers and mutators

extension QuestionModel {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(QuestionModel.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    

  
}

enum OptionD: String, Codable {
    case empty = ""
    case noneOfThese = "none of these"
}

enum RightAns: String, Codable {
    case a = "a"
    case b = "b"
    case c = "c"
}

// MARK: - Helper functions for creating encoders and decoders

