// To parse the JSON, add this file to your project and do:
//
//   let babyMonthlyTipModel = try BabyMonthlyTipModel(json)
//
// To read values from URLs:
//
//   let task = URLSession.shared.babyMonthlyTipModelTask(with: url) { babyMonthlyTipModel, response, error in
//     if let babyMonthlyTipModel = babyMonthlyTipModel {
//       ...
//     }
//   }
//   task.resume()

import Foundation

struct BabyMonthlyTipModel: Codable {
    let status: Int?
    let data: [Datum]?
    let disclaimer: Disclaimer?
}

struct Datum: Codable {
    let title: String?
    let image: String?
    let content, date, authorName: String?
    let authorImage: String?
    
    enum CodingKeys: String, CodingKey {
        case title, image, content, date
        case authorName = "author_name"
        case authorImage = "author_image"
    }
}

struct Disclaimer: Codable {
    let discHeading, doctorName: String?
    let profileImage: String?
    let doctorDegree, doctorExp, doctorProfile, doctorVerified: String?
    let doctorDescription: String?
    
    enum CodingKeys: String, CodingKey {
        case discHeading = "disc_heading"
        case doctorName = "doctor_name"
        case profileImage = "profile_image"
        case doctorDegree = "doctor_degree"
        case doctorExp = "doctor_exp"
        case doctorProfile = "doctor_profile"
        case doctorVerified = "doctor_verified"
        case doctorDescription = "doctor_description"
    }
}

// MARK: Convenience initializers and mutators

extension BabyMonthlyTipModel {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(BabyMonthlyTipModel.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        status: Int?? = nil,
        data: [Datum]?? = nil,
        disclaimer: Disclaimer?? = nil
        ) -> BabyMonthlyTipModel {
        return BabyMonthlyTipModel(
            status: status ?? self.status,
            data: data ?? self.data,
            disclaimer: disclaimer ?? self.disclaimer
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Datum {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Datum.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        title: String?? = nil,
        image: String?? = nil,
        content: String?? = nil,
        date: String?? = nil,
        authorName: String?? = nil,
        authorImage: String?? = nil
        ) -> Datum {
        return Datum(
            title: title ?? self.title,
            image: image ?? self.image,
            content: content ?? self.content,
            date: date ?? self.date,
            authorName: authorName ?? self.authorName,
            authorImage: authorImage ?? self.authorImage
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Disclaimer {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Disclaimer.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        discHeading: String?? = nil,
        doctorName: String?? = nil,
        profileImage: String?? = nil,
        doctorDegree: String?? = nil,
        doctorExp: String?? = nil,
        doctorProfile: String?? = nil,
        doctorVerified: String?? = nil,
        doctorDescription: String?? = nil
        ) -> Disclaimer {
        return Disclaimer(
            discHeading: discHeading ?? self.discHeading,
            doctorName: doctorName ?? self.doctorName,
            profileImage: profileImage ?? self.profileImage,
            doctorDegree: doctorDegree ?? self.doctorDegree,
            doctorExp: doctorExp ?? self.doctorExp,
            doctorProfile: doctorProfile ?? self.doctorProfile,
            doctorVerified: doctorVerified ?? self.doctorVerified,
            doctorDescription: doctorDescription ?? self.doctorDescription
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}


// MARK: - URLSession response handlers

extension URLSession {
    fileprivate func codableTask<T: Codable>(with url: URL, completionHandler: @escaping (T?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                completionHandler(nil, response, error)
                return
            }
            completionHandler(try? newJSONDecoder().decode(T.self, from: data), response, nil)
        }
    }
    
    func babyMonthlyTipModelTask(with url: URL, completionHandler: @escaping (BabyMonthlyTipModel?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
}

