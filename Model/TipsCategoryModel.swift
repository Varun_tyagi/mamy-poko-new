

//   let tipsCategoryModel = try TipsCategoryModel(json)

import Foundation

typealias TipsCategoryModel = [TipsCategoryModelElement]

struct TipsCategoryModelElement: Codable {
    let catAlias, catName: String?

    enum CodingKeys: String, CodingKey {
        case catAlias = "cat_alias"
        case catName = "cat_name"
    }
}

// MARK: Convenience initializers

extension TipsCategoryModelElement {
    init(data: Data) throws {
        self = try JSONDecoder().decode(TipsCategoryModelElement.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Array where Element == TipsCategoryModel.Element {
    init(data: Data) throws {
        self = try JSONDecoder().decode(TipsCategoryModel.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
