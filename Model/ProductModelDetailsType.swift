// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let productModelDetails = try ProductModelDetails(json)

import Foundation

// MARK: - ProductModelDetails
struct ProductModelDetails: Codable {
    let status: Int?
    let data: DataClasses?
}

// MARK: ProductModelDetails convenience initializers and mutators

extension ProductModelDetails {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(ProductModelDetails.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        status: Int?? = nil,
        data: DataClasses?? = nil
    ) -> ProductModelDetails {
        return ProductModelDetails(
            status: status ?? self.status,
            data: data ?? self.data
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - DataClass
struct DataClasses: Codable {
    let recomended: JSONNull?
    let productID, productName, metaDescription, metaKey: String?
    let price, specifications, type, size: String?
    let steps, stepCount, dataDescription, productSubheading: String?
    let alias, sku, shortDescription: String?
    let pieces: [JSONAny]?
    let weightInKg, copyright, related, imageAlt: String?
    let imageTitle: String?
    let productImage: [String]?
    let reviewCount: Int?
    let sizes: [Size]?

    enum CodingKeys: String, CodingKey {
        case recomended
        case productID = "product_id"
        case productName = "product_name"
        case metaDescription = "meta_description"
        case metaKey = "meta_key"
        case price, specifications, type, size, steps
        case stepCount = "step_count"
        case dataDescription = "description"
        case productSubheading = "product_subheading"
        case alias, sku
        case shortDescription = "short_description"
        case pieces
        case weightInKg = "weight_in_kg"
        case copyright, related
        case imageAlt = "image_alt"
        case imageTitle = "image_title"
        case productImage = "product_image"
        case reviewCount = "ReviewCount"
        case sizes
    }
}

// MARK: DataClass convenience initializers and mutators

extension DataClasses {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(DataClasses.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        recomended: JSONNull?? = nil,
        productID: String?? = nil,
        productName: String?? = nil,
        metaDescription: String?? = nil,
        metaKey: String?? = nil,
        price: String?? = nil,
        specifications: String?? = nil,
        type: String?? = nil,
        size: String?? = nil,
        steps: String?? = nil,
        stepCount: String?? = nil,
        dataDescription: String?? = nil,
        productSubheading: String?? = nil,
        alias: String?? = nil,
        sku: String?? = nil,
        shortDescription: String?? = nil,
        pieces: [JSONAny]?? = nil,
        weightInKg: String?? = nil,
        copyright: String?? = nil,
        related: String?? = nil,
        imageAlt: String?? = nil,
        imageTitle: String?? = nil,
        productImage: [String]?? = nil,
        reviewCount: Int?? = nil,
        sizes: [Size]?? = nil
    ) -> DataClasses {
        return DataClasses(
            recomended: recomended ?? self.recomended,
            productID: productID ?? self.productID,
            productName: productName ?? self.productName,
            metaDescription: metaDescription ?? self.metaDescription,
            metaKey: metaKey ?? self.metaKey,
            price: price ?? self.price,
            specifications: specifications ?? self.specifications,
            type: type ?? self.type,
            size: size ?? self.size,
            steps: steps ?? self.steps,
            stepCount: stepCount ?? self.stepCount,
            dataDescription: dataDescription ?? self.dataDescription,
            productSubheading: productSubheading ?? self.productSubheading,
            alias: alias ?? self.alias,
            sku: sku ?? self.sku,
            shortDescription: shortDescription ?? self.shortDescription,
            pieces: pieces ?? self.pieces,
            weightInKg: weightInKg ?? self.weightInKg,
            copyright: copyright ?? self.copyright,
            related: related ?? self.related,
            imageAlt: imageAlt ?? self.imageAlt,
            imageTitle: imageTitle ?? self.imageTitle,
            productImage: productImage ?? self.productImage,
            reviewCount: reviewCount ?? self.reviewCount,
            sizes: sizes ?? self.sizes
        )
    }


}

// MARK: - Size
struct Size: Codable {
    let sizeCode, sizeValue: String?
}

// MARK: Size convenience initializers and mutators

extension Size {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Size.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        sizeCode: String?? = nil,
        sizeValue: String?? = nil
    ) -> Size {
        return Size(
            sizeCode: sizeCode ?? self.sizeCode,
            sizeValue: sizeValue ?? self.sizeValue
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}


// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

class JSONCodingKey: CodingKey {
    let key: String

    required init?(intValue: Int) {
        return nil
    }

    required init?(stringValue: String) {
        key = stringValue
    }

    var intValue: Int? {
        return nil
    }

    var stringValue: String {
        return key
    }
}

class JSONAny: Codable {

    let value: Any

    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
        let context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
        return DecodingError.typeMismatch(JSONAny.self, context)
    }

    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
        let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
        return EncodingError.invalidValue(value, context)
    }

    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if container.decodeNil() {
            return JSONNull()
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if let value = try? container.decodeNil() {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer() {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
        if let value = try? container.decode(Bool.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Int64.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Double.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(String.self, forKey: key) {
            return value
        }
        if let value = try? container.decodeNil(forKey: key) {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
        var arr: [Any] = []
        while !container.isAtEnd {
            let value = try decode(from: &container)
            arr.append(value)
        }
        return arr
    }

    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
        var dict = [String: Any]()
        for key in container.allKeys {
            let value = try decode(from: &container, forKey: key)
            dict[key.stringValue] = value
        }
        return dict
    }

    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
        for value in array {
            if let value = value as? Bool {
                try container.encode(value)
            } else if let value = value as? Int64 {
                try container.encode(value)
            } else if let value = value as? Double {
                try container.encode(value)
            } else if let value = value as? String {
                try container.encode(value)
            } else if value is JSONNull {
                try container.encodeNil()
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer()
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
        for (key, value) in dictionary {
            let key = JSONCodingKey(stringValue: key)!
            if let value = value as? Bool {
                try container.encode(value, forKey: key)
            } else if let value = value as? Int64 {
                try container.encode(value, forKey: key)
            } else if let value = value as? Double {
                try container.encode(value, forKey: key)
            } else if let value = value as? String {
                try container.encode(value, forKey: key)
            } else if value is JSONNull {
                try container.encodeNil(forKey: key)
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer(forKey: key)
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
        if let value = value as? Bool {
            try container.encode(value)
        } else if let value = value as? Int64 {
            try container.encode(value)
        } else if let value = value as? Double {
            try container.encode(value)
        } else if let value = value as? String {
            try container.encode(value)
        } else if value is JSONNull {
            try container.encodeNil()
        } else {
            throw encodingError(forValue: value, codingPath: container.codingPath)
        }
    }

    public required init(from decoder: Decoder) throws {
        if var arrayContainer = try? decoder.unkeyedContainer() {
            self.value = try JSONAny.decodeArray(from: &arrayContainer)
        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
            self.value = try JSONAny.decodeDictionary(from: &container)
        } else {
            let container = try decoder.singleValueContainer()
            self.value = try JSONAny.decode(from: container)
        }
    }

    public func encode(to encoder: Encoder) throws {
        if let arr = self.value as? [Any] {
            var container = encoder.unkeyedContainer()
            try JSONAny.encode(to: &container, array: arr)
        } else if let dict = self.value as? [String: Any] {
            var container = encoder.container(keyedBy: JSONCodingKey.self)
            try JSONAny.encode(to: &container, dictionary: dict)
        } else {
            var container = encoder.singleValueContainer()
            try JSONAny.encode(to: &container, value: self.value)
        }
    }
}
