// To parse the JSON, add this file to your project and do:
//
//   let vaccineScheduleModel = try VaccineScheduleModel(json)

import Foundation

struct VaccineScheduleModel: Codable {
    let status: Int?
    let scheduleArray: [String: [ScheduleArray]]?
    let babyName, babyDob, babyDobs: String?
    let pendingVacc: PendingVacc?
    let reminderVacc: ReminderVacc?
    let vaccAges, vaccAgesKey: [String: String]?
    let imageChart: [ImageChart]?
    
    enum CodingKeys: String, CodingKey {
        case status
        case scheduleArray = "schedule_array"
        case babyName = "baby_name"
        case babyDob = "baby_dob"
        case babyDobs = "baby_dobs"
        case pendingVacc = "pending_vacc"
        case reminderVacc = "reminder_vacc"
        case vaccAges = "vacc_ages"
        case vaccAgesKey = "vacc_ages_key"
        case imageChart = "image_chart"
    }
}

struct ImageChart: Codable {
    let title: String?
    let image: String?
}

struct PendingVacc: Codable {
    let pendingAge, pendingVacc, pendingDose, pendingDate: String?
    let pendingDateTxt: String?
    
    enum CodingKeys: String, CodingKey {
        case pendingAge = "pending_age"
        case pendingVacc = "pending_vacc"
        case pendingDose = "pending_dose"
        case pendingDate = "pending_date"
        case pendingDateTxt = "pending_date_txt"
    }
}

struct ReminderVacc: Codable {
    let remVacc, remDateTxt: String?
    
    enum CodingKeys: String, CodingKey {
        case remVacc = "rem_vacc"
        case remDateTxt = "rem_date_txt"
    }
}

struct ScheduleArray: Codable {
    let vaccines, dose, protection, dateFrom: String?
    let dateTo, vaccChooseDate, vaccDate: String?
    let request: Int?
    let dateFromTxt, dateToTxt: String?
    let vaccChooseDateTxt: String?
    let vaccDateTxt: String?
    let validtionDate: String?
    
    enum CodingKeys: String, CodingKey {
        case vaccines, dose, protection, dateFrom, dateTo
        case vaccChooseDate = "vacc_choose_date"
        case vaccDate = "vacc_date"
        case request
        case dateFromTxt = "date_from_txt"
        case dateToTxt = "date_to_txt"
        case vaccChooseDateTxt = "vacc_choose_date_txt"
        case vaccDateTxt = "vacc_date_txt"
        case validtionDate = "validtion_date"
    }
}

enum VaccChooseDateTxt: String, Codable {
    case empty = ""
    case oct262018 = "Oct 26, 2018"
}

enum VaccDateTxt: String, Codable {
    case empty = ""
    case oct252018 = "Oct 25, 2018"
}

// MARK: Convenience initializers and mutators

extension VaccineScheduleModel {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(VaccineScheduleModel.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        status: Int?? = nil,
        scheduleArray: [String: [ScheduleArray]]?? = nil,
        babyName: String?? = nil,
        babyDob: String?? = nil,
        babyDobs: String?? = nil,
        pendingVacc: PendingVacc?? = nil,
        reminderVacc: ReminderVacc?? = nil,
        vaccAges: [String: String]?? = nil,
        vaccAgesKey: [String: String]?? = nil,
        imageChart: [ImageChart]?? = nil
        ) -> VaccineScheduleModel {
        return VaccineScheduleModel(
            status: status ?? self.status,
            scheduleArray: scheduleArray ?? self.scheduleArray,
            babyName: babyName ?? self.babyName,
            babyDob: babyDob ?? self.babyDob,
            babyDobs: babyDobs ?? self.babyDobs,
            pendingVacc: pendingVacc ?? self.pendingVacc,
            reminderVacc: reminderVacc ?? self.reminderVacc,
            vaccAges: vaccAges ?? self.vaccAges,
            vaccAgesKey: vaccAgesKey ?? self.vaccAgesKey,
            imageChart: imageChart ?? self.imageChart
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension ImageChart {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(ImageChart.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        title: String?? = nil,
        image: String?? = nil
        ) -> ImageChart {
        return ImageChart(
            title: title ?? self.title,
            image: image ?? self.image
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension PendingVacc {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(PendingVacc.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        pendingAge: String?? = nil,
        pendingVacc: String?? = nil,
        pendingDose: String?? = nil,
        pendingDate: String?? = nil,
        pendingDateTxt: String?? = nil
        ) -> PendingVacc {
        return PendingVacc(
            pendingAge: pendingAge ?? self.pendingAge,
            pendingVacc: pendingVacc ?? self.pendingVacc,
            pendingDose: pendingDose ?? self.pendingDose,
            pendingDate: pendingDate ?? self.pendingDate,
            pendingDateTxt: pendingDateTxt ?? self.pendingDateTxt
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension ReminderVacc {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(ReminderVacc.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        remVacc: String?? = nil,
        remDateTxt: String?? = nil
        ) -> ReminderVacc {
        return ReminderVacc(
            remVacc: remVacc ?? self.remVacc,
            remDateTxt: remDateTxt ?? self.remDateTxt
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension ScheduleArray {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(ScheduleArray.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        vaccines: String?? = nil,
        dose: String?? = nil,
        protection: String?? = nil,
        dateFrom: String?? = nil,
        dateTo: String?? = nil,
        vaccChooseDate: String?? = nil,
        vaccDate: String?? = nil,
        request: Int?? = nil,
        dateFromTxt: String?? = nil,
        dateToTxt: String?? = nil,
        vaccChooseDateTxt: String?? = nil,
        vaccDateTxt: String?? = nil,
        validtionDate: String?? = nil
        ) -> ScheduleArray {
        return ScheduleArray(
            vaccines: vaccines ?? self.vaccines,
            dose: dose ?? self.dose,
            protection: protection ?? self.protection,
            dateFrom: dateFrom ?? self.dateFrom,
            dateTo: dateTo ?? self.dateTo,
            vaccChooseDate: vaccChooseDate ?? self.vaccChooseDate,
            vaccDate: vaccDate ?? self.vaccDate,
            request: request ?? self.request,
            dateFromTxt: dateFromTxt ?? self.dateFromTxt,
            dateToTxt: dateToTxt ?? self.dateToTxt,
            vaccChooseDateTxt: vaccChooseDateTxt ?? self.vaccChooseDateTxt,
            vaccDateTxt: vaccDateTxt ?? self.vaccDateTxt,
            validtionDate: validtionDate ?? self.validtionDate
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}



