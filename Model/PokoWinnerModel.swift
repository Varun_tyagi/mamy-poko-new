//
//  PokoWonnerModel.swift
//  MamyPoko
//
//  Created by Varun Tyagi on 26/06/19.
//  Copyright © 2019 Varun Tyagi. All rights reserved.
//

//
//   let pokoWinnerModel = try PokoWinnerModel(json)

import Foundation

// MARK: - PokoWinnerModel
struct PokoWinnerModel: Codable {
    static var sharedWinnerModel:PokoWinnerModel?
    var data: [PokoData]?
    let message: String?
    let status: Int?
    let weeks: [Week]?
    var winnerVideos: [WinnerVideo]?
    
    enum CodingKeys: String, CodingKey {
        case data, message, status, weeks
        case winnerVideos = "winner_videos"
    }
}

// MARK: PokoWinnerModel convenience initializers and mutators

extension PokoWinnerModel {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(PokoWinnerModel.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        data: [PokoData]?? = nil,
        message: String?? = nil,
        status: Int?? = nil,
        weeks: [Week]?? = nil,
        winnerVideos: [WinnerVideo]?? = nil
        ) -> PokoWinnerModel {
        return PokoWinnerModel(
            data: data ?? self.data,
            message: message ?? self.message,
            status: status ?? self.status,
            weeks: weeks ?? self.weeks,
            winnerVideos: winnerVideos ?? self.winnerVideos
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - PokoData
struct PokoData: Codable {
    let id, createdAt, dob: String?
    let image: String?
    let childName, mobileNo, winner, week: String?
    let videoURL, updatedAt: String?
    let gender: Gender?
    let status: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case createdAt = "created_at"
        case dob, image
        case childName = "child_name"
        case mobileNo = "mobile_no"
        case winner, week
        case videoURL = "video_url"
        case updatedAt = "updated_at"
        case gender, status
    }
}

// MARK: PokoData convenience initializers and mutators

extension PokoData {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(PokoData.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        id: String?? = nil,
        createdAt: String?? = nil,
        dob: String?? = nil,
        image: String?? = nil,
        childName: String?? = nil,
        mobileNo: String?? = nil,
        winner: String?? = nil,
        week: String?? = nil,
        videoURL: String?? = nil,
        updatedAt: String?? = nil,
        gender: Gender?? = nil,
        status: String?? = nil
        ) -> PokoData {
        return PokoData(
            id: id ?? self.id,
            createdAt: createdAt ?? self.createdAt,
            dob: dob ?? self.dob,
            image: image ?? self.image,
            childName: childName ?? self.childName,
            mobileNo: mobileNo ?? self.mobileNo,
            winner: winner ?? self.winner,
            week: week ?? self.week,
            videoURL: videoURL ?? self.videoURL,
            updatedAt: updatedAt ?? self.updatedAt,
            gender: gender ?? self.gender,
            status: status ?? self.status
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

enum Gender: String, Codable {
    case female = "Female"
    case male = "Male"
}

// MARK: - Week
struct Week: Codable {
    let week: String?
}

// MARK: Week convenience initializers and mutators

extension Week {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Week.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        week: String?? = nil
        ) -> Week {
        return Week(
            week: week ?? self.week
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - WinnerVideo
struct WinnerVideo: Codable {
    let status: String?
    let videoURL: String?
    let createdAt, week, videoID, isDeleted: String?
    let updatedAt: String?
    
    enum CodingKeys: String, CodingKey {
        case status
        case videoURL = "video_url"
        case createdAt = "created_at"
        case week
        case videoID = "video_id"
        case isDeleted = "is_deleted"
        case updatedAt = "updated_at"
    }
}

// MARK: WinnerVideo convenience initializers and mutators

extension WinnerVideo {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(WinnerVideo.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        status: String?? = nil,
        videoURL: String?? = nil,
        createdAt: String?? = nil,
        week: String?? = nil,
        videoID: String?? = nil,
        isDeleted: String?? = nil,
        updatedAt: String?? = nil
        ) -> WinnerVideo {
        return WinnerVideo(
            status: status ?? self.status,
            videoURL: videoURL ?? self.videoURL,
            createdAt: createdAt ?? self.createdAt,
            week: week ?? self.week,
            videoID: videoID ?? self.videoID,
            isDeleted: isDeleted ?? self.isDeleted,
            updatedAt: updatedAt ?? self.updatedAt
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

