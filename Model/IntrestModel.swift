

// To parse the JSON, add this file to your project and do:
//
//   let intrestModel = try IntrestModel(json)

import Foundation

typealias IntrestModel = [IntrestModelElement]

struct IntrestModelElement: Codable {
    let name, exploreImage, image, alias: String?
    let selected: String?
    
    enum CodingKeys: String, CodingKey {
        case name
        case exploreImage = "explore_image"
        case image, alias, selected
    }
}

// MARK: Convenience initializers

extension IntrestModelElement {
    init(data: Data) throws {
        self = try JSONDecoder().decode(IntrestModelElement.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Array where Element == IntrestModel.Element {
    init(data: Data) throws {
        self = try JSONDecoder().decode(IntrestModel.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

