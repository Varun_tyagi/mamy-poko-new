//
//
//   let tipModel = try TipModel(json)

import Foundation

typealias TipModel = [TipModelElement]

struct TipModelElement: Codable {
    var status, city, story, name, like_count: String?
  let id : String?
    var image, share, headtype: String?
    var isReadMore:Bool? = true
	//var isLikeByMe:Bool? = false
    var isLikeByMe:Int?
	
}

// MARK: Convenience initializers

extension TipModelElement {
    init(data: Data) throws {
        self = try JSONDecoder().decode(TipModelElement.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Array where Element == TipModel.Element {
    init(data: Data) throws {
        self = try JSONDecoder().decode(TipModel.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
