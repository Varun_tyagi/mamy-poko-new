// To parse the JSON, add this file to your project and do:
//
//   let babyGrowthModel = try BabyGrowthModel(json)
//
// To read values from URLs:
//
//   let task = URLSession.shared.babyGrowthModelTask(with: url) { babyGrowthModel, response, error in
//     if let babyGrowthModel = babyGrowthModel {
//       ...
//     }
//   }
//   task.resume()

import Foundation

struct BabyGrowthModel: Codable {
    static var sharedBabyGrowthModel:BabyGrowthModel?
    let status: Int?
    let data: DataClass?
    let measurementData: [MeasurementDatum]?
    let file: File?
    
    enum CodingKeys: String, CodingKey {
        case status, data
        case measurementData = "measurement_data"
        case file
    }
}

struct DataClass: Codable {
    let weights, heights, headCircums: [HeadCircum]?
    let weightUnit, heightUnit, headCircumUnit, weight: String?
    let height, headCircum, gender, measureDate: String?
    let measureDateTxt, name, month, heading: String?
    let dob: String?
    
    enum CodingKeys: String, CodingKey {
        case weights, heights
        case headCircums = "head_circums"
        case weightUnit = "weight_unit"
        case heightUnit = "height_unit"
        case headCircumUnit = "head_circum_unit"
        case weight, height
        case headCircum = "head_circum"
        case gender
        case measureDate = "measure_date"
        case measureDateTxt = "measure_date_txt"
        case name, month, heading
        case dob
    }
}

struct HeadCircum: Codable {
    let x, y: String?
}

struct File: Codable {
    let girlWeightKg, girlWeightLbs, girlHeadCircumferenceCM, girlHeadCircumferenceInch: [[String: String]]?
    let girlHeightCM, girlHeightInch, boyWeightKg, boyWeightLbs: [[String: String]]?
    let boyHeadCircumferenceCM, boyHeadCircumferenceInch, boyHeightCM, boyHeightInch: [[String: String]]?
    let weightlbs, weightkg, heightcm, heightinch: [Headcm]?
    let headcm, headinch: [Headcm]?
    
    enum CodingKeys: String, CodingKey {
        case girlWeightKg, girlWeightLbs
        case girlHeadCircumferenceCM = "girlHeadCircumferenceCm"
        case girlHeadCircumferenceInch
        case girlHeightCM = "girlHeightCm"
        case girlHeightInch, boyWeightKg, boyWeightLbs
        case boyHeadCircumferenceCM = "boyHeadCircumferenceCm"
        case boyHeadCircumferenceInch
        case boyHeightCM = "boyHeightCm"
        case boyHeightInch, weightlbs, weightkg, heightcm, heightinch, headcm, headinch
    }
}

struct Headcm: Codable {
    let maxm, minm: Int?
    let unit: String?
}

struct MeasurementDatum: Codable {
    let babyID, measureDate, month, weight: String?
    let weightUnit, height, heightUnit, headCircum: String?
    let headCircumUnit: String?
    
    enum CodingKeys: String, CodingKey {
        case babyID = "baby_id"
        case measureDate = "measure_date"
        case month, weight
        case weightUnit = "weight_unit"
        case height
        case heightUnit = "height_unit"
        case headCircum = "head_circum"
        case headCircumUnit = "head_circum_unit"
    }
}

// MARK: Convenience initializers and mutators

extension BabyGrowthModel {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(BabyGrowthModel.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        status: Int?? = nil,
        data: DataClass?? = nil,
        measurementData: [MeasurementDatum]?? = nil,
        file: File?? = nil
        ) -> BabyGrowthModel {
        return BabyGrowthModel(
            status: status ?? self.status,
            data: data ?? self.data,
            measurementData: measurementData ?? self.measurementData,
            file: file ?? self.file
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension DataClass {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(DataClass.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        weights: [HeadCircum]?? = nil,
        heights: [HeadCircum]?? = nil,
        headCircums: [HeadCircum]?? = nil,
        weightUnit: String?? = nil,
        heightUnit: String?? = nil,
        headCircumUnit: String?? = nil,
        weight: String?? = nil,
        height: String?? = nil,
        headCircum: String?? = nil,
        gender: String?? = nil,
        measureDate: String?? = nil,
        measureDateTxt: String?? = nil,
        name: String?? = nil,
        month: String?? = nil,
        heading: String?? = nil,
        dob: String?? = nil
        ) -> DataClass {
        return DataClass(
            weights: weights ?? self.weights,
            heights: heights ?? self.heights,
            headCircums: headCircums ?? self.headCircums,
            weightUnit: weightUnit ?? self.weightUnit,
            heightUnit: heightUnit ?? self.heightUnit,
            headCircumUnit: headCircumUnit ?? self.headCircumUnit,
            weight: weight ?? self.weight,
            height: height ?? self.height,
            headCircum: headCircum ?? self.headCircum,
            gender: gender ?? self.gender,
            measureDate: measureDate ?? self.measureDate,
            measureDateTxt: measureDateTxt ?? self.measureDateTxt,
            name: name ?? self.name,
            month: month ?? self.month,
            heading: heading ?? self.heading,
            dob: dob ?? self.dob
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension HeadCircum {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(HeadCircum.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        x: String?? = nil,
        y: String?? = nil
        ) -> HeadCircum {
        return HeadCircum(
            x: x ?? self.x,
            y: y ?? self.y
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension File {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(File.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        girlWeightKg: [[String: String]]?? = nil,
        girlWeightLbs: [[String: String]]?? = nil,
        girlHeadCircumferenceCM: [[String: String]]?? = nil,
        girlHeadCircumferenceInch: [[String: String]]?? = nil,
        girlHeightCM: [[String: String]]?? = nil,
        girlHeightInch: [[String: String]]?? = nil,
        boyWeightKg: [[String: String]]?? = nil,
        boyWeightLbs: [[String: String]]?? = nil,
        boyHeadCircumferenceCM: [[String: String]]?? = nil,
        boyHeadCircumferenceInch: [[String: String]]?? = nil,
        boyHeightCM: [[String: String]]?? = nil,
        boyHeightInch: [[String: String]]?? = nil,
        weightlbs: [Headcm]?? = nil,
        weightkg: [Headcm]?? = nil,
        heightcm: [Headcm]?? = nil,
        heightinch: [Headcm]?? = nil,
        headcm: [Headcm]?? = nil,
        headinch: [Headcm]?? = nil
        ) -> File {
        return File(
            girlWeightKg: girlWeightKg ?? self.girlWeightKg,
            girlWeightLbs: girlWeightLbs ?? self.girlWeightLbs,
            girlHeadCircumferenceCM: girlHeadCircumferenceCM ?? self.girlHeadCircumferenceCM,
            girlHeadCircumferenceInch: girlHeadCircumferenceInch ?? self.girlHeadCircumferenceInch,
            girlHeightCM: girlHeightCM ?? self.girlHeightCM,
            girlHeightInch: girlHeightInch ?? self.girlHeightInch,
            boyWeightKg: boyWeightKg ?? self.boyWeightKg,
            boyWeightLbs: boyWeightLbs ?? self.boyWeightLbs,
            boyHeadCircumferenceCM: boyHeadCircumferenceCM ?? self.boyHeadCircumferenceCM,
            boyHeadCircumferenceInch: boyHeadCircumferenceInch ?? self.boyHeadCircumferenceInch,
            boyHeightCM: boyHeightCM ?? self.boyHeightCM,
            boyHeightInch: boyHeightInch ?? self.boyHeightInch,
            weightlbs: weightlbs ?? self.weightlbs,
            weightkg: weightkg ?? self.weightkg,
            heightcm: heightcm ?? self.heightcm,
            heightinch: heightinch ?? self.heightinch,
            headcm: headcm ?? self.headcm,
            headinch: headinch ?? self.headinch
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Headcm {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Headcm.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        maxm: Int?? = nil,
        minm: Int?? = nil,
        unit: String?? = nil
        ) -> Headcm {
        return Headcm(
            maxm: maxm ?? self.maxm,
            minm: minm ?? self.minm,
            unit: unit ?? self.unit
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension MeasurementDatum {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(MeasurementDatum.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        babyID: String?? = nil,
        measureDate: String?? = nil,
        month: String?? = nil,
        weight: String?? = nil,
        weightUnit: String?? = nil,
        height: String?? = nil,
        heightUnit: String?? = nil,
        headCircum: String?? = nil,
        headCircumUnit: String?? = nil
        ) -> MeasurementDatum {
        return MeasurementDatum(
            babyID: babyID ?? self.babyID,
            measureDate: measureDate ?? self.measureDate,
            month: month ?? self.month,
            weight: weight ?? self.weight,
            weightUnit: weightUnit ?? self.weightUnit,
            height: height ?? self.height,
            heightUnit: heightUnit ?? self.heightUnit,
            headCircum: headCircum ?? self.headCircum,
            headCircumUnit: headCircumUnit ?? self.headCircumUnit
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}



// MARK: - URLSession response handlers

extension URLSession {
    fileprivate func codableTask<T: Codable>(with url: URL, completionHandler: @escaping (T?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                completionHandler(nil, response, error)
                return
            }
            completionHandler(try? newJSONDecoder().decode(T.self, from: data), response, nil)
        }
    }
    
    func babyGrowthModelTask(with url: URL, completionHandler: @escaping (BabyGrowthModel?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
}

