// To parse the JSON, add this file to your project and do:
//
//   let userDataModel = try UserDataModel(json)

import Foundation

struct UserDataModel: Codable {
    let name: String?
    var babyDetails: BabyDetails?
    let userEmail, image, userDob, parentalStatus: String?
    let gender, userCity: String?
    let pregnancy: Pregnancy?
    let userPhone, userName, pincode: String?
	let offerSubscribed : Int?
    
    enum CodingKeys: String, CodingKey {
        case name
        case babyDetails = "baby_details"
        case userEmail = "user_email"
        case image
        case userDob = "user_dob"
        case parentalStatus = "parental_status"
        case gender
        case userCity = "user_city"
        case pregnancy
        case userPhone = "user_phone"
        case userName = "user_name"
        case pincode
		case offerSubscribed = "offer_subscribed"
    }
}

struct BabyDetails: Codable {
    var babyDob, babyWeight, babyHeight, babyGender: String?
    var babyHeightUnit, babyWeightUnit, babyName, headCircumfrenceUnit: String?
    var headCircumfrence: String?
    
    enum CodingKeys: String, CodingKey {
        case babyDob = "baby_dob"
        case babyWeight = "baby_weight"
        case babyHeight = "baby_height"
        case babyGender = "baby_gender"
        case babyHeightUnit = "baby_height_unit"
        case babyWeightUnit = "baby_weight_unit"
        case babyName = "baby_name"
        case headCircumfrenceUnit = "head_circumfrence_unit"
        case headCircumfrence = "head_circumfrence"
    }
}

struct Pregnancy: Codable {
    let week:Int?
    let  firstDelivery, deliveryDate, lastDateOfPeriod: String?
    
    enum CodingKeys: String, CodingKey {
        case week
        case firstDelivery = "first_delivery"
        case deliveryDate = "delivery_date"
        case lastDateOfPeriod = "last_date_of_period"
    }
}

// MARK: Convenience initializers and mutators

extension UserDataModel {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(UserDataModel.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        name: String?? = nil,
        babyDetails: BabyDetails?? = nil,
        userEmail: String?? = nil,
        image: String?? = nil,
        userDob: String?? = nil,
        parentalStatus: String?? = nil,
        gender: String?? = nil,
        userCity: String?? = nil,
        pregnancy: Pregnancy?? = nil,
        userPhone: String?? = nil,
        userName: String?? = nil,
        pincode: String?? = nil,
		offerSubscribed: Int?? = nil
        ) -> UserDataModel {
        return UserDataModel(
            name: name ?? self.name,
            babyDetails: babyDetails ?? self.babyDetails,
            userEmail: userEmail ?? self.userEmail,
            image: image ?? self.image,
            userDob: userDob ?? self.userDob,
            parentalStatus: parentalStatus ?? self.parentalStatus,
            gender: gender ?? self.gender,
            userCity: userCity ?? self.userCity,
            pregnancy: pregnancy ?? self.pregnancy,
            userPhone: userPhone ?? self.userPhone,
            userName: userName ?? self.userName,
            pincode: pincode ?? self.pincode,
			offerSubscribed: offerSubscribed ?? self.offerSubscribed
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension BabyDetails {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(BabyDetails.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        babyDob: String?? = nil,
        babyWeight: String?? = nil,
        babyHeight: String?? = nil,
        babyGender: String?? = nil,
        babyHeightUnit: String?? = nil,
        babyWeightUnit: String?? = nil,
        babyName: String?? = nil,
        headCircumfrenceUnit: String?? = nil,
        headCircumfrence: String?? = nil
        ) -> BabyDetails {
        return BabyDetails(
            babyDob: babyDob ?? self.babyDob,
            babyWeight: babyWeight ?? self.babyWeight,
            babyHeight: babyHeight ?? self.babyHeight,
            babyGender: babyGender ?? self.babyGender,
            babyHeightUnit: babyHeightUnit ?? self.babyHeightUnit,
            babyWeightUnit: babyWeightUnit ?? self.babyWeightUnit,
            babyName: babyName ?? self.babyName,
            headCircumfrenceUnit: headCircumfrenceUnit ?? self.headCircumfrenceUnit,
            headCircumfrence: headCircumfrence ?? self.headCircumfrence
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Pregnancy {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Pregnancy.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        week: Int?? = nil,
        firstDelivery: String?? = nil,
        deliveryDate: String?? = nil,
        lastDateOfPeriod: String?? = nil
        ) -> Pregnancy {
        return Pregnancy(
            week: week ?? self.week,
            firstDelivery: firstDelivery ?? self.firstDelivery,
            deliveryDate: deliveryDate ?? self.deliveryDate,
            lastDateOfPeriod: lastDateOfPeriod ?? self.lastDateOfPeriod
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

