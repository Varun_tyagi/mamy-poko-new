// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let answerModel = try AnswerModel(json)

import Foundation

// MARK: - AnswerModel
struct AnswerModel: Codable {
    let data: [ansModel]?
    let userID: Int?
	

    enum CodingKeys: String, CodingKey {
        case data
        case userID = "user_id"
    }
}

// MARK: AnswerModel convenience initializers and mutators

extension AnswerModel {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(AnswerModel.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        data: [ansModel]?? = nil,
        userID: Int?? = nil
    ) -> AnswerModel {
        return AnswerModel(
            data: data ?? self.data,
            userID: userID ?? self.userID
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - Datum
struct ansModel: Codable {
    let questionID: Int?
    let question, userAnswer, rightAns: String?
	
    enum CodingKeys: String, CodingKey {
        case questionID = "question_id"
        case question
        case userAnswer = "user_answer"
        case rightAns = "right_ans"
    }
}

// MARK: Datum convenience initializers and mutators

extension ansModel {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(ansModel.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        questionID: Int?? = nil,
        question: String?? = nil,
        userAnswer: String?? = nil,
        rightAns: String?? = nil
    ) -> ansModel {
        return ansModel(
            questionID: questionID ?? self.questionID,
            question: question ?? self.question,
            userAnswer: userAnswer ?? self.userAnswer,
            rightAns: rightAns ?? self.rightAns
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

