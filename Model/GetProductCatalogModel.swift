// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let getProductModel = try GetProductModel(json)

import Foundation

// MARK: - GetProductModel
struct GetProductModel: Codable {
    let status: Int?
    let data: [GetproductModels]?
}

// MARK: GetProductModel convenience initializers and mutators

extension GetProductModel {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(GetProductModel.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        status: Int?? = nil,
        data: [GetproductModels]?? = nil
    ) -> GetProductModel {
        return GetProductModel(
            status: status ?? self.status,
            data: data ?? self.data
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - Datum
struct GetproductModels: Codable {
    let productID, type, alias, shortDescription: String?
    let datumDescription, price: String?
    let name, fullName, nameCode, weight: String?
    let productImage: String?

    enum CodingKeys: String, CodingKey {
        case productID = "product_id"
        case type, alias
        case shortDescription = "short_description"
        case datumDescription = "description"
        case price, name
        case fullName = "full_name"
        case nameCode = "name_code"
        case weight
        case productImage = "product_image"
    }
}

// MARK: Datum convenience initializers and mutators

extension GetproductModels {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(GetproductModels.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        productID: String?? = nil,
        type: String?? = nil,
        alias: String?? = nil,
        shortDescription: String?? = nil,
        datumDescription: String?? = nil,
        price: String?? = nil,
        name: String?? = nil,
        fullName: String?? = nil,
        nameCode: String?? = nil,
        weight: String?? = nil,
        productImage: String?? = nil
    ) -> GetproductModels {
        return GetproductModels(
            productID: productID ?? self.productID,
            type: type ?? self.type,
            alias: alias ?? self.alias,
            shortDescription: shortDescription ?? self.shortDescription,
            datumDescription: datumDescription ?? self.datumDescription,
            price: price ?? self.price,
            name: name ?? self.name,
            fullName: fullName ?? self.fullName,
            nameCode: nameCode ?? self.nameCode,
            weight: weight ?? self.weight,
            productImage: productImage ?? self.productImage
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - Helper functions for creating encoders and decoders

