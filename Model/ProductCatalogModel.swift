//
//  ProductCatalogModel.swift
//  MamyPoko
//
//  Created by Hitesh Dhawan on 05/07/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import Foundation

struct ProductCatalogModel{
    var name = ""
    var image = ""
    var tagLine = ""
    var btnName = ""
    var pageUrl = ""
    
}
