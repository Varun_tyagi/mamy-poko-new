// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let productModel = try ProductModel(json)

import Foundation

// MARK: - ProductModel
struct ProductModel: Codable {
    let status: Int?
    let data: [ProductCategoryCatalogModel]?
}

// MARK: ProductModel convenience initializers and mutators

extension ProductModel {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(ProductModel.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        status: Int?? = nil,
        data: [ProductCategoryCatalogModel]?? = nil
    ) -> ProductModel {
        return ProductModel(
            status: status ?? self.status,
            data: data ?? self.data
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - Datum
struct ProductCategoryCatalogModel: Codable {
    let type: String?
    let image: String?
}

// MARK: Datum convenience initializers and mutators

extension ProductCategoryCatalogModel {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(ProductCategoryCatalogModel.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        type: String?? = nil,
        image: String?? = nil
    ) -> ProductCategoryCatalogModel {
        return ProductCategoryCatalogModel(
            type: type ?? self.type,
            image: image ?? self.image
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}


